USE [master]
GO
/****** Object:  Database [Admin_DEV]    Script Date: 1/30/2019 3:50:33 PM ******/
CREATE DATABASE [Admin_DEV]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Admin_DEV_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Admin_DEV_Data.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'Admin_DEV_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Admin_DEV_Log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 1024KB )
GO
ALTER DATABASE [Admin_DEV] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Admin_DEV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Admin_DEV] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Admin_DEV] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Admin_DEV] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Admin_DEV] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Admin_DEV] SET ARITHABORT OFF 
GO
ALTER DATABASE [Admin_DEV] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Admin_DEV] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Admin_DEV] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Admin_DEV] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Admin_DEV] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Admin_DEV] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Admin_DEV] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Admin_DEV] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Admin_DEV] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Admin_DEV] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Admin_DEV] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Admin_DEV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Admin_DEV] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Admin_DEV] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Admin_DEV] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Admin_DEV] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Admin_DEV] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Admin_DEV] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Admin_DEV] SET RECOVERY FULL 
GO
ALTER DATABASE [Admin_DEV] SET  MULTI_USER 
GO
ALTER DATABASE [Admin_DEV] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Admin_DEV] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Admin_DEV] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Admin_DEV] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Admin_DEV', N'ON'
GO
USE [Admin_DEV]
GO
/****** Object:  User [DGAdmin]    Script Date: 1/30/2019 3:50:35 PM ******/
CREATE USER [DGAdmin] FROM LOGIN [##MS_PolicyEventProcessingLogin##];
EXEC sp_addrolemember N'db_owner', 'DGAdmin';
GO
/****** Object:  Schema [Admin]    Script Date: 1/30/2019 3:50:37 PM ******/
CREATE SCHEMA [Admin]
GO
/****** Object:  Table [Admin].[ECMCapability]    Script Date: 1/30/2019 3:50:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Admin].[ECMCapability](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Description] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Admin].[ECMGroup]    Script Date: 1/30/2019 3:50:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Admin].[ECMGroup](
	[Name] [varchar](200) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Admin].[Group_X_Capability]    Script Date: 1/30/2019 3:50:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Admin].[Group_X_Capability](
	[G_ID] [int] NOT NULL,
	[C_ID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [Admin].[ECMUser]    Script Date: 1/30/2019 3:50:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Admin].[ECMUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](200) NOT NULL,
	[DisplayName] [varchar](200) NULL,
	[Password] [varchar](200) NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Enabled] [char](1) NULL,
	[LastPasswordResetDate] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Admin].[User_X_Group]    Script Date: 1/30/2019 3:50:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Admin].[User_X_Group](
	[U_ID] [int] NOT NULL,
	[G_ID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ecmuserView]    Script Date: 1/30/2019 3:50:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ecmuserView]
AS
SELECT ID, UserName, DisplayName, FirstName, LastName
FROM     Admin.[ECMUser]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User (Admin)"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 299
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ecmuserView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ecmuserView'
GO
USE [master]
GO
ALTER DATABASE [Admin_DEV] SET  READ_WRITE 
GO
