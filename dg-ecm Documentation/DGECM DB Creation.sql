USE [master]
GO
/****** Object:  Database [DGECM]    Script Date: 1/13/2019 4:08:32 PM ******/
CREATE DATABASE [DGECM]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DGECM', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\DGECM.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DGECM_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\DGECM_log.ldf' , SIZE = 5184KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DGECM] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DGECM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DGECM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DGECM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DGECM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DGECM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DGECM] SET ARITHABORT OFF 
GO
ALTER DATABASE [DGECM] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DGECM] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [DGECM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DGECM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DGECM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DGECM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DGECM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DGECM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DGECM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DGECM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DGECM] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DGECM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DGECM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DGECM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DGECM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DGECM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DGECM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DGECM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DGECM] SET RECOVERY FULL 
GO
ALTER DATABASE [DGECM] SET  MULTI_USER 
GO
ALTER DATABASE [DGECM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DGECM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DGECM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DGECM] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DGECM', N'ON'
GO
USE [DGECM]
GO
/****** Object:  User [DGCmgmt]    Script Date: 1/13/2019 4:08:32 PM ******/
/*
CREATE USER [DGCmgmt] FOR LOGIN [DGCmgmt] WITH DEFAULT_SCHEMA=[DGCmgmt]
GO
ALTER ROLE [db_owner] ADD MEMBER [DGCmgmt]
GO
*/
CREATE USER [DGCmgmt] FROM LOGIN [##MS_PolicyEventProcessingLogin##];
EXEC sp_addrolemember N'db_owner', 'DGCmgmt';
GO
/****** Object:  Schema [DGCmgmt]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE SCHEMA [DGCmgmt]
GO
/****** Object:  StoredProcedure [DGCmgmt].[case_rk_seq_inc]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       create procedure [DGCmgmt].[case_rk_seq_inc](@count int, @retval numeric(10) output ) as declare @currval numeric(10), @endval numeric(10) begin transaction select  @currval = IDENT_CURRENT('case_rk_seq') set @retval = @currval + 1 set @endval = @currval + @count dbcc checkident('case_rk_seq', reseed, @endval) commit 
GO
/****** Object:  StoredProcedure [DGCmgmt].[case_rk_seq_next]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    create procedure [DGCmgmt].[case_rk_seq_next](@retval numeric(10) output) as declare @tran bit set @tran = 0 if @@trancount > 0 begin save tran seq set @tran = 1 end else begin tran insert case_rk_seq default values set @retval=scope_identity() if @tran = 1 rollback tran seq else rollback 
GO
/****** Object:  StoredProcedure [DGCmgmt].[efile_rk_seq_inc]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       create procedure [DGCmgmt].[efile_rk_seq_inc](@count int, @retval numeric(10) output ) as declare @currval numeric(10), @endval numeric(10) begin transaction select  @currval = IDENT_CURRENT('efile_rk_seq') set @retval = @currval + 1 set @endval = @currval + @count dbcc checkident('case_rk_seq', reseed, @endval) commit 
GO
/****** Object:  StoredProcedure [DGCmgmt].[efile_rk_seq_next]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      create procedure [DGCmgmt].[efile_rk_seq_next](@retval numeric(10) output) as declare @tran bit set @tran = 0 if @@trancount > 0 begin save tran seq set @tran = 1 end else begin tran insert efile_rk_seq default values set @retval=scope_identity() if @tran = 1 rollback tran seq else rollback 
GO
/****** Object:  StoredProcedure [DGCmgmt].[event_rk_seq_inc]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      create procedure [DGCmgmt].[event_rk_seq_inc](@count int, @retval numeric(10) output ) as declare @currval numeric(10), @endval numeric(10) begin transaction select  @currval = IDENT_CURRENT('event_rk_seq') set @retval = @currval + 1 set @endval = @currval + @count dbcc checkident('event_rk_seq', reseed, @endval) commit 
GO
/****** Object:  StoredProcedure [DGCmgmt].[event_rk_seq_next]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      create procedure [DGCmgmt].[event_rk_seq_next](@retval numeric(10) output) as declare @tran bit set @tran = 0 if @@trancount > 0 begin save tran seq set @tran = 1 end else begin tran insert event_rk_seq default values set @retval=scope_identity() if @tran = 1 rollback tran seq else rollback 
GO
/****** Object:  StoredProcedure [DGCmgmt].[occs_rk_seq_inc]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       create procedure [DGCmgmt].[occs_rk_seq_inc](@count int, @retval numeric(10) output ) as declare @currval numeric(10), @endval numeric(10) begin transaction select  @currval = IDENT_CURRENT('occs_rk_seq') set @retval = @currval + 1 set @endval = @currval + @count dbcc checkident('occs_rk_seq', reseed, @endval) commit 
GO
/****** Object:  StoredProcedure [DGCmgmt].[occs_rk_seq_next]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      create procedure [DGCmgmt].[occs_rk_seq_next](@retval numeric(10) output) as declare @tran bit set @tran = 0 if @@trancount > 0 begin save tran seq set @tran = 1 end else begin tran insert occs_rk_seq default values set @retval=scope_identity() if @tran = 1 rollback tran seq else rollback 
GO
/****** Object:  StoredProcedure [DGCmgmt].[cust_rk_seq_inc]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      create procedure [DGCmgmt].[cust_rk_seq_inc](@count int, @retval numeric(10) output ) as declare @currval numeric(10), @endval numeric(10) begin transaction select  @currval = IDENT_CURRENT('cust_rk_seq') set @retval = @currval + 1 set @endval = @currval + @count dbcc checkident('cust_rk_seq', reseed, @endval) commit 
GO
/****** Object:  StoredProcedure [DGCmgmt].[party_rk_seq_next]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      create procedure [DGCmgmt].[cust_rk_seq_next](@retval numeric(10) output) as declare @tran bit set @tran = 0 if @@trancount > 0 begin save tran seq set @tran = 1 end else begin tran insert cust_rk_seq default values set @retval=scope_identity() if @tran = 1 rollback tran seq else rollback 
GO
/****** Object:  Table [DGCmgmt].[Case_Conf]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Conf](
	[Case_Conf_Seq_No] [numeric](10, 0) NOT NULL,
	[Case_Type_Cd] [nvarchar](32) NOT NULL,
	[Case_Ctgry_Cd] [nvarchar](32) NULL,
	[Case_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[UI_Def_File_Name] [nvarchar](100) NOT NULL,
	[Invest_WF_Def_Name] [nvarchar](100) NOT NULL,
	[Reopen_WF_Def_Name] [nvarchar](100) NULL,
	[Investr_User_Id] [nvarchar](60) NULL,
 CONSTRAINT [Case_Conf_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Conf_Seq_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Conf_X_User_Grp]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Conf_X_User_Grp](
	[Case_Conf_Seq_No] [numeric](10, 0) NOT NULL,
	[User_Grp_Name] [nvarchar](60) NOT NULL,
 CONSTRAINT [Casecfgxusrgrp_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Conf_Seq_No] ASC,
	[User_Grp_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Live]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DGCmgmt].[Case_Live](
	[Case_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NULL,
	[Valid_To_Date] [datetime] NULL,
	[Case_Id] [nvarchar](64) NOT NULL,
	[Src_Sys_Cd] [nvarchar](10) NOT NULL,
	[Case_Type_Cd] [nvarchar](32) NULL,
	[Case_Ctgry_Cd] [nvarchar](32) NULL,
	[Case_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[Case_Stat_Cd] [nvarchar](10) NULL,
	[Case_Dispos_Cd] [nvarchar](10) NULL,
	[Case_Desc] [nvarchar](100) NULL,
	[Case_Link_Sk] [numeric](10, 0) NULL,
	[Priority_Cd] [nvarchar](10) NULL,
	[Regu_Rpt_Rqd_Flag] [char](1) NOT NULL,
	[Investr_User_Id] [nvarchar](60) NULL,
	[Open_Date] [datetime] NULL,
	[REOpen_Date] [datetime] NULL,
	[Close_Date] [datetime] NULL,
	[UI_Def_File_Name] [nvarchar](100) NULL,
	[Create_User_Id] [nvarchar](60) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Update_User_Id] [nvarchar](60) NULL,
	[Ver_No] [numeric](10, 0) NOT NULL,
	[Delete_Flag] [char](1) NOT NULL,
 CONSTRAINT [Case_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DGCmgmt].[case_rk_seq]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[case_rk_seq](
	[id] [numeric](10, 0) IDENTITY(10001,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Search_Crtria_Fld]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Search_Crtria_Fld](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
	[Fmt_Text] [nvarchar](40) NULL,
 CONSTRAINT [Case_Search_Criteria_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Search_Fltr_Fld]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Search_Fltr_Fld](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NOT NULL,
 CONSTRAINT [Case_Search_Filter_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Search_Rslt_Fld]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Search_Rslt_Fld](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
	[Fmt_Text] [nvarchar](40) NULL,
 CONSTRAINT [Case_Search_Result_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Udf_Char_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Udf_Char_Val](
	[Case_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [nvarchar](1000) NULL,
 CONSTRAINT [Case_Udf_Char_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Udf_Date_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Udf_Date_Val](
	[Case_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [datetime] NOT NULL,
 CONSTRAINT [Case_Udf_Date_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Udf_Def]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Udf_Def](
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Udf_Type_Name] [nvarchar](10) NOT NULL,
	[Udf_Desc] [nvarchar](100) NULL,
	[Max_Char_Cnt] [numeric](6, 0) NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
 CONSTRAINT [Case_Udf_Def_Pk] PRIMARY KEY CLUSTERED 
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Udf_Lgchr_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Udf_Lgchr_Val](
	[Case_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [nvarchar](max) NULL,
 CONSTRAINT [Case_Udf_Lgchr_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Udf_No_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_Udf_No_Val](
	[Case_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [float] NOT NULL,
 CONSTRAINT [Case_Udf_Num_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_Ver]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DGCmgmt].[Case_Ver](
	[Case_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Valid_To_Date] [datetime] NULL,
	[Case_Id] [nvarchar](64) NOT NULL,
	[Src_Sys_Cd] [nvarchar](10) NOT NULL,
	[Case_Type_Cd] [nvarchar](32) NULL,
	[Case_Ctgry_Cd] [nvarchar](32) NULL,
	[Case_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[Case_Stat_Cd] [nvarchar](10) NULL,
	[Case_Dispos_Cd] [nvarchar](10) NULL,
	[Case_Desc] [nvarchar](100) NULL,
	[Case_Link_Sk] [numeric](10, 0) NULL,
	[Priority_Cd] [nvarchar](10) NULL,
	[Regu_Rpt_Rqd_Flag] [char](1) NOT NULL,
	[Investr_User_Id] [nvarchar](60) NULL,
	[Open_Date] [datetime] NULL,
	[REOpen_Date] [datetime] NULL,
	[Close_Date] [datetime] NULL,
	[UI_Def_File_Name] [nvarchar](100) NULL,
	[Create_User_Id] [nvarchar](60) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Update_User_Id] [nvarchar](60) NULL,
	[Ver_No] [numeric](10, 0) NOT NULL,
	[Delete_Flag] [char](1) NOT NULL,
 CONSTRAINT [Case_Version_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DGCmgmt].[Case_X_Cust]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_X_Cust](
	[Case_Rk] [numeric](10, 0) NOT NULL,
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[Rel_Type_Cd] [nvarchar](10) NOT NULL,
	[Rel_Desc] [nvarchar](100) NULL,
	[Create_Date] [datetime] NOT NULL,
 CONSTRAINT [Case_X_Customer_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Rk] ASC,
	[Cust_Rk] ASC,
	[Rel_Type_Cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Case_X_User_Grp]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Case_X_User_Grp](
	[Case_Rk] [numeric](10, 0) NOT NULL,
	[User_Grp_Name] [nvarchar](60) NOT NULL,
 CONSTRAINT [Case_X_User_Grp_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Rk] ASC,
	[User_Grp_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[ECM_COLUMN_LABEL]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[ECM_Column_Label](
	[Source_Name] [nvarchar](30) NOT NULL,
	[Obj_Name] [nvarchar](30) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Column_Name] [nvarchar](30) NOT NULL,
	[Locale] [nvarchar](5) NOT NULL,
	[Label_Text] [nvarchar](200) NOT NULL,
 CONSTRAINT [Ecm_Column_Label_Pk] PRIMARY KEY CLUSTERED 
(
	[Source_Name] ASC,
	[Obj_Name] ASC,
	[Table_Name] ASC,
	[Column_Name] ASC,
	[Locale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[ECM_ENTITY_LOCK]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[ECM_Entity_Lock](
	[Entity_Rk] [numeric](10, 0) NOT NULL,
	[Entity_Name] [nvarchar](100) NOT NULL,
	[Lock_User_Id] [nvarchar](60) NOT NULL,
	[Lock_Date] [datetime] NOT NULL,
 CONSTRAINT [Ecm_Entity_Lock_Pk] PRIMARY KEY CLUSTERED 
(
	[Entity_Rk] ASC,
	[Entity_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[ECM_Event]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[ECM_Event](
	[Event_Rk] [numeric](10, 0) NOT NULL,
	[Business_Object_Name] [nvarchar](100) NOT NULL,
	[Business_Object_Rk] [numeric](10, 0) NOT NULL,
	[Event_Type_Cd] [nvarchar](10) NOT NULL,
	[Event_Desc] [nvarchar](256) NULL,
	[Create_User_Id] [nvarchar](60) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Linked_Object_Name] [nvarchar](100) NULL,
	[Linked_Object_Rk] [numeric](10, 0) NULL,
 CONSTRAINT [Event_Pk] PRIMARY KEY CLUSTERED 
(
	[Event_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[ECM_Locale]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[ECM_Locale](
	[Locale] [nvarchar](5) NOT NULL,
	[Fallback_Locale] [nvarchar](5) NOT NULL,
 CONSTRAINT [ECM_Locale_Pk] PRIMARY KEY CLUSTERED 
(
	[Locale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[ECM_Table_Label]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[ECM_Table_Label](
	[Source_Name] [nvarchar](30) NOT NULL,
	[Obj_Name] [nvarchar](30) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Locale] [nvarchar](5) NOT NULL,
	[Label_Text] [nvarchar](200) NOT NULL,
 CONSTRAINT [Ecm_Table_Label_Pk] PRIMARY KEY CLUSTERED 
(
	[Source_Name] ASC,
	[Obj_Name] ASC,
	[Table_Name] ASC,
	[Locale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_Conf]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Conf](
	[Efile_Conf_Seq_No] [numeric](10, 0) NOT NULL,
	[Efile_Type_Cd] [nvarchar](32) NULL,
	[Efile_Ctgry_Cd] [nvarchar](32) NULL,
	[Efile_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[UI_Def_File_Name] [nvarchar](100) NOT NULL,
	[Form_Agency_Cd] [nvarchar](32) NOT NULL,
	[Form_Type_Cd] [nvarchar](32) NOT NULL,
 CONSTRAINT [Efile_Conf_Pk] PRIMARY KEY CLUSTERED 
(
	[Efile_Conf_Seq_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_Conf_X_USER_Grp]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Conf_X_User_Grp](
	[Efile_Conf_Seq_No] [numeric](10, 0) NOT NULL,
	[User_Grp_Name] [nvarchar](60) NOT NULL,
 CONSTRAINT [Efile_Conf_X_User_Grp_Pk] PRIMARY KEY CLUSTERED 
(
	[Efile_Conf_Seq_No] ASC,
	[User_Grp_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_LIVE]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DGCmgmt].[Efile_Live](
	[Efile_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NULL,
	[Valid_To_Date] [datetime] NULL,
	[Efile_Id] [nvarchar](64) NOT NULL,
	[Src_Sys_Cd] [nvarchar](10) NOT NULL,
	[Form_Conf_Rk] [numeric](10, 0) NULL,
	[Efile_Type_Cd] [nvarchar](32) NULL,
	[Efile_Ctgry_Cd] [nvarchar](32) NULL,
	[Efile_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[Efile_Stat_Cd] [nvarchar](10) NULL,
	[Efile_Desc] [nvarchar](100) NULL,
	[Output_Path_Name] [nvarchar](100) NULL,
	[Output_Name] [nvarchar](100) NULL,
	[Output_Rr_Cnt] [numeric](10, 0) NULL,
	[Output_Line_Cnt] [numeric](10, 0) NULL,
	[Output_File_Size] [numeric](10, 0) NULL,
	[Output_Create_Date] [datetime] NULL,
	[Transmission_Id] [nvarchar](100) NULL,
	[Transmission_Dttm] [datetime] NULL,
	[Efile_Agency_Ref_Id] [nvarchar](100) NULL,
	[Efile_Agency_Status_Cd] [nvarchar](10) NULL,
	[Coverage_Start_Dt] [date] NULL,
	[Coverage_End_Dt] [date] NULL,
	[Correction_Flg] [char](1) NOT NULL,
	[Owner_User_Id] [nvarchar](60) NULL,
	[UI_Def_File_Name] [nvarchar](100) NULL,
	[Create_User_Id] [nvarchar](60) NULL,
	[Create_Date] [datetime] NULL,
	[Update_User_Id] [nvarchar](60) NULL,
	[Ver_No] [numeric](10, 0) NOT NULL,
	[Delete_Flag] [char](1) NOT NULL,
 CONSTRAINT [Efile_Live_Pk] PRIMARY KEY CLUSTERED 
(
	[Efile_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DGCmgmt].[efile_rk_seq]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[efile_rk_seq](
	[id] [numeric](10, 0) IDENTITY(10001,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_SEARCH_CRITERIA_FIELD]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Search_Criteria_Field](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
	[Fmt_Text] [nvarchar](40) NULL,
 CONSTRAINT [Efile_Search_Criteria_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_SEARCH_FILTER_FIELD]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Search_Filter_Field](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NOT NULL,
 CONSTRAINT [Efile_Search_Filter_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_SEARCH_RESULT_FIELD]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Search_Result_Field](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
	[Fmt_Text] [nvarchar](40) NULL,
 CONSTRAINT [Efile_Search_Result_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_UDF_CHAR_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Udf_Char_Val](
	[Efile_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [nvarchar](1000) NULL,
 CONSTRAINT [Efile_Udf_Char_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Efile_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Efile_Udf_Date_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Udf_Date_Val](
	[Efile_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [datetime] NOT NULL,
 CONSTRAINT [Efile_Udf_Date_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Efile_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Efile_Udf_Def]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Udf_Def](
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Udf_Type_Name] [nvarchar](10) NOT NULL,
	[Udf_Desc] [nvarchar](100) NULL,
	[Max_Char_Cnt] [numeric](6, 0) NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
 CONSTRAINT [Efile_Udf_Def_Pk] PRIMARY KEY CLUSTERED 
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_UDF_LGCHR_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Udf_Lgchr_Val](
	[Efile_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [nvarchar](max) NULL,
 CONSTRAINT [Efile_Udf_Lgchr_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Efile_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_UDF_NUM_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_Udf_Num_Val](
	[Efile_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [float] NOT NULL,
 CONSTRAINT [Efile_Udf_Num_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Efile_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[EFILE_VERSION]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DGCmgmt].[Efile_Ver](
	[Efile_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Valid_To_Date] [datetime] NULL,
	[Efile_Id] [nvarchar](64) NOT NULL,
	[Src_Sys_Cd] [nvarchar](10) NOT NULL,
	[Form_Conf_Rk] [numeric](10, 0) NULL,
	[Efile_Type_Cd] [nvarchar](32) NULL,
	[Efile_Ctgry_Cd] [nvarchar](32) NULL,
	[Efile_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[Efile_Stat_Cd] [nvarchar](10) NULL,
	[Efile_Desc] [nvarchar](100) NULL,
	[Output_Path_Name] [nvarchar](100) NULL,
	[Output_Name] [nvarchar](100) NULL,
	[Output_Rr_Cnt] [numeric](10, 0) NULL,
	[Output_Line_Cnt] [numeric](10, 0) NULL,
	[Output_File_Size] [numeric](10, 0) NULL,
	[OUTPUT_Create_Date] [datetime] NULL,
	[Transmission_Id] [nvarchar](100) NULL,
	[Transmission_Dttm] [datetime] NULL,
	[Efile_Agency_Ref_Id] [nvarchar](100) NULL,
	[Efile_Agency_Status_Cd] [nvarchar](10) NULL,
	[Coverage_Start_Dt] [date] NULL,
	[Coverage_End_Dt] [date] NULL,
	[Correction_Flg] [char](1) NOT NULL,
	[OWNER_User_Id] [nvarchar](60) NULL,
	[UI_Def_File_Name] [nvarchar](100) NULL,
	[Create_User_Id] [nvarchar](60) NULL,
	[Create_Date] [datetime] NULL,
	[Update_User_Id] [nvarchar](60) NULL,
	[Ver_No] [numeric](10, 0) NOT NULL,
	[Delete_Flag] [char](1) NOT NULL,
 CONSTRAINT [Efile_Version_Pk] PRIMARY KEY CLUSTERED 
(
	[Efile_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DGCmgmt].[EFILE_X_USER_Grp]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Efile_X_User_Grp](
	[Efile_Rk] [numeric](10, 0) NOT NULL,
	[User_Grp_Name] [nvarchar](60) NOT NULL,
 CONSTRAINT [Efile_X_User_Grp_Pk] PRIMARY KEY CLUSTERED 
(
	[Efile_Rk] ASC,
	[User_Grp_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[event_rk_seq]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[event_rk_seq](
	[id] [numeric](10, 0) IDENTITY(10001,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Conf]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Conf](
	[Occs_Conf_Seq_No] [numeric](10, 0) NOT NULL,
	[Occs_Type_Cd] [nvarchar](32) NOT NULL,
	[Occs_Ctgry_Cd] [nvarchar](32) NULL,
	[Occs_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[UI_Def_File_Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [Occurrence_Conf_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Conf_Seq_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Conf_X_User_Grp]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Conf_X_User_Grp](
	[Occs_Conf_Seq_No] [numeric](10, 0) NOT NULL,
	[User_Grp_Name] [nvarchar](60) NOT NULL,
 CONSTRAINT [Occurrenceconfxusergrp_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Conf_Seq_No] ASC,
	[User_Grp_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Live]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Live](
	[Occs_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NULL,
	[Valid_To_Date] [datetime] NULL,
	[Case_Rk] [numeric](10, 0) NULL,
	[Occs_Id] [nvarchar](64) NOT NULL,
	[Src_Sys_Cd] [nvarchar](10) NOT NULL,
	[Occs_Type_Cd] [nvarchar](32) NULL,
	[Occs_Ctgry_Cd] [nvarchar](32) NULL,
	[Occs_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[Occs_Desc] [nvarchar](100) NULL,
	[Occs_From_Date] [date] NULL,
	[Occs_From_Time] [char](8) NULL,
	[Occs_To_Date] [date] NULL,
	[Occs_To_Time] [char](8) NULL,
	[Dtction_Date] [date] NULL,
	[Dtction_Time] [char](8) NULL,
	[Notif_Date] [date] NULL,
	[Notif_Time] [char](8) NULL,
	[UI_Def_File_Name] [nvarchar](100) NULL,
	[Create_User_Id] [nvarchar](60) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Update_User_Id] [nvarchar](60) NULL,
	[Ver_No] [numeric](10, 0) NOT NULL,
	[Delete_Flag] [char](1) NOT NULL,
	[Investr_User_Id] [nvarchar](60) NULL,
	[Occs_Dispos_Cd] [nvarchar](10) NULL,
	[Close_Date] [datetime] NULL,
	[Occs_Stat_Cd] [nvarchar](10) NULL,
 CONSTRAINT [Occurrence_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DGCmgmt].[incident_rk_seq]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[occs_rk_seq](
	[id] [numeric](10, 0) IDENTITY(10001,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Search_Crtria_Fld]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Search_Crtria_Fld](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
	[Fmt_Text] [nvarchar](40) NULL,
 CONSTRAINT [Inc_Search_Criteria_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Search_Fltr_Fld]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Search_Fltr_Fld](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NOT NULL,
 CONSTRAINT [Inc_Search_Filter_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Search_Rslt_Fld]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Search_Rslt_Fld](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
	[Fmt_Text] [nvarchar](40) NULL,
 CONSTRAINT [Inc_Search_Result_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Udf_Char_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Udf_Char_Val](
	[Occs_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [nvarchar](1000) NULL,
 CONSTRAINT [Occurrence_Udf_Char_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Udf_Date_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Udf_Date_Val](
	[Occs_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [datetime] NOT NULL,
 CONSTRAINT [Occurrence_Udf_Date_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Udf_Def]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Udf_Def](
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Udf_Type_Name] [nvarchar](10) NOT NULL,
	[Udf_Desc] [nvarchar](100) NULL,
	[Max_Char_Cnt] [numeric](6, 0) NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
 CONSTRAINT [Occurrence_Udf_Def_Pk] PRIMARY KEY CLUSTERED 
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Udf_Lgchr_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Udf_Lgchr_Val](
	[Occs_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [nvarchar](max) NULL,
 CONSTRAINT [Occurrence_Udf_Lgchr_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Udf_No_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Udf_No_Val](
	[Occs_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [float] NOT NULL,
 CONSTRAINT [Occurrence_Udf_Num_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_Ver]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_Ver](
	[Occs_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Valid_To_Date] [datetime] NULL,
	[Case_Rk] [numeric](10, 0) NULL,
	[Occs_Id] [nvarchar](64) NOT NULL,
	[Src_Sys_Cd] [nvarchar](10) NOT NULL,
	[Occs_Type_Cd] [nvarchar](32) NULL,
	[Occs_Ctgry_Cd] [nvarchar](32) NULL,
	[Occs_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[Occs_Desc] [nvarchar](100) NULL,
	[Occs_From_Date] [date] NULL,
	[Occs_From_Time] [char](8) NULL,
	[Occs_To_Date] [date] NULL,
	[Occs_To_Time] [char](8) NULL,
	[Dtction_Date] [date] NULL,
	[Dtction_Time] [char](8) NULL,
	[Notif_Date] [date] NULL,
	[Notif_Time] [char](8) NULL,
	[UI_Def_File_Name] [nvarchar](100) NULL,
	[Create_User_Id] [nvarchar](60) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Update_User_Id] [nvarchar](60) NULL,
	[Ver_No] [numeric](10, 0) NOT NULL,
	[Delete_Flag] [char](1) NOT NULL,
	[Investr_User_Id] [nvarchar](60) NULL,
	[Occs_Dispos_Cd] [nvarchar](10) NULL,
	[Close_Date] [datetime] NULL,
	[Occs_Stat_Cd] [nvarchar](10) NULL,
 CONSTRAINT [Occurrence_Ver_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DGCmgmt].[Occurrence_X_Customer]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_X_Customer](
	[Occs_Rk] [numeric](10, 0) NOT NULL,
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[Rel_Type_Cd] [nvarchar](10) NOT NULL,
	[Rel_Desc] [nvarchar](100) NULL,
	[Create_Date] [datetime] NOT NULL,
 CONSTRAINT [Occurrence_X_Customer_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Rk] ASC,
	[Cust_Rk] ASC,
	[Rel_Type_Cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Occurrence_X_User_Grp]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Occurrence_X_User_Grp](
	[Occs_Rk] [numeric](10, 0) NOT NULL,
	[User_Grp_Name] [nvarchar](60) NOT NULL,
 CONSTRAINT [Occurrence_X_User_Grp_Pk] PRIMARY KEY CLUSTERED 
(
	[Occs_Rk] ASC,
	[User_Grp_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[link_sk_seq]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[link_sk_seq](
	[id] [numeric](10, 0) IDENTITY(10001,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Conf]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Conf](
	[Cust_Conf_Seq_No] [numeric](10, 0) NOT NULL,
	[Cust_Type_Cd] [nvarchar](32) NOT NULL,
	[Cust_Ctgry_Cd] [nvarchar](32) NULL,
	[Cust_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[UI_Def_File_Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [Customer_Conf_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Conf_Seq_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Conf_X_User_Grp]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Conf_X_User_Grp](
	[Cust_Conf_Seq_No] [numeric](10, 0) NOT NULL,
	[User_Grp_Name] [nvarchar](60) NOT NULL,
 CONSTRAINT [Customerconfxusergrp_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Conf_Seq_No] ASC,
	[User_Grp_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Live]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DGCmgmt].[Customer_Live](
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NULL,
	[Valid_To_Date] [datetime] NULL,
	[Cust_Id] [nvarchar](64) NOT NULL,
	[Src_Sys_Cd] [nvarchar](10) NOT NULL,
	[Cust_Type_Cd] [nvarchar](32) NULL,
	[Cust_Ctgry_Cd] [nvarchar](32) NULL,
	[Cust_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[Ident_Cust_Link_Sk] [numeric](10, 0) NULL,
	[Indv_Flag] [char](1) NOT NULL,
	[Cust_Full_Name] [nvarchar](200) NULL,
	[Nat_Id] [nvarchar](32) NULL,
	[Nat_Id_TYPE_CD] [nvarchar](10) NULL,
	[UI_Def_File_Name] [nvarchar](100) NULL,
	[Create_User_Id] [nvarchar](60) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Update_User_Id] [nvarchar](60) NULL,
	[Ver_No] [numeric](10, 0) NOT NULL,
	[Delete_Flag] [char](1) NOT NULL,
 CONSTRAINT [Customer_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DGCmgmt].[party_rk_seq]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[cust_rk_seq](
	[id] [numeric](10, 0) IDENTITY(10001,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Search_Crtria_Fld]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Search_Crtria_Fld](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
	[Fmt_Text] [nvarchar](40) NULL,
 CONSTRAINT [Customer_Search_Criteria_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Search_Fltr_Fld]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Search_Fltr_Fld](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NOT NULL,
 CONSTRAINT [Customer_Search_Filter_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Search_Rslt_Fld]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Search_Rslt_Fld](
	[User_Id] [nvarchar](60) NOT NULL,
	[Table_Name] [nvarchar](30) NOT NULL,
	[Fld_Name] [nvarchar](30) NOT NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
	[Fmt_Text] [nvarchar](40) NULL,
 CONSTRAINT [Customer_Search_Result_Field_Pk] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Table_Name] ASC,
	[Fld_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Udf_Char_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Udf_Char_Val](
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [nvarchar](1000) NULL,
 CONSTRAINT [Customer_Udf_Char_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Udf_Date_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Udf_Date_Val](
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [datetime] NOT NULL,
 CONSTRAINT [Customer_Udf_Date_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Udf_Def]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Udf_Def](
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Udf_Type_Name] [nvarchar](10) NOT NULL,
	[Udf_Desc] [nvarchar](100) NULL,
	[Max_Char_Cnt] [numeric](6, 0) NULL,
	[Ref_Table_Name] [nvarchar](30) NULL,
 CONSTRAINT [Customer_Udf_Def_Pk] PRIMARY KEY CLUSTERED 
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Udf_Lgchr_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Udf_Lgchr_Val](
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [nvarchar](max) NULL,
 CONSTRAINT [Customer_Udf_Lgchr_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Udf_No_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_Udf_No_Val](
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Udf_Table_Name] [nvarchar](30) NOT NULL,
	[Udf_Name] [nvarchar](30) NOT NULL,
	[Row_No] [numeric](10, 0) NOT NULL,
	[Udf_Val] [float] NOT NULL,
 CONSTRAINT [Customer_Udf_Num_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Rk] ASC,
	[Valid_From_Date] ASC,
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC,
	[Row_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_Ver]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DGCmgmt].[Customer_Ver](
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[Valid_From_Date] [datetime] NOT NULL,
	[Valid_To_Date] [datetime] NULL,
	[Cust_Id] [nvarchar](64) NOT NULL,
	[Src_Sys_Cd] [nvarchar](10) NOT NULL,
	[Cust_Type_Cd] [nvarchar](32) NULL,
	[Cust_Ctgry_Cd] [nvarchar](32) NULL,
	[Cust_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[Ident_Cust_Link_Sk] [numeric](10, 0) NULL,
	[Indv_Flag] [char](1) NOT NULL,
	[Cust_Full_Name] [nvarchar](200) NULL,
	[Nat_Id] [nvarchar](32) NULL,
	[Nat_Id_TYPE_CD] [nvarchar](10) NULL,
	[UI_Def_File_Name] [nvarchar](100) NULL,
	[Create_User_Id] [nvarchar](60) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Update_User_Id] [nvarchar](60) NULL,
	[Ver_No] [numeric](10, 0) NOT NULL,
	[Delete_Flag] [char](1) NOT NULL,
 CONSTRAINT [Customer_Ver_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [DGCmgmt].[Customer_X_Customer]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_X_Customer](
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[Mmbr_Cust_Rk] [numeric](10, 0) NOT NULL,
	[Mmbr_Type_Cd] [nvarchar](10) NOT NULL,
	[Mmbr_Desc] [nvarchar](100) NULL,
 CONSTRAINT [Customer_X_Customer_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Rk] ASC,
	[Mmbr_Cust_Rk] ASC,
	[Mmbr_Type_Cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[Customer_X_User_Grp]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Customer_X_User_Grp](
	[Cust_Rk] [numeric](10, 0) NOT NULL,
	[User_Grp_Name] [nvarchar](60) NOT NULL,
 CONSTRAINT [Customer_X_User_Grp_Pk] PRIMARY KEY CLUSTERED 
(
	[Cust_Rk] ASC,
	[User_Grp_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[REF_TABLE_TRANS]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DGCmgmt].[Ref_Table_Trans](
	[Ref_Table_Name] [nvarchar](30) NOT NULL,
	[Val_Cd] [nvarchar](32) NOT NULL,
	[Locale] [nvarchar](5) NOT NULL,
	[Val_Desc] [nvarchar](100) NOT NULL,
 CONSTRAINT [Ref_Table_Trans_Pk] PRIMARY KEY CLUSTERED 
(
	[Ref_Table_Name] ASC,
	[Val_Cd] ASC,
	[Locale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DGCmgmt].[REF_TABLE_Val]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [DGCmgmt].[Ref_Table_Val](
	[Ref_Table_Name] [nvarchar](30) NOT NULL,
	[Val_Cd] [nvarchar](32) NOT NULL,
	[Val_Desc] [nvarchar](100) NOT NULL,
	[Parent_Ref_Table_Name] [nvarchar](30) NULL,
	[Parent_Val_Cd] [nvarchar](32) NULL,
	[Display_Ordr_No] [numeric](6, 0) NOT NULL,
	[Active_Flg] [char](1) NOT NULL,
 CONSTRAINT [Ref_Table_Val_Pk] PRIMARY KEY CLUSTERED 
(
	[Ref_Table_Name] ASC,
	[Val_Cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Case_Conf]    Script Date: 1/13/2019 4:08:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Case_Conf](
	[Case_Conf_Seq_No] [numeric](10, 0) NOT NULL,
	[Case_Type_Cd] [nvarchar](32) NOT NULL,
	[Case_Ctgry_Cd] [nvarchar](32) NULL,
	[Case_Sub_Ctgry_Cd] [nvarchar](32) NULL,
	[UI_Def_File_Name] [nvarchar](100) NOT NULL,
	[Invest_WF_Def_Name] [nvarchar](100) NOT NULL,
	[Reopen_WF_Def_Name] [nvarchar](100) NULL,
	[Investr_User_Id] [nvarchar](60) NULL,
 CONSTRAINT [Case_Conf_Pk] PRIMARY KEY CLUSTERED 
(
	[Case_Conf_Seq_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [CASE_Conf_UX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [CASE_Conf_UX1] ON [DGCmgmt].[Case_Conf]
(
	[Case_Type_Cd] ASC,
	[Case_Ctgry_Cd] ASC,
	[Case_Sub_Ctgry_Cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CASECFGXUSRGRP_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASECFGXUSRGRP_FK1] ON [DGCmgmt].[Case_Conf_X_User_Grp]
(
	[Case_Conf_Seq_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Case_IdX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [Case_IdX1] ON [DGCmgmt].[Case_Live]
(
	[Case_Link_Sk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CASE_UX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [CASE_UX1] ON [DGCmgmt].[Case_Live]
(
	[Src_Sys_Cd] ASC,
	[Case_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CASEUDFCHARVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASEUDFCHARVAL_FK1] ON [DGCmgmt].[Case_Udf_Char_Val]
(
	[Case_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CASEUDFCHARVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASEUDFCHARVAL_FK2] ON [DGCmgmt].[Case_Udf_Char_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CASEUDFDATEVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASEUDFDATEVAL_FK1] ON [DGCmgmt].[Case_Udf_Char_Val]
(
	[Case_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CASEUDFDATEVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASEUDFDATEVAL_FK2] ON [DGCmgmt].[Case_Udf_Char_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CASEUDFLGCHRVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASEUDFLGCHRVAL_FK1] ON [DGCmgmt].[Case_Udf_Lgchr_Val]
(
	[Case_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CASEUDFLGCHRVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASEUDFLGCHRVAL_FK2] ON [DGCmgmt].[Case_Udf_Lgchr_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CASEUDFNUMVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASEUDFNUMVAL_FK1] ON [DGCmgmt].[Case_Udf_No_Val]
(
	[Case_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CASEUDFNUMVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASEUDFNUMVAL_FK2] ON [DGCmgmt].[Case_Udf_No_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CASE_X_PARTY_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASE_X_CUSTOMER_FK1] ON [DGCmgmt].[Case_X_Cust]
(
	[Cust_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CASE_X_PARTY_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASE_X_CUSTOMER_FK2] ON [DGCmgmt].[Case_X_Cust]
(
	[Case_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CASEXUSRGROUP_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CASEXUSRGRP_FK1] ON [DGCmgmt].[Case_X_User_Grp]
(
	[Case_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ECM_COLUMN_LABEL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [ECM_COLUMN_LABEL_FK1] ON [DGCmgmt].[ECM_Column_Label]
(
	[Locale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ECM_EVENT_IDX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [ECM_EVENT_IDX1] ON [DGCmgmt].[ECM_Event]
(
	[Business_Object_Name] ASC,
	[Business_Object_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ECM_EVENT_IDX2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [ECM_EVENT_IDX2] ON [DGCmgmt].[ECM_Event]
(
	[Linked_Object_Name] ASC,
	[Linked_Object_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ECM_TABLE_LABEL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [ECM_TABLE_LABEL_FK1] ON [DGCmgmt].[ECM_Table_Label]
(
	[Locale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EFILE_Conf_X_USER_GROUP_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_CONF_X_USER_GRP_FK1] ON [DGCmgmt].[Efile_Conf_X_User_Grp]
(
	[Efile_Conf_Seq_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [EFILE_LIVE_UX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [EFILE_LIVE_UX1] ON [DGCmgmt].[Efile_Live]
(
	[Src_Sys_Cd] ASC,
	[Efile_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [EFILE_UDF_CHAR_VAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_UDF_CHAR_VAL_FK1] ON [DGCmgmt].[Efile_Udf_Char_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EFILE_UDF_CHAR_VAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_UDF_CHAR_VAL_FK2] ON [DGCmgmt].[Efile_Udf_Char_Val]
(
	[Efile_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [EFILE_UDF_DATE_VAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_UDF_DATE_VAL_FK1] ON [DGCmgmt].[Efile_Udf_Date_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EFILE_UDF_DATE_VAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_UDF_DATE_VAL_FK2] ON [DGCmgmt].[Efile_Udf_Date_Val]
(
	[Efile_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [EFILE_UDF_LGCHR_VAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_UDF_LGCHR_VAL_FK1] ON [DGCmgmt].[Efile_Udf_Lgchr_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EFILE_UDF_LGCHR_VAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_UDF_LGCHR_VAL_FK2] ON [DGCmgmt].[Efile_Udf_Lgchr_Val]
(
	[Efile_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [EFILE_UDF_NUM_VAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_UDF_NUM_VAL_FK1] ON [DGCmgmt].[Efile_Udf_Num_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EFILE_UDF_NUM_VAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_UDF_NUM_VAL_FK2] ON [DGCmgmt].[Efile_Udf_Num_Val]
(
	[Efile_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EFILE_X_USER_GROUP_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [EFILE_X_USER_GRP_FK1] ON [DGCmgmt].[Efile_X_User_Grp]
(
	[Efile_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Occurrence_Conf_UX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Occurrence_Conf_UX1] ON [DGCmgmt].[Occurrence_Conf]
(
	[Occs_Type_Cd] ASC,
	[Occs_Ctgry_Cd] ASC,
	[Occs_Sub_Ctgry_Cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [INCCFGXUSRGRP_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [INCCFGXUSRGRP_FK1] ON [DGCmgmt].[Occurrence_Conf_X_User_Grp]
(
	[Occs_Conf_Seq_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [INCIDENT_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCS_FK1] ON [DGCmgmt].[Occurrence_Live]
(
	[Case_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Occs_UX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Occs_UX1] ON [DGCmgmt].[Occurrence_Live]
(
	[Src_Sys_Cd] ASC,
	[Occs_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OCCUDFCHARVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCUDFCHARVAL_FK1] ON [DGCmgmt].[Occurrence_Udf_Char_Val]
(
	[Occs_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OCCUDFCHARVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCUDFCHARVAL_FK2] ON [DGCmgmt].[Occurrence_Udf_Char_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OCCUDFDATEVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCUDFDATEVAL_FK1] ON [DGCmgmt].[Occurrence_Udf_Date_Val]
(
	[Occs_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OCCUDFDATEVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCUDFDATEVAL_FK2] ON [DGCmgmt].[Occurrence_Udf_Date_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OCCUDFLGCHRVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCUDFLGCHRVAL_FK1] ON [DGCmgmt].[Occurrence_Udf_Lgchr_Val]
(
	[Occs_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OCCUDFLGCHRVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCUDFLGCHRVAL_FK2] ON [DGCmgmt].[Occurrence_Udf_Lgchr_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OCCUDFNUMVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCUDFNUMVAL_FK1] ON [DGCmgmt].[Occurrence_Udf_No_Val]
(
	[Occs_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OCCUDFNUMVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCUDFNUMVAL_FK2] ON [DGCmgmt].[Occurrence_Udf_No_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OccsVRSION_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCSVRSION_FK1] ON [DGCmgmt].[Occurrence_Ver]
(
	[Case_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OccsXPARTY_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCSXCUSTOMER_FK1] ON [DGCmgmt].[Occurrence_X_Customer]
(
	[Cust_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OccsXPARTY_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCSXCUSTOMER_FK2] ON [DGCmgmt].[Occurrence_X_Customer]
(
	[Occs_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OCCXUSRGROUP_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [OCCXUSRGRP_FK1] ON [DGCmgmt].[Occurrence_X_User_Grp]
(
	[Occs_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Customer_Conf_UX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Customer_Conf_UX1] ON [DGCmgmt].[Customer_Conf]
(
	[Cust_Type_Cd] ASC,
	[Cust_Ctgry_Cd] ASC,
	[Cust_Sub_Ctgry_Cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CUSTCFGXUSRGRP_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTCFGXUSRGRP_FK1] ON [DGCmgmt].[Customer_Conf_X_User_Grp]
(
	[Cust_Conf_Seq_No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Cust_IdX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [Cust_IdX1] ON [DGCmgmt].[Customer_Live]
(
	[Ident_Cust_Link_Sk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Customer_UX1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Customer_UX1] ON [DGCmgmt].[Customer_Live]
(
	[Src_Sys_Cd] ASC,
	[Cust_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PARTYUDFCHRVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERUDFCHRVAL_FK1] ON [DGCmgmt].[Customer_Udf_Char_Val]
(
	[Cust_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PARTYUDFCHRVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERUDFCHRVAL_FK2] ON [DGCmgmt].[Customer_Udf_Char_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PARTYUDFDTVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERUDFDTVAL_FK1] ON [DGCmgmt].[Customer_Udf_Date_Val]
(
	[Cust_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PARTYUDFDTVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERUDFDTVAL_FK2] ON [DGCmgmt].[Customer_Udf_Date_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PARTYUDFLGCHRVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERUDFLGCHRVAL_FK1] ON [DGCmgmt].[Customer_Udf_Lgchr_Val]
(
	[Cust_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PARTYUDFLGCHRVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERUDFLGCHRVAL_FK2] ON [DGCmgmt].[Customer_Udf_Lgchr_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PARTYUDFNUMVAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERUDFNUMVAL_FK1] ON [DGCmgmt].[Customer_Udf_No_Val]
(
	[Cust_Rk] ASC,
	[Valid_From_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PARTYUDFNUMVAL_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERUDFNUMVAL_FK2] ON [DGCmgmt].[Customer_Udf_No_Val]
(
	[Udf_Table_Name] ASC,
	[Udf_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PARTYXPARTY_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERXCUSTOMER_FK1] ON [DGCmgmt].[Customer_X_Customer]
(
	[Cust_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PARTYXPARTY_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERXCUSTOMER_FK2] ON [DGCmgmt].[Customer_X_Customer]
(
	[Mmbr_Cust_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PARTYXUSRGROUP_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [CUSTOMERXUSRGRP_FK1] ON [DGCmgmt].[Customer_X_User_Grp]
(
	[Cust_Rk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [REF_TABLE_TRANS_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [REF_TABLE_TRANS_FK1] ON [DGCmgmt].[Ref_Table_Trans]
(
	[Locale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [REF_TABLE_TRANS_FK2]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [REF_TABLE_TRANS_FK2] ON [DGCmgmt].[Ref_Table_Trans]
(
	[Ref_Table_Name] ASC,
	[Val_Cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [REF_TABLE_VAL_FK1]    Script Date: 1/13/2019 4:08:33 PM ******/
CREATE NONCLUSTERED INDEX [REF_TABLE_VAL_FK1] ON [DGCmgmt].[Ref_Table_Val]
(
	[PARENT_Ref_Table_Name] ASC,
	[Parent_Val_Cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/*************************************************/
ALTER TABLE [DGCmgmt].[Case_Conf_X_User_Grp]  WITH CHECK ADD  CONSTRAINT [CASECFGXUSRGRP_FK1] FOREIGN KEY([Case_Conf_Seq_No])
REFERENCES [DGCmgmt].[Case_Conf] ([Case_Conf_Seq_No])
GO
ALTER TABLE [DGCmgmt].[Case_Conf_X_User_Grp] CHECK CONSTRAINT [CASECFGXUSRGRP_FK1]
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [CASEUDFCHARVAL_FK1] FOREIGN KEY([Case_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Case_Ver] ([Case_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Char_Val] CHECK CONSTRAINT [CASEUDFCHARVAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [CASEUDFCHARVAL_FK2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Case_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Char_Val] CHECK CONSTRAINT [CASEUDFCHARVAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [CASEUDFDATEVAL_FK1] FOREIGN KEY([Case_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Case_Ver] ([Case_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Char_Val] CHECK CONSTRAINT [CASEUDFDATEVAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [CASEUDFDATEVAL_FK2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Case_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Char_Val] CHECK CONSTRAINT [CASEUDFDATEVAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Lgchr_Val]  WITH CHECK ADD  CONSTRAINT [CASEUDFLGCHRVAL_FK1] FOREIGN KEY([Case_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Case_Ver] ([Case_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Lgchr_Val] CHECK CONSTRAINT [CASEUDFLGCHRVAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Lgchr_Val]  WITH CHECK ADD  CONSTRAINT [CASEUDFLGCHRVAL_FK2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Case_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Case_Udf_Lgchr_Val] CHECK CONSTRAINT [CASEUDFLGCHRVAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Case_Udf_No_Val]  WITH CHECK ADD  CONSTRAINT [CASEUDFNUMVAL_FK1] FOREIGN KEY([Case_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Case_Ver] ([Case_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Case_Udf_No_Val] CHECK CONSTRAINT [CASEUDFNUMVAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Case_Udf_No_Val]  WITH CHECK ADD  CONSTRAINT [CASEUDFNUMVAL_FK2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Case_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Case_Udf_No_Val] CHECK CONSTRAINT [CASEUDFNUMVAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Case_X_Cust]  WITH CHECK ADD  CONSTRAINT [CASE_X_CUSTOMER_FK1] FOREIGN KEY([Cust_Rk])
REFERENCES [DGCmgmt].[Customer_Live] ([Cust_Rk])
GO
ALTER TABLE [DGCmgmt].[Case_X_Cust] CHECK CONSTRAINT [CASE_X_CUSTOMER_FK1]
GO
ALTER TABLE [DGCmgmt].[Case_X_Cust]  WITH CHECK ADD  CONSTRAINT [CASE_X_CUSTOMER_FK2] FOREIGN KEY([Case_Rk])
REFERENCES [DGCmgmt].[Case_Live] ([Case_Rk])
GO
ALTER TABLE [DGCmgmt].[Case_X_Cust] CHECK CONSTRAINT [CASE_X_CUSTOMER_FK2]
GO
ALTER TABLE [DGCmgmt].[Case_X_User_Grp]  WITH CHECK ADD  CONSTRAINT [CASEXUSRGRP_FK1] FOREIGN KEY([Case_Rk])
REFERENCES [DGCmgmt].[Case_Live] ([Case_Rk])
GO
ALTER TABLE [DGCmgmt].[Case_X_User_Grp] CHECK CONSTRAINT [CASEXUSRGRP_FK1]
GO
ALTER TABLE [DGCmgmt].[ECM_Column_Label]  WITH CHECK ADD  CONSTRAINT [ECM_COLUMN_LABEL_FK1] FOREIGN KEY([Locale])
REFERENCES [DGCmgmt].[ECM_Locale] ([Locale])
GO
ALTER TABLE [DGCmgmt].[ECM_Column_Label] CHECK CONSTRAINT [ECM_COLUMN_LABEL_FK1]
GO
ALTER TABLE [DGCmgmt].[ECM_Table_Label]  WITH CHECK ADD  CONSTRAINT [ECM_TABLE_LABEL_FK1] FOREIGN KEY([Locale])
REFERENCES [DGCmgmt].[ECM_Locale] ([Locale])
GO
ALTER TABLE [DGCmgmt].[ECM_Table_Label] CHECK CONSTRAINT [ECM_TABLE_LABEL_FK1]
GO
ALTER TABLE [DGCmgmt].[Efile_Conf_X_User_Grp]  WITH CHECK ADD  CONSTRAINT [EFILE_CONF_X_USER_GRP_FK1] FOREIGN KEY([Efile_Conf_Seq_No])
REFERENCES [DGCmgmt].[Efile_Conf] ([Efile_Conf_Seq_No])
GO
ALTER TABLE [DGCmgmt].[Efile_Conf_X_User_Grp] CHECK CONSTRAINT [EFILE_CONF_X_USER_GRP_FK1]
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [EFILE_UDF_CHAR_VAL_FK1] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Efile_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Char_Val] CHECK CONSTRAINT [EFILE_UDF_CHAR_VAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [EFILE_UDF_CHAR_VAL_FK2] FOREIGN KEY([Efile_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Efile_Ver] ([Efile_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Char_Val] CHECK CONSTRAINT [EFILE_UDF_CHAR_VAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Date_Val]  WITH CHECK ADD  CONSTRAINT [EFILE_UDF_DATE_VAL_FK1] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Efile_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Date_Val] CHECK CONSTRAINT [EFILE_UDF_DATE_VAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Date_Val]  WITH CHECK ADD  CONSTRAINT [EFILE_UDF_DATE_VAL_FK2] FOREIGN KEY([Efile_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Efile_Ver] ([Efile_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Date_Val] CHECK CONSTRAINT [EFILE_UDF_DATE_VAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Lgchr_Val]  WITH CHECK ADD  CONSTRAINT [EFILE_UDF_LGCHR_VAL_FK1] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Efile_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Lgchr_Val] CHECK CONSTRAINT [EFILE_UDF_LGCHR_VAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Lgchr_Val]  WITH CHECK ADD  CONSTRAINT [EFILE_UDF_LGCHR_VAL_FK2] FOREIGN KEY([Efile_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Efile_Ver] ([Efile_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Lgchr_Val] CHECK CONSTRAINT [EFILE_UDF_LGCHR_VAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Num_Val]  WITH CHECK ADD  CONSTRAINT [EFILE_UDF_NUM_VAL_FK1] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Efile_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Num_Val] CHECK CONSTRAINT [EFILE_UDF_NUM_VAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Num_Val]  WITH CHECK ADD  CONSTRAINT [EFILE_UDF_NUM_VAL_FK2] FOREIGN KEY([Efile_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Efile_Ver] ([Efile_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Efile_Udf_Num_Val] CHECK CONSTRAINT [EFILE_UDF_NUM_VAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Efile_X_User_Grp]  WITH CHECK ADD  CONSTRAINT [EFILE_X_USER_GRP_FK1] FOREIGN KEY([Efile_Rk])
REFERENCES [DGCmgmt].[Efile_Live] ([Efile_Rk])
GO
ALTER TABLE [DGCmgmt].[Efile_X_User_Grp] CHECK CONSTRAINT [EFILE_X_USER_GRP_FK1]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Conf_X_User_Grp]  WITH CHECK ADD  CONSTRAINT [OCCCFGXUSRGRP_FK1] FOREIGN KEY([Occs_Conf_Seq_No])
REFERENCES [DGCmgmt].[Occurrence_Conf] ([Occs_Conf_Seq_No])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Conf_X_User_Grp] CHECK CONSTRAINT [OCCCFGXUSRGRP_FK1]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Live]  WITH CHECK ADD  CONSTRAINT [Occs_FK1] FOREIGN KEY([Case_Rk])
REFERENCES [DGCmgmt].[Case_Live] ([Case_Rk])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Live] CHECK CONSTRAINT [Occs_FK1]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [OCCUDFCHARVAL_FK1] FOREIGN KEY([Occs_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Occurrence_Ver] ([Occs_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Char_Val] CHECK CONSTRAINT [OCCUDFCHARVAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [OCCUDFCHARVAL_FK2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Occurrence_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Char_Val] CHECK CONSTRAINT [OCCUDFCHARVAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Date_Val]  WITH CHECK ADD  CONSTRAINT [OCCUDFDATEVAL_FK1] FOREIGN KEY([Occs_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Occurrence_Ver] ([Occs_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Date_Val] CHECK CONSTRAINT [OCCUDFDATEVAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Date_Val]  WITH CHECK ADD  CONSTRAINT [OCCUDFDATEVAL_FK2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Occurrence_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Date_Val] CHECK CONSTRAINT [OCCUDFDATEVAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Lgchr_Val]  WITH CHECK ADD  CONSTRAINT [OCCUDFLGCHRVAL_FK1] FOREIGN KEY([Occs_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Occurrence_Ver] ([Occs_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Lgchr_Val] CHECK CONSTRAINT [OCCUDFLGCHRVAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Lgchr_Val]  WITH CHECK ADD  CONSTRAINT [OCCUDFLGCHRVAL_FK2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Occurrence_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_Lgchr_Val] CHECK CONSTRAINT [OCCUDFLGCHRVAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_No_Val]  WITH CHECK ADD  CONSTRAINT [OCCUDFNUMVAL_FK1] FOREIGN KEY([Occs_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Occurrence_Ver] ([Occs_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_No_Val] CHECK CONSTRAINT [OCCUDFNUMVAL_FK1]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_No_Val]  WITH CHECK ADD  CONSTRAINT [OCCUDFNUMVAL_FK2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Occurrence_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Udf_No_Val] CHECK CONSTRAINT [OCCUDFNUMVAL_FK2]
GO
ALTER TABLE [DGCmgmt].[Occurrence_Ver]  WITH CHECK ADD  CONSTRAINT [OccsVRSION_FK1] FOREIGN KEY([Case_Rk])
REFERENCES [DGCmgmt].[Case_Live] ([Case_Rk])
GO
ALTER TABLE [DGCmgmt].[Occurrence_Ver] CHECK CONSTRAINT [OccsVRSION_FK1]
GO
ALTER TABLE [DGCmgmt].[Occurrence_X_Customer]  WITH CHECK ADD  CONSTRAINT [OccsXCustomer_FK1] FOREIGN KEY([Cust_Rk])
REFERENCES [DGCmgmt].[Customer_Live] ([Cust_Rk])
GO
ALTER TABLE [DGCmgmt].[Occurrence_X_Customer] CHECK CONSTRAINT [OccsXCustomer_FK1]
GO
ALTER TABLE [DGCmgmt].[Occurrence_X_Customer]  WITH CHECK ADD  CONSTRAINT [OccsXCustomer_FK2] FOREIGN KEY([Occs_Rk])
REFERENCES [DGCmgmt].[Occurrence_Live] ([Occs_Rk])
GO
ALTER TABLE [DGCmgmt].[Occurrence_X_Customer] CHECK CONSTRAINT [OccsXCustomer_FK2]
GO
ALTER TABLE [DGCmgmt].[Occurrence_X_User_Grp]  WITH CHECK ADD  CONSTRAINT [OCCXUSRGRP_FK1] FOREIGN KEY([Occs_Rk])
REFERENCES [DGCmgmt].[Occurrence_Live] ([Occs_Rk])
GO
ALTER TABLE [DGCmgmt].[Occurrence_X_User_Grp] CHECK CONSTRAINT [OCCXUSRGRP_FK1]
GO
ALTER TABLE [DGCmgmt].[Customer_Conf_X_User_Grp]  WITH CHECK ADD  CONSTRAINT [CUSTCFGXUSRGRP_FK1] FOREIGN KEY([Cust_Conf_Seq_No])
REFERENCES [DGCmgmt].[Customer_Conf] ([Cust_Conf_Seq_No])
GO
ALTER TABLE [DGCmgmt].[Customer_Conf_X_User_Grp] CHECK CONSTRAINT [CUSTCFGXUSRGRP_FK1]
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [Customerudfchrval_Fk1] FOREIGN KEY([Cust_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Customer_Ver] ([Cust_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Char_Val] CHECK CONSTRAINT [Customerudfchrval_Fk1]
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Char_Val]  WITH CHECK ADD  CONSTRAINT [Customerudfchrval_Fk2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Customer_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Char_Val] CHECK CONSTRAINT [Customerudfchrval_Fk2]
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Date_Val]  WITH CHECK ADD  CONSTRAINT [Customerudfdtval_Fk1] FOREIGN KEY([Cust_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Customer_Ver] ([Cust_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Date_Val] CHECK CONSTRAINT [Customerudfdtval_Fk1]
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Date_Val]  WITH CHECK ADD  CONSTRAINT [Customerudfdtval_Fk2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Customer_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Date_Val] CHECK CONSTRAINT [Customerudfdtval_Fk2]
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Lgchr_Val]  WITH CHECK ADD  CONSTRAINT [Customerudflgchrval_Fk1] FOREIGN KEY([Cust_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Customer_Ver] ([Cust_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Lgchr_Val] CHECK CONSTRAINT [Customerudflgchrval_Fk1]
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Lgchr_Val]  WITH CHECK ADD  CONSTRAINT [Customerudflgchrval_Fk2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Customer_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_Lgchr_Val] CHECK CONSTRAINT [Customerudflgchrval_Fk2]
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_No_Val]  WITH CHECK ADD  CONSTRAINT [Customerudfnumval_Fk1] FOREIGN KEY([Cust_Rk], [Valid_From_Date])
REFERENCES [DGCmgmt].[Customer_Ver] ([Cust_Rk], [Valid_From_Date])
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_No_Val] CHECK CONSTRAINT [Customerudfnumval_Fk1]
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_No_Val]  WITH CHECK ADD  CONSTRAINT [Customerudfnumval_Fk2] FOREIGN KEY([Udf_Table_Name], [Udf_Name])
REFERENCES [DGCmgmt].[Customer_Udf_Def] ([Udf_Table_Name], [Udf_Name])
GO
ALTER TABLE [DGCmgmt].[Customer_Udf_No_Val] CHECK CONSTRAINT [Customerudfnumval_Fk2]
GO
ALTER TABLE [DGCmgmt].[Customer_X_Customer]  WITH CHECK ADD  CONSTRAINT [CustomerxCustomer_Fk1] FOREIGN KEY([Cust_Rk])
REFERENCES [DGCmgmt].[Customer_Live] ([Cust_Rk])
GO
ALTER TABLE [DGCmgmt].[Customer_X_Customer] CHECK CONSTRAINT [CustomerxCustomer_Fk1]
GO
ALTER TABLE [DGCmgmt].[Customer_X_Customer]  WITH CHECK ADD  CONSTRAINT [CustomerxCustomer_Fk2] FOREIGN KEY([Mmbr_Cust_Rk])
REFERENCES [DGCmgmt].[Customer_Live] ([Cust_Rk])
GO
ALTER TABLE [DGCmgmt].[Customer_X_Customer] CHECK CONSTRAINT [CustomerxCustomer_Fk2]
GO
ALTER TABLE [DGCmgmt].[Customer_X_User_Grp]  WITH CHECK ADD  CONSTRAINT [Customerxusrgrp_Fk1] FOREIGN KEY([Cust_Rk])
REFERENCES [DGCmgmt].[Customer_Live] ([Cust_Rk])
GO
ALTER TABLE [DGCmgmt].[Customer_X_User_Grp] CHECK CONSTRAINT [Customerxusrgrp_Fk1]
GO
ALTER TABLE [DGCmgmt].[Ref_Table_Trans]  WITH CHECK ADD  CONSTRAINT [REF_TABLE_TRANS_FK1] FOREIGN KEY([Locale])
REFERENCES [DGCmgmt].[ECM_Locale] ([Locale])
GO
ALTER TABLE [DGCmgmt].[Ref_Table_Trans] CHECK CONSTRAINT [REF_TABLE_TRANS_FK1]
GO
ALTER TABLE [DGCmgmt].[Ref_Table_Trans]  WITH CHECK ADD  CONSTRAINT [REF_TABLE_TRANS_FK2] FOREIGN KEY([Ref_Table_Name], [Val_Cd])
REFERENCES [DGCmgmt].[Ref_Table_Val] ([Ref_Table_Name], [Val_Cd])
GO
ALTER TABLE [DGCmgmt].[Ref_Table_Trans] CHECK CONSTRAINT [REF_TABLE_TRANS_FK2]
GO
ALTER TABLE [DGCmgmt].[Ref_Table_Val]  WITH CHECK ADD  CONSTRAINT [REF_TABLE_VAL_FK1] FOREIGN KEY([PARENT_Ref_Table_Name], [PARENT_Val_CD])
REFERENCES [DGCmgmt].[Ref_Table_Val] ([Ref_Table_Name], [Val_Cd])
GO
ALTER TABLE [DGCmgmt].[Ref_Table_Val] CHECK CONSTRAINT [REF_TABLE_VAL_FK1]
GO
USE [master]
GO
ALTER DATABASE [DGECM] SET  READ_WRITE 
GO
