package com.datagear.uploader.service;

import com.datagear.uploader.model.UiFile;

import java.util.List;

public interface UiFileService {
    UiFile getByUiName(String ui_name);

    UiFile save(UiFile model);

    List<UiFile> findAll();
}
