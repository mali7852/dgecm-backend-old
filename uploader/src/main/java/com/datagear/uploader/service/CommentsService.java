package com.datagear.uploader.service;

import java.util.List;

import com.datagear.uploader.model.Comments;

public interface CommentsService {
	
	Comments save(Comments model);
	List<Comments> findAll();
	Comments findById(long commentId);
	List<Comments> findByEntityTypeAndEntityRk(String entityType, long entityRk);
	
}
