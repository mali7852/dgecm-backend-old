package com.datagear.uploader.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.uploader.model.Comments;
import com.datagear.uploader.repository.CommentsRepository;

import javax.transaction.Transactional;

@Service
public class CommentsServiceImpl implements CommentsService {
	
	@Autowired
	private CommentsRepository repo;

	@Override
	@Transactional
	public Comments save(Comments model) {
		return this.repo.save(model);
	}

	@Override
	public List<Comments> findAll() {
		return this.repo.findAll();
	}

	@Override
	public Comments findById(long id) {
		Optional<Comments> c = this.repo.findById(id);
		return c.isPresent() ? c.get() : null;
	}

	@Override
	public List<Comments> findByEntityTypeAndEntityRk(String entityType, long entityRk) {
		return this.repo.findByEntityTypeAndEntityRkAndDeleteFlg(entityType, entityRk, '0');
	}

}
