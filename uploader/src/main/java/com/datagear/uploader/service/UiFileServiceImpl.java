package com.datagear.uploader.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.uploader.model.UiFile;
import com.datagear.uploader.repository.UiFileRepository;

import java.util.List;

@Service
public class UiFileServiceImpl implements UiFileService {
	
	@Autowired
	private UiFileRepository repo;

	@Override
	public UiFile getByUiName(String ui_name) {
		return repo.findFirstByName(ui_name);
	}

	@Override
	public UiFile save(UiFile model) {
		return this.repo.save(model);
	}

	@Override
	public List<UiFile> findAll() {
		return this.repo.findByDeleteFlgOrderByUploadDateDesc('0');
	}

}
