package com.datagear.uploader.service;

import java.util.List;

import com.datagear.uploader.model.Attachment;

public interface AttachmentService {
	
	Attachment save(Attachment model);
	List<Attachment> findAll();
	void deleteById(long fileId);
	List<Attachment> findByName(String filename);
	Attachment findById(long attachmentid);
	List<Attachment> findByCommentId(long id);
	void deleteByCommentId(long id);
	
}
