package com.datagear.uploader.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.uploader.model.Attachment;
import com.datagear.uploader.repository.AttachmentRepository;

import javax.transaction.Transactional;

@Service
public class AttachmentServiceImpl implements AttachmentService {
	
	@Autowired
	private AttachmentRepository repo;

	@Override
	@Transactional
	public Attachment save(Attachment model) {
		return this.repo.save(model);
	}

	@Override
	public List<Attachment> findAll() {
		return this.repo.findAll();
	}

	@Override
	public void deleteById(long id) {
		this.repo.deleteById(id);
	}

	@Override
	public List<Attachment> findByName(String filename) {
		return this.repo.findByName(filename);
	}

	@Override
	public Attachment findById(long attachmentid) {
		Optional<Attachment> a = this.repo.findById(attachmentid);
		return a.isPresent() ? a.get() : null;
	}

	@Override
	public List<Attachment> findByCommentId(long id) {
		return this.repo.findByCommentId(id);
	}

	@Override
	public void deleteByCommentId(long commentId) {
		this.repo.deleteByCommentId(commentId);
	}

}
