package com.datagear.uploader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.uploader.model.CustomProperties;
import com.datagear.uploader.repository.CustomPropertiesRepository;

@Service
public class CustomPropertiesServiceImpl implements CustomPropertiesService {
	
	@Autowired
	private CustomPropertiesRepository repo;

	@Override
	public CustomProperties save(CustomProperties model) {
		return this.repo.save(model);
	}

	@Override
	public List<CustomProperties> findAll() {
		return this.repo.findByDeleteFlgOrderByUploadDateDesc('0');
	}

	@Override
	public CustomProperties getByName(String fileName) {
		return this.repo.findFirstByName(fileName);
	}

}
