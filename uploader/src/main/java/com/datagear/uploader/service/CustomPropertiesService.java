package com.datagear.uploader.service;

import java.util.List;

import com.datagear.uploader.model.CustomProperties;

public interface CustomPropertiesService {

	CustomProperties save(CustomProperties model);
	List<CustomProperties> findAll();
    CustomProperties getByName(String fileName);

}
