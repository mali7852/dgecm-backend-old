package com.datagear.uploader.service;

import com.datagear.uploader.Exception.FileStorageException;
import com.datagear.uploader.Exception.MyFileNotFoundException;
import com.datagear.uploader.helper.FileStorageProperties;
import com.datagear.uploader.model.Attachment;
import com.datagear.uploader.model.Comments;
import com.datagear.uploader.model.CustomProperties;
import com.datagear.uploader.model.UiFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;
    private final Path fileDeleteLocation;
    private final Path attachmentStorageLocation;
    private final Path attachmentDeleteLocation;
    private final Path uiTemplateStorageLocation;
    private final Path uiTemplateDeleteLocation;
    private final Path customPropertiesStorageLocation;
    private final Path customPropertiesDeleteLocation;

    @Autowired
    private AttachmentService attachmentServ;

    @Autowired
    private CommentsService commentsServ;

    @Autowired
    private UiFileService uiTemplateServ;

    @Autowired
    private CustomPropertiesService customPropertiesServ;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
        this.fileDeleteLocation = Paths.get(fileStorageProperties.getDeleteDir()).toAbsolutePath().normalize();
        this.attachmentStorageLocation = Paths.get(fileStorageProperties.getUploadDir() + "/attachments/").toAbsolutePath().normalize();
        this.attachmentDeleteLocation = Paths.get(fileStorageProperties.getDeleteDir() + "/attachments/").toAbsolutePath().normalize();
        this.uiTemplateStorageLocation = Paths.get(fileStorageProperties.getUploadDir() + "/templates/").toAbsolutePath().normalize();
        this.uiTemplateDeleteLocation = Paths.get(fileStorageProperties.getDeleteDir() + "/templates/").toAbsolutePath().normalize();
        this.customPropertiesStorageLocation = Paths.get(fileStorageProperties.getUploadDir() + "/custom-properties/").toAbsolutePath().normalize();
        this.customPropertiesDeleteLocation = Paths.get(fileStorageProperties.getDeleteDir() + "/custom-properties/").toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
            Files.createDirectories(this.fileDeleteLocation);
            Files.createDirectories(this.attachmentStorageLocation);
            Files.createDirectories(this.attachmentDeleteLocation);
            Files.createDirectories(this.uiTemplateStorageLocation);
            Files.createDirectories(this.uiTemplateDeleteLocation);
            Files.createDirectories(this.customPropertiesStorageLocation);
            Files.createDirectories(this.customPropertiesDeleteLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
                    ex);
        }
    }

    private String storeFile(MultipartFile file, String fileName, Path fileLocation) {
        // Normalize file name
        Path targetLocation;

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence: " + fileName);
            }

            targetLocation = fileLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    private Resource loadFileAsResource(String fileName, Path fileLocation) {
        try {
            Path filePath = fileLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

    public void deleteFile(long fileId) {
        this.attachmentServ.deleteById(fileId);
    }

    private boolean moveToDelete(String name, Path fileStorageLocation, Path fileDeleteLocation) throws MyFileNotFoundException {

        Path originalLocation = fileStorageLocation.resolve(name);
        Path deleteLocation = fileDeleteLocation.resolve(name);

        try {
            // Files.copy(file.getInputStream(), deleteLocation,
            // StandardCopyOption.REPLACE_EXISTING);
            if (!Files.isRegularFile(originalLocation)) {
                throw new MyFileNotFoundException("File not found " + name);
            }
            Files.copy(originalLocation, deleteLocation, StandardCopyOption.REPLACE_EXISTING);
            Files.delete(originalLocation);
        } catch (IOException e) {
            throw new MyFileNotFoundException("File not found " + name, e);
        }
        return true;
    }

    private String checkFileName(String filename) {
        List<Attachment> files = this.attachmentServ.findByName(filename);
        String fileName;
        if (files.size() > 0) {

            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");

            String strDate = dateFormat.format(date);

            fileName = StringUtils.cleanPath(filename);
            int extensionIndex = fileName.lastIndexOf('.');

            fileName = fileName.substring(0, extensionIndex) + "-" + strDate + fileName.substring(extensionIndex);

        } else {
            fileName = StringUtils.cleanPath(filename);

        }
        return fileName;

    }

    // state 4
    public void deleteComment(Comments comment, String updaterId) {
        if (comment != null) {
            comment.setDeleteFlg('1');
            comment.setUserId(updaterId);
            comment.setUploadDate(new Timestamp(System.currentTimeMillis()));

            this.commentsServ.save(comment);

            this.deleteCommentAttachments(comment);
        }

    }

    private void deleteCommentAttachments(Comments comment) {
        comment.getAttachments().forEach(attach -> {
            try {
                this.moveToDelete(attach.getName(), this.attachmentStorageLocation, this.attachmentDeleteLocation);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        });

        this.attachmentServ.deleteByCommentId(comment.getId());
    }

    // state 2
    // user id the id of user who add the file
    public void addNewFileToComment(MultipartFile file, long commentId, String userId, Timestamp date) {
        this.uploadAttachment(file, commentId, userId, date);
    }

    public void addNewFilesToComment(MultipartFile[] files, long commentId, String userId, Timestamp date) {
        for (MultipartFile file : files) {
            this.addNewFileToComment(file, commentId, userId, date);
        }
    }

    // state 3
    public void removeAttachment(Attachment attachment, String userId) {
        if (attachment != null) {
            this.attachmentServ.deleteById(attachment.getId());
            this.moveToDelete(attachment.getName(), this.attachmentStorageLocation, this.attachmentDeleteLocation);
        }
    }

    public Comments updateComment(Comments comment, MultipartFile[] files, String userId) {

        Timestamp cur_date = new Timestamp(System.currentTimeMillis());
        comment.setUploadDate(cur_date);
        comment.setUserId(userId);

        Arrays.asList(files).stream()
                .map(file -> this.uploadAttachment(file, comment.getId(), userId, cur_date))
                .collect(Collectors.toList());

        List<Attachment> attachmentList = this.attachmentServ.findByCommentId(comment.getId());
        Set<Attachment> attachments = new HashSet<>(attachmentList);
        comment.setAttachments(attachments);
        return this.commentsServ.save(comment);

    }

    public Attachment uploadAttachment(MultipartFile file, long commentId, String userId, Timestamp date) {
        String fileName = storeFile(file, this.checkFileName(file.getOriginalFilename()), this.attachmentStorageLocation);
        Attachment attachment = new Attachment(fileName, commentId, date);
        this.attachmentServ.save(attachment);
        return attachment;
    }

    public UiFile uploadUiTemplate(MultipartFile file, String desc, String userId, Timestamp cur_date) {
        UiFile uiTemplate = this.uiTemplateServ.getByUiName(file.getOriginalFilename());
        this.storeFile(file, file.getOriginalFilename(), this.uiTemplateStorageLocation);
        if (uiTemplate == null)
            uiTemplate = new UiFile(file.getOriginalFilename(), desc, userId, cur_date);
        else {
            uiTemplate.setDescription(desc);
            uiTemplate.setUploadUserId(userId);
            uiTemplate.setUploadDate(cur_date);
        }
        return this.uiTemplateServ.save(uiTemplate);
    }

    public Resource loadAttachmentAsResource(String fileName) {
        return this.loadFileAsResource(fileName, this.attachmentStorageLocation);
    }

    public Resource loadUiTemplateAsResource(String fileName) {
        return this.loadFileAsResource(fileName, this.uiTemplateStorageLocation);
    }

    public boolean deleteUiTemplate(String uiName) {
        return this.moveToDelete(uiName, this.uiTemplateStorageLocation, this.uiTemplateDeleteLocation);
    }

    public CustomProperties uploadCustomProperties(MultipartFile file, String desc, String username, Timestamp date) {
        CustomProperties customProperties = customPropertiesServ.getByName(file.getOriginalFilename());
        this.storeFile(file, file.getOriginalFilename(), customPropertiesStorageLocation);
        if (customProperties == null) {
            customProperties = new CustomProperties(file.getOriginalFilename(), desc, username, date);
        } else {
            customProperties.setUploadUserId(username);
            customProperties.setDescription(desc);
            customProperties.setUploadDate(date);
        }
        return customPropertiesServ.save(customProperties);
    }

    public Resource loadCustomPropertiesAsResource(String fileName) {
        return this.loadFileAsResource(fileName, this.customPropertiesStorageLocation);
    }

    public boolean deleteCustomProperties(String fileName) {
        return this.moveToDelete(fileName, this.customPropertiesStorageLocation, this.customPropertiesDeleteLocation);
    }

    public Resource writeListOfMapsToXlsx(List<Map<String, Object>> list) throws IOException {

        File file = File.createTempFile("search-result", ".xlsx", null);
        FileOutputStream out = new FileOutputStream(file);
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Search Result");
        // Write Sheet Header

        if (list.size() > 0) {
            Map<String, Object> map = list.get(0);
            Row headerRow = sheet.createRow(0);
            int cellnum = 0;
            Set<String> keys = map.keySet();
            for (String key : keys) {
                Cell cell = headerRow.createCell(cellnum++);
                cell.setCellValue(key);
            }
            for (int i = 0; i < list.size(); i++) {
                map = list.get(i);
                Row row = sheet.createRow(i + 1);
                cellnum = 0;
                keys = map.keySet();
                for (String key : keys) {
                    Cell cell = row.createCell(cellnum++);
                    Object obj = map.get(key);
                    if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Boolean) {
                        cell.setCellValue((Boolean) obj);
                    } else if (obj instanceof Date) {
                        cell.setCellValue((Date) obj);
                    } else if (obj instanceof Double) {
                        cell.setCellValue((Double) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    } else if (obj instanceof Long) {
                        cell.setCellValue(new Timestamp((Long) obj).toString());
                    }
                }
            }
        }
        workbook.write(out);
        out.close();
        return new UrlResource(file.toPath().toUri());
    }

    public Resource writeListOfMapsToCsv(List<Map<String, Object>> list) throws IOException {

        File file = File.createTempFile("search-result", ".csv", null);
        FileWriter out = new FileWriter(file);
        if (list.size() > 0) {
            // Write Sheet Header
            Map<String, Object> map = list.get(0);
            Set<String> keys = map.keySet();
            List<String> headerRow = new ArrayList<>();
            for (String key : keys) {
                headerRow.add(key);
            }
            out.write(String.join(",", headerRow));
            out.append("\n");
            for (int i = 0; i < list.size(); i++) {
                map = list.get(i);
                List<String> row = new ArrayList<>();
                keys = map.keySet();
                for (String key : keys) {
                    Object obj = map.get(key);
                    if (obj instanceof String) {
                        row.add((String) obj);
                    } else if (obj instanceof Boolean) {
                        row.add(((Boolean) obj).toString());
                    } else if (obj instanceof Date) {
                        row.add(obj.toString());
                    } else if (obj instanceof Double) {
                        row.add(obj.toString());
                    } else if (obj instanceof Integer) {
                        row.add(((Integer) obj).toString());
                    } else if (obj instanceof Long) {
                        row.add(new Timestamp((Long) obj).toString());
                    } else {
                        row.add("");
                    }
                }
                out.append(String.join(",", row));
                out.append("\n");
            }
        }
        out.close();
        return new UrlResource(file.toPath().toUri());
    }
}