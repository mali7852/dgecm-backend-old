package com.datagear.uploader.config;

import com.datagear.uploader.model.Comments;
import com.datagear.uploader.repository.CommentsRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@PropertySource(value = {"classpath:uploader-application.properties"})
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = {
        CommentsRepository.class}, entityManagerFactoryRef = "dgEcmMetaDataEntityManagerFactory", transactionManagerRef = "dgEcmMetaDataTransactionManager")
public class UploaderConfiguration {

    @Bean
    @ConfigurationProperties("uploader.datasource")
    public DataSourceProperties dgEcmMetaDataDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    public DataSource dgEcmMetaDataDataSource() {
        return dgEcmMetaDataDataSourceProperties().initializeDataSourceBuilder().build();
        // return DataSourceBuilder.create().build();

    }

    @Bean(name = "dgEcmMetaDataEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean dgEcmMetaDataEntityManagerFactory(
            EntityManagerFactoryBuilder builder) {
        return builder.dataSource(dgEcmMetaDataDataSource()).packages(Comments.class).persistenceUnit("MetaData")
                .build();
    }

    @Bean(name = "dgEcmMetaDataTransactionManager")
    public PlatformTransactionManager dgEcmMetaDataTransactionManager(
            @Qualifier("dgEcmMetaDataEntityManagerFactory") EntityManagerFactory dgEcmMetaDataEntityManagerFactory) {
        return new JpaTransactionManager(dgEcmMetaDataEntityManagerFactory);
    }

}
