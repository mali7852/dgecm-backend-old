package com.datagear.uploader.repository;

import com.datagear.uploader.model.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;
import java.util.List;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {

    List<Attachment> findByName(String fileName);

    List<Attachment> findByCommentId(long commentId);

	@Modifying
	@Transactional
    long deleteByCommentId(long commentId);
}
