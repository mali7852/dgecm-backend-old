package com.datagear.uploader.repository;

import java.util.List;

import com.datagear.uploader.model.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentsRepository extends JpaRepository<Comments, Long> {

	public List<Comments> findByEntityTypeAndEntityRkAndDeleteFlg(String type, long id, char f);
}
