package com.datagear.uploader.repository;

import com.datagear.uploader.model.UiFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UiFileRepository extends JpaRepository<UiFile, Long> {

    UiFile findFirstByName(String ui_name);

    List<UiFile> findByDeleteFlgOrderByUploadDateDesc(char c);

}
