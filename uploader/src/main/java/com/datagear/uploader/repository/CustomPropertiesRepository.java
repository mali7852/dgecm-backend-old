package com.datagear.uploader.repository;

import com.datagear.uploader.model.CustomProperties;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomPropertiesRepository extends JpaRepository<CustomProperties, Long> {
    List<CustomProperties> findByDeleteFlgOrderByUploadDateDesc(char c);
    CustomProperties findFirstByName(String fileName);

}
