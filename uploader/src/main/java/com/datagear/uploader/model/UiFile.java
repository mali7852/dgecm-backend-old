package com.datagear.uploader.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the UI_FILES database table.
 * 
 */
@Entity
@Table(name = "UI_FILES")
@NamedQuery(name = "UiFile.findAll", query = "SELECT u FROM UiFile u")
public class UiFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID")
	private long id;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "NAME")
	private String name;

	@Column(name = "UPLOAD_USER_ID")
	private String uploadUserId;

	@Column(name = "DEL_USER_ID")
	private String delUserId;

	@Column(name = "UPLOAD_DATE")
	private Timestamp uploadDate;

	@Column(name = "DELETE_FLG")
	private char deleteFlg = '0';

	public UiFile() {
	}

	public UiFile(String name, String description, String uploadUserId, Timestamp uploadDate) {
		this.description = description;
		this.name = name;
		this.uploadUserId = uploadUserId;
		this.uploadDate = uploadDate;
	}

	public char getDeleteFlg() {
		return deleteFlg;
	}

	public String getDelUserId() {
		return delUserId;
	}

	public String getDescription() {
		return description;
	}

	@JsonIgnore
	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Timestamp getUploadDate() {
		return uploadDate;
	}

	public String getUploadUserId() {
		return uploadUserId;
	}

	public void setDeleteFlg(char deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public void setDelUserId(String delUserId) {
		this.delUserId = delUserId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUploadDate(Timestamp uploadDate) {
		this.uploadDate = uploadDate;
	}

	public void setUploadUserId(String uploadUserId) {
		this.uploadUserId = uploadUserId;
	}

}