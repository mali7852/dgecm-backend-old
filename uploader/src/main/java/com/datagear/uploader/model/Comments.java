package com.datagear.uploader.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@NamedQuery(name = "Comments.findAll", query = "SELECT c FROM Comments c")
@Table(name = "COMMENTS")
public class Comments implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID")
	private long id;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "USER_ID")
	private String userId;

	@Column(name = "UPLOAD_DATE")
	private Timestamp uploadDate;

	@Column(name = "SUBJECT")
	private String subject;

	@Column(name = "ENTITY_RK")
	private long entityRk;
	
	@Column(name= "ENTITY_TYPE")
	private String entityType;
	
	@Column(name= "DELETE_FLG")
	private char deleteFlg;
	
	@OneToMany(mappedBy = "comment", fetch = FetchType.LAZY)
//	@JsonBackReference
	private Set<Attachment> attachments;

	public Comments() {}

	public Comments(String subject, String description, Timestamp uploadDate, long entityRk, String entityType, String userId) {
		this.description = description;
		this.userId = userId;
		this.uploadDate = uploadDate;
		this.subject = subject;
		this.entityRk = entityRk;
		this.entityType = entityType;
		this.deleteFlg = '0';
	}
	
	public Attachment addAttachment(Attachment attachment) {
		getAttachments().add(attachment);
		attachment.setComment(this);

		return attachment;
	}
	
	public Set<Attachment> getAttachments() {
		return attachments;
	}

	public char getDeleteFlg() {
		return deleteFlg;
	}

	public String getDescription() {
		return description;
	}

	public long getEntityRk() {
		return entityRk;
	}

	public String getEntityType() {
		return entityType;
	}

	public long getId() {
		return id;
	}

	public String getSubject() {
		return subject;
	}

	public Timestamp getUploadDate() {
		return uploadDate;
	}

	public String getUserId() {
		return userId;
	}

	public Attachment removeCaseUdfCharVal(Attachment attachment) {
		getAttachments().remove(attachment);
		attachment.setComment(null);

		return attachment;
	}

	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}

	public void setDeleteFlg(char deleteFlg) {
		this.deleteFlg = deleteFlg;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEntityRk(long entityRk) {
		this.entityRk = entityRk;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setUploadDate(Timestamp uploadDate) {
		this.uploadDate = uploadDate;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Comments [description=" + description + ", userId=" + userId + ", uploadDate=" + uploadDate
				+ ", subject=" + subject + ", entityRk=" + entityRk + ", entityType=" + entityType + ", deleteFlg="
				+ deleteFlg + "]";
	}

}
