package com.datagear.uploader.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@NamedQuery(name = "Attachment.findAll", query = "SELECT c FROM Attachment c")
@Table(name = "ATTACHMENT")
public class Attachment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID")
	private long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "COMMENT_ID")
	private long commentId;

	@Column(name = "UPLOAD_DATE")
	private Timestamp uploadDate;

	@ManyToOne(fetch = FetchType.LAZY)
//	@JsonManagedReference
	@JsonIgnore
	@JoinColumn(name = "COMMENT_ID", insertable = false, updatable = false)
	private Comments comment;

	public Attachment() {
	}

	public Attachment(String name, long commentId, Timestamp curDate) {
		this.name = name;
		this.commentId = commentId;
		this.uploadDate = curDate;
	}

	public long getCommentId() {
		return commentId;
	}

	public Comments getComment() {
		return comment;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Timestamp getUploadDate() {
		return uploadDate;
	}

	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}

	public void setComment(Comments comment) {
		this.comment = comment;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUploadDate(Timestamp uploadDate) {
		this.uploadDate = uploadDate;
	}

}
