package com.datagear.service;

import java.util.List;

public interface EntityDetailsService {
	
	Object getEntityDetails();
	void setEntityHeader(Object ent_live);
	void setEntityData(List<?> char_vals, List<?> date_vals, List<?> lg_vals);
	void setEditFlag(String editFlag);
	void setLockUserName(String userName); /* Display name or user name if first not exists */
	void setEntityConfig(Object ent_conf);
	void setEntityFields(String fields);

}
