package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_Search_Rslt_FldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_Search_Rslt_Fld;

@Service
public class Occurrence_Search_Rslt_FldServiceImpl implements Occurrence_Search_Rslt_FldService {

	@Autowired
	private Occurrence_Search_Rslt_FldRepository repo;
	
	@Override
	@Transactional
	public Occurrence_Search_Rslt_Fld save(Occurrence_Search_Rslt_Fld model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_Search_Rslt_Fld> findAll() {
		return repo.findAll();
	}

}
