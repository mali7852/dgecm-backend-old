package com.datagear.service;

import java.util.List;

import com.datagear.model.Ref_Table_Trans;

public interface Ref_Table_TransService {
	
	Ref_Table_Trans save(Ref_Table_Trans model);
	List<Ref_Table_Trans> findAll();
	
}
