package com.datagear.service;

import java.util.List;

import com.datagear.model.Case_X_User_Grp;

public interface Case_X_User_GrpService {
	
	Case_X_User_Grp save(Case_X_User_Grp model);
	List<Case_X_User_Grp> findAll();
	List<Long> getCaseRkListByGroupName(String group);
	List<Long> getCaseRkListByGroupNameList(List<String> groups);
    List<Case_X_User_Grp> getByCaseRk(long case_rk);
}
