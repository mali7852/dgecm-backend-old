package com.datagear.service;

import java.util.List;

import com.datagear.model.Case_Udf_Def;

public interface Case_Udf_DefService {
	
	Case_Udf_Def save(Case_Udf_Def model);
	List<Case_Udf_Def> findAll();
	List<Case_Udf_Def> findByUdfNameList(List<String> names);
	Case_Udf_Def findFirstByUdf_Name(String udf_name);
	
}
