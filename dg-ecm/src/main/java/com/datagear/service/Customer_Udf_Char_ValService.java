package com.datagear.service;

import com.datagear.customModel.HistoryVerDiff;
import com.datagear.model.Customer_Udf_Char_Val;

import java.sql.Timestamp;
import java.util.List;

public interface Customer_Udf_Char_ValService {

    Customer_Udf_Char_Val save(Customer_Udf_Char_Val model);

    List<Customer_Udf_Char_Val> findAll();

    List<Customer_Udf_Char_Val> getById(long cust_rk, Timestamp valid_From_Date);

    List<Customer_Udf_Char_Val> saveList(List<Customer_Udf_Char_Val> char_vals);

    List<HistoryVerDiff> getHistoryVerDiff(long custRk, Timestamp curVerDate, Timestamp oldVerDate);
}
