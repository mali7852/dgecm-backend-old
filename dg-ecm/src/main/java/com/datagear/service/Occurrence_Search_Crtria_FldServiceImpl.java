package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_Search_Crtria_FldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_Search_Crtria_Fld;

@Service
public class Occurrence_Search_Crtria_FldServiceImpl implements Occurrence_Search_Crtria_FldService {

	@Autowired
	private Occurrence_Search_Crtria_FldRepository repo;
	
	@Override
	@Transactional
	public Occurrence_Search_Crtria_Fld save(Occurrence_Search_Crtria_Fld model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_Search_Crtria_Fld> findAll() {
		return repo.findAll();
	}

}
