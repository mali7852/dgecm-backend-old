package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Case_Search_Crtria_FldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Case_Search_Crtria_Fld;

@Service
public class Case_Search_Crtria_FldServiceImpl implements Case_Search_Crtria_FldService {

	@Autowired
	private Case_Search_Crtria_FldRepository repo;
	
	@Override
	@Transactional
	public Case_Search_Crtria_Fld save(Case_Search_Crtria_Fld model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_Search_Crtria_Fld> findAll() {
		return repo.findAll();
	}

}
