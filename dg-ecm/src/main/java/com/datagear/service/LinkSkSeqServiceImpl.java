package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.LinkSkSeqRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.LinkSkSeq;

@Service
public class LinkSkSeqServiceImpl implements LinkSkSeqService {

	@Autowired
	private LinkSkSeqRepository repo;
	
	@Override
	@Transactional
	public LinkSkSeq save(LinkSkSeq model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<LinkSkSeq> findAll() {
		return repo.findAll();
	}

}
