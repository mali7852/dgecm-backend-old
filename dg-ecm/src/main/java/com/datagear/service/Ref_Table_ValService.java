package com.datagear.service;

import java.util.List;

import com.datagear.model.Ref_Table_Val;

public interface Ref_Table_ValService {
	
	Ref_Table_Val save(Ref_Table_Val model);
	List<Ref_Table_Val> findAll();
	List<Ref_Table_Val> getLookUpList(List<String> lookups);
	
}
