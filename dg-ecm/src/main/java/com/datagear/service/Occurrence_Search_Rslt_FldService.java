package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_Search_Rslt_Fld;

public interface Occurrence_Search_Rslt_FldService {
	
	Occurrence_Search_Rslt_Fld save(Occurrence_Search_Rslt_Fld model);
	List<Occurrence_Search_Rslt_Fld> findAll();
	
}
