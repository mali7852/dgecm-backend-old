package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_Conf;

public interface Customer_ConfService {
	
	Customer_Conf save(Customer_Conf model);
	List<Customer_Conf> findAll();
	Customer_Conf getByCustTypeCd(String cust_type_cd);
	Customer_Conf getByUiDefFileName(String ui_Def_File_Name);
	
}
