package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_Udf_No_Val;

public interface Occurrence_Udf_No_ValService {
	
	Occurrence_Udf_No_Val save(Occurrence_Udf_No_Val model);
	List<Occurrence_Udf_No_Val> findAll();
	
}
