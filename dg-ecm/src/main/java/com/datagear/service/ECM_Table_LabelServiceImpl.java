package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.ECM_Table_LabelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.ECM_Table_Label;

@Service
public class ECM_Table_LabelServiceImpl implements ECM_Table_LabelService {

	@Autowired
	private ECM_Table_LabelRepository repo;
	
	@Override
	@Transactional
	public ECM_Table_Label save(ECM_Table_Label model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<ECM_Table_Label> findAll() {
		return repo.findAll();
	}

}
