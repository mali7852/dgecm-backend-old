package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Udf_Def;

public interface Efile_Udf_DefService {
	
	Efile_Udf_Def save(Efile_Udf_Def model);
	List<Efile_Udf_Def> findAll();
	
}
