package com.datagear.service;

import com.datagear.model.Customer_Udf_Lgchr_Val;
import com.datagear.repository.Customer_Udf_Lgchr_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
public class Customer_Udf_Lgchr_ValServiceImpl implements Customer_Udf_Lgchr_ValService {

    @Autowired
    private Customer_Udf_Lgchr_ValRepository repo;

    @Override
    @Transactional
    public Customer_Udf_Lgchr_Val save(Customer_Udf_Lgchr_Val model) {
        repo.save(model);
        return model;
    }

    @Override
    public List<Customer_Udf_Lgchr_Val> findAll() {
        return repo.findAll();
    }

    @Override
    public List<Customer_Udf_Lgchr_Val> getById(long cust_rk, Timestamp validFromDate) {
        return this.repo.findByIdCustRkAndIdValidFromDate(cust_rk, validFromDate);
    }

}
