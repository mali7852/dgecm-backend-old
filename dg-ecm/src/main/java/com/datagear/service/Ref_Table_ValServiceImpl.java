package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Ref_Table_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Ref_Table_Val;

@Service
public class Ref_Table_ValServiceImpl implements Ref_Table_ValService {

	@Autowired
	private Ref_Table_ValRepository repo;
	
	@Override
	@Transactional
	public Ref_Table_Val save(Ref_Table_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Ref_Table_Val> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Ref_Table_Val> getLookUpList(List<String> lookups) {
		return repo.findByIdRefTableNameIn(lookups);
	}

}
