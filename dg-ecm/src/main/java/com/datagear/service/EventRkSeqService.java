package com.datagear.service;

import java.util.List;

import com.datagear.model.EventRkSeq;

public interface EventRkSeqService {
	
	EventRkSeq save(EventRkSeq model);
	List<EventRkSeq> findAll();
	Long getNextEvent_Rk();
	
}
