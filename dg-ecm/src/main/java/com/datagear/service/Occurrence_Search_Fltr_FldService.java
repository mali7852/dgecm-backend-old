package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_Search_Fltr_Fld;

public interface Occurrence_Search_Fltr_FldService {
	
	Occurrence_Search_Fltr_Fld save(Occurrence_Search_Fltr_Fld model);
	List<Occurrence_Search_Fltr_Fld> findAll();
	
}
