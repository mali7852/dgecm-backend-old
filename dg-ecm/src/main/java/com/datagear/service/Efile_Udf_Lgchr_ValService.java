package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Udf_Lgchr_Val;

public interface Efile_Udf_Lgchr_ValService {
	
	Efile_Udf_Lgchr_Val save(Efile_Udf_Lgchr_Val model);
	List<Efile_Udf_Lgchr_Val> findAll();
	
}
