package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.datagear.customModel.HistoryVerDiff;
import com.datagear.model.Case_Udf_Char_Val;

public interface Case_Udf_Char_ValService {
	
	Case_Udf_Char_Val save(Case_Udf_Char_Val model);
	List<Case_Udf_Char_Val> findAll();
	List<Case_Udf_Char_Val> getById(long case_rk, Timestamp valid_From_Date);
	void saveList(List<Case_Udf_Char_Val> new_chars);
	List<HistoryVerDiff> getHistoryVerDiff(long case_rk, Timestamp curDate, Timestamp oldDate);
    List<Map<String, String>> getSanctionAlertList(long caseRk, Timestamp validFromDate);
	List<Map<String, String>> getSanctionRelAlertList(long caseRk, Timestamp validFromDate);

}
