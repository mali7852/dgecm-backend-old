package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.ECM_Column_LabelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.ECM_Column_Label;

@Service
public class ECM_Column_LabelServiceImpl implements ECM_Column_LabelService {

	@Autowired
	private ECM_Column_LabelRepository repo;
	
	@Override
	@Transactional
	public ECM_Column_Label save(ECM_Column_Label model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<ECM_Column_Label> findAll() {
		return repo.findAll();
	}

}
