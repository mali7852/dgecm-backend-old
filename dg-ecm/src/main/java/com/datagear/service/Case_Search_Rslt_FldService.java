package com.datagear.service;

import java.util.List;

import com.datagear.model.Case_Search_Rslt_Fld;

public interface Case_Search_Rslt_FldService {
	
	Case_Search_Rslt_Fld save(Case_Search_Rslt_Fld model);
	List<Case_Search_Rslt_Fld> findAll();
	
}
