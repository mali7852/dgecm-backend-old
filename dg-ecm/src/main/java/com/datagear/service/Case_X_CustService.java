package com.datagear.service;

import java.util.List;

import com.datagear.model.Case_X_Cust;

public interface Case_X_CustService {
	
	Case_X_Cust save(Case_X_Cust model);
	List<Case_X_Cust> findAll();
	boolean isExists(long entityRk, long memberRk);
	List<Case_X_Cust> getByCaseRk(long memberRk);
	List<Case_X_Cust> getByCustRk(long cust_rk);
	
}
