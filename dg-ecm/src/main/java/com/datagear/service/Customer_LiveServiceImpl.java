package com.datagear.service;

import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import com.datagear.repository.Customer_LiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Customer_Live;

@Service
public class Customer_LiveServiceImpl implements Customer_LiveService {

	@Autowired
	private Customer_LiveRepository repo;

	@Override
	@Transactional
	public Customer_Live save(Customer_Live model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Customer_Live> findAll() {
		return repo.findAll();
	}

	@Override
	public boolean isCustomer_IdExists(String id) {
		if (repo.findFirstByCustId(id) != null)
			return true;
		return false;
	}

	@Override
	public Customer_Live findFirstByCust_Id(String cust_id) {
		return repo.findFirstByCustId(cust_id);
	}

	@Override
	public boolean isExists(long cust_rk) {
		Customer_Live cust_live = getByCustRk(cust_rk);
		if (cust_live != null) {
			return true;
		}
		return false;
	}

	@Override
	public Customer_Live getByCustRk(long cust_rk) {
		Customer_Live cust_live = null;
		try {
			cust_live = repo.findById(cust_rk).get();
		} catch (NoSuchElementException e) {
		}
		return cust_live;
	}

}
