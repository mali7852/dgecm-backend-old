package com.datagear.service;

import java.util.List;

import com.datagear.model.Case_Udf_No_Val;

public interface Case_Udf_No_ValService {
	
	Case_Udf_No_Val save(Case_Udf_No_Val model);
	List<Case_Udf_No_Val> findAll();
	
}
