package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Case_Udf_No_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Case_Udf_No_Val;

@Service
public class Case_Udf_No_ValServiceImpl implements Case_Udf_No_ValService {

	@Autowired
	private Case_Udf_No_ValRepository repo;
	
	@Override
	@Transactional
	public Case_Udf_No_Val save(Case_Udf_No_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_Udf_No_Val> findAll() {
		return repo.findAll();
	}

}
