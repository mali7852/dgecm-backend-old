package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_Udf_Char_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_Udf_Char_Val;

@Service
public class Occurrence_Udf_Char_ValServiceImpl implements Occurrence_Udf_Char_ValService {

	@Autowired
	private Occurrence_Udf_Char_ValRepository repo;
	
	@Override
	@Transactional
	public Occurrence_Udf_Char_Val save(Occurrence_Udf_Char_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_Udf_Char_Val> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Occurrence_Udf_Char_Val> getById(long occs_rk, Timestamp valid_From_Date) {
		return repo.findByIdOccsRkAndIdValidFromDate(occs_rk, valid_From_Date);
	}

	@Override
	public void saveList(List<Occurrence_Udf_Char_Val> new_chars) {
		repo.saveAll(new_chars);
		
	}

}
