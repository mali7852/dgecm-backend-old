package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Udf_Char_Val;

public interface Efile_Udf_Char_ValService {
	
	Efile_Udf_Char_Val save(Efile_Udf_Char_Val model);
	List<Efile_Udf_Char_Val> findAll();
	
}
