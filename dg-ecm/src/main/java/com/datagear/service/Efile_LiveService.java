package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Live;

public interface Efile_LiveService {
	
	Efile_Live save(Efile_Live model);
	List<Efile_Live> findAll();
	
}
