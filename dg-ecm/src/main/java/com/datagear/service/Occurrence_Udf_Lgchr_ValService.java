package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_Udf_Lgchr_Val;

public interface Occurrence_Udf_Lgchr_ValService {
	
	Occurrence_Udf_Lgchr_Val save(Occurrence_Udf_Lgchr_Val model);
	List<Occurrence_Udf_Lgchr_Val> findAll();
	
}
