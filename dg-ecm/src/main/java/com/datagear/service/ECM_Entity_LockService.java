package com.datagear.service;

import com.datagear.model.ECM_Entity_Lock;

import java.util.List;

public interface ECM_Entity_LockService {

    ECM_Entity_Lock save(ECM_Entity_Lock model);

    List<ECM_Entity_Lock> findAll();

    ECM_Entity_Lock getById(long entityRk, ENTITY_TYPE entityName);

    boolean isEntityLocked(long entityRk, ENTITY_TYPE entityName);

    void unlockEntity(long entityRk, ENTITY_TYPE entityName);

    List<ECM_Entity_Lock> getByEntityRkList(List<Long> case_rks, String name);

    enum ENTITY_TYPE {
        Case("CASE"),
        Customer("CUST"),
        Occurrence("OCCS");

        private final String name;

        private ENTITY_TYPE(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}
