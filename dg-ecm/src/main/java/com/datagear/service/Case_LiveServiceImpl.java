package com.datagear.service;

import com.datagear.customModel.CaseLiveStatus;
import com.datagear.customModel.DashboardStatistics;
import com.datagear.model.Case_Live;
import com.datagear.repository.Case_LiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class Case_LiveServiceImpl implements Case_LiveService {

	@Autowired
	private Case_LiveRepository repo;
	
	@Override
	@Transactional
	public Case_Live save(Case_Live model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_Live> findAll() {
		return repo.findAll();
	}

	@Override
	public boolean isCase_IdExists(String id) {
		if (repo.findFirstByCaseId(id) != null)
			return true;
		return false;
	}

	@Override
	public Case_Live getByCaseId(String case_id) {
		return repo.findFirstByCaseId(case_id);
	}

	@Override
	public boolean isExists(long id) {
		Case_Live case_live = getByCaseRk(id);
		if (case_live != null) 
			return true;
		return false;
	}

	@Override
	public Case_Live getByCaseRk(long case_rk) {
		Case_Live case_live = null;
		try {
			case_live = repo.findById(case_rk).get();
		} catch (NoSuchElementException e) {
		}
		return case_live;
	}

	@Override
	public List<Case_Live> getAssociatedCases(long case_rk) {
		return repo.findByCaseLinkSk(BigDecimal.valueOf(case_rk));
	}

	@Override
	public List<Case_Live> getByCaseRkList(List<Long> list) {
		return repo.findByCaseRkIn(list);
	}

	@Override
	public List<Case_Live> saveList(List<Case_Live> case_live_list) {
		return repo.saveAll(case_live_list);
	}

	@Override
	public List<Case_Live> getLastCasesUnderInvestigation() {
		return repo.getLastCasesUnderInvestigation();
	}

	@Override
	public List<DashboardStatistics> getTopWorkingOnUsers() {
		return repo.getTopWorkingOnUsers();
	}

	@Override
	public List<DashboardStatistics> getCaseTypeStatistics() {
		return repo.getCaseTypeStatistics();
	}

	@Override
	public List<CaseLiveStatus> getAllCasesStatus() {
		return this.repo.getAllCasesStatus();
	}

}
