package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Search_Criteria_Field;

public interface Efile_Search_Criteria_FieldService {
	
	Efile_Search_Criteria_Field save(Efile_Search_Criteria_Field model);
	List<Efile_Search_Criteria_Field> findAll();
	
}
