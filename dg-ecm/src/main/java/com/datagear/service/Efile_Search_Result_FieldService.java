package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Search_Result_Field;

public interface Efile_Search_Result_FieldService {
	
	Efile_Search_Result_Field save(Efile_Search_Result_Field model);
	List<Efile_Search_Result_Field> findAll();
	
}
