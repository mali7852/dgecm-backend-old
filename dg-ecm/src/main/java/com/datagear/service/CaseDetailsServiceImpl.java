package com.datagear.service;

import com.datagear.customModel.CaseDetails;
import com.datagear.model.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CaseDetailsServiceImpl implements EntityDetailsService {

    CaseDetails caseDetails = new CaseDetails();
    ;

    @Override
    public CaseDetails getEntityDetails() {
        return this.caseDetails;
    }

    @Override
    public void setEntityHeader(Object case_live) {
        this.caseDetails.setEntityHeader((Case_Live) case_live);

    }

    @Override
    public void setEntityData(List<?> char_vals, List<?> date_vals, List<?> lg_vals) {
        Map<String, String> values = new HashMap<>();

        Map<String, String> char_map = char_vals.parallelStream().filter(ch -> ((Case_Udf_Char_Val) ch).getUdfVal() != null)
                .collect(Collectors.toMap(ch -> ((Case_Udf_Char_Val) ch).getId().getUdfName(), ch -> ((Case_Udf_Char_Val) ch).getUdfVal(),
                        (existing, replacement) -> existing));

        Map<String, String> date_map = date_vals.parallelStream().filter(dt -> ((Case_Udf_Date_Val) dt).getUdfVal() != null)
                .collect(Collectors.toMap(dt -> ((Case_Udf_Date_Val) dt).getId().getUdfName(), dt -> ((Case_Udf_Date_Val) dt).getUdfVal().toString(),
                        (existing, replacement) -> existing));

        Map<String, String> lg_map = lg_vals.parallelStream().filter(lg -> ((Case_Udf_Lgchr_Val) lg).getUdfVal() != null)
                .collect(Collectors.toMap(lg -> ((Case_Udf_Lgchr_Val) lg).getId().getUdfName(), lg -> ((Case_Udf_Lgchr_Val) lg).getUdfVal(),
                        (existing, replacement) -> existing));

        values.putAll(char_map);
        values.putAll(date_map);
        values.putAll(lg_map);

        this.caseDetails.setEntityData(values);
    }

    @Override
    public void setEditFlag(String editFlag) {
        this.caseDetails.setEditFlag(editFlag);
    }

    @Override
    public void setLockUserName(String userName) {
        this.caseDetails.setLockUserName(userName);

    }

    @Override
    public void setEntityConfig(Object case_conf) {
        this.caseDetails.setEntityConfig((Case_Conf) case_conf);

    }

    @Override
    public void setEntityFields(String fields) {
        this.caseDetails.setEntityFields(fields);

    }

}
