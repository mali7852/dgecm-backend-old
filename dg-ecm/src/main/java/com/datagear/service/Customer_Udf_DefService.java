package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_Udf_Def;

public interface Customer_Udf_DefService {
	
	Customer_Udf_Def save(Customer_Udf_Def model);
	List<Customer_Udf_Def> findAll();
	Customer_Udf_Def findFirstByUdf_Name(String key);
    List<Customer_Udf_Def> findByUdfNameList(List<String> udfNameList);
}
