package com.datagear.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.datagear.customModel.CustomerDetails;
import com.datagear.model.*;
import org.springframework.stereotype.Service;

@Service
public class CustomerDetailsServiceImpl implements EntityDetailsService {

	private CustomerDetails customerDetails = new CustomerDetails();

	@Override
	public CustomerDetails getEntityDetails() {
		return this.customerDetails;
	}

	@Override
	public void setEntityHeader(Object cust_live) {
		this.customerDetails.setEntityHeader((Customer_Live) cust_live);

	}

	@Override
	public void setEntityData(List<?> char_vals, List<?> date_vals, List<?> lg_vals) {
		Map<String, String> values = new HashMap<>();

		Map<String, String> char_map = char_vals.parallelStream()
				.collect(Collectors.toMap(ch -> ((Customer_Udf_Char_Val) ch).getId().getUdfName(), ch -> ((Customer_Udf_Char_Val) ch).getUdfVal(),
						(existing, replacement) -> existing));

		Map<String, String> date_map = date_vals.parallelStream()
				.collect(Collectors.toMap(dt -> ((Customer_Udf_Date_Val) dt).getId().getUdfName(), dt -> ((Customer_Udf_Date_Val) dt).getUdfVal().toString(),
						(existing, replacement) -> existing));

		Map<String, String> lg_map = lg_vals.parallelStream()
				.collect(Collectors.toMap(lg -> ((Customer_Udf_Lgchr_Val) lg).getId().getUdfName(), lg -> ((Customer_Udf_Lgchr_Val) lg).getUdfVal(),
						(existing, replacement) -> existing));

		values.putAll(char_map);
		values.putAll(date_map);
		values.putAll(lg_map);

		this.customerDetails.setEntityData(values);
	}

	@Override
	public void setEditFlag(String editFlag) {
		this.customerDetails.setEditFlag(editFlag);
	}

	@Override
	public void setLockUserName(String userName) {
//		this.customerDetails.setLockUserName(userName);

	}

	@Override
	public void setEntityConfig(Object cust_conf) {
		this.customerDetails.setEntityConfig((Customer_Conf) cust_conf);

	}

	@Override
	public void setEntityFields(String fields) {
		this.customerDetails.setEntityFields(fields);

	}

}
