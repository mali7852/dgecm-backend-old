package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Conf;

public interface Efile_ConfService {
	
	Efile_Conf save(Efile_Conf model);
	List<Efile_Conf> findAll();
	
}
