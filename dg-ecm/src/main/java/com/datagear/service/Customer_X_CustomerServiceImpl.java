package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Customer_X_CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Customer_X_Customer;

@Service
public class Customer_X_CustomerServiceImpl implements Customer_X_CustomerService {

	@Autowired
	private Customer_X_CustomerRepository repo;
	
	@Override
	@Transactional
	public Customer_X_Customer save(Customer_X_Customer model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Customer_X_Customer> findAll() {
		return repo.findAll();
	}

	@Override
	public boolean isExists(long cust_rk, long mmbr_cust_rk) {
		if (repo.findFirstByIdCustRkAndIdMmbrCustRk(cust_rk, mmbr_cust_rk) != null)
			return true;
		return false;
	}

	@Override
	public List<Customer_X_Customer> getByCustRk(long cust_rk) {
		return repo.findByIdCustRk(cust_rk);
	}

}
