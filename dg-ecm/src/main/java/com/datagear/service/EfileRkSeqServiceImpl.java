package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.EfileRkSeqRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.EfileRkSeq;

@Service
public class EfileRkSeqServiceImpl implements EfileRkSeqService {

	@Autowired
	private EfileRkSeqRepository repo;
	
	@Override
	@Transactional
	public EfileRkSeq save(EfileRkSeq model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<EfileRkSeq> findAll() {
		return repo.findAll();
	}

}
