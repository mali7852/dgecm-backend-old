package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_Udf_Date_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_Udf_Date_Val;

@Service
public class Occurrence_Udf_Date_ValServiceImpl implements Occurrence_Udf_Date_ValService {

	@Autowired
	private Occurrence_Udf_Date_ValRepository repo;
	
	@Override
	@Transactional
	public Occurrence_Udf_Date_Val save(Occurrence_Udf_Date_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_Udf_Date_Val> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Occurrence_Udf_Date_Val> getById(long occs_rk, Timestamp valid_From_Date) {
		return repo.findByIdOccsRkAndIdValidFromDate(occs_rk, valid_From_Date);
	}

	@Override
	public void saveList(List<Occurrence_Udf_Date_Val> new_dates) {
		repo.saveAll(new_dates);
		
	}

}
