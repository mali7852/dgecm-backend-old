package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_Udf_No_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_Udf_No_Val;

@Service
public class Occurrence_Udf_No_ValServiceImpl implements Occurrence_Udf_No_ValService {

	@Autowired
	private Occurrence_Udf_No_ValRepository repo;
	
	@Override
	@Transactional
	public Occurrence_Udf_No_Val save(Occurrence_Udf_No_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_Udf_No_Val> findAll() {
		return repo.findAll();
	}

}
