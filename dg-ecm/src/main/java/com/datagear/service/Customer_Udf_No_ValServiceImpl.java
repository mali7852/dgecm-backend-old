package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Customer_Udf_No_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Customer_Udf_No_Val;

@Service
public class Customer_Udf_No_ValServiceImpl implements Customer_Udf_No_ValService {

	@Autowired
	private Customer_Udf_No_ValRepository repo;
	
	@Override
	@Transactional
	public Customer_Udf_No_Val save(Customer_Udf_No_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Customer_Udf_No_Val> findAll() {
		return repo.findAll();
	}

}
