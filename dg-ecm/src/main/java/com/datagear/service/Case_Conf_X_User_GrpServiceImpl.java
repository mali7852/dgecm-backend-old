package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Case_Conf_X_User_GrpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Case_Conf_X_User_Grp;

@Service
public class Case_Conf_X_User_GrpServiceImpl implements Case_Conf_X_User_GrpService {

	@Autowired
	private Case_Conf_X_User_GrpRepository repo;
	
	@Override
	@Transactional
	public Case_Conf_X_User_Grp save(Case_Conf_X_User_Grp model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_Conf_X_User_Grp> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Case_Conf_X_User_Grp> getByCaseConfSeqNo(long case_conf_seq_no) {
		return repo.findByIdCaseConfSeqNo(case_conf_seq_no);
	}

}
