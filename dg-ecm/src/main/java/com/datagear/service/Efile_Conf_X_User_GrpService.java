package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Conf_X_User_Grp;

public interface Efile_Conf_X_User_GrpService {
	
	Efile_Conf_X_User_Grp save(Efile_Conf_X_User_Grp model);
	List<Efile_Conf_X_User_Grp> findAll();
	
}
