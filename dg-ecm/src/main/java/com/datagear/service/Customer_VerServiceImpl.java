package com.datagear.service;

import com.datagear.customModel.SearchCustCriteria;
import com.datagear.model.Customer_Udf_Char_Val;
import com.datagear.model.Customer_Ver;
import com.datagear.repository.Customer_VerRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Customer_VerServiceImpl implements Customer_VerService {

    @Autowired
    private Customer_VerRepository repo;

    @Override
    @Transactional
    public Customer_Ver save(Customer_Ver model) {
        repo.save(model);
        return model;
    }

    @Override
    public List<Customer_Ver> findAll() {
        return repo.findAll();
    }

    @Override
    public Page<Customer_Ver> findBySearchCriteria(SearchCustCriteria custCriteria) {
        if (custCriteria.getPageNo() == null)
            custCriteria.setPageNo("1");

        if (custCriteria.getRowNo() == null)
            custCriteria.setRowNo("20");

        Page<Customer_Ver> page = repo.findAll(new Specification<Customer_Ver>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Customer_Ver> root, CriteriaQuery<?> query,
                                         CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                List<Order> orders = new ArrayList<>();
                predicates.add(criteriaBuilder.isNull(root.get("validToDate")));

                if (StringUtils.isNotBlank(custCriteria.getCustTypeCd())) {
                    predicates.add(criteriaBuilder.equal(root.get("custTypeCd"), custCriteria.getCustTypeCd()));
                }

                if (StringUtils.isNotBlank(custCriteria.getCustCtgryCd())) {
                    predicates.add(criteriaBuilder.equal(root.get("custCtgryCd"), custCriteria.getCustCtgryCd()));
                }

                if (StringUtils.isNotBlank(custCriteria.getCustId())) {
                    predicates.add(criteriaBuilder.equal(root.get("custId"), "%" + custCriteria.getCustId() + "%"));
                }

                if (StringUtils.isNotBlank(custCriteria.getCreatedDateFrom())
                        || StringUtils.isNotBlank(custCriteria.getCreatedDateTo())) {
                    Date dtFrom = strToDate(custCriteria.getCreatedDateFrom(), true),
                            dtTo = strToDate(custCriteria.getCreatedDateTo(), false);

                    predicates.add(criteriaBuilder.between(root.get("createDate"), dtFrom, dtTo));
                }

                if (StringUtils.isNotBlank(custCriteria.getLastModifiedDateFrom())
                        || StringUtils.isNotBlank(custCriteria.getLastModifiedDateTo())) {
                    Date dtFrom = strToDate(custCriteria.getLastModifiedDateFrom(), true),
                            dtTo = strToDate(custCriteria.getLastModifiedDateTo(), false);

                    predicates.add(criteriaBuilder.between(root.get("id").get("validFromDate"), dtFrom, dtTo));
                }

                if (StringUtils.isNotBlank(custCriteria.getFullNameAr())
                        || StringUtils.isNotBlank(custCriteria.getFullNameEn())) {

                    Fetch<Customer_Ver, Customer_Udf_Char_Val> fetch = root.fetch("customerUdfCharVals", JoinType.LEFT);
                    Join<Customer_Ver, Customer_Udf_Char_Val> join = (Join<Customer_Ver, Customer_Udf_Char_Val>) fetch;

                    if (StringUtils.isNotBlank(custCriteria.getFullNameAr())) {
                        predicates.add(criteriaBuilder.and(
                                criteriaBuilder.like(join.get("udfVal"), "%" + custCriteria.getFullNameAr() + "%"),
                                criteriaBuilder.equal(join.get("id").get("udfName"), "X_KYC_FULL_NAME_AR")));
                    }

                    if (StringUtils.isNotBlank(custCriteria.getFullNameEn())) {
                        predicates.add(criteriaBuilder.and(
                                criteriaBuilder.like(join.get("udfVal"), "%" + custCriteria.getFullNameEn() + "%"),
                                criteriaBuilder.equal(join.get("id").get("udfName"), "X_KYC_FULL_NAME_EN")));
                    }
                }

                if (StringUtils.isNotBlank(custCriteria.getSortActive()) && StringUtils.isNotBlank(custCriteria.getSortDirection())) {
                    Order o = getOrder(criteriaBuilder, root, custCriteria.getSortActive(), custCriteria.getSortDirection());
                    orders.add(o);
                } else {
                    orders.add(criteriaBuilder.desc(root.get("id").get("validFromDate")));
                }
                query.orderBy(orders);

                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, PageRequest.of(Integer.valueOf(custCriteria.getPageNo()), Integer.valueOf(custCriteria.getRowNo())));

        return page;
    }

    @Override
    public Customer_Ver getById(long cust_rk, Timestamp valid_from_date) {
        return this.repo.findFirstByIdCustRkAndIdValidFromDate(cust_rk, valid_from_date);
    }

    @Override
    public Customer_Ver getByVerNo(long custRk, BigDecimal verNo) {
        return repo.findFirstByIdCustRkAndVerNo(custRk, verNo);
    }

    private Order getOrder(CriteriaBuilder criteriaBuilder, Root<Customer_Ver> root, String sortActive, String sortDirection) {
        if (sortActive.equals("CUST_ID")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("custId")) : criteriaBuilder.desc(root.get("custId"));
        } else if (sortActive.equals("CUST_TYPE_CD")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("custTypeCd")) : criteriaBuilder.desc(root.get("custTypeCd"));
        } else if (sortActive.equals("CUST_CATEGORY_CD")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("custCtgryCd")) : criteriaBuilder.desc(root.get("custCtgryCd"));
        } else if (sortActive.equals("CUST_FULL_NAME")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("custFullName")) : criteriaBuilder.desc(root.get("custFullName"));
        } else if (sortActive.equals("CREATE_DTTM")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("createDate")) : criteriaBuilder.desc(root.get("createDate"));
        } else if (sortActive.equals("CREATE_USER_ID")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("createUserId")) : criteriaBuilder.desc(root.get("createUserId"));
        }
        return criteriaBuilder.desc(root.get("id").get("validFromDate"));
    }

    private Date strToDate(String strDt, boolean before) {
        String DEFAULT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        Date dt = null;
        if (!StringUtils.isNotBlank(strDt)) {
            if (!before)
                dt = new Date(System.currentTimeMillis());
            else
                dt = new Date(0);
        } else {
            try {
                dt = new SimpleDateFormat(DEFAULT_PATTERN).parse(strDt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
//		System.out.println("Date: " + strDt + " -- Reterned: " + dt);
        return dt;
    }

}
