package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_Udf_No_Val;

public interface Customer_Udf_No_ValService {
	
	Customer_Udf_No_Val save(Customer_Udf_No_Val model);
	List<Customer_Udf_No_Val> findAll();
	
}
