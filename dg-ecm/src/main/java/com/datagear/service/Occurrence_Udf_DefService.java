package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_Udf_Def;

public interface Occurrence_Udf_DefService {
	
	Occurrence_Udf_Def save(Occurrence_Udf_Def model);
	List<Occurrence_Udf_Def> findAll();
	Occurrence_Udf_Def findFirstByUdf_Name(String udf_name);
    List<Occurrence_Udf_Def> findByUdfNameList(List<String> udfNameList);
}
