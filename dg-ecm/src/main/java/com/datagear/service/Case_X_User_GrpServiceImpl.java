package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Case_X_User_GrpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Case_X_User_Grp;

@Service
public class Case_X_User_GrpServiceImpl implements Case_X_User_GrpService {

	@Autowired
	private Case_X_User_GrpRepository repo;
	
	@Override
	@Transactional
	public Case_X_User_Grp save(Case_X_User_Grp model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_X_User_Grp> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Long> getCaseRkListByGroupName(String group) {
		return repo.findIdCaseRkByIdUserGrpName(group);
	}

	@Override
	public List<Long> getCaseRkListByGroupNameList(List<String> groups) {
		return repo.findIdCaseRkByIdUserGrpNameIn(groups);
	}

	@Override
	public List<Case_X_User_Grp> getByCaseRk(long case_rk) {
		return this.repo.findByIdCaseRk(case_rk);
	}

}
