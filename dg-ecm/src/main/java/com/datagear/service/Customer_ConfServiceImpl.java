package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Customer_ConfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Customer_Conf;

@Service
public class Customer_ConfServiceImpl implements Customer_ConfService {

	@Autowired
	private Customer_ConfRepository repo;
	
	@Override
	@Transactional
	public Customer_Conf save(Customer_Conf model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Customer_Conf> findAll() {
		return repo.findAll();
	}

	@Override
	public Customer_Conf getByCustTypeCd(String cust_type_cd) {
		return repo.findFirstByCustTypeCd(cust_type_cd);
	}

	@Override
	public Customer_Conf getByUiDefFileName(String ui_Def_File_Name) {
		return repo.findFirstByUiDefFileName(ui_Def_File_Name);
	}

}
