package com.datagear.service;

import com.datagear.customModel.SearchCustCriteria;
import com.datagear.model.Customer_Ver;
import org.springframework.data.domain.Page;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public interface Customer_VerService {

    Customer_Ver save(Customer_Ver model);

    List<Customer_Ver> findAll();

    Page<Customer_Ver> findBySearchCriteria(SearchCustCriteria custCriteria);

    Customer_Ver getById(long cust_rk, Timestamp valid_from_date);

    Customer_Ver getByVerNo(long custRk, BigDecimal verNo);
}
