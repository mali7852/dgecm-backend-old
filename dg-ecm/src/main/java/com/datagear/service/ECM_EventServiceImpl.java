package com.datagear.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.ECM_EventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.ECM_Event;

@Service
public class ECM_EventServiceImpl implements ECM_EventService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ECM_EventRepository repo;

	@Autowired
	private EventRkSeqService eventRkServ;

	@Transactional
	private ECM_Event save(ECM_Event model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<ECM_Event> findAll() {
		return repo.findAll();
	}

	private Timestamp getCurDate() {
		return new Timestamp(System.currentTimeMillis());
	}

	@Override
	public boolean loadCase(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"LODCASE", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean loadCustomer(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CUST", entity_rk, getCurDate(), user_id, event_desc,
					"LODCUST", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean loadEFile(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "EFILE", entity_rk, getCurDate(), user_id, event_desc,
					"LODEFIL", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean loadOccurrence(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "OCCS", entity_rk, getCurDate(), user_id, event_desc,
					"LODOCCS", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean loadComment(long entity_rk, String event_desc, String user_id, long parent_rk, String parent_type) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CMNT", entity_rk, getCurDate(), user_id, event_desc,
					"LODCMNT", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean loadAttachment(long entity_rk, String event_desc, String user_id, long parent_rk,
			String parent_type) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "ATCH", entity_rk, getCurDate(), user_id, event_desc,
					"LODATCH", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateCase(long entity_rk, String event_desc, Timestamp date, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, date, user_id, event_desc,
					"UPDCASE", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateCustomer(long entity_rk, String event_desc, Timestamp date, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CUST", entity_rk, date, user_id, event_desc,
					"UPDCUST", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateEFile(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "EFILE", entity_rk, getCurDate(), user_id, event_desc,
					"UPDEFIL", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateOccurrence(long entity_rk, String event_desc, Timestamp date, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "OCCS", entity_rk, date, user_id, event_desc,
					"UPDOCCS", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateComment(long entity_rk, String event_desc, Timestamp date, String user_id, long parent_rk,
			String parent_type) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CMNT", entity_rk, date, user_id, event_desc,
					"UPDCMNT", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateAttachment(long entity_rk, String event_desc, Timestamp date, String user_id, long parent_rk,
			String parent_type) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "ATCH", entity_rk, date, user_id, event_desc,
					"UPDATCH", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean updateCaseWF(long entity_rk, String event_desc, Timestamp date, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, date, user_id, event_desc,
					"UPDCWF", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean removeCase(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"RMVCASE", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean removeCustomer(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CUST", entity_rk, getCurDate(), user_id, event_desc,
					"RMVCUST", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean removeEFile(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "EFILE", entity_rk, getCurDate(), user_id, event_desc,
					"RMVEFIL", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean removeComment(long entity_rk, String event_desc, String user_id, long parent_rk,
			String parent_type) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CMNT", entity_rk, getCurDate(), user_id, event_desc,
					"RMVCMNT", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean removeAttachment(long entity_rk, String event_desc, String user_id, long parent_rk,
			String parent_type) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "ATCH", entity_rk, getCurDate(), user_id, event_desc,
					"RMVATCH", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean removeOccurrence(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "OCCS", entity_rk, getCurDate(), user_id, event_desc,
					"RMVOCCS", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean removeLink(long entity_rk, String entity_type, String event_desc, String user_id, long parent_rk,
			String parent_type) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), entity_type, entity_rk, getCurDate(), user_id, event_desc,
					"RMVLINK", parent_type, new BigDecimal(parent_rk)));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean linkCaseToCustomer(long entity_rk, String event_desc, String user_id, long parent_rk) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"LNKCUST", "CUST", new BigDecimal(parent_rk)));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean linkCustomerToCustomer(long entity_rk, String event_desc, String user_id, long parent_rk) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CUST", entity_rk, getCurDate(), user_id, event_desc,
					"LNKCUST", "CUST", new BigDecimal(parent_rk)));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean linkOccurrenceToCustomer(long entity_rk, String event_desc, String user_id, long parent_rk) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "OCCS", entity_rk, getCurDate(), user_id, event_desc,
					"LNKCUST", "CUST", new BigDecimal(parent_rk)));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean linkCaseToCase(long entity_rk, String event_desc, String user_id, long parent_rk) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"LNKCASE", "CASE", new BigDecimal(parent_rk)));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean saveCase(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"SAVCASE", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean saveCustomer(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CUST", entity_rk, getCurDate(), user_id, event_desc,
					"SAVCUST", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean saveOccurrence(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "OCCS", entity_rk, getCurDate(), user_id, event_desc,
					"SAVOCCS", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean saveEFile(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "EFILE", entity_rk, getCurDate(), user_id, event_desc,
					"SAVEFIL", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean lockCase(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"LOCK", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean lockCustomer(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CUST", entity_rk, getCurDate(), user_id, event_desc,
					"LOCK", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean lockOccurrence(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "OCCS", entity_rk, getCurDate(), user_id, event_desc,
					"LOCK", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean lockEFile(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "EFILE", entity_rk, getCurDate(), user_id, event_desc,
					"LOCK", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean unlockCase(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"UNLOCK", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean unlockCustomer(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CUST", entity_rk, getCurDate(), user_id, event_desc,
					"UNLOCK", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean unlockOccurrence(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "OCCS", entity_rk, getCurDate(), user_id, event_desc,
					"UNLOCK", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean unlockEFile(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "EFILE", entity_rk, getCurDate(), user_id, event_desc,
					"UNLOCK", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean activeCaseWorkFlow(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"ACTVCWF", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean terminateCaseWorkFlow(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"TERMCWF", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean reassignCase(long entity_rk, String event_desc, String user_id) {
		try {
			save(new ECM_Event(eventRkServ.getNextEvent_Rk(), "CASE", entity_rk, getCurDate(), user_id, event_desc,
					"REASSIN", null, null));
		} catch (Exception e) {
			logger.error("Error while saving event");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public List<ECM_Event> getEntityEvents(long object_rk, String object_name) {
		return repo.findByBusinessObjectRkAndBusinessObjectName(object_rk, object_name);
	}

}
