package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Efile_Udf_Lgchr_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Efile_Udf_Lgchr_Val;

@Service
public class Efile_Udf_Lgchr_ValServiceImpl implements Efile_Udf_Lgchr_ValService {

	@Autowired
	private Efile_Udf_Lgchr_ValRepository repo;
	
	@Override
	@Transactional
	public Efile_Udf_Lgchr_Val save(Efile_Udf_Lgchr_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Efile_Udf_Lgchr_Val> findAll() {
		return repo.findAll();
	}

}
