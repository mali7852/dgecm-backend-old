package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_ConfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_Conf;

@Service
public class Occurrence_ConfServiceImpl implements Occurrence_ConfService {

	@Autowired
	private Occurrence_ConfRepository repo;
	
	@Override
	@Transactional
	public Occurrence_Conf save(Occurrence_Conf model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_Conf> findAll() {
		return repo.findAll();
	}

	@Override
	public Occurrence_Conf getByOccsTypeCd(String occs_type_cd) {
		return repo.findFirstByOccsTypeCd(occs_type_cd);
	}

	@Override
	public Occurrence_Conf getByUiDefFileName(String ui_Def_File_Name) {
		return repo.findFirstByUiDefFileName(ui_Def_File_Name);
	}

}
