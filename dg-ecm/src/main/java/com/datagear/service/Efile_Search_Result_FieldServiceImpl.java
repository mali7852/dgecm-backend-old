package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Efile_Search_Result_FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Efile_Search_Result_Field;

@Service
public class Efile_Search_Result_FieldServiceImpl implements Efile_Search_Result_FieldService {

	@Autowired
	private Efile_Search_Result_FieldRepository repo;
	
	@Override
	@Transactional
	public Efile_Search_Result_Field save(Efile_Search_Result_Field model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Efile_Search_Result_Field> findAll() {
		return repo.findAll();
	}

}
