package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Ref_Table_TransRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Ref_Table_Trans;

@Service
public class Ref_Table_TransServiceImpl implements Ref_Table_TransService {

	@Autowired
	private Ref_Table_TransRepository repo;
	
	@Override
	@Transactional
	public Ref_Table_Trans save(Ref_Table_Trans model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Ref_Table_Trans> findAll() {
		return repo.findAll();
	}

}
