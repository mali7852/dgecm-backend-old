package com.datagear.service;

import com.datagear.customModel.DashboardStatistics;
import com.datagear.customModel.SearchOccsCriteria;
import com.datagear.model.Occurrence_Live;
import org.springframework.data.domain.Page;

import java.util.List;

public interface Occurrence_LiveService {

    Occurrence_Live save(Occurrence_Live model);

    List<Occurrence_Live> findAll();

    boolean isOccs_IdExists(String occs_id);

    Occurrence_Live findFirstByOccs_Id(String occs_id);

    Occurrence_Live getByOccsRk(long occs_rk);

    Page<Occurrence_Live> getByCaseRk(long case_rk);

    boolean isExists(long occs_rk);

    List<Occurrence_Live> getByOccsRkList(List<Long> occs_rks);

    List<DashboardStatistics> getOccsTypeStatistics();

    Page<Occurrence_Live> findBySearchCriteria(SearchOccsCriteria occsCriteria);
}
