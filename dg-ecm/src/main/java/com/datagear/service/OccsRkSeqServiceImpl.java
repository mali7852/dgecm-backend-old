package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.OccsRkSeqRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.OccsRkSeq;

@Service
public class OccsRkSeqServiceImpl implements OccsRkSeqService {

	@Autowired
	private OccsRkSeqRepository repo;
	
	@Override
	@Transactional
	public OccsRkSeq save(OccsRkSeq model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<OccsRkSeq> findAll() {
		return repo.findAll();
	}

}
