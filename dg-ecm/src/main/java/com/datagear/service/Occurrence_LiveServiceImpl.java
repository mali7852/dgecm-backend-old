package com.datagear.service;

import com.datagear.customModel.DashboardStatistics;
import com.datagear.customModel.SearchOccsCriteria;
import com.datagear.model.Occurrence_Live;
import com.datagear.repository.Occurrence_LiveRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class Occurrence_LiveServiceImpl implements Occurrence_LiveService {

    @Autowired
    private Occurrence_LiveRepository repo;

    @Override
    @Transactional
    public Occurrence_Live save(Occurrence_Live model) {
        repo.save(model);
        return model;
    }

    @Override
    public List<Occurrence_Live> findAll() {
        return repo.findAll();
    }

    @Override
    public boolean isOccs_IdExists(String occs_id) {
        if (repo.findFirstByOccsId(occs_id) != null)
            return true;
        return false;
    }

    @Override
    public Occurrence_Live findFirstByOccs_Id(String occs_id) {
        return repo.findFirstByOccsId(occs_id);
    }

    @Override
    public Occurrence_Live getByOccsRk(long occs_rk) {
        Occurrence_Live occs_live = null;
        try {
            occs_live = repo.findById(occs_rk).get();
        } catch (NoSuchElementException e) {
        }
        return occs_live;
    }

    @Override
    public Page<Occurrence_Live> getByCaseRk(long case_rk) {
        return repo.findByCaseLiveCaseRk(case_rk, (Pageable) PageRequest.of(0, 1000));
    }

    @Override
    public boolean isExists(long occs_rk) {
        if (getByOccsRk(occs_rk) != null)
            return true;
        return false;
    }

    @Override
    public List<Occurrence_Live> getByOccsRkList(List<Long> occs_rks) {
        return repo.findByOccsRkIn(occs_rks);
    }

    @Override
    public List<DashboardStatistics> getOccsTypeStatistics() {
        return repo.getOccsTypeStatistics();
    }

    @Override
    public Page<Occurrence_Live> findBySearchCriteria(SearchOccsCriteria occsCriteria) {
        if (occsCriteria.getPageNo() == null)
            occsCriteria.setPageNo("1");

        if (occsCriteria.getRowNo() == null)
            occsCriteria.setRowNo("20");

        Page<Occurrence_Live> page = repo.findAll(new Specification<Occurrence_Live>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Occurrence_Live> root, CriteriaQuery<?> query,
                                         CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                List<Order> orders = new ArrayList<>();

                if (StringUtils.isNotBlank(occsCriteria.getOccsTypeCd())) {
                    predicates.add(criteriaBuilder.equal(root.get("occsTypeCd"), occsCriteria.getOccsTypeCd()));
                }

                if (StringUtils.isNotBlank(occsCriteria.getOccsId())) {
                    predicates.add(criteriaBuilder.equal(root.get("occsId"), occsCriteria.getOccsId()));
                }

                if (StringUtils.isNotBlank(occsCriteria.getCaseId())) {
                    predicates.add(criteriaBuilder.equal(root.get("caseLive").get("caseId"), occsCriteria.getCaseId()));
                }

                if (StringUtils.isNotBlank(occsCriteria.getCreatedDateFrom())
                        || StringUtils.isNotBlank(occsCriteria.getCreatedDateTo())) {
                    Date dtFrom = strToDate(occsCriteria.getCreatedDateFrom(), true),
                            dtTo = strToDate(occsCriteria.getCreatedDateTo(), false);

                    predicates.add(criteriaBuilder.between(root.get("createDate"), dtFrom, dtTo));
                }

                if (StringUtils.isNotBlank(occsCriteria.getSortActive()) && StringUtils.isNotBlank(occsCriteria.getSortDirection())) {
                    Order o = getOrder(criteriaBuilder, root, occsCriteria.getSortActive(), occsCriteria.getSortDirection());
                    orders.add(o);
                } else {
                    orders.add(criteriaBuilder.desc(root.get("createDate")));
                }
                query.orderBy(orders);

                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, PageRequest.of(Integer.valueOf(occsCriteria.getPageNo()), Integer.valueOf(occsCriteria.getRowNo())));

        return page;
    }

	private Order getOrder(CriteriaBuilder criteriaBuilder, Root<Occurrence_Live> root, String sortActive, String sortDirection) {
		if (sortActive.equals("OCCS_ID")) {
			return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("occsId")) : criteriaBuilder.desc(root.get("occsId"));
		} else if (sortActive.equals("OCCS_TYPE_CD")) {
			return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("occsTypeCd")) : criteriaBuilder.desc(root.get("occsTypeCd"));
		} else if (sortActive.equals("CREATE_DTTM")) {
			return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("createDate")) : criteriaBuilder.desc(root.get("createDate"));
		} else if (sortActive.equals("CASE_ID")) {
			return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("caseLive").get("caseId")) : criteriaBuilder.desc(root.get("caseLive").get("caseId"));
		}
		return criteriaBuilder.desc(root.get("createDate"));
	}

    private Date strToDate(String strDt, boolean before) {
        String DEFAULT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        Date dt = null;
        if (!StringUtils.isNotBlank(strDt)) {
            if (!before)
                dt = new Date(System.currentTimeMillis());
            else
                dt = new Date(0);
        } else {
            try {
                dt = new SimpleDateFormat(DEFAULT_PATTERN).parse(strDt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return dt;
    }

}
