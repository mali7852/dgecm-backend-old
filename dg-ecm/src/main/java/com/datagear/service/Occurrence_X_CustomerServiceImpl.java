package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_X_CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_X_Customer;

@Service
public class Occurrence_X_CustomerServiceImpl implements Occurrence_X_CustomerService {

	@Autowired
	private Occurrence_X_CustomerRepository repo;
	
	@Override
	@Transactional
	public Occurrence_X_Customer save(Occurrence_X_Customer model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_X_Customer> findAll() {
		return repo.findAll();
	}

	@Override
	public boolean isExists(long occsRk, long custRk) {
		if (repo.findFirstByIdOccsRkAndIdCustRk(occsRk, custRk) != null)
			return true;
		return false;
	}

	@Override
	public List<Occurrence_X_Customer> getByCustRk(long cust_rk) {
		return repo.findByIdCustRk(cust_rk);
	}

}
