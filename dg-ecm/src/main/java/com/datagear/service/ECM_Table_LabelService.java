package com.datagear.service;

import java.util.List;

import com.datagear.model.ECM_Table_Label;

public interface ECM_Table_LabelService {
	
	ECM_Table_Label save(ECM_Table_Label model);
	List<ECM_Table_Label> findAll();
	
}
