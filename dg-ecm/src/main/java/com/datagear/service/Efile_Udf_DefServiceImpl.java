package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Efile_Udf_DefRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Efile_Udf_Def;

@Service
public class Efile_Udf_DefServiceImpl implements Efile_Udf_DefService {

	@Autowired
	private Efile_Udf_DefRepository repo;
	
	@Override
	@Transactional
	public Efile_Udf_Def save(Efile_Udf_Def model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Efile_Udf_Def> findAll() {
		return repo.findAll();
	}

}
