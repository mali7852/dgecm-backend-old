package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_Udf_DefRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_Udf_Def;

@Service
public class Occurrence_Udf_DefServiceImpl implements Occurrence_Udf_DefService {

	@Autowired
	private Occurrence_Udf_DefRepository repo;
	
	@Override
	@Transactional
	public Occurrence_Udf_Def save(Occurrence_Udf_Def model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_Udf_Def> findAll() {
		return repo.findAll();
	}

	@Override
	public Occurrence_Udf_Def findFirstByUdf_Name(String udf_name) {
		return repo.findFirstByIdUdfName(udf_name);
	}

	@Override
	public List<Occurrence_Udf_Def> findByUdfNameList(List<String> udfNameList) {
		return this.repo.findByIdUdfNameIn(udfNameList);
	}

}
