package com.datagear.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;

import com.datagear.repository.EventRkSeqRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.EventRkSeq;

@Service
public class EventRkSeqServiceImpl implements EventRkSeqService {

	@Autowired
	private EventRkSeqRepository repo;
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
    public Long getNextEvent_Rk() {

        //"login" this is the name of your procedure
        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("event_rk_seq_next"); 

        //Declare the parameters in the same order
        query.registerStoredProcedureParameter(1, Long.class, ParameterMode.OUT);

        //Execute query
        query.execute();

        //Get output parameters
        Long outCode = (Long) query.getOutputParameterValue(1);

        return outCode; 
    }
	
	@Override
	@Transactional
	public EventRkSeq save(EventRkSeq model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<EventRkSeq> findAll() {
		return repo.findAll();
	}

}
