package com.datagear.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.datagear.model.Case_Ver;
import org.springframework.data.domain.Page;

import com.datagear.customModel.SearchCaseCriteria;

public interface Case_VerService {
	
	Case_Ver save(Case_Ver model);
	List<Case_Ver> findAll();
	Case_Ver getById(long case_Rk, Timestamp valid_From_Date);
	Page<Case_Ver> findBySearchCriteria(SearchCaseCriteria caseCriteria);
	Case_Ver getByVerNo(long caseRk, BigDecimal verNo);
	
}
