package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_Conf_X_User_GrpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_Conf_X_User_Grp;

@Service
public class Occurrence_Conf_X_User_GrpServiceImpl implements Occurrence_Conf_X_User_GrpService {

	@Autowired
	private Occurrence_Conf_X_User_GrpRepository repo;
	
	@Override
	@Transactional
	public Occurrence_Conf_X_User_Grp save(Occurrence_Conf_X_User_Grp model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_Conf_X_User_Grp> findAll() {
		return repo.findAll();
	}

	@Override
	public Occurrence_Conf_X_User_Grp getUserGrpName(long occs_Conf_Seq_No) {
		return repo.findFirstByIdOccsConfSeqNo(occs_Conf_Seq_No);
	}

}
