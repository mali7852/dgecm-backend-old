package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Case_ConfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Case_Conf;

@Service
public class Case_ConfServiceImpl implements Case_ConfService {

	@Autowired
	private Case_ConfRepository repo;
	
	@Override
	@Transactional
	public Case_Conf save(Case_Conf model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_Conf> findAll() {
		return repo.findAll();
	}

	@Override
	public Case_Conf getByCaseTypeCd(String case_Type_Cd) {
		return repo.findFirstByCaseTypeCd(case_Type_Cd);
	}

	@Override
	public Case_Conf getByUiDefFileName(String ui_Def_File_Name) {
		return repo.findFirstByUiDefFileName(ui_Def_File_Name);
	}

}
