package com.datagear.service;

import com.datagear.model.Customer_Udf_Lgchr_Val;

import java.sql.Timestamp;
import java.util.List;

public interface Customer_Udf_Lgchr_ValService {

    Customer_Udf_Lgchr_Val save(Customer_Udf_Lgchr_Val model);

    List<Customer_Udf_Lgchr_Val> findAll();

    List<Customer_Udf_Lgchr_Val> getById(long cust_rk, Timestamp validFromDate);

}
