package com.datagear.service;

import com.datagear.customModel.CaseLiveStatus;
import com.datagear.customModel.DashboardStatistics;
import com.datagear.model.Case_Live;

import java.util.List;

public interface Case_LiveService {

    Case_Live save(Case_Live model);

    List<Case_Live> findAll();

    boolean isCase_IdExists(String id);

    Case_Live getByCaseId(String case_id);

    boolean isExists(long case_rk);

    Case_Live getByCaseRk(long l);

    List<Case_Live> getAssociatedCases(long case_rk);

    List<Case_Live> getByCaseRkList(List<Long> case_rk_list);

    List<Case_Live> saveList(List<Case_Live> case_live_list);

    List<Case_Live> getLastCasesUnderInvestigation();

    List<DashboardStatistics> getTopWorkingOnUsers();

    List<DashboardStatistics> getCaseTypeStatistics();
    
    List<CaseLiveStatus> getAllCasesStatus();
}
