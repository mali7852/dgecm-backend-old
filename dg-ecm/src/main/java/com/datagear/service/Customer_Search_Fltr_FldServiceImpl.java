package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Customer_Search_Fltr_FldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Customer_Search_Fltr_Fld;

@Service
public class Customer_Search_Fltr_FldServiceImpl implements Customer_Search_Fltr_FldService {

	@Autowired
	private Customer_Search_Fltr_FldRepository repo;
	
	@Override
	@Transactional
	public Customer_Search_Fltr_Fld save(Customer_Search_Fltr_Fld model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Customer_Search_Fltr_Fld> findAll() {
		return repo.findAll();
	}

}
