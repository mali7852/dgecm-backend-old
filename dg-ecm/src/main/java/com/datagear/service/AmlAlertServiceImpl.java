package com.datagear.service;

import java.util.List;

import com.datagear.repository.AmlAlertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.datagear.model.AmlAlert;

@Service
public class AmlAlertServiceImpl implements AmlAlertService {
	
	@Autowired
	private AmlAlertRepository repo;

	@Override
	public Page<AmlAlert> findAll(int pageNumber, int pageSize) {
		return repo.findAll(PageRequest.of(pageNumber, pageSize));
	}

	@Override
	public List<AmlAlert> findAll() {
		return repo.findAll();
	}

	@Override
	public List<AmlAlert> findByEntityNum(String entity_num) {
		return repo.findByEntityNum(entity_num);
	}

}
