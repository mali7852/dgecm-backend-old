package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Customer_Udf_DefRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Customer_Udf_Def;

@Service
public class Customer_Udf_DefServiceImpl implements Customer_Udf_DefService {

	@Autowired
	private Customer_Udf_DefRepository repo;
	
	@Override
	@Transactional
	public Customer_Udf_Def save(Customer_Udf_Def model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Customer_Udf_Def> findAll() {
		return repo.findAll();
	}

	@Override
	public Customer_Udf_Def findFirstByUdf_Name(String udf_name) {
		return repo.findFirstByIdUdfName(udf_name);
	}

	@Override
	public List<Customer_Udf_Def> findByUdfNameList(List<String> udfNameList) {
		return this.repo.findByIdUdfNameIn(udfNameList);
	}

}
