package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Case_X_CustRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Case_X_Cust;

@Service
public class Case_X_CustServiceImpl implements Case_X_CustService {

	@Autowired
	private Case_X_CustRepository repo;
	
	@Override
	@Transactional
	public Case_X_Cust save(Case_X_Cust model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_X_Cust> findAll() {
		return repo.findAll();
	}

	@Override
	public boolean isExists(long entityRk, long mmberRk) {
		if (repo.findFirstByIdCaseRkAndIdCustRk(entityRk, mmberRk) != null)
			return true;
		return false;
	}

	@Override
	public List<Case_X_Cust> getByCaseRk(long case_rk) {
		return repo.findByIdCaseRk(case_rk);
	}

	@Override
	public List<Case_X_Cust> getByCustRk(long cust_rk) {
		return repo.findByIdCustRk(cust_rk);
	}

}
