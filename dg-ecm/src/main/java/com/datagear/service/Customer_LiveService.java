package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_Live;

public interface Customer_LiveService {
	
	Customer_Live save(Customer_Live model);
	List<Customer_Live> findAll();
	boolean isCustomer_IdExists(String string);
	Customer_Live findFirstByCust_Id(String cust_id);
	boolean isExists(long cust_rk);
	Customer_Live getByCustRk(long cust_Rk);
	
}
