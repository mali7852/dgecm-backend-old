package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Udf_Date_Val;

public interface Efile_Udf_Date_ValService {
	
	Efile_Udf_Date_Val save(Efile_Udf_Date_Val model);
	List<Efile_Udf_Date_Val> findAll();
	
}
