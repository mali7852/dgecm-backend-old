package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Ver;

public interface Efile_VerService {
	
	Efile_Ver save(Efile_Ver model);
	List<Efile_Ver> findAll();
	
}
