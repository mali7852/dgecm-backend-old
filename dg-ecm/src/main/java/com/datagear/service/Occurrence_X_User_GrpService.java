package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_X_User_Grp;

public interface Occurrence_X_User_GrpService {
	
	Occurrence_X_User_Grp save(Occurrence_X_User_Grp model);
	List<Occurrence_X_User_Grp> findAll();
	
}
