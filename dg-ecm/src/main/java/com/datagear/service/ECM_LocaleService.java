package com.datagear.service;

import java.util.List;

import com.datagear.model.ECM_Locale;

public interface ECM_LocaleService {
	
	ECM_Locale save(ECM_Locale model);
	List<ECM_Locale> findAll();
	
}
