package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Efile_LiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Efile_Live;

@Service
public class Efile_LiveServiceImpl implements Efile_LiveService {

	@Autowired
	private Efile_LiveRepository repo;
	
	@Override
	@Transactional
	public Efile_Live save(Efile_Live model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Efile_Live> findAll() {
		return repo.findAll();
	}

}
