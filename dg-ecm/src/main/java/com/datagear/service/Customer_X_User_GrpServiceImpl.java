package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Customer_X_User_GrpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Customer_X_User_Grp;

@Service
public class Customer_X_User_GrpServiceImpl implements Customer_X_User_GrpService {

	@Autowired
	private Customer_X_User_GrpRepository repo;
	
	@Override
	@Transactional
	public Customer_X_User_Grp save(Customer_X_User_Grp model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Customer_X_User_Grp> findAll() {
		return repo.findAll();
	}

}
