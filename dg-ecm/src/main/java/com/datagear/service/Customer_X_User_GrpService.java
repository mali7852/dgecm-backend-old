package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_X_User_Grp;

public interface Customer_X_User_GrpService {
	
	Customer_X_User_Grp save(Customer_X_User_Grp model);
	List<Customer_X_User_Grp> findAll();
	
}
