package com.datagear.service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Service;

@Service
public class Case_RkServiceImpl implements Case_RkService {

	@PersistenceContext
    private EntityManager entityManager;

    public Long getNextCase_Rk() {

        //this is the name of your procedure
        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("case_rk_seq_next"); 

        //Declare the parameters in the same order
        query.registerStoredProcedureParameter(1, Long.class, ParameterMode.OUT);

        //Execute query
        query.execute();

        //Get output parameters
        Long outCode = (Long) query.getOutputParameterValue(1);

        return outCode; 
    }
}
