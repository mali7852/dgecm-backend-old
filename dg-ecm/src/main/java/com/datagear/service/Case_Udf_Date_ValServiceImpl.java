package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Case_Udf_Date_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Case_Udf_Date_Val;

@Service
public class Case_Udf_Date_ValServiceImpl implements Case_Udf_Date_ValService {

	@Autowired
	private Case_Udf_Date_ValRepository repo;
	
	@Override
	@Transactional
	public Case_Udf_Date_Val save(Case_Udf_Date_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_Udf_Date_Val> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Case_Udf_Date_Val> getById(long case_rk, Timestamp valid_from_date) {
		return repo.findByIdCaseRkAndIdValidFromDate(case_rk, valid_from_date);
	}

	@Override
	public void saveList(List<Case_Udf_Date_Val> new_dates) {
		repo.saveAll(new_dates);
	}

}
