package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_Search_Crtria_Fld;

public interface Customer_Search_Crtria_FldService {
	
	Customer_Search_Crtria_Fld save(Customer_Search_Crtria_Fld model);
	List<Customer_Search_Crtria_Fld> findAll();
	
}
