package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_Conf_X_User_Grp;

public interface Occurrence_Conf_X_User_GrpService {
	
	Occurrence_Conf_X_User_Grp save(Occurrence_Conf_X_User_Grp model);
	List<Occurrence_Conf_X_User_Grp> findAll();
	Occurrence_Conf_X_User_Grp getUserGrpName(long occs_Conf_Seq_No);
	
}
