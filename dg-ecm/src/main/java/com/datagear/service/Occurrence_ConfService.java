package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_Conf;

public interface Occurrence_ConfService {
	
	Occurrence_Conf save(Occurrence_Conf model);
	List<Occurrence_Conf> findAll();
	Occurrence_Conf getByOccsTypeCd(String string);
	Occurrence_Conf getByUiDefFileName(String ui_Def_File_Name);
	
}
