package com.datagear.service;

import java.util.List;

import com.datagear.model.Case_Conf_X_User_Grp;

public interface Case_Conf_X_User_GrpService {
	
	Case_Conf_X_User_Grp save(Case_Conf_X_User_Grp model);
	List<Case_Conf_X_User_Grp> findAll();
	List<Case_Conf_X_User_Grp> getByCaseConfSeqNo(long case_conf_seq_no);
	
}
