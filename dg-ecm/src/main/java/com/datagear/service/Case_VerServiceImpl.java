package com.datagear.service;

import com.datagear.customModel.SearchCaseCriteria;
import com.datagear.model.Case_Live;
import com.datagear.model.Case_Ver;
import com.datagear.model.Case_X_User_Grp;
import com.datagear.repository.Case_VerRepository;
import com.datagear.security.model.LoginUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class Case_VerServiceImpl implements Case_VerService {

    @Autowired
    private Case_VerRepository repo;

    @Override
    @Transactional
    public Case_Ver save(Case_Ver model) {
        repo.save(model);
        return model;
    }

    @Override
    public List<Case_Ver> findAll() {
        return repo.findAll();
    }

    @Override
    public Case_Ver getById(long case_rk, Timestamp valid_From_Date) {
        return repo.findFirstByIdCaseRkAndIdValidFromDate(case_rk, valid_From_Date);
    }

    @Override
    public Page<Case_Ver> findBySearchCriteria(SearchCaseCriteria caseCriteria) {
        if (caseCriteria.getPageNo() == null)
            caseCriteria.setPageNo("0");

        if (caseCriteria.getRowNo() == null)
            caseCriteria.setRowNo("20");

        return repo.findAll(new Specification<Case_Ver>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<Case_Ver> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                List<Order> orders = new ArrayList<>();
                predicates.add(criteriaBuilder.isNull(root.get("validToDate")));

                Join<Case_Live, Case_X_User_Grp> caseLiveCaseXUserGrpJoin = root.join("caseLive", JoinType.LEFT).join("caseXUserGrps", JoinType.LEFT);

                predicates.add(caseLiveCaseXUserGrpJoin.get("id").get("userGrpName").in(LoginUser.getInstance().getGroups()));

                if (StringUtils.isNotBlank(caseCriteria.getCaseTypeCd())) {
                    predicates.add(criteriaBuilder.equal(root.get("caseTypeCd"), caseCriteria.getCaseTypeCd()));
                }

                if (StringUtils.isNotBlank(caseCriteria.getCaseCtgryCd())) {
                    predicates.add(criteriaBuilder.equal(root.get("caseCtgryCd"), caseCriteria.getCaseCtgryCd()));
                }

                if (StringUtils.isNotBlank(caseCriteria.getCaseId())) {
                    predicates.add(criteriaBuilder.like(root.get("caseId"), "%" + caseCriteria.getCaseId() + "%"));
                }

                if (StringUtils.isNotBlank(caseCriteria.getCaseStatusCd())) {
                    predicates.add(criteriaBuilder.equal(root.get("caseStatCd"), caseCriteria.getCaseStatusCd()));
                }

                if (StringUtils.isNotBlank(caseCriteria.getPriority())) {
                    predicates.add(criteriaBuilder.equal(root.get("priorityCd"), caseCriteria.getPriority()));
                }

                if (StringUtils.isNotBlank(caseCriteria.getCreatedDateFrom())
                        || StringUtils.isNotBlank(caseCriteria.getCreatedDateTo())) {
                    Date dtFrom = strToDate(caseCriteria.getCreatedDateFrom(), true),
                            dtTo = strToDate(caseCriteria.getCreatedDateTo(), false);

                    if (dtFrom.compareTo(dtTo) == 0) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(dtFrom.getTime());
                        cal.add(Calendar.DATE, 1);
                        dtTo.setTime(cal.getTimeInMillis());
                    }

                    predicates.add(criteriaBuilder.between(root.get("createDate"), dtFrom, dtTo));
                }

                if (StringUtils.isNotBlank(caseCriteria.getLastModifiedDateFrom())
                        || StringUtils.isNotBlank(caseCriteria.getLastModifiedDateTo())) {
                    Date dtFrom = strToDate(caseCriteria.getLastModifiedDateFrom(), true),
                            dtTo = strToDate(caseCriteria.getLastModifiedDateTo(), false);

                    if (dtFrom.compareTo(dtTo) == 0) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(dtFrom.getTime());
                        cal.add(Calendar.DATE, 1);
                        dtTo.setTime(cal.getTimeInMillis());
                    }

                    predicates.add(criteriaBuilder.between(root.get("id").get("validFromDate"), dtFrom, dtTo));
                }

                if (StringUtils.isNotBlank(caseCriteria.getOpenedDateFrom())
                        || StringUtils.isNotBlank(caseCriteria.getOpenedDateTo())) {
                    Date dtFrom = strToDate(caseCriteria.getOpenedDateFrom(), true),
                            dtTo = strToDate(caseCriteria.getOpenedDateTo(), false);

                    if (dtFrom.compareTo(dtTo) == 0) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(dtFrom.getTime());
                        cal.add(Calendar.DATE, 1);
                        dtTo.setTime(cal.getTimeInMillis());
                    }

                    predicates.add(criteriaBuilder.between(root.get("openDate"), dtFrom, dtTo));
                }

                if (StringUtils.isNotBlank(caseCriteria.getClosedDateFrom())
                        || StringUtils.isNotBlank(caseCriteria.getClosedDateTo())) {
                    Date dtFrom = strToDate(caseCriteria.getClosedDateFrom(), true),
                            dtTo = strToDate(caseCriteria.getClosedDateTo(), false);

                    if (dtFrom.compareTo(dtTo) == 0) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(dtFrom.getTime());
                        cal.add(Calendar.DATE, 1);
                        dtTo.setTime(cal.getTimeInMillis());
                    }

                    predicates.add(criteriaBuilder.between(root.get("closeDate"), dtFrom, dtTo));
                }

                if (StringUtils.isNotBlank(caseCriteria.getCol1())) {
                    predicates.add(criteriaBuilder.like(root.get("col1"), "%" + caseCriteria.getCol1() + "%"));
                }

                if (StringUtils.isNotBlank(caseCriteria.getCol2())) {
                    predicates.add(criteriaBuilder.like(root.get("col2"), "%" + caseCriteria.getCol2() + "%"));
                }

                if (StringUtils.isNotBlank(caseCriteria.getCol3())) {
                    predicates.add(criteriaBuilder.like(root.get("col3"), "%" + caseCriteria.getCol3() + "%"));
                }

                if (StringUtils.isNotBlank(caseCriteria.getCol4())) {
                    predicates.add(criteriaBuilder.like(root.get("col4"), "%" + caseCriteria.getCol4() + "%"));
                }

                if (StringUtils.isNotBlank(caseCriteria.getCol5())) {
                    predicates.add(criteriaBuilder.like(root.get("col5"), "%" + caseCriteria.getCol5() + "%"));
                }

                if (StringUtils.isNotBlank(caseCriteria.getSortActive()) && StringUtils.isNotBlank(caseCriteria.getSortDirection())) {
                    Order o = getOrder(criteriaBuilder, root, caseCriteria.getSortActive(), caseCriteria.getSortDirection());
                    orders.add(o);
                } else {
                    orders.add(criteriaBuilder.desc(root.get("id").get("validFromDate")));
                }
                query.orderBy(orders);

                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, PageRequest.of(Integer.parseInt(caseCriteria.getPageNo()), Integer.parseInt(caseCriteria.getRowNo())));
    }

    private Order getOrder(CriteriaBuilder criteriaBuilder, Root<Case_Ver> root, String sortActive, String sortDirection) {
        if (sortActive.equals("CASE_ID")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("caseId")) : criteriaBuilder.desc(root.get("caseId"));
        } else if (sortActive.equals("CASE_TYPE_CD")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("caseTypeCd")) : criteriaBuilder.desc(root.get("caseTypeCd"));
        } else if (sortActive.equals("CASE_CATEGORY_CD")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("caseCtgryCd")) : criteriaBuilder.desc(root.get("caseCtgryCd"));
        } else if (sortActive.equals("CASE_DESC")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("caseDesc")) : criteriaBuilder.desc(root.get("caseDesc"));
        } else if (sortActive.equals("CREATE_DTTM")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("createDate")) : criteriaBuilder.desc(root.get("createDate"));
        } else if (sortActive.equals("INVESTIGATOR_USER_ID")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("investrUserId")) : criteriaBuilder.desc(root.get("investrUserId"));
        } else if (sortActive.equals("CASE_STATUS_CD")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("caseStatCd")) : criteriaBuilder.desc(root.get("caseStatCd"));
        } else if (sortActive.equals("PRIORITY_CD")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("priorityCd")) : criteriaBuilder.desc(root.get("priorityCd"));
        } else if (sortActive.equals("CREATE_USER_ID")) {
            return sortDirection.equals("asc") ? criteriaBuilder.asc(root.get("createUserId")) : criteriaBuilder.desc(root.get("createUserId"));
        }
        return criteriaBuilder.desc(root.get("id").get("validFromDate"));
    }

    private Date strToDate(String strDt, boolean before) {
        String DEFAULT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        Date dt = null;
        if (!StringUtils.isNotBlank(strDt)) {
            if (!before)
                dt = new Date(System.currentTimeMillis());
            else
                dt = new Date(0);
        } else {
            try {
                dt = new SimpleDateFormat(DEFAULT_PATTERN).parse(strDt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
//		System.out.println("Date: " + strDt + " -- Reterned: " + dt);
        return dt;
    }

    @Override
    public Case_Ver getByVerNo(long caseRk, BigDecimal verNo) {
        return this.repo.findFirstByIdCaseRkAndVerNo(caseRk, verNo);
    }

}
