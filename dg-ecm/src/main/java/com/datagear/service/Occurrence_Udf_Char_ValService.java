package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import com.datagear.model.Occurrence_Udf_Char_Val;

public interface Occurrence_Udf_Char_ValService {
	
	Occurrence_Udf_Char_Val save(Occurrence_Udf_Char_Val model);
	List<Occurrence_Udf_Char_Val> findAll();
	List<Occurrence_Udf_Char_Val> getById(long occs_rk, Timestamp valid_From_Date);
	void saveList(List<Occurrence_Udf_Char_Val> new_chars);
	
}
