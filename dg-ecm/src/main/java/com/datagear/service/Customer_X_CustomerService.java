package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_X_Customer;

public interface Customer_X_CustomerService {
	
	Customer_X_Customer save(Customer_X_Customer model);
	List<Customer_X_Customer> findAll();
	boolean isExists(long entityRk, long memberRk);
	List<Customer_X_Customer> getByCustRk(long cust_rk);
	
}
