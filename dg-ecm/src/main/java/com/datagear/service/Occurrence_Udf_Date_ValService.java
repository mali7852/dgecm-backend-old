package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import com.datagear.model.Occurrence_Udf_Date_Val;

public interface Occurrence_Udf_Date_ValService {
	
	Occurrence_Udf_Date_Val save(Occurrence_Udf_Date_Val model);
	List<Occurrence_Udf_Date_Val> findAll();
	List<Occurrence_Udf_Date_Val> getById(long occs_rk, Timestamp valid_From_Date);
	void saveList(List<Occurrence_Udf_Date_Val> new_dates);
	
}
