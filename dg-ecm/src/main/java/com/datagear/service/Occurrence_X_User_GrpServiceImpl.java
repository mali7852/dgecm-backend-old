package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_X_User_GrpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_X_User_Grp;

@Service
public class Occurrence_X_User_GrpServiceImpl implements Occurrence_X_User_GrpService {

	@Autowired
	private Occurrence_X_User_GrpRepository repo;
	
	@Override
	@Transactional
	public Occurrence_X_User_Grp save(Occurrence_X_User_Grp model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Occurrence_X_User_Grp> findAll() {
		return repo.findAll();
	}

}
