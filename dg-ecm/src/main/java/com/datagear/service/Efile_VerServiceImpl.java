package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Efile_VerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Efile_Ver;

@Service
public class Efile_VerServiceImpl implements Efile_VerService {

	@Autowired
	private Efile_VerRepository repo;
	
	@Override
	@Transactional
	public Efile_Ver save(Efile_Ver model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Efile_Ver> findAll() {
		return repo.findAll();
	}

}
