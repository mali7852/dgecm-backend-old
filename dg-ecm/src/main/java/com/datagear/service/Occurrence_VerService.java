package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import com.datagear.model.Occurrence_Ver;

public interface Occurrence_VerService {
	
	Occurrence_Ver save(Occurrence_Ver model);
	List<Occurrence_Ver> findAll();
	Occurrence_Ver getById(long occs_rk, Timestamp valid_from_date);
	
}
