package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import com.datagear.model.Customer_Udf_Date_Val;

public interface Customer_Udf_Date_ValService {
	
	Customer_Udf_Date_Val save(Customer_Udf_Date_Val model);
	List<Customer_Udf_Date_Val> findAll();
	List<Customer_Udf_Date_Val> getById(long cust_rk, Timestamp valid_From_Date);
	List<Customer_Udf_Date_Val> saveList(List<Customer_Udf_Date_Val> date_vals);
}
