package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Case_Udf_DefRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Case_Udf_Def;

@Service
public class Case_Udf_DefServiceImpl implements Case_Udf_DefService {

	@Autowired
	private Case_Udf_DefRepository repo;
	
	@Override
	@Transactional
	public Case_Udf_Def save(Case_Udf_Def model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_Udf_Def> findAll() {
		return repo.findAll();
	}

	@Override
	public Case_Udf_Def findFirstByUdf_Name(String udf_name) {
		return repo.findFirstByIdUdfName(udf_name);
	}

	@Override
	public List<Case_Udf_Def> findByUdfNameList(List<String> names) {
		return this.repo.findByIdUdfNameIn(names);
	}

}
