package com.datagear.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.datagear.model.AmlAlert;

public interface AmlAlertService {
	
	Page<AmlAlert> findAll(int x, int y);
	List<AmlAlert> findAll();
	List<AmlAlert> findByEntityNum(String entity_num);

}
