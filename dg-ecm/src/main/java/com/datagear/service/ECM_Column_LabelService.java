package com.datagear.service;

import java.util.List;

import com.datagear.model.ECM_Column_Label;

public interface ECM_Column_LabelService {
	
	ECM_Column_Label save(ECM_Column_Label model);
	List<ECM_Column_Label> findAll();
	
}
