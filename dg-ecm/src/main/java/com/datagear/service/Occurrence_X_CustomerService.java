package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_X_Customer;

public interface Occurrence_X_CustomerService {
	
	Occurrence_X_Customer save(Occurrence_X_Customer model);
	List<Occurrence_X_Customer> findAll();
	boolean isExists(long occsRk, long custRk);
	List<Occurrence_X_Customer> getByCustRk(long cust_rk);
	
}
