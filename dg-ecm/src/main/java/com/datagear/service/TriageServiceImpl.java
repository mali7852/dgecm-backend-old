package com.datagear.service;

import java.util.List;

import com.datagear.repository.Triage_TableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.datagear.model.Triage_Table;

@Service
public class TriageServiceImpl implements TriageService {

	@Autowired
	private Triage_TableRepository repo;

	@Override
	public Page<Triage_Table> findAll(int pageNumber, int pageSize) {
		return repo.findAll(PageRequest.of(pageNumber, pageSize));
	}

	@Override
	public List<Triage_Table> findAll() {
		return repo.findAll();
	}

	
}
