package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Efile_ConfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Efile_Conf;

@Service
public class Efile_ConfServiceImpl implements Efile_ConfService {

	@Autowired
	private Efile_ConfRepository repo;
	
	@Override
	@Transactional
	public Efile_Conf save(Efile_Conf model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Efile_Conf> findAll() {
		return repo.findAll();
	}

}
