package com.datagear.service;

import java.util.List;

import com.datagear.model.Case_Search_Fltr_Fld;

public interface Case_Search_Fltr_FldService {
	
	Case_Search_Fltr_Fld save(Case_Search_Fltr_Fld model);
	List<Case_Search_Fltr_Fld> findAll();
	
}
