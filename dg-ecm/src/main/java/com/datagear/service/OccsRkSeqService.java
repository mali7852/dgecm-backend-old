package com.datagear.service;

import java.util.List;

import com.datagear.model.OccsRkSeq;

public interface OccsRkSeqService {
	
	OccsRkSeq save(OccsRkSeq model);
	List<OccsRkSeq> findAll();
	
}
