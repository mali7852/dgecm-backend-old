package com.datagear.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.datagear.customModel.OccurrenceDetails;
import com.datagear.model.*;
import org.springframework.stereotype.Service;

@Service
public class OccurrenceDetailsServiceImpl implements EntityDetailsService {

	private OccurrenceDetails occsDetails = new OccurrenceDetails();
	
	@Override
	public OccurrenceDetails getEntityDetails() {
		return this.occsDetails;
	}

	@Override
	public void setEntityHeader(Object occs_live) {
		this.occsDetails.setEntityHeader((Occurrence_Live)occs_live);
		
	}

	@Override
	public void setEntityData(List<?> char_vals, List<?> date_vals, List<?> lg_vals) {
		Map<String, String> values = new HashMap<>();

		Map<String, String> char_map = char_vals.parallelStream()
				.collect(Collectors.toMap(ch -> ((Occurrence_Udf_Char_Val) ch).getId().getUdfName(), ch -> ((Occurrence_Udf_Char_Val) ch).getUdfVal(),
						(existing, replacement) -> existing));

		Map<String, String> date_map = date_vals.parallelStream()
				.collect(Collectors.toMap(dt -> ((Occurrence_Udf_Date_Val) dt).getId().getUdfName(), dt -> ((Occurrence_Udf_Date_Val) dt).getUdfVal().toString(),
						(existing, replacement) -> existing));

		Map<String, String> lg_map = lg_vals.parallelStream()
				.collect(Collectors.toMap(lg -> ((Occurrence_Udf_Lgchr_Val) lg).getId().getUdfName(), lg -> ((Occurrence_Udf_Lgchr_Val) lg).getUdfVal(),
						(existing, replacement) -> existing));

		values.putAll(char_map);
		values.putAll(date_map);
		values.putAll(lg_map);
		
		this.occsDetails.setEntityData(values);
	}

	@Override
	public void setEditFlag(String editFlag) {
		this.occsDetails.setEditFlag(editFlag);
	}

	@Override
	public void setLockUserName(String userName) {
//		this.occsDetails.setLockUserName(userName);
		
	}

	@Override
	public void setEntityConfig(Object occs_conf) {
		this.occsDetails.setEntityConfig((Occurrence_Conf)occs_conf);
		
	}

	@Override
	public void setEntityFields(String fields) {
		this.occsDetails.setEntityFields(fields);
		
	}

}
