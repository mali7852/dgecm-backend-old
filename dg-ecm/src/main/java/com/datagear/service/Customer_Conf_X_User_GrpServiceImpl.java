package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Customer_Conf_X_User_GrpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Customer_Conf_X_User_Grp;

@Service
public class Customer_Conf_X_User_GrpServiceImpl implements Customer_Conf_X_User_GrpService {

	@Autowired
	private Customer_Conf_X_User_GrpRepository repo;
	
	@Override
	@Transactional
	public Customer_Conf_X_User_Grp save(Customer_Conf_X_User_Grp model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Customer_Conf_X_User_Grp> findAll() {
		return repo.findAll();
	}

	@Override
	public Customer_Conf_X_User_Grp getUserGrpName(long cust_Conf_Seq_No) {
		return repo.findFirstByIdCustConfSeqNo(cust_Conf_Seq_No);
	}

}
