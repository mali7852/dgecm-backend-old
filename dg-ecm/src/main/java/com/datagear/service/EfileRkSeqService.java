package com.datagear.service;

import java.util.List;

import com.datagear.model.EfileRkSeq;

public interface EfileRkSeqService {
	
	EfileRkSeq save(EfileRkSeq model);
	List<EfileRkSeq> findAll();
	
}
