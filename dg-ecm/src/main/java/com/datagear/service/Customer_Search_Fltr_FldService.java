package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_Search_Fltr_Fld;

public interface Customer_Search_Fltr_FldService {
	
	Customer_Search_Fltr_Fld save(Customer_Search_Fltr_Fld model);
	List<Customer_Search_Fltr_Fld> findAll();
	
}
