package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_X_User_Grp;

public interface Efile_X_User_GrpService {
	
	Efile_X_User_Grp save(Efile_X_User_Grp model);
	List<Efile_X_User_Grp> findAll();
	
}
