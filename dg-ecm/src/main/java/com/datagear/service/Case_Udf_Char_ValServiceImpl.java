package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import com.datagear.model.Case_Udf_Char_Val;
import com.datagear.repository.Case_Udf_Char_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.customModel.HistoryVerDiff;

@Service
public class Case_Udf_Char_ValServiceImpl implements Case_Udf_Char_ValService {

	@Autowired
	private Case_Udf_Char_ValRepository repo;
	
	@Override
	@Transactional
	public Case_Udf_Char_Val save(Case_Udf_Char_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Case_Udf_Char_Val> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Case_Udf_Char_Val> getById(long case_rk, Timestamp valid_from_date) {
		return repo.findByIdCaseRkAndIdValidFromDate(case_rk, valid_from_date);
	}

	@Override
	public void saveList(List<Case_Udf_Char_Val> new_chars) {
		repo.saveAll(new_chars);
		
	}

	@Override
	public List<HistoryVerDiff> getHistoryVerDiff(long case_rk, Timestamp curDate, Timestamp oldDate) {
		return repo.getHistoryVerDiff(case_rk, curDate, oldDate);
	}

	@Override
	public List<Map<String, String>> getSanctionAlertList(long caseRk, Timestamp validFromDate) {
		return this.repo.getSanctionAlertList(caseRk, validFromDate);
	}

	@Override
	public List<Map<String, String>> getSanctionRelAlertList(long caseRk, Timestamp validFromDate) {
		return this.repo.getSanctionRelAlertList(caseRk, validFromDate);
	}

}
