package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.ECM_Entity_LockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.ECM_Entity_Lock;

@Service
public class ECM_Entity_LockServiceImpl implements ECM_Entity_LockService {

	@Autowired
	private ECM_Entity_LockRepository repo;
	
	@Override
	@Transactional
	public ECM_Entity_Lock save(ECM_Entity_Lock model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<ECM_Entity_Lock> findAll() {
		return repo.findAll();
	}

	@Override
	public ECM_Entity_Lock getById(long entityRk, ENTITY_TYPE entityName) {
		return this.repo.findFirstByIdEntityRkAndIdEntityName(entityRk, entityName.getName());
	}

	@Override
	public boolean isEntityLocked(long entityRk, ENTITY_TYPE entityName) {
		return this.getById(entityRk, entityName) != null;
	}

	@Override
	public void unlockEntity(long entityRk, ENTITY_TYPE entityName) {
		this.repo.deleteByIdEntityRkAndIdEntityName(entityRk, entityName.getName());
	}

	@Override
	public List<ECM_Entity_Lock> getByEntityRkList(List<Long> case_rks, String name) {
		return this.repo.findByIdEntityRkInAndIdEntityName(case_rks, name);
	}

}
