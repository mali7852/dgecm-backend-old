package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Customer_Udf_Date_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Customer_Udf_Date_Val;

@Service
public class Customer_Udf_Date_ValServiceImpl implements Customer_Udf_Date_ValService {

	@Autowired
	private Customer_Udf_Date_ValRepository repo;
	
	@Override
	@Transactional
	public Customer_Udf_Date_Val save(Customer_Udf_Date_Val model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Customer_Udf_Date_Val> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Customer_Udf_Date_Val> getById(long cust_rk, Timestamp valid_From_Date) {
		return repo.findByIdCustRkAndIdValidFromDate(cust_rk, valid_From_Date);
	}

	@Override
	public List<Customer_Udf_Date_Val> saveList(List<Customer_Udf_Date_Val> date_vals) {
		return this.repo.saveAll(date_vals);
	}

}
