package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_Conf_X_User_Grp;

public interface Customer_Conf_X_User_GrpService {
	
	Customer_Conf_X_User_Grp save(Customer_Conf_X_User_Grp model);
	List<Customer_Conf_X_User_Grp> findAll();
	Customer_Conf_X_User_Grp getUserGrpName(long cust_Conf_Seq_No);
	
}
