package com.datagear.service;

import java.util.List;

import com.datagear.model.Occurrence_Search_Crtria_Fld;

public interface Occurrence_Search_Crtria_FldService {
	
	Occurrence_Search_Crtria_Fld save(Occurrence_Search_Crtria_Fld model);
	List<Occurrence_Search_Crtria_Fld> findAll();
	
}
