package com.datagear.service;

import java.util.List;

import com.datagear.model.Efile_Search_Filter_Field;

public interface Efile_Search_Filter_FieldService {
	
	Efile_Search_Filter_Field save(Efile_Search_Filter_Field model);
	List<Efile_Search_Filter_Field> findAll();
	
}
