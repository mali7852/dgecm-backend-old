package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import com.datagear.model.Case_Udf_Lgchr_Val;

public interface Case_Udf_Lgchr_ValService {
	
	Case_Udf_Lgchr_Val save(Case_Udf_Lgchr_Val model);
	List<Case_Udf_Lgchr_Val> findAll();
    List<Case_Udf_Lgchr_Val> getById(long case_rk, Timestamp valid_from_date);
	void saveList(List<Case_Udf_Lgchr_Val> new_lgchrs);

}
