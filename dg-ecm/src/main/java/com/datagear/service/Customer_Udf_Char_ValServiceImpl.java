package com.datagear.service;

import com.datagear.customModel.HistoryVerDiff;
import com.datagear.model.Customer_Udf_Char_Val;
import com.datagear.repository.Customer_Udf_Char_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
public class Customer_Udf_Char_ValServiceImpl implements Customer_Udf_Char_ValService {

    @Autowired
    private Customer_Udf_Char_ValRepository repo;

    @Override
    @Transactional
    public Customer_Udf_Char_Val save(Customer_Udf_Char_Val model) {
        repo.save(model);
        return model;
    }

    @Override
    public List<Customer_Udf_Char_Val> findAll() {
        return repo.findAll();
    }

    @Override
    public List<Customer_Udf_Char_Val> getById(long cust_rk, Timestamp valid_From_Date) {
        return repo.findByIdCustRkAndIdValidFromDate(cust_rk, valid_From_Date);
    }

    @Override
    public List<Customer_Udf_Char_Val> saveList(List<Customer_Udf_Char_Val> char_vals) {
        return this.repo.saveAll(char_vals);
    }

    @Override
    public List<HistoryVerDiff> getHistoryVerDiff(long custRk, Timestamp curVerDate, Timestamp oldVerDate) {
        return repo.getHistoryVerDiff(custRk, curVerDate, oldVerDate);
    }

}
