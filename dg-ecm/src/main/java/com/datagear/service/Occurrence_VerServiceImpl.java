package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Occurrence_VerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Occurrence_Ver;

@Service
public class Occurrence_VerServiceImpl implements Occurrence_VerService {

	@Autowired
	private Occurrence_VerRepository repo;
	
	@Override
	@Transactional
	public Occurrence_Ver save(Occurrence_Ver model) {
		return repo.save(model);
	}

	@Override
	public List<Occurrence_Ver> findAll() {
		return repo.findAll();
	}

	@Override
	public Occurrence_Ver getById(long occs_rk, Timestamp valid_from_date) {
		return this.repo.findFirstByIdOccsRkAndIdValidFromDate(occs_rk, valid_from_date);
	}

}
