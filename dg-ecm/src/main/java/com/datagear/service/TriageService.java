package com.datagear.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.datagear.model.Triage_Table;

public interface TriageService {
	Page<Triage_Table> findAll(int x, int y);
	List<Triage_Table> findAll();
}
