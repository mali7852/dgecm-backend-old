package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.ECM_LocaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.ECM_Locale;

@Service
public class ECM_LocaleServiceImpl implements ECM_LocaleService {

	@Autowired
	private ECM_LocaleRepository repo;
	
	@Override
	@Transactional
	public ECM_Locale save(ECM_Locale model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<ECM_Locale> findAll() {
		return repo.findAll();
	}

}
