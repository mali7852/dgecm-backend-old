package com.datagear.service;

import java.util.List;

import com.datagear.model.Case_Search_Crtria_Fld;

public interface Case_Search_Crtria_FldService {
	
	Case_Search_Crtria_Fld save(Case_Search_Crtria_Fld model);
	List<Case_Search_Crtria_Fld> findAll();
	
}
