package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import com.datagear.model.ECM_Event;

public interface ECM_EventService {

	List<ECM_Event> findAll();

	boolean loadCase(long entity_rk, String event_desc, String user_id);

	boolean loadCustomer(long entity_rk, String event_desc, String user_id);

	boolean loadEFile(long entity_rk, String event_desc, String user_id);

	boolean loadOccurrence(long entity_rk, String event_desc, String user_id);

	boolean loadComment(long entity_rk, String event_desc, String user_id, long parent_rk, String parent_type);

	boolean loadAttachment(long entity_rk, String event_desc, String user_id, long parent_rk, String parent_type);

	boolean updateCase(long entity_rk, String event_desc, Timestamp date, String user_id);

	boolean updateCustomer(long entity_rk, String event_desc, Timestamp date, String user_id);

	boolean updateEFile(long entity_rk, String event_desc, String user_id);

	boolean updateOccurrence(long entity_rk, String event_desc, Timestamp date, String user_id);

	boolean updateComment(long entity_rk, String event_desc, Timestamp date, String user_id, long parent_rk, String parent_type);

	boolean updateAttachment(long entity_rk, String event_desc, Timestamp date, String user_id, long parent_rk, String parent_type);

	boolean updateCaseWF(long entity_rk, String event_desc, Timestamp date, String user_id);

	boolean removeCase(long entity_rk, String event_desc, String user_id);

	boolean removeCustomer(long entity_rk, String event_desc, String user_id);

	boolean removeEFile(long entity_rk, String event_desc, String user_id);

	boolean removeComment(long entity_rk, String event_desc, String user_id, long parent_rk, String parent_type);

	boolean removeAttachment(long entity_rk, String event_desc, String user_id, long parent_rk, String parent_type);

	boolean removeOccurrence(long entity_rk, String event_desc, String user_id);

	boolean removeLink(long entity_rk, String entity_type, String event_desc, String user_id, long parent_rk, String parent_type);

	boolean linkCaseToCustomer(long entity_rk, String event_desc, String user_id, long parent_rk);

	boolean linkCustomerToCustomer(long entity_rk, String event_desc, String user_id, long parent_rk);

	boolean linkOccurrenceToCustomer(long entity_rk, String event_desc, String user_id, long parent_rk);

	boolean linkCaseToCase(long entity_rk, String event_desc, String user_id, long parent_rk);

	boolean saveCase(long entity_rk, String event_desc, String user_id);

	boolean saveCustomer(long entity_rk, String event_desc, String user_id);

	boolean saveOccurrence(long entity_rk, String event_desc, String user_id);

	boolean saveEFile(long entity_rk, String event_desc, String user_id);

	boolean lockCase(long entity_rk, String event_desc, String user_id);

	boolean lockCustomer(long entity_rk, String event_desc, String user_id);

	boolean lockOccurrence(long entity_rk, String event_desc, String user_id);

	boolean lockEFile(long entity_rk, String event_desc, String user_id);

	boolean unlockCase(long entity_rk, String event_desc, String user_id);

	boolean unlockCustomer(long entity_rk, String event_desc, String user_id);

	boolean unlockOccurrence(long entity_rk, String event_desc, String user_id);

	boolean unlockEFile(long entity_rk, String event_desc, String user_id);

	boolean activeCaseWorkFlow(long entity_rk, String event_desc, String user_id);

	boolean terminateCaseWorkFlow(long entity_rk, String event_desc, String user_id);

	boolean reassignCase(long entity_rk, String event_desc, String user_id);

	List<ECM_Event> getEntityEvents(long object_rk, String object_name);

}
