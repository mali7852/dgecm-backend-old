package com.datagear.service;

import com.datagear.model.Case_Udf_Lgchr_Val;
import com.datagear.repository.Case_Udf_Lgchr_ValRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
public class Case_Udf_Lgchr_ValServiceImpl implements Case_Udf_Lgchr_ValService {

    @Autowired
    private Case_Udf_Lgchr_ValRepository repo;

    @Override
    @Transactional
    public Case_Udf_Lgchr_Val save(Case_Udf_Lgchr_Val model) {
        repo.save(model);
        return model;
    }

    @Override
    public List<Case_Udf_Lgchr_Val> findAll() {
        return repo.findAll();
    }

    @Override
    public List<Case_Udf_Lgchr_Val> getById(long case_rk, Timestamp valid_from_date) {
        return this.repo.findByIdCaseRkAndIdValidFromDate(case_rk, valid_from_date);
    }

    @Override
    public void saveList(List<Case_Udf_Lgchr_Val> new_lgchrs) {
        this.repo.saveAll(new_lgchrs);
    }

}
