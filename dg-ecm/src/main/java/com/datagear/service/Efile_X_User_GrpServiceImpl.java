package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Efile_X_User_GrpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Efile_X_User_Grp;

@Service
public class Efile_X_User_GrpServiceImpl implements Efile_X_User_GrpService {

	@Autowired
	private Efile_X_User_GrpRepository repo;
	
	@Override
	@Transactional
	public Efile_X_User_Grp save(Efile_X_User_Grp model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Efile_X_User_Grp> findAll() {
		return repo.findAll();
	}

}
