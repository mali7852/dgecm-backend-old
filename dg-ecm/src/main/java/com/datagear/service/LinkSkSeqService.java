package com.datagear.service;

import java.util.List;

import com.datagear.model.LinkSkSeq;

public interface LinkSkSeqService {
	
	LinkSkSeq save(LinkSkSeq model);
	List<LinkSkSeq> findAll();
	
}
