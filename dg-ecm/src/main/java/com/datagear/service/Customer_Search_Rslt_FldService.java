package com.datagear.service;

import java.util.List;

import com.datagear.model.Customer_Search_Rslt_Fld;

public interface Customer_Search_Rslt_FldService {
	
	Customer_Search_Rslt_Fld save(Customer_Search_Rslt_Fld model);
	List<Customer_Search_Rslt_Fld> findAll();
	
}
