package com.datagear.service;

import java.sql.Timestamp;
import java.util.List;

import com.datagear.model.Case_Udf_Date_Val;

public interface Case_Udf_Date_ValService {
	
	Case_Udf_Date_Val save(Case_Udf_Date_Val model);
	List<Case_Udf_Date_Val> findAll();
	List<Case_Udf_Date_Val> getById(long case_rk, Timestamp valid_From_Date);
	void saveList(List<Case_Udf_Date_Val> new_dates);
	
}
