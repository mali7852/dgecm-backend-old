package com.datagear.service;

import java.util.List;

import com.datagear.model.Case_Conf;

public interface Case_ConfService {
	
	Case_Conf save(Case_Conf model);
	List<Case_Conf> findAll();
	Case_Conf getByCaseTypeCd(String case_type_cd);
	Case_Conf getByUiDefFileName(String ui_Def_File_Name);
	
}
