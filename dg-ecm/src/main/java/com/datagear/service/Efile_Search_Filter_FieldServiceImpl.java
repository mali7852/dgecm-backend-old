package com.datagear.service;

import java.util.List;

import javax.transaction.Transactional;

import com.datagear.repository.Efile_Search_Filter_FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datagear.model.Efile_Search_Filter_Field;

@Service
public class Efile_Search_Filter_FieldServiceImpl implements Efile_Search_Filter_FieldService {

	@Autowired
	private Efile_Search_Filter_FieldRepository repo;
	
	@Override
	@Transactional
	public Efile_Search_Filter_Field save(Efile_Search_Filter_Field model) {
		repo.save(model);
		return model;
	}

	@Override
	public List<Efile_Search_Filter_Field> findAll() {
		return repo.findAll();
	}

}
