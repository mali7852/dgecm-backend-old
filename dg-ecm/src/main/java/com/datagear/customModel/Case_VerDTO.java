package com.datagear.customModel;

import com.datagear.model.Case_Udf_Char_Val;
import com.datagear.model.Case_Ver;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;
import java.util.Set;

public class Case_VerDTO {
    private final Case_Ver case_ver;

    private String lockedBy;

    public Case_VerDTO(Case_Ver case_ver) {
        this.case_ver = case_ver;
    }

    @JsonGetter("CASE_ID")
    public String getCaseId() {
        return this.case_ver.getCaseId();
    }

    @JsonGetter("CASE_TYPE_CD")
    public String getCaseTypeCd() {
        return this.case_ver.getCaseTypeCd();
    }

    @JsonGetter("COL1")
    public String getCol1() {
        return this.case_ver.getCol1();
    }

    @JsonGetter("COL2")
    public String getCol2() {
        return this.case_ver.getCol2();
    }

    @JsonGetter("COL3")
    public String getCol3() {
        return this.case_ver.getCol3();
    }

    @JsonGetter("CREATE_DTTM")
    public Timestamp getCreateDate() {
        return this.case_ver.getCreateDate();
    }

    @JsonGetter("VALID_FROM_DTTM")
    public Timestamp getValidFromDate() {
        return this.case_ver.getId().getValidFromDate();
    }

    @JsonGetter("CASE_STATUS_CD")
    public String getCaseStatCd() {
        return this.case_ver.getCaseStatCd();
    }

    @JsonGetter("COL4")
    public String getCol4() {
        return this.case_ver.getCol4();
    }

    @JsonGetter("COL5")
    public String getCol5() {
        return this.case_ver.getCol5();
    }

    @JsonGetter("LOCKED_BY")
    public String getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(String lockedBy) {
        this.lockedBy = lockedBy;
    }

    @JsonGetter("CASE_RK")
    public long getCaseRk() {
        return this.case_ver.getId().getCaseRk();
    }
}
