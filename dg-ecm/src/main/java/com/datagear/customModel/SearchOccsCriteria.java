package com.datagear.customModel;

import com.fasterxml.jackson.annotation.JsonSetter;

public class SearchOccsCriteria {
    private String caseId;
    private String occsId;
    private String occsTypeCd;
    private String createdDateFrom;
    private String createdDateTo;
    private String rowNo;
    private String pageNo;
    private String sortActive;
    private String sortDirection;

    public SearchOccsCriteria() {
    }

    public String getOccsId() {
        return occsId;
    }

    @JsonSetter("OCCS_ID")
    public void setOccsId(String occsId) {
        this.occsId = occsId;
    }

    public String getOccsTypeCd() {
        return occsTypeCd;
    }

    @JsonSetter("OCCS_TYPE_CD")
    public void setOccsTypeCd(String occsTypeCd) {
        this.occsTypeCd = occsTypeCd;
    }

    public String getSortActive() {
        return sortActive;
    }

    @JsonSetter("SORT_ACTIVE")
    public void setSortActive(String sortActive) {
        this.sortActive = sortActive;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    @JsonSetter("SORT_DIRECTION")
    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getCaseId() {
        return caseId;
    }

    @JsonSetter("CASE_ID")
    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getCreatedDateFrom() {
        return createdDateFrom;
    }

    @JsonSetter("createdDateFrom")
    public void setCreatedDateFrom(String createdDateFrom) {
        this.createdDateFrom = createdDateFrom;
    }

    public String getCreatedDateTo() {
        return createdDateTo;
    }

    @JsonSetter("createdDateTo")
    public void setCreatedDateTo(String createdDateTo) {
        this.createdDateTo = createdDateTo;
    }

    public String getPageNo() {
        return pageNo;
    }

    @JsonSetter("CURRENT_PAGE")
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    public String getRowNo() {
        return rowNo;
    }

    @JsonSetter("NUMBERS_OF_ROWS")
    public void setRowNo(String rowNo) {
        this.rowNo = rowNo;
    }

}