package com.datagear.customModel;

import java.util.List;

import com.datagear.model.Occurrence_Live;
import com.fasterxml.jackson.annotation.JsonGetter;

public class SearchOccsResult {
	private final PagingDetails paging;
	private final List<Occurrence_Live> data;

	public SearchOccsResult(PagingDetails pd, List<Occurrence_Live> data) {
		this.paging = pd;
		this.data = data;
	}

	@JsonGetter("PAGING")
	public PagingDetails getPaging() {
		return paging;
	}

	@JsonGetter("DATA")
	public List<Occurrence_Live> getData() {
		return data;
	}
}
