package com.datagear.customModel;

import java.util.Map;

import com.datagear.model.Customer_Conf;
import com.datagear.model.Customer_Live;

public class CustomerDetails {
	private Customer_Live entityHeader;
	private Map<String, String> entityData;
	private Customer_Conf entityConfig;
	private String editFlag;
//	private String lockUserName; /* Display name or user name if first not exists */
	private String entityFields;

	public CustomerDetails() {

	}

	public String getEditFlag() {
		return editFlag;
	}

	public Customer_Conf getEntityConfig() {
		return entityConfig;
	}

	public Map<String, String> getEntityData() {
		return entityData;
	}

	public String getEntityFields() {
		return entityFields;
	}

	public Customer_Live getEntityHeader() {
		return entityHeader;
	}

//	public String getLockUserName() {
//		return lockUserName;
//	}

	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	}

	public void setEntityConfig(Customer_Conf entityConfig) {
		this.entityConfig = entityConfig;
	}

	public void setEntityData(Map<String, String> entityData) {
		this.entityData = entityData;
	}

	public void setEntityFields(String entityFields) {
		this.entityFields = entityFields;
	}

	public void setEntityHeader(Customer_Live entityHeader) {
		this.entityHeader = entityHeader;
	}

//	public void setLockUserName(String lockUserName) {
//		this.lockUserName = lockUserName;
//	}
}
