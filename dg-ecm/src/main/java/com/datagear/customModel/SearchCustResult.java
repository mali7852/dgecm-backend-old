package com.datagear.customModel;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonGetter;

public class SearchCustResult {
	
	private final PagingDetails paging;
	private final List<Customer_VerDTO> data;
	
	public SearchCustResult(PagingDetails pd, List<Customer_VerDTO> data) {
		this.paging = pd;
		this.data = data;
	}

	@JsonGetter("PAGING")
	public PagingDetails getPaging() {
		return paging;
	}

	@JsonGetter("DATA")
	public List<Customer_VerDTO> getData() {
		return data;
	}
	
}
