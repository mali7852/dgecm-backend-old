package com.datagear.customModel;

import java.util.Map;

import com.datagear.model.Case_Conf;
import com.datagear.model.Case_Live;

public class CaseDetails {

	private Case_Live entityHeader;
	private Map<String, String> entityData;
	private Case_Conf entityConfig;
	private String editFlag;
	private String lockUserName; /* Display name or user name if first not exists */
	private String entityFields;

	public CaseDetails() {

	}

//	public CaseDetails(Case_Live case_live, Map<String, String> map, String editFlag) {
//		this.entityHeader = case_live;
//		this.entityData = map;
//		this.editFlag = editFlag;
//	}

	public String getEditFlag() {
		return editFlag;
	}

	public Case_Conf getEntityConfig() {
		return entityConfig;
	}

	public Map<String, String> getEntityData() {
		return entityData;
	}

	public String getEntityFields() {
		return entityFields;
	}

	public Case_Live getEntityHeader() {
		return entityHeader;
	}

	public String getLockUserName() {
		return lockUserName;
	}

	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	}

	public void setEntityConfig(Case_Conf entityConfig) {
		this.entityConfig = entityConfig;
	}

	public void setEntityData(Map<String, String> entityData) {
		this.entityData = entityData;
	}

	public void setEntityFields(String entityFields) {
		this.entityFields = entityFields;
	}

	public void setEntityHeader(Case_Live entityHeader) {
		this.entityHeader = entityHeader;
	}

	public void setLockUserName(String lockUserName) {
		this.lockUserName = lockUserName;
	}

}
