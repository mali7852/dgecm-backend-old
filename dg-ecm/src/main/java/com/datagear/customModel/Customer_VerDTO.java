package com.datagear.customModel;

import com.datagear.model.Customer_Ver;
import com.fasterxml.jackson.annotation.JsonGetter;

import java.sql.Timestamp;

public class Customer_VerDTO {
    private final Customer_Ver cust_ver;

    public Customer_VerDTO(Customer_Ver cust_ver) {
        this.cust_ver = cust_ver;
    }

    @JsonGetter("CUST_ID")
    public String getCustId() {
        return this.cust_ver.getCustId();
    }

    @JsonGetter("CUST_TYPE_CD")
    public String getCustTypeCd() {
        return this.cust_ver.getCustTypeCd();
    }

    @JsonGetter("CUST_CATEGORY_CD")
    public String getCustCtgryCd() {
        return this.cust_ver.getCustCtgryCd();
    }

    @JsonGetter("CUST_FULL_NAME")
    public String getCustFullName() {
        return this.cust_ver.getCustFullName();
    }

    @JsonGetter("CREATE_DTTM")
    public Timestamp getCreateDate() {
        return this.cust_ver.getCreateDate();
    }

    @JsonGetter("CREATE_USER_ID")
    public String getCreateUserId() {
        return this.cust_ver.getCreateUserId();
    }

    @JsonGetter("CUST_RK")
    public long getCustRk() {
        return this.cust_ver.getId().getCustRk();
    }

    @JsonGetter("VALID_FROM_DTTM")
    public Timestamp getValidFromDate() {
        return this.cust_ver.getId().getValidFromDate();
    }

}
