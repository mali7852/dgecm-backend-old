package com.datagear.customModel;

import java.util.Map;

import com.datagear.model.Occurrence_Conf;
import com.datagear.model.Occurrence_Live;

public class OccurrenceDetails {
	private Occurrence_Live entityHeader;
	private Map<String, String> entityData;
	private Occurrence_Conf entityConfig;
	private String editFlag;
//	private String lockUserName; /* Display name or user name if first not exists */
	private String entityFields;

	public OccurrenceDetails() {

	}

	public String getEditFlag() {
		return editFlag;
	}

	public Occurrence_Conf getEntityConfig() {
		return entityConfig;
	}

	public Map<String, String> getEntityData() {
		return entityData;
	}

	public String getEntityFields() {
		return entityFields;
	}

	public Occurrence_Live getEntityHeader() {
		return entityHeader;
	}

//	public String getLockUserName() {
//		return lockUserName;
//	}

	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	}

	public void setEntityConfig(Occurrence_Conf entityConfig) {
		this.entityConfig = entityConfig;
	}

	public void setEntityData(Map<String, String> entityData) {
		this.entityData = entityData;
	}

	public void setEntityFields(String entityFields) {
		this.entityFields = entityFields;
	}

	public void setEntityHeader(Occurrence_Live entityHeader) {
		this.entityHeader = entityHeader;
	}

//	public void setLockUserName(String lockUserName) {
//		this.lockUserName = lockUserName;
//	}
}
