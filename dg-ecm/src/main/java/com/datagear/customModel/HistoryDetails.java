package com.datagear.customModel;

import java.util.List;

import com.datagear.model.ECM_Event;

public class HistoryDetails {

	private final ECM_Event event;
	private final List<HistoryVerDiff> verDiff;

	public HistoryDetails(ECM_Event events, List<HistoryVerDiff> verDiff) {
		this.event = events;
		this.verDiff = verDiff;
	}

	public ECM_Event getEvent() {
		return event;
	}

	public List<HistoryVerDiff> getVerDiff() {
		return verDiff;
	}
}
