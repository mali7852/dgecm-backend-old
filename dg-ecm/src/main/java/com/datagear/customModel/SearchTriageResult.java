package com.datagear.customModel;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonGetter;

public class SearchTriageResult {
	
	private final PagingDetails paging;
	private final List<Map<String, String>> data;
	
	public SearchTriageResult(PagingDetails pd, List<Map<String, String>> d) {
		this.paging = pd;
		this.data = d;
	}

	@JsonGetter("PAGING")
	public PagingDetails getPaging() {
		return paging;
	}

	@JsonGetter("DATA")
	public List<Map<String, String>> getData() {
		return data;
	}

}
