package com.datagear.customModel;

public class PagingDetails {
	private final int currentPageNo;
	private final int pageSize;
	private final long totalElements;
	private final int totalPages;
	
	public PagingDetails(int currentPageNo, int pageSize, long totalElements, int totalPages) {
		super();
		this.currentPageNo = currentPageNo;
		this.pageSize = pageSize;
		this.totalElements = totalElements;
		this.totalPages = totalPages;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public int getTotalPages() {
		return totalPages;
	}
	
}