package com.datagear.customModel;

public interface CaseLiveStatus {
	String getCaseId();
	String getCaseStatCd();
	String getCaseTypeCd();
}
