package com.datagear.customModel;

public interface HistoryVerDiff {
	
	String getField();
	String getOldVal();
	String getCurVal();
	
}
