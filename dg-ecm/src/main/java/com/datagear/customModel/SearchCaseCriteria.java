package com.datagear.customModel;

import com.fasterxml.jackson.annotation.JsonSetter;

public class SearchCaseCriteria {

    private String caseId;

    private String fullNameAr;

    private String fullNameEn;

    private String caseStatusCd;

    private String caseTypeCd;

    private String caseCtgryCd;

    private String closedDateFrom;

    private String closedDateTo;

    private String createdDateFrom;

    private String createdDateTo;

    private String lastModifiedDateFrom;

    private String lastModifiedDateTo;

    private String openedDateFrom;

    private String openedDateTo;

    private String priority;

    private String col1;

    private String col2;

    private String col3;

    private String col4;

    private String col5;

    private String rowNo;

    private String pageNo;

    private String sortActive;

    private String sortDirection;

    public SearchCaseCriteria() {
    }

    public String getPriority() {
        return priority;
    }

    @JsonSetter("PRIORITY")
    public void setPriority(String priority) {
        this.priority = priority;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    public String getSortActive() {
        return sortActive;
    }

    @JsonSetter("SORT_ACTIVE")
    public void setSortActive(String sortActive) {
        this.sortActive = sortActive;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    @JsonSetter("SORT_DIRECTION")
    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    public String getCaseCtgryCd() {
        return caseCtgryCd;
    }

    @JsonSetter("CASE_CATEGORY_CD")
    public void setCaseCtgryCd(String caseCtgryCd) {
        this.caseCtgryCd = caseCtgryCd;
    }

    public String getCaseId() {
        return caseId;
    }

    @JsonSetter("CASE_ID")
    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getCaseStatusCd() {
        return caseStatusCd;
    }

    @JsonSetter("CASE_STATUS_CD")
    public void setCaseStatusCd(String caseStatusCd) {
        this.caseStatusCd = caseStatusCd;
    }

    public String getCaseTypeCd() {
        return caseTypeCd;
    }

    @JsonSetter("CASE_TYPE_CD")
    public void setCaseTypeCd(String caseTypeCd) {
        this.caseTypeCd = caseTypeCd;
    }

    public String getClosedDateFrom() {
        return closedDateFrom;
    }

    @JsonSetter("closedDateFrom")
    public void setClosedDateFrom(String closedDateFrom) {
        this.closedDateFrom = closedDateFrom;
    }

    public String getClosedDateTo() {
        return closedDateTo;
    }

    @JsonSetter("closedDateTo")
    public void setClosedDateTo(String closedDateTo) {
        this.closedDateTo = closedDateTo;
    }

    public String getCreatedDateFrom() {
        return createdDateFrom;
    }

    @JsonSetter("createdDateFrom")
    public void setCreatedDateFrom(String createdDateFrom) {
        this.createdDateFrom = createdDateFrom;
    }

    public String getCreatedDateTo() {
        return createdDateTo;
    }

    @JsonSetter("createdDateTo")
    public void setCreatedDateTo(String createdDateTo) {
        this.createdDateTo = createdDateTo;
    }

    public String getCol1() {
        return col1;
    }

    @JsonSetter("COL1")
    public void setCol1(String col) {
        this.col1 = col;
    }

    public String getCol2() {
        return col2;
    }

    @JsonSetter("COL2")
    public void setCol2(String col) {
        this.col2 = col;
    }

    public String getCol3() {
        return col3;
    }

    @JsonSetter("COL3")
    public void setCol3(String col) {
        this.col3 = col;
    }

    public String getCol4() {
        return col4;
    }

    @JsonSetter("COL4")
    public void setCol4(String col) {
        this.col4 = col;
    }

    public String getCol5() {
        return col5;
    }

    @JsonSetter("COL5")
    public void setCol5(String col) {
        this.col5 = col;
    }

    public String getLastModifiedDateFrom() {
        return lastModifiedDateFrom;
    }

    @JsonSetter("lastModifiedDateFrom")
    public void setLastModifiedDateFrom(String lastModifiedDateFrom) {
        this.lastModifiedDateFrom = lastModifiedDateFrom;
    }

    public String getLastModifiedDateTo() {
        return lastModifiedDateTo;
    }

    @JsonSetter("lastModifiedDateTo")
    public void setLastModifiedDateTo(String lastModifiedDateTo) {
        this.lastModifiedDateTo = lastModifiedDateTo;
    }

    public String getOpenedDateFrom() {
        return openedDateFrom;
    }

    @JsonSetter("openedDateFrom")
    public void setOpenedDateFrom(String openedDateFrom) {
        this.openedDateFrom = openedDateFrom;
    }

    public String getOpenedDateTo() {
        return openedDateTo;
    }

    @JsonSetter("openedDateTo")
    public void setOpenedDateTo(String openedDateTo) {
        this.openedDateTo = openedDateTo;
    }

    public String getPageNo() {
        return pageNo;
    }

    @JsonSetter("CURRENT_PAGE")
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    public String getRowNo() {
        return rowNo;
    }

    @JsonSetter("NUMBERS_OF_ROWS")
    public void setRowNo(String rowNo) {
        this.rowNo = rowNo;
    }

    @Override
    public String toString() {
        return "SearchCaseCriteria{" +
                "caseId='" + caseId + '\'' +
                ", fullNameAr='" + fullNameAr + '\'' +
                ", fullNameEn='" + fullNameEn + '\'' +
                ", caseStatusCd='" + caseStatusCd + '\'' +
                ", caseTypeCd='" + caseTypeCd + '\'' +
                ", caseCtgryCd='" + caseCtgryCd + '\'' +
                ", closedDateFrom='" + closedDateFrom + '\'' +
                ", closedDateTo='" + closedDateTo + '\'' +
                ", createdDateFrom='" + createdDateFrom + '\'' +
                ", createdDateTo='" + createdDateTo + '\'' +
                ", lastModifiedDateFrom='" + lastModifiedDateFrom + '\'' +
                ", lastModifiedDateTo='" + lastModifiedDateTo + '\'' +
                ", openedDateFrom='" + openedDateFrom + '\'' +
                ", openedDateTo='" + openedDateTo + '\'' +
                ", rowNo='" + rowNo + '\'' +
                ", pageNo='" + pageNo + '\'' +
                ", sortActive='" + sortActive + '\'' +
                ", sortDirection='" + sortDirection + '\'' +
                '}';
    }
}