package com.datagear.customModel;

import com.fasterxml.jackson.annotation.JsonSetter;

public class SearchCustCriteria {

    private String custId;
    private String fullNameAr;
    private String fullNameEn;
    private String custCtgryCd;
    private String custTypeCd;
    private String createdDateFrom;
    private String createdDateTo;
    private String lastModifiedDateFrom;
    private String lastModifiedDateTo;
    private String rowNo;
    private String pageNo;
    private String sortActive;
    private String sortDirection;

    public SearchCustCriteria() {
    }

    public String getSortActive() {
        return sortActive;
    }

    @JsonSetter("SORT_ACTIVE")
    public void setSortActive(String sortActive) {
        this.sortActive = sortActive;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    @JsonSetter("SORT_DIRECTION")
    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getCreatedDateFrom() {
        return createdDateFrom;
    }

    @JsonSetter("createdDateFrom")
    public void setCreatedDateFrom(String createdDateFrom) {
        this.createdDateFrom = createdDateFrom;
    }

    public String getCreatedDateTo() {
        return createdDateTo;
    }

    @JsonSetter("createdDateTo")
    public void setCreatedDateTo(String createdDateTo) {
        this.createdDateTo = createdDateTo;
    }

    public String getCustCtgryCd() {
        return custCtgryCd;
    }

    @JsonSetter("CUST_CATEGORY_CD")
    public void setCustCtgryCd(String custCtgryCd) {
        this.custCtgryCd = custCtgryCd;
    }

    public String getCustId() {
        return custId;
    }

    @JsonSetter("CUST_ID")
    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustTypeCd() {
        return custTypeCd;
    }

    @JsonSetter("CUST_TYPE_CD")
    public void setCustTypeCd(String custTypeCd) {
        this.custTypeCd = custTypeCd;
    }

    public String getFullNameAr() {
        return fullNameAr;
    }

    @JsonSetter("X_KYC_FULL_NAME_AR")
    public void setFullNameAr(String fullNameAr) {
        this.fullNameAr = fullNameAr;
    }

    public String getFullNameEn() {
        return fullNameEn;
    }

    @JsonSetter("X_KYC_FULL_NAME_EN")
    public void setFullNameEn(String fullNameEn) {
        this.fullNameEn = fullNameEn;
    }

    public String getLastModifiedDateFrom() {
        return lastModifiedDateFrom;
    }

    @JsonSetter("lastModifiedDateFrom")
    public void setLastModifiedDateFrom(String lastModifiedDateFrom) {
        this.lastModifiedDateFrom = lastModifiedDateFrom;
    }

    public String getLastModifiedDateTo() {
        return lastModifiedDateTo;
    }

    @JsonSetter("lastModifiedDateTo")
    public void setLastModifiedDateTo(String lastModifiedDateTo) {
        this.lastModifiedDateTo = lastModifiedDateTo;
    }

    public String getPageNo() {
        return pageNo;
    }

    @JsonSetter("CURRENT_PAGE")
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    public String getRowNo() {
        return rowNo;
    }

    @JsonSetter("NUMBERS_OF_ROWS")
    public void setRowNo(String rowNo) {
        this.rowNo = rowNo;
    }

    @Override
    public String toString() {
        return "SearchCustCriteria [custId=" + custId + ", fullNameAr=" + fullNameAr + ", fullNameEn=" + fullNameEn
                + ", custCtgryCd=" + custCtgryCd + ", custTypeCd=" + custTypeCd + ", createdDateFrom=" + createdDateFrom
                + ", createdDateTo=" + createdDateTo + ", lastModifiedDateFrom=" + lastModifiedDateFrom
                + ", lastModifiedDateTo=" + lastModifiedDateTo + ", rowNo=" + rowNo + ", pageNo=" + pageNo + "]";
    }

}
