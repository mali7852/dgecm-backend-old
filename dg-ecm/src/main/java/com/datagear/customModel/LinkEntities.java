package com.datagear.customModel;

public class LinkEntities {
	private long entityRk;
	private long memberRk;
	
	public LinkEntities () {
		
	}
	
	public LinkEntities (long entityRk, long memberRk) {
		this.entityRk = entityRk;
		this.memberRk = memberRk;
	}

	public long getEntityRk() {
		return entityRk;
	}

	public long getMemberRk() {
		return memberRk;
	}

	public void setEntityRk(long entityRk) {
		this.entityRk = entityRk;
	}

	public void setMemberRk(long memberRk) {
		this.memberRk = memberRk;
	}
}
