package com.datagear.customModel;

public interface DashboardStatistics {
    String getName();

    int getCount();

    double getY();
}
