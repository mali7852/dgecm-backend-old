package com.datagear.customModel;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonGetter;

public class SearchCaseResult {
	private final PagingDetails paging;
	private final List<Case_VerDTO> data;
//	private final List<Case_Ver> case_ver;
	
	public SearchCaseResult(PagingDetails pd, List<Case_VerDTO> data) {
		this.paging = pd;
		this.data = data;
//		this.case_ver = cv;
	}

//	@JsonGetter("DATA")
//	public List<Case_Ver> getCase_ver() {
//		return case_ver;
//	}

	@JsonGetter("PAGING")
	public PagingDetails getPaging() {
		return paging;
	}

	@JsonGetter("DATA")
	public List<Case_VerDTO> getData() {
		return data;
	}
	
	
}
