package com.datagear.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="Efile_Udf_Char_Val.findAll", query="SELECT c FROM Efile_Udf_Char_Val c")
public class Efile_Udf_Char_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Efile_Udf_Char_ValPK id;

	@Column(name="Udf_Val")
	private String udfVal;

	public Efile_Udf_Char_Val() {
	}

	public Efile_Udf_Char_ValPK getId() {
		return this.id;
	}

	public void setId(Efile_Udf_Char_ValPK id) {
		this.id = id;
	}

	public String getUdfVal() {
		return this.udfVal;
	}

	public void setUdfVal(String udfVal) {
		this.udfVal = udfVal;
	}
}
