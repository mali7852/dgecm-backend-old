package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The persistent class for the Case_Udf_Date_Val database table.
 */
@Entity
@NamedQuery(name = "Case_Udf_Date_Val.findAll", query = "SELECT c FROM Case_Udf_Date_Val c")
public class Case_Udf_Date_Val implements Serializable {

    private static final long serialVersionUID = 751265597038153446L;

    @EmbeddedId
    private Case_Udf_Date_ValPK id;

    @Column(name = "Udf_Val")
    private Timestamp udfVal;

    // bi-directional many-to-one association to Case_Udf_Def
    @ManyToOne(fetch = FetchType.LAZY)
//	@JsonManagedReference
    @JsonIgnore
    @JoinColumns({
            @JoinColumn(name = "Udf_Name", referencedColumnName = "Udf_Name", insertable = false, updatable = false),
            @JoinColumn(name = "Udf_Table_Name", referencedColumnName = "Udf_Table_Name", insertable = false, updatable = false)})
    private Case_Udf_Def caseUdfDef;

    // bi-directional many-to-one association to Case_Ver
    @ManyToOne(fetch = FetchType.LAZY)
//	@JsonManagedReference
    @JsonIgnore
    @JoinColumns({
            @JoinColumn(name = "Case_Rk", referencedColumnName = "Case_Rk", insertable = false, updatable = false),
            @JoinColumn(name = "Valid_From_Date", referencedColumnName = "Valid_From_Date", insertable = false, updatable = false)})
    private Case_Ver caseVer;

    public Case_Udf_Date_Val() {
    }

    public Case_Udf_Date_Val(Long case_rk, Timestamp cur_datetime, String udfName, Timestamp value, String udfTableName, long rowNo) {
        this.id = new Case_Udf_Date_ValPK();
        this.id.setCaseRk(case_rk);
        this.id.setValidFromDate(cur_datetime);
        this.id.setUdfTableName(udfTableName);
        this.id.setUdfName(udfName);
        this.id.setRowNo(rowNo);
        this.setUdfVal(value);
    }

    @JsonUnwrapped
    public Case_Udf_Date_ValPK getId() {
        return this.id;
    }

    public void setId(Case_Udf_Date_ValPK id) {
        this.id = id;
    }

    public Timestamp getUdfVal() {
        return this.udfVal;
    }

    public void setUdfVal(Timestamp udfVal) {
        this.udfVal = udfVal;
    }

    public Case_Ver getCaseVer() {
        return this.caseVer;
    }

    public void setCaseVer(Case_Ver caseVer) {
        this.caseVer = caseVer;
    }

}