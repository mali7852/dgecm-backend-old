package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Case_Udf_Date_Val database table.
 * 
 */
@Embeddable
public class Case_Udf_Char_ValPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Case_Rk")
	private long caseRk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Valid_From_Date")
	private java.util.Date validFromDate;

	@Column(name="Udf_Table_Name")
	private String udfTableName;

	@Column(name="Udf_Name")
	private String udfName;

	@Column(name="Row_No")
	private long rowNo;

	public Case_Udf_Char_ValPK() {
	}
	public long getCaseRk() {
		return this.caseRk;
	}
	public void setCaseRk(long caseRk) {
		this.caseRk = caseRk;
	}
	public java.util.Date getValidFromDate() {
		return this.validFromDate;
	}
	public void setValidFromDate(java.util.Date validFromDate) {
		this.validFromDate = validFromDate;
	}
	public String getUdfTableName() {
		return this.udfTableName;
	}
	public void setUdfTableName(String udfTableName) {
		this.udfTableName = udfTableName;
	}
	public String getUdfName() {
		return this.udfName;
	}
	public void setUdfName(String udfName) {
		this.udfName = udfName;
	}
	public long getRowNo() {
		return this.rowNo;
	}
	public void setRowNo(long rowNo) {
		this.rowNo = rowNo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Case_Udf_Char_ValPK)) {
			return false;
		}
		Case_Udf_Char_ValPK castOther = (Case_Udf_Char_ValPK)other;
		return 
			(this.caseRk == castOther.caseRk)
			&& this.validFromDate.equals(castOther.validFromDate)
			&& this.udfTableName.equals(castOther.udfTableName)
			&& this.udfName.equals(castOther.udfName)
			&& (this.rowNo == castOther.rowNo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.caseRk ^ (this.caseRk >>> 32)));
		hash = hash * prime + this.validFromDate.hashCode();
		hash = hash * prime + this.udfTableName.hashCode();
		hash = hash * prime + this.udfName.hashCode();
		hash = hash * prime + ((int) (this.rowNo ^ (this.rowNo >>> 32)));
		
		return hash;
	}
}