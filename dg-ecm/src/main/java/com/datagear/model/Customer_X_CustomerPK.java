package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Customer_X_Customer database table.
 * 
 */
@Embeddable
public class Customer_X_CustomerPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Cust_Rk", insertable = false, updatable = false)
	private long custRk;

	@Column(name = "Mmbr_Cust_Rk", insertable = false, updatable = false)
	private long mmbrCustRk;

	@Column(name = "Mmbr_Type_Cd")
	private String mmbrTypeCd;

	public Customer_X_CustomerPK() {
	}

	public long getCustRk() {
		return this.custRk;
	}

	public void setCustRk(long CustRk) {
		this.custRk = CustRk;
	}

	public long getMmbrCustRk() {
		return this.mmbrCustRk;
	}

	public void setMmbrCustRk(long MmbrCustRk) {
		this.mmbrCustRk = MmbrCustRk;
	}

	public String getMmbrTypeCd() {
		return this.mmbrTypeCd;
	}

	public void setMmbrTypeCd(String MmbrTypeCd) {
		this.mmbrTypeCd = MmbrTypeCd;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Customer_X_CustomerPK)) {
			return false;
		}
		Customer_X_CustomerPK castOther = (Customer_X_CustomerPK) other;
		return (this.custRk == castOther.custRk) && (this.mmbrCustRk == castOther.mmbrCustRk)
				&& this.mmbrTypeCd.equals(castOther.mmbrTypeCd);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.custRk ^ (this.custRk >>> 32)));
		hash = hash * prime + ((int) (this.mmbrCustRk ^ (this.mmbrCustRk >>> 32)));
		hash = hash * prime + this.mmbrTypeCd.hashCode();

		return hash;
	}
}