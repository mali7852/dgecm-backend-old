package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Efile_Conf database table.
 * 
 */
@Entity
@NamedQuery(name="Efile_Conf.findAll", query="SELECT e FROM Efile_Conf e")
public class Efile_Conf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Efile_Conf_Seq_No")
	private long efileConfSeqNo;

	@Column(name="Efile_Ctgry_Cd")
	private String efileCtgryCd;

	@Column(name="Efile_Sub_Ctgry_Cd")
	private String efileSubCtgryCd;

	@Column(name="Efile_Type_Cd")
	private String efileTypeCd;

	@Column(name="Form_Agency_Cd")
	private String formAgencyCd;

	@Column(name="Form_Type_Cd")
	private String formTypeCd;

	@Column(name="UI_Def_File_Name")
	private String uiDefFileName;

	public Efile_Conf() {
	}

	public long getEfileConfSeqNo() {
		return this.efileConfSeqNo;
	}

	public void setEfileConfSeqNo(long efileConfSeqNo) {
		this.efileConfSeqNo = efileConfSeqNo;
	}

	public String getEfileCtgryCd() {
		return this.efileCtgryCd;
	}

	public void setEfileCtgryCd(String efileCtgryCd) {
		this.efileCtgryCd = efileCtgryCd;
	}

	public String getEfileSubCtgryCd() {
		return this.efileSubCtgryCd;
	}

	public void setEfileSubCtgryCd(String efileSubCtgryCd) {
		this.efileSubCtgryCd = efileSubCtgryCd;
	}

	public String getEfileTypeCd() {
		return this.efileTypeCd;
	}

	public void setEfileTypeCd(String efileTypeCd) {
		this.efileTypeCd = efileTypeCd;
	}

	public String getFormAgencyCd() {
		return this.formAgencyCd;
	}

	public void setFormAgencyCd(String formAgencyCd) {
		this.formAgencyCd = formAgencyCd;
	}

	public String getFormTypeCd() {
		return this.formTypeCd;
	}

	public void setFormTypeCd(String formTypeCd) {
		this.formTypeCd = formTypeCd;
	}

	public String getUiDefFileName() {
		return this.uiDefFileName;
	}

	public void setUiDefFileName(String UIDefFileName) {
		this.uiDefFileName = UIDefFileName;
	}

}