package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the Customer_Ver database table.
 */
@Entity
@NamedQuery(name = "Customer_Ver.findAll", query = "SELECT c FROM Customer_Ver c")
public class Customer_Ver implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private Customer_VerPK id;

    @Column(name = "Create_Date")
    private Timestamp createDate;

    @Column(name = "Create_User_Id")
    private String createUserId;

    @Column(name = "Cust_Ctgry_Cd")
    private String custCtgryCd;

    @Column(name = "Cust_Full_Name")
    private String custFullName;

    @Column(name = "Cust_Id")
    private String custId;

    @Column(name = "Cust_Sub_Ctgry_Cd")
    private String custSubCtgryCd;

    @Column(name = "Cust_Type_Cd")
    private String custTypeCd;

    @Column(name = "Delete_Flag")
    private String deleteFlag;

    @Column(name = "Ident_Cust_Link_Sk")
    private BigDecimal identCustLinkSk;

    @Column(name = "Indv_Flag")
    private String indvFlag;

    @Column(name = "Nat_Id")
    private String natId;

    @Column(name = "Nat_Id_TYPE_CD")
    private String natIdTypeCd;

    @Column(name = "Src_Sys_Cd")
    private String srcSysCd;

    @Column(name = "UI_Def_File_Name")
    private String uiDefFileName;

    @Column(name = "Update_User_Id")
    private String updateUserId;

    @Column(name = "Valid_To_Date")
    private Timestamp validToDate;

    @Column(name = "Ver_No")
    private BigDecimal verNo;

    // bi-directional many-to-one association to Customer_Udf_Lgchr_Val
    @JsonIgnore
    @OneToMany(mappedBy = "customerVer", fetch = FetchType.LAZY)
    private List<Customer_Udf_Lgchr_Val> customerUdfLgchrVals;

    // bi-directional many-to-one association to Customer_Udf_No_Val
    @JsonIgnore
    @OneToMany(mappedBy = "customerVer", fetch = FetchType.LAZY)
    private List<Customer_Udf_No_Val> customerUdfNoVals;

    //	@JsonBackReference
    @JsonIgnore
    @OneToMany(mappedBy = "customerVer", fetch = FetchType.LAZY)
    private List<Customer_Udf_Char_Val> customerUdfCharVals;

    //	@JsonBackReference
    @JsonIgnore
    @OneToMany(mappedBy = "customerVer", fetch = FetchType.LAZY)
    private List<Customer_Udf_Date_Val> customerUdfDateVals;

    public Customer_Ver() {
    }

    public Customer_Ver(Long cust_rk, Timestamp cur_datetime, String cust_id, String cust_type_cd,
                        String ui_def_file_name) {
        this.id = new Customer_VerPK();
        this.id.setCustRk(cust_rk);
        this.id.setValidFromDate(cur_datetime);
        this.createDate = cur_datetime;
        this.custId = cust_id;
        this.custTypeCd = cust_type_cd;
        this.srcSysCd = "DGECM";
        this.createUserId = "DGECM";
        this.verNo = new BigDecimal(1.0);
        this.deleteFlag = "0";
        this.indvFlag = "0";
    }

    @JsonUnwrapped
    public Customer_VerPK getId() {
        return this.id;
    }

    public void setId(Customer_VerPK id) {
        this.id = id;
    }

    public Timestamp getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return this.createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCustCtgryCd() {
        return this.custCtgryCd;
    }

    public void setCustCtgryCd(String custCtgryCd) {
        this.custCtgryCd = custCtgryCd;
    }

    public String getCustFullName() {
        return this.custFullName;
    }

    public void setCustFullName(String custFullName) {
        this.custFullName = custFullName;
    }

    public String getCustId() {
        return this.custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustSubCtgryCd() {
        return this.custSubCtgryCd;
    }

    public void setCustSubCtgryCd(String custSubCtgryCd) {
        this.custSubCtgryCd = custSubCtgryCd;
    }

    public String getCustTypeCd() {
        return this.custTypeCd;
    }

    public void setCustTypeCd(String custTypeCd) {
        this.custTypeCd = custTypeCd;
    }

    public String getDeleteFlag() {
        return this.deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public BigDecimal getIdentCustLinkSk() {
        return this.identCustLinkSk;
    }

    public void setIdentCustLinkSk(BigDecimal identCustLinkSk) {
        this.identCustLinkSk = identCustLinkSk;
    }

    public String getIndvFlag() {
        return this.indvFlag;
    }

    public void setIndvFlag(String indvFlag) {
        this.indvFlag = indvFlag;
    }

    public String getNatId() {
        return this.natId;
    }

    public void setNatId(String natId) {
        this.natId = natId;
    }

    public String getNatIdTYPECD() {
        return this.natIdTypeCd;
    }

    public void setNatIdTYPECD(String natIdTYPECD) {
        this.natIdTypeCd = natIdTYPECD;
    }

    public String getSrcSysCd() {
        return this.srcSysCd;
    }

    public void setSrcSysCd(String srcSysCd) {
        this.srcSysCd = srcSysCd;
    }

    public String getUiDefFileName() {
        return this.uiDefFileName;
    }

    public void setUiDefFileName(String UIDefFileName) {
        this.uiDefFileName = UIDefFileName;
    }

    public String getUpdateUserId() {
        return this.updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Timestamp getValidToDate() {
        return this.validToDate;
    }

    public void setValidToDate(Timestamp validToDate) {
        this.validToDate = validToDate;
    }

    public BigDecimal getVerNo() {
        return this.verNo;
    }

    public void setVerNo(BigDecimal verNo) {
        this.verNo = verNo;
    }

    public List<Customer_Udf_Lgchr_Val> getCustomerUdfLgchrVals() {
        return this.customerUdfLgchrVals;
    }

    public void setCustomerUdfLgchrVals(List<Customer_Udf_Lgchr_Val> customerUdfLgchrVals) {
        this.customerUdfLgchrVals = customerUdfLgchrVals;
    }

    public Customer_Udf_Lgchr_Val addCustomerUdfLgchrVal(Customer_Udf_Lgchr_Val customerUdfLgchrVal) {
        getCustomerUdfLgchrVals().add(customerUdfLgchrVal);
        customerUdfLgchrVal.setCustomerVer(this);

        return customerUdfLgchrVal;
    }

    public Customer_Udf_Lgchr_Val removeCustomerUdfLgchrVal(Customer_Udf_Lgchr_Val customerUdfLgchrVal) {
        getCustomerUdfLgchrVals().remove(customerUdfLgchrVal);
        customerUdfLgchrVal.setCustomerVer(null);

        return customerUdfLgchrVal;
    }

    public List<Customer_Udf_No_Val> getCustomerUdfNoVals() {
        return this.customerUdfNoVals;
    }

    public void setCustomerUdfNoVals(List<Customer_Udf_No_Val> customerUdfNoVals) {
        this.customerUdfNoVals = customerUdfNoVals;
    }

    public Customer_Udf_No_Val addCustomerUdfNoVal(Customer_Udf_No_Val customerUdfNoVal) {
        getCustomerUdfNoVals().add(customerUdfNoVal);
        customerUdfNoVal.setCustomerVer(this);

        return customerUdfNoVal;
    }

    public Customer_Udf_No_Val removeCustomerUdfNoVal(Customer_Udf_No_Val customerUdfNoVal) {
        getCustomerUdfNoVals().remove(customerUdfNoVal);
        customerUdfNoVal.setCustomerVer(null);

        return customerUdfNoVal;
    }

    public List<Customer_Udf_Char_Val> getCustomerUdfCharVals() {
        return this.customerUdfCharVals;
    }

    public void setCustomerUdfCharVals(List<Customer_Udf_Char_Val> customerUdfCharVals) {
        this.customerUdfCharVals = customerUdfCharVals;
    }

    public Customer_Udf_Char_Val addCustomerUdfCharVal(Customer_Udf_Char_Val customerUdfCharVal) {
        getCustomerUdfCharVals().add(customerUdfCharVal);
        customerUdfCharVal.setCustomerVer(this);

        return customerUdfCharVal;
    }

    public Customer_Udf_Char_Val removeCustomerUdfCharVal(Customer_Udf_Char_Val customerUdfCharVal) {
        getCustomerUdfCharVals().remove(customerUdfCharVal);
        customerUdfCharVal.setCustomerVer(null);

        return customerUdfCharVal;
    }

    public List<Customer_Udf_Date_Val> getCustomerUdfDateVals() {
        return this.customerUdfDateVals;
    }

    public void setCustomerUdfDateVals(List<Customer_Udf_Date_Val> customerUdfDateVals) {
        this.customerUdfDateVals = customerUdfDateVals;
    }

    public Customer_Udf_Date_Val addCustomerUdfDateVal(Customer_Udf_Date_Val customerUdfDateVal) {
        getCustomerUdfDateVals().add(customerUdfDateVal);
        customerUdfDateVal.setCustomerVer(this);

        return customerUdfDateVal;
    }

    public Customer_Udf_Date_Val removeCustomerUdfDateVal(Customer_Udf_Date_Val customerUdfDateVal) {
        getCustomerUdfDateVals().remove(customerUdfDateVal);
        customerUdfDateVal.setCustomerVer(null);

        return customerUdfDateVal;
    }

}