package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Efile_Search_Result_Field database table.
 * 
 */
@Entity
@NamedQuery(name="Efile_Search_Result_Field.findAll", query="SELECT e FROM Efile_Search_Result_Field e")
public class Efile_Search_Result_Field implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Efile_Search_Result_FieldPK id;

	@Column(name="Display_Ordr_No")
	private BigDecimal displayOrdrNo;

	@Column(name="Fmt_Text")
	private String fmtText;

	@Column(name="Ref_Table_Name")
	private String refTableName;

	public Efile_Search_Result_Field() {
	}

	public Efile_Search_Result_FieldPK getId() {
		return this.id;
	}

	public void setId(Efile_Search_Result_FieldPK id) {
		this.id = id;
	}

	public BigDecimal getDisplayOrdrNo() {
		return this.displayOrdrNo;
	}

	public void setDisplayOrdrNo(BigDecimal displayOrdrNo) {
		this.displayOrdrNo = displayOrdrNo;
	}

	public String getFmtText() {
		return this.fmtText;
	}

	public void setFmtText(String fmtText) {
		this.fmtText = fmtText;
	}

	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

}