package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the Occurrence_Udf_No_Val database table.
 * 
 */
@Entity
@NamedQuery(name = "Occurrence_Udf_No_Val.findAll", query = "SELECT o FROM Occurrence_Udf_No_Val o")
public class Occurrence_Udf_No_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_Udf_No_ValPK id;

	@Column(name = "Udf_Val")
	private double udfVal;

	// bi-directional many-to-one association to Occurrence_Udf_Def
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "Udf_Name", referencedColumnName = "Udf_Name", insertable = false, updatable = false),
			@JoinColumn(name = "Udf_Table_Name", referencedColumnName = "Udf_Table_Name", insertable = false, updatable = false) })
	private Occurrence_Udf_Def occurrenceUdfDef;

	// bi-directional many-to-one association to Occurrence_Ver
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "Occs_Rk", referencedColumnName = "Occs_Rk", insertable = false, updatable = false),
			@JoinColumn(name = "Valid_From_Date", referencedColumnName = "Valid_From_Date", insertable = false, updatable = false) })
	private Occurrence_Ver occurrenceVer;

	public Occurrence_Udf_No_Val() {
	}

	public Occurrence_Udf_No_ValPK getId() {
		return this.id;
	}

	public void setId(Occurrence_Udf_No_ValPK id) {
		this.id = id;
	}

	public double getUdfVal() {
		return this.udfVal;
	}

	public void setUdfVal(double udfVal) {
		this.udfVal = udfVal;
	}

	public Occurrence_Udf_Def getOccurrenceUdfDef() {
		return this.occurrenceUdfDef;
	}

	public void setOccurrenceUdfDef(Occurrence_Udf_Def occurrenceUdfDef) {
		this.occurrenceUdfDef = occurrenceUdfDef;
	}

	public Occurrence_Ver getOccurrenceVer() {
		return this.occurrenceVer;
	}

	public void setOccurrenceVer(Occurrence_Ver occurrenceVer) {
		this.occurrenceVer = occurrenceVer;
	}

}