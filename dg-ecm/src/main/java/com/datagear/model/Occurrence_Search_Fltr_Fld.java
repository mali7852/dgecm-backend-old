package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Occurrence_Search_Fltr_Fld database table.
 * 
 */
@Entity
@NamedQuery(name="Occurrence_Search_Fltr_Fld.findAll", query="SELECT o FROM Occurrence_Search_Fltr_Fld o")
public class Occurrence_Search_Fltr_Fld implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_Search_Fltr_FldPK id;

	@Column(name="Display_Ordr_No")
	private BigDecimal displayOrdrNo;

	@Column(name="Ref_Table_Name")
	private String refTableName;

	public Occurrence_Search_Fltr_Fld() {
	}

	public Occurrence_Search_Fltr_FldPK getId() {
		return this.id;
	}

	public void setId(Occurrence_Search_Fltr_FldPK id) {
		this.id = id;
	}

	public BigDecimal getDisplayOrdrNo() {
		return this.displayOrdrNo;
	}

	public void setDisplayOrdrNo(BigDecimal displayOrdrNo) {
		this.displayOrdrNo = displayOrdrNo;
	}

	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

}