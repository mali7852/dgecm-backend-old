package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The persistent class for the ECM_Event database table.
 * 
 */
@Entity
@NamedQuery(name = "ECM_Event.findAll", query = "SELECT e FROM ECM_Event e")
public class ECM_Event implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "Event_Rk")
	private long eventRk;

	@Column(name = "Business_Object_Name")
	private String businessObjectName;

	@Column(name = "Business_Object_Rk")
	private long businessObjectRk;

	@Column(name = "Create_Date")
	private Timestamp createDate;

	@Column(name = "Create_User_Id")
	private String createUserId;

	@Column(name = "Event_Desc")
	private String eventDesc;

	@Column(name = "Event_Type_Cd")
	private String eventTypeCd;

	@Column(name = "Linked_Object_Name")
	private String linkedObjectName;

	@Column(name = "Linked_Object_Rk")
	private BigDecimal linkedObjectRk;

	public ECM_Event() {
	}

	public ECM_Event(long eventRk, String businessObjectName, long businessObjectRk, Timestamp createDate,
			String createUserId, String eventDesc, String eventTypeCd, String linkedObjectName,
			BigDecimal linkedObjectRk) {
		this.eventRk = eventRk;
		this.businessObjectName = businessObjectName;
		this.businessObjectRk = businessObjectRk;
		this.createDate = createDate;
		this.createUserId = createUserId;
		this.eventDesc = eventDesc;
		this.eventTypeCd = eventTypeCd;
		this.linkedObjectName = linkedObjectName;
		this.linkedObjectRk = linkedObjectRk;
	}

	public long getEventRk() {
		return this.eventRk;
	}

	public void setEventRk(long eventRk) {
		this.eventRk = eventRk;
	}

	public String getBusinessObjectName() {
		return this.businessObjectName;
	}

	public void setBusinessObjectName(String businessObjectName) {
		this.businessObjectName = businessObjectName;
	}

	public long getBusinessObjectRk() {
		return this.businessObjectRk;
	}

	public void setBusinessObjectRk(long businessObjectRk) {
		this.businessObjectRk = businessObjectRk;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserId() {
		return this.createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getEventDesc() {
		return this.eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public String getEventTypeCd() {
		return this.eventTypeCd;
	}

	public void setEventTypeCd(String eventTypeCd) {
		this.eventTypeCd = eventTypeCd;
	}

	public String getLinkedObjectName() {
		return this.linkedObjectName;
	}

	public void setLinkedObjectName(String linkedObjectName) {
		this.linkedObjectName = linkedObjectName;
	}

	public BigDecimal getLinkedObjectRk() {
		return this.linkedObjectRk;
	}

	public void setLinkedObjectRk(BigDecimal linkedObjectRk) {
		this.linkedObjectRk = linkedObjectRk;
	}

}