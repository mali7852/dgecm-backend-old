package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Customer_Conf database table.
 * 
 */
@Entity
@NamedQuery(name="Customer_Conf.findAll", query="SELECT c FROM Customer_Conf c")
public class Customer_Conf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Cust_Conf_Seq_No")
	private long custConfSeqNo;

	@Column(name="Cust_Ctgry_Cd")
	private String custCtgryCd;

	@Column(name="Cust_Sub_Ctgry_Cd")
	private String custSubCtgryCd;

	@Column(name="Cust_Type_Cd")
	private String custTypeCd;

	@Column(name="UI_Def_File_Name")
	private String uiDefFileName;

	public Customer_Conf() {
	}

	public long getCustConfSeqNo() {
		return this.custConfSeqNo;
	}

	public void setCustConfSeqNo(long custConfSeqNo) {
		this.custConfSeqNo = custConfSeqNo;
	}

	public String getCustCtgryCd() {
		return this.custCtgryCd;
	}

	public void setCustCtgryCd(String custCtgryCd) {
		this.custCtgryCd = custCtgryCd;
	}

	public String getCustSubCtgryCd() {
		return this.custSubCtgryCd;
	}

	public void setCustSubCtgryCd(String custSubCtgryCd) {
		this.custSubCtgryCd = custSubCtgryCd;
	}

	public String getCustTypeCd() {
		return this.custTypeCd;
	}

	public void setCustTypeCd(String custTypeCd) {
		this.custTypeCd = custTypeCd;
	}

	public String getUiDefFileName() {
		return this.uiDefFileName;
	}

	public void setUiDefFileName(String UIDefFileName) {
		this.uiDefFileName = UIDefFileName;
	}

}