package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Efile_Ver database table.
 * 
 */
@Embeddable
public class Efile_VerPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Efile_Rk")
	private long efileRk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Valid_From_Date")
	private java.util.Date validFromDate;

	public Efile_VerPK() {
	}

	public long getEfileRk() {
		return this.efileRk;
	}

	public void setEfileRk(long efileRk) {
		this.efileRk = efileRk;
	}

	public java.util.Date getValidFromDate() {
		return this.validFromDate;
	}

	public void setValidFromDate(java.util.Date validFromDate) {
		this.validFromDate = validFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Efile_VerPK)) {
			return false;
		}
		Efile_VerPK castOther = (Efile_VerPK) other;
		return (this.efileRk == castOther.efileRk) && this.validFromDate.equals(castOther.validFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.efileRk ^ (this.efileRk >>> 32)));
		hash = hash * prime + this.validFromDate.hashCode();

		return hash;
	}
}