package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Occurrence_X_User_Grp database table.
 * 
 */
@Embeddable
public class Occurrence_X_User_GrpPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Occs_Rk", insertable = false, updatable = false)
	private long occsRk;

	@Column(name = "User_Grp_Name")
	private String userGrpName;

	public Occurrence_X_User_GrpPK() {
	}

	public long getOccsRk() {
		return this.occsRk;
	}

	public void setOccsRk(long occsRk) {
		this.occsRk = occsRk;
	}

	public String getUserGrpName() {
		return this.userGrpName;
	}

	public void setUserGrpName(String userGrpName) {
		this.userGrpName = userGrpName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Occurrence_X_User_GrpPK)) {
			return false;
		}
		Occurrence_X_User_GrpPK castOther = (Occurrence_X_User_GrpPK) other;
		return (this.occsRk == castOther.occsRk) && this.userGrpName.equals(castOther.userGrpName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.occsRk ^ (this.occsRk >>> 32)));
		hash = hash * prime + this.userGrpName.hashCode();

		return hash;
	}
}