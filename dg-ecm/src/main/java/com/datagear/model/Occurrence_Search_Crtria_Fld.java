package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Occurrence_Search_Crtria_Fld database table.
 * 
 */
@Entity
@NamedQuery(name="Occurrence_Search_Crtria_Fld.findAll", query="SELECT o FROM Occurrence_Search_Crtria_Fld o")
public class Occurrence_Search_Crtria_Fld implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_Search_Crtria_FldPK id;

	@Column(name="Display_Ordr_No")
	private BigDecimal displayOrdrNo;

	@Column(name="Fmt_Text")
	private String fmtText;

	@Column(name="Ref_Table_Name")
	private String refTableName;

	public Occurrence_Search_Crtria_Fld() {
	}

	public Occurrence_Search_Crtria_FldPK getId() {
		return this.id;
	}

	public void setId(Occurrence_Search_Crtria_FldPK id) {
		this.id = id;
	}

	public BigDecimal getDisplayOrdrNo() {
		return this.displayOrdrNo;
	}

	public void setDisplayOrdrNo(BigDecimal displayOrdrNo) {
		this.displayOrdrNo = displayOrdrNo;
	}

	public String getFmtText() {
		return this.fmtText;
	}

	public void setFmtText(String fmtText) {
		this.fmtText = fmtText;
	}

	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

}