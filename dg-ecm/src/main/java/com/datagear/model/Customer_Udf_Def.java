package com.datagear.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the Customer_Udf_Def database table.
 * 
 */
@Entity
@NamedQuery(name="Customer_Udf_Def.findAll", query="SELECT c FROM Customer_Udf_Def c")
public class Customer_Udf_Def implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Customer_Udf_DefPK id;

	@Column(name="Max_Char_Cnt")
	private BigDecimal maxCharCnt;

	@Column(name="Ref_Table_Name")
	private String refTableName;

	@Column(name="Udf_Desc")
	private String udfDesc;

	@Column(name="Udf_Type_Name")
	private String udfTypeName;

	//bi-directional many-to-one association to Customer_Udf_Lgchr_Val
	@JsonIgnore
	@OneToMany(mappedBy="customerUdfDef")
	private List<Customer_Udf_Lgchr_Val> customerUdfLgchrVals;

	//bi-directional many-to-one association to Customer_Udf_No_Val
	@JsonIgnore
	@OneToMany(mappedBy="customerUdfDef")
	private List<Customer_Udf_No_Val> customerUdfNoVals;

	//bi-directional many-to-one association to Customer_Udf_Char_Val
	@JsonIgnore
	@OneToMany(mappedBy="customerUdfDef")
	private List<Customer_Udf_Char_Val> customerUdfCharVals;

	//bi-directional many-to-one association to Customer_Udf_Date_Val
	@JsonIgnore
	@OneToMany(mappedBy="customerUdfDef")
	private List<Customer_Udf_Date_Val> customerUdfDateVals;

	public Customer_Udf_Def() {
	}

	public Customer_Udf_DefPK getId() {
		return this.id;
	}

	public void setId(Customer_Udf_DefPK id) {
		this.id = id;
	}

	public BigDecimal getMaxCharCnt() {
		return this.maxCharCnt;
	}

	public void setMaxCharCnt(BigDecimal maxCharCnt) {
		this.maxCharCnt = maxCharCnt;
	}

	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

	public String getUdfDesc() {
		return this.udfDesc;
	}

	public void setUdfDesc(String udfDesc) {
		this.udfDesc = udfDesc;
	}

	public String getUdfTypeName() {
		return this.udfTypeName;
	}

	public void setUdfTypeName(String udfTypeName) {
		this.udfTypeName = udfTypeName;
	}

	public List<Customer_Udf_Lgchr_Val> getCustomerUdfLgchrVals() {
		return this.customerUdfLgchrVals;
	}

	public void setCustomerUdfLgchrVals(List<Customer_Udf_Lgchr_Val> customerUdfLgchrVals) {
		this.customerUdfLgchrVals = customerUdfLgchrVals;
	}

	public Customer_Udf_Lgchr_Val addCustomerUdfLgchrVal(Customer_Udf_Lgchr_Val customerUdfLgchrVal) {
		getCustomerUdfLgchrVals().add(customerUdfLgchrVal);
		customerUdfLgchrVal.setCustomerUdfDef(this);

		return customerUdfLgchrVal;
	}

	public Customer_Udf_Lgchr_Val removeCustomerUdfLgchrVal(Customer_Udf_Lgchr_Val customerUdfLgchrVal) {
		getCustomerUdfLgchrVals().remove(customerUdfLgchrVal);
		customerUdfLgchrVal.setCustomerUdfDef(null);

		return customerUdfLgchrVal;
	}

	public List<Customer_Udf_No_Val> getCustomerUdfNoVals() {
		return this.customerUdfNoVals;
	}

	public void setCustomerUdfNoVals(List<Customer_Udf_No_Val> customerUdfNoVals) {
		this.customerUdfNoVals = customerUdfNoVals;
	}

	public Customer_Udf_No_Val addCustomerUdfNoVal(Customer_Udf_No_Val customerUdfNoVal) {
		getCustomerUdfNoVals().add(customerUdfNoVal);
		customerUdfNoVal.setCustomerUdfDef(this);

		return customerUdfNoVal;
	}

	public Customer_Udf_No_Val removeCustomerUdfNoVal(Customer_Udf_No_Val customerUdfNoVal) {
		getCustomerUdfNoVals().remove(customerUdfNoVal);
		customerUdfNoVal.setCustomerUdfDef(null);

		return customerUdfNoVal;
	}

}