package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Occurrence_Udf_Date_Val database table.
 * 
 */
@Entity
@NamedQuery(name="Occurrence_Udf_Date_Val.findAll", query="SELECT c FROM Occurrence_Udf_Date_Val c")
public class Occurrence_Udf_Date_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_Udf_Date_ValPK id;

	@Column(name="Udf_Val")
	private Timestamp udfVal;

	public Occurrence_Udf_Date_Val() {
	}
	
	public Occurrence_Udf_Date_Val(Long occs_rk, Timestamp valid_from_date, String udf_name, Timestamp udf_val) {
		this.id = new Occurrence_Udf_Date_ValPK();
		this.id.setOccsRk(occs_rk);
		this.id.setValidFromDate(valid_from_date);
		this.id.setUdfTableName("OCCS");
		this.id.setUdfName(udf_name);
		this.id.setRowNo(1);
		this.setUdfVal(udf_val);
	}

	public Occurrence_Udf_Date_ValPK getId() {
		return this.id;
	}

	public void setId(Occurrence_Udf_Date_ValPK id) {
		this.id = id;
	}

	public Timestamp getUdfVal() {
		return this.udfVal;
	}

	public void setUdfVal(Timestamp udfVal) {
		this.udfVal = udfVal;
	}

}