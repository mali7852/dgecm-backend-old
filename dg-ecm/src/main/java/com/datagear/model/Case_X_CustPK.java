package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Case_X_Cust database table.
 * 
 */
@Embeddable
public class Case_X_CustPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Case_Rk", insertable=false, updatable=false)
	private long caseRk;

	@Column(name="Cust_Rk", insertable=false, updatable=false)
	private long custRk;

	@Column(name="Rel_Type_Cd")
	private String relTypeCd;

	public Case_X_CustPK() {
	}
	public long getCaseRk() {
		return this.caseRk;
	}
	public void setCaseRk(long CaseRk) {
		this.caseRk = CaseRk;
	}
	public long getCustRk() {
		return this.custRk;
	}
	public void setCustRk(long custRk) {
		this.custRk = custRk;
	}
	public String getRelTypeCd() {
		return this.relTypeCd;
	}
	public void setRelTypeCd(String relTypeCd) {
		this.relTypeCd = relTypeCd;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Case_X_CustPK)) {
			return false;
		}
		Case_X_CustPK castOther = (Case_X_CustPK)other;
		return 
			(this.caseRk == castOther.caseRk)
			&& (this.custRk == castOther.custRk)
			&& this.relTypeCd.equals(castOther.relTypeCd);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.caseRk ^ (this.caseRk >>> 32)));
		hash = hash * prime + ((int) (this.custRk ^ (this.custRk >>> 32)));
		hash = hash * prime + this.relTypeCd.hashCode();
		
		return hash;
	}
}