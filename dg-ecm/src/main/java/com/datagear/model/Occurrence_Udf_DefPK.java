package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Occurrence_Udf_Def database table.
 * 
 */
@Embeddable
public class Occurrence_Udf_DefPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Udf_Table_Name")
	private String udfTableName;

	@Column(name = "Udf_Name")
	private String udfName;

	public Occurrence_Udf_DefPK() {
	}

	public String getUdfTableName() {
		return this.udfTableName;
	}

	public void setUdfTableName(String udfTableName) {
		this.udfTableName = udfTableName;
	}

	public String getUdfName() {
		return this.udfName;
	}

	public void setUdfName(String udfName) {
		this.udfName = udfName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Occurrence_Udf_DefPK)) {
			return false;
		}
		Occurrence_Udf_DefPK castOther = (Occurrence_Udf_DefPK) other;
		return this.udfTableName.equals(castOther.udfTableName) && this.udfName.equals(castOther.udfName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.udfTableName.hashCode();
		hash = hash * prime + this.udfName.hashCode();

		return hash;
	}
}