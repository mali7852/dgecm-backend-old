package com.datagear.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * The persistent class for the Ref_Table_Val database table.
 * 
 */
@Entity
@NamedQuery(name = "Ref_Table_Val.findAll", query = "SELECT r FROM Ref_Table_Val r")
public class Ref_Table_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonUnwrapped
	@EmbeddedId
	private Ref_Table_ValPK id;

	@Column(name = "Active_Flg")
	private String activeFlg;

	@Column(name = "Display_Ordr_No")
	private BigDecimal displayOrdrNo;

	@Column(name = "Val_Desc")
	private String valDesc;

	// bi-directional many-to-one association to Ref_Table_Val
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "Parent_Ref_Table_Name", referencedColumnName = "Ref_Table_Name", insertable = false, updatable = false),
			@JoinColumn(name = "Parent_Val_Cd", referencedColumnName = "Val_Cd", insertable = false, updatable = false) })
	private Ref_Table_Val parentRefTableVal;

	// bi-directional many-to-one association to Ref_Table_Val
	@JsonIgnore
	@OneToMany(mappedBy = "parentRefTableVal")
	private List<Ref_Table_Val> refTableVals;

	// bi-directional many-to-one association to Ref_Table_Trans
	@JsonIgnore
	@OneToMany(mappedBy = "refTablVal")
	private List<Ref_Table_Trans> refTableTrans;

	public Ref_Table_Val() {
	}

	public Ref_Table_Val addRefTableVal(Ref_Table_Val refTableVal) {
		getRefTableVals().add(refTableVal);
		refTableVal.setRefTableVal(this);

		return refTableVal;
	}

	@JsonGetter("activeFlg")
	public String getActiveFlg() {
		return this.activeFlg;
	}

	@JsonGetter("displayOrderNo")
	public BigDecimal getDisplayOrdrNo() {
		return this.displayOrdrNo;
	}

	public Ref_Table_ValPK getId() {
		return this.id;
	}

	public List<Ref_Table_Trans> getRefTableTrans() {
		return refTableTrans;
	}

	@JsonIgnore
	public Ref_Table_Val getParentRefTableVal() {
		return this.parentRefTableVal;
	}

	@JsonAnyGetter
	public Map<String, String> getRefTableVal() {
		Map<String, String> properties = new HashMap<>();
		properties.put("parentValueCd",
				this.parentRefTableVal != null ? this.parentRefTableVal.getId().getValCd() : null);
		properties.put("parentRefTableNm",
				this.parentRefTableVal != null ? this.parentRefTableVal.getId().getRefTableName() : null);
		return properties;
	}

	public List<Ref_Table_Val> getRefTableVals() {
		return this.refTableVals;
	}

	@JsonGetter("valueDesc")
	public String getValDesc() {
		return this.valDesc;
	}

	public Ref_Table_Val removeRefTableVal(Ref_Table_Val refTableVal) {
		getRefTableVals().remove(refTableVal);
		refTableVal.setRefTableVal(null);

		return refTableVal;
	}

	public void setActiveFlg(String activeFlg) {
		this.activeFlg = activeFlg;
	}

	public void setDisplayOrdrNo(BigDecimal displayOrdrNo) {
		this.displayOrdrNo = displayOrdrNo;
	}

	public void setId(Ref_Table_ValPK id) {
		this.id = id;
	}

	public void setRefTableTrans(List<Ref_Table_Trans> refTableTrans) {
		this.refTableTrans = refTableTrans;
	}

	public void setRefTableVal(Ref_Table_Val refTableVal) {
		this.parentRefTableVal = refTableVal;
	}

	public void setRefTableVals(List<Ref_Table_Val> refTableVals) {
		this.refTableVals = refTableVals;
	}

	public void setValDesc(String valDesc) {
		this.valDesc = valDesc;
	}

}