package com.datagear.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The primary key class for the Occurrence_Ver database table.
 * 
 */
@Embeddable
public class Occurrence_VerPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Occs_Rk")
	private long occsRk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Valid_From_Date")
	private java.util.Date validFromDate;

	public Occurrence_VerPK() {
	}

	public Occurrence_VerPK(long occs_rk, Timestamp valid_from_date) {
		this.occsRk = occs_rk;
		this.validFromDate = valid_from_date;
	}

	public long getOccsRk() {
		return this.occsRk;
	}

	public void setOccsRk(long occsRk) {
		this.occsRk = occsRk;
	}

	public java.util.Date getValidFromDate() {
		return this.validFromDate;
	}

	public void setValidFromDate(java.util.Date validFromDate) {
		this.validFromDate = validFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Occurrence_VerPK)) {
			return false;
		}
		Occurrence_VerPK castOther = (Occurrence_VerPK) other;
		return (this.occsRk == castOther.occsRk) && this.validFromDate.equals(castOther.validFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.occsRk ^ (this.occsRk >>> 32)));
		hash = hash * prime + this.validFromDate.hashCode();

		return hash;
	}
}