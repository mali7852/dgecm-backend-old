package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the occs_rk_seq database table.
 * 
 */
@Entity
@Table(name="occs_rk_seq")
@NamedQuery(name="OccsRkSeq.findAll", query="SELECT o FROM OccsRkSeq o")
public class OccsRkSeq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private BigDecimal id;

	public OccsRkSeq() {
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

}