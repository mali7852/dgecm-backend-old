package com.datagear.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="ECM_Column_Label.findAll", query="SELECT o FROM ECM_Column_Label o")
public class ECM_Column_Label implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private ECM_Column_LabelPK id;
	
	@Column(name="Label_Text")
	private String labelText;
	
	public ECM_Column_Label(){
		
	}

	public ECM_Column_LabelPK getId() {
		return id;
	}

	public String getLabel_Text() {
		return labelText;
	}

	public void setId(ECM_Column_LabelPK id) {
		this.id = id;
	}

	public void setLabel_Text(String label_Text) {
		this.labelText = label_Text;
	}
	
	
}
