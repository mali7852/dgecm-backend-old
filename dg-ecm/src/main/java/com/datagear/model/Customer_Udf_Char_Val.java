package com.datagear.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Customer_Udf_Char_Val.findAll", query = "SELECT c FROM Customer_Udf_Char_Val c")
public class Customer_Udf_Char_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Customer_Udf_Char_ValPK id;

	@Column(name = "Udf_Val")
	private String udfVal;

	// bi-directional many-to-one association to Customer_Udf_Def
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "Udf_Name", referencedColumnName = "Udf_Name", insertable = false, updatable = false),
			@JoinColumn(name = "Udf_Table_Name", referencedColumnName = "Udf_Table_Name", insertable = false, updatable = false) })
	private Customer_Udf_Def customerUdfDef;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "Cust_Rk", referencedColumnName = "Cust_Rk", insertable = false, updatable = false),
			@JoinColumn(name = "Valid_From_Date", referencedColumnName = "Valid_From_Date", insertable = false, updatable = false) })
	private Customer_Ver customerVer;

	public Customer_Udf_Char_Val() {
	}

	public Customer_Udf_Char_Val(Long cust_rk, Timestamp valid_From_Date, String udf_Name, String udf_Val) {
		this.id = new Customer_Udf_Char_ValPK();
		this.id.setCustRk(cust_rk);
		this.id.setValidFromDate(valid_From_Date);
		this.id.setUdfTableName("CUST");
		this.id.setUdfName(udf_Name);
		this.id.setRowNo(1);
		this.setUdfVal(udf_Val);
	}

	public Customer_Ver getCustomerVer() {
		return customerVer;
	}

	public Customer_Udf_Char_ValPK getId() {
		return this.id;
	}

	public String getUdfVal() {
		return this.udfVal;
	}

	public void setCustomerVer(Customer_Ver customerVer) {
		this.customerVer = customerVer;
	}

	public void setId(Customer_Udf_Char_ValPK id) {
		this.id = id;
	}

	public void setUdfVal(String udfVal) {
		this.udfVal = udfVal;
	}
}
