package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the Customer_X_User_Grp database table.
 * 
 */
@Entity
@NamedQuery(name = "Customer_X_User_Grp.findAll", query = "SELECT c FROM Customer_X_User_Grp c")
public class Customer_X_User_Grp implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Customer_X_User_GrpPK id;

	// bi-directional many-to-one association to Customer_Live
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Cust_Rk", insertable = false, updatable = false)
	private Customer_Live customerLive;

	public Customer_X_User_Grp() {
	}

	public Customer_X_User_Grp(Long cust_rk, String user_grp_name) {
		this.id = new Customer_X_User_GrpPK();
		this.id.setCustRk(cust_rk);
		this.id.setUserGrpName(user_grp_name);
	}

	public Customer_X_User_GrpPK getId() {
		return this.id;
	}

	public void setId(Customer_X_User_GrpPK id) {
		this.id = id;
	}

	public Customer_Live getCustomerLive() {
		return this.customerLive;
	}

	public void setCustomerLive(Customer_Live customerLive) {
		this.customerLive = customerLive;
	}

}