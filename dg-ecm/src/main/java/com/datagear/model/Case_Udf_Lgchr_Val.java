package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;


/**
 * The persistent class for the Case_Udf_Lgchr_Val database table.
 * 
 */
@Entity
@NamedQuery(name="Case_Udf_Lgchr_Val.findAll", query="SELECT c FROM Case_Udf_Lgchr_Val c")
public class Case_Udf_Lgchr_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Case_Udf_Lgchr_ValPK id;

	@Column(name="Udf_Val")
	private String udfVal;

	//bi-directional many-to-one association to Case_Udf_Def
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
		@JoinColumn(name="Udf_Name", referencedColumnName="Udf_Name", insertable=false, updatable=false),
		@JoinColumn(name="Udf_Table_Name", referencedColumnName="Udf_Table_Name", insertable=false, updatable=false)
		})
	private Case_Udf_Def CaseUdfDef;

	//bi-directional many-to-one association to Case_Ver
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumns({
		@JoinColumn(name="Case_Rk", referencedColumnName="Case_Rk", insertable=false, updatable=false),
		@JoinColumn(name="Valid_From_Date", referencedColumnName="Valid_From_Date", insertable=false, updatable=false)
		})
	private Case_Ver caseVer;

	public Case_Udf_Lgchr_Val() {
	}

	public Case_Udf_Lgchr_Val(Long caseRk, Timestamp validFromDate, String udfName, String udfVal, String udfTableName, long rowNo) {
		this.id = new Case_Udf_Lgchr_ValPK();
		this.id.setCaseRk(caseRk);
		this.id.setValidFromDate(validFromDate);
		this.id.setUdfName(udfName);
		this.id.setUdfTableName(udfTableName);
		this.id.setRowNo(rowNo);
		this.udfVal = udfVal;
	}

	@JsonUnwrapped
	public Case_Udf_Lgchr_ValPK getId() {
		return this.id;
	}

	public void setId(Case_Udf_Lgchr_ValPK id) {
		this.id = id;
	}

	public String getUdfVal() {
		return this.udfVal;
	}

	public void setUdfVal(String udfVal) {
		this.udfVal = udfVal;
	}

	public Case_Udf_Def getCaseUdfDef() {
		return this.CaseUdfDef;
	}

	public void setCaseUdfDef(Case_Udf_Def CaseUdfDef) {
		this.CaseUdfDef = CaseUdfDef;
	}

	public Case_Ver getCaseVer() {
		return this.caseVer;
	}

	public void setCaseVer(Case_Ver CaseVer) {
		this.caseVer = CaseVer;
	}

}