package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the Case_Live database table.
 */
@Entity
@NamedQuery(name = "Case_Live.findAll", query = "SELECT c FROM Case_Live c")
public class Case_Live implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Case_Rk")
    private long caseRk;

    @Column(name = "Case_Ctgry_Cd")
    private String caseCtgryCd;

    @Column(name = "Case_Desc")
    private String caseDesc;

    @Column(name = "Case_Dispos_Cd")
    private String caseDisposCd;

    @Column(name = "Case_Id")
    private String caseId;

    @Column(name = "Case_Link_Sk")
    private BigDecimal caseLinkSk;

    @Column(name = "Case_Stat_Cd")
    private String caseStatCd;

    @Column(name = "Case_Sub_Ctgry_Cd")
    private String caseSubCtgryCd;

    @Column(name = "Case_Type_Cd")
    private String caseTypeCd;

    @Column(name = "Close_Date")
    private Timestamp closeDate;

    @Column(name = "Create_Date")
    private Timestamp createDate;

    @Column(name = "Create_User_Id")
    private String createUserId;

    @Column(name = "Delete_Flag")
    private String deleteFlag;

    @Column(name = "Investr_User_Id")
    private String investrUserId;

    @Column(name = "Open_Date")
    private Timestamp openDate;

    @Column(name = "Priority_Cd")
    private String priorityCd;

    @Column(name = "Regu_Rpt_Rqd_Flag")
    private String reguRptRqdFlag;

    @Column(name = "REOpen_Date")
    private Timestamp reopenDate;

    @Column(name = "Src_Sys_Cd")
    private String srcSysCd;

    @Column(name = "UI_Def_File_Name")
    private String uiDefFileName;

    @Column(name = "Update_User_Id")
    private String updateUserId;

    @Column(name = "Valid_From_Date")
    private Timestamp validFromDate;

    @Column(name = "Valid_To_Date")
    private Timestamp validToDate;

    @Column(name = "Ver_No")
    private BigDecimal verNo;

    @Column(name = "Col1")
    private String col1;

    @Column(name = "Col2")
    private String col2;

    @Column(name = "Col3")
    private String col3;

    @Column(name = "Col4")
    private String col4;

    @Column(name = "Col5")
    private String col5;

    // bi-directional many-to-one association to Case_X_User_Grp
    @JsonIgnore
    @OneToMany(mappedBy = "caseLive")
    private List<Case_X_User_Grp> caseXUserGrps;
    //
//	// bi-directional many-to-one association to Occurrence_Live
    @JsonIgnore
    @OneToMany(mappedBy = "caseLive", cascade = CascadeType.ALL)
    private List<Occurrence_Live> occurrenceLives;

    @JsonIgnore
    @OneToMany(mappedBy = "caseLive")
    private List<Case_Ver> caseVers;
//
//	// bi-directional many-to-one association to Occurrence_Ver
//	@JsonIgnore
//	@OneToMany(mappedBy = "caseLive")
//	private List<Occurrence_Ver> occurrenceVers;

    public Case_Live() {
    }

    public Case_Live(long case_rk, Timestamp cur_datetime, String case_id, String case_type_cd, String ui_def_file_name) {
        this.caseRk = case_rk;
        this.validFromDate = cur_datetime;
        this.createDate = cur_datetime;
        this.caseId = case_id;
        this.caseTypeCd = case_type_cd;
        this.reguRptRqdFlag = "0";
        this.srcSysCd = "DGECM";
        this.createUserId = "DGECM";
        this.verNo = new BigDecimal(1.0);
        this.deleteFlag = "0";
        this.uiDefFileName = ui_def_file_name;
    }

    public Case_X_User_Grp addCaseXUserGrp(Case_X_User_Grp caseXUserGrp) {
        if (caseXUserGrp != null) {
            if (getCaseXUserGrps() == null)
                this.setCaseXUserGrps(new ArrayList<>());
            getCaseXUserGrps().add(caseXUserGrp);
            caseXUserGrp.setCaseLive(this);
        }

        return caseXUserGrp;
    }

    public Occurrence_Live addOccurrenceLive(Occurrence_Live occurrenceLive) {
        if (occurrenceLive != null) {
            if (getOccurrenceLives() == null)
                this.setOccurrenceLives(new ArrayList<>());
            getOccurrenceLives().add(occurrenceLive);
            occurrenceLive.setCaseLive(this);
        }

        return occurrenceLive;
    }
//
//	public Occurrence_Ver addOccurrenceVer(Occurrence_Ver occurrenceVer) {
//		getOccurrenceVers().add(occurrenceVer);
//		occurrenceVer.setCaseLive(this);
//
//		return occurrenceVer;
//	}

    public String getCaseCtgryCd() {
        return this.caseCtgryCd;
    }

    public void setCaseCtgryCd(String caseCtgryCd) {
        this.caseCtgryCd = caseCtgryCd;
    }

    public String getCaseDesc() {
        return this.caseDesc;
    }

    public void setCaseDesc(String caseDesc) {
        this.caseDesc = caseDesc;
    }

    public String getCaseDisposCd() {
        return this.caseDisposCd;
    }

    public void setCaseDisposCd(String caseDisposCd) {
        this.caseDisposCd = caseDisposCd;
    }

    public String getCaseId() {
        return this.caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public BigDecimal getCaseLinkSk() {
        return this.caseLinkSk;
    }

    public void setCaseLinkSk(BigDecimal caseLinkSk) {
        this.caseLinkSk = caseLinkSk;
    }

    public long getCaseRk() {
        return this.caseRk;
    }

    public void setCaseRk(long caseRk) {
        this.caseRk = caseRk;
    }

    public String getCaseStatCd() {
        return this.caseStatCd;
    }

    public void setCaseStatCd(String caseStatCd) {
        this.caseStatCd = caseStatCd;
    }

    public String getCaseSubCtgryCd() {
        return this.caseSubCtgryCd;
    }

    public void setCaseSubCtgryCd(String caseSubCtgryCd) {
        this.caseSubCtgryCd = caseSubCtgryCd;
    }

//	public List<Occurrence_Ver> getOccurrenceVers() {
//		return this.occurrenceVers;
//	}

    public String getCaseTypeCd() {
        return this.caseTypeCd;
    }

    public void setCaseTypeCd(String caseTypeCd) {
        this.caseTypeCd = caseTypeCd;
    }

    public List<Case_X_User_Grp> getCaseXUserGrps() {
        return this.caseXUserGrps;
    }

    public void setCaseXUserGrps(List<Case_X_User_Grp> caseXUserGrps) {
        this.caseXUserGrps = caseXUserGrps;
    }

    public List<Occurrence_Live> getOccurrenceLives() {
        return this.occurrenceLives;
    }

    public void setOccurrenceLives(List<Occurrence_Live> occurrenceLives) {
        this.occurrenceLives = occurrenceLives;
    }

    public Timestamp getCloseDate() {
        return this.closeDate;
    }

    public void setCloseDate(Timestamp closeDate) {
        this.closeDate = closeDate;
    }

    public Timestamp getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return this.createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getDeleteFlag() {
        return this.deleteFlag;
    }
//
//	public Occurrence_Ver removeOccurrenceVer(Occurrence_Ver occurrenceVer) {
//		getOccurrenceVers().remove(occurrenceVer);
//		occurrenceVer.setCaseLive(null);
//
//		return occurrenceVer;
//	}

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Occurrence_Live removeOccurrenceLive(Occurrence_Live occurrenceLive) {
        getOccurrenceLives().remove(occurrenceLive);
        occurrenceLive.setCaseLive(null);

        return occurrenceLive;
    }

    public String getInvestrUserId() {
        return this.investrUserId;
    }

    public void setInvestrUserId(String investrUserId) {
        this.investrUserId = investrUserId;
    }

    public Timestamp getOpenDate() {
        return this.openDate;
    }

    public void setOpenDate(Timestamp openDate) {
        this.openDate = openDate;
    }

    public String getPriorityCd() {
        return this.priorityCd;
    }

    public void setPriorityCd(String priorityCd) {
        this.priorityCd = priorityCd;
    }

    public String getReguRptRqdFlag() {
        return this.reguRptRqdFlag;
    }

    public void setReguRptRqdFlag(String reguRptRqdFlag) {
        this.reguRptRqdFlag = reguRptRqdFlag;
    }

    public Timestamp getREOpenDate() {
        return this.reopenDate;
    }

    public void setREOpenDate(Timestamp REOpenDate) {
        this.reopenDate = REOpenDate;
    }

    public String getSrcSysCd() {
        return this.srcSysCd;
    }

    public void setSrcSysCd(String srcSysCd) {
        this.srcSysCd = srcSysCd;
    }

    public String getUiDefFileName() {
        return this.uiDefFileName;
    }

    public void setUiDefFileName(String UIDefFileName) {
        this.uiDefFileName = UIDefFileName;
    }

//	public void setOccurrenceVers(List<Occurrence_Ver> occurrenceVers) {
//		this.occurrenceVers = occurrenceVers;
//	}

    public String getUpdateUserId() {
        return this.updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Timestamp getValidFromDate() {
        return this.validFromDate;
    }

    public void setValidFromDate(Timestamp validFromDate) {
        this.validFromDate = validFromDate;
    }

    public Timestamp getValidToDate() {
        return this.validToDate;
    }

    public void setValidToDate(Timestamp validToDate) {
        this.validToDate = validToDate;
    }

    public BigDecimal getVerNo() {
        return this.verNo;
    }

    public void setVerNo(BigDecimal verNo) {
        this.verNo = verNo;
    }

    public Case_X_User_Grp removeCaseXUserGrp(Case_X_User_Grp caseXUserGrp) {
        getCaseXUserGrps().remove(caseXUserGrp);
        caseXUserGrp.setCaseLive(null);

        return caseXUserGrp;
    }

    public List<Case_Ver> getCaseVers() {
        return caseVers;
    }

    public void setCaseVers(List<Case_Ver> caseVers) {
        this.caseVers = caseVers;
    }

    public String getCol1() {
        return col1;
    }

    public void setCol1(String col1) {
        this.col1 = col1;
    }

    public String getCol2() {
        return col2;
    }

    public void setCol2(String col2) {
        this.col2 = col2;
    }

    public String getCol3() {
        return col3;
    }

    public void setCol3(String col3) {
        this.col3 = col3;
    }

    public String getCol4() {
        return col4;
    }

    public void setCol4(String col4) {
        this.col4 = col4;
    }

    public String getCol5() {
        return col5;
    }

    public void setCol5(String col5) {
        this.col5 = col5;
    }

    @Override
    public String toString() {
        return "Case_Live [caseRk=" + caseRk + ", caseCtgryCd=" + caseCtgryCd + ", caseDesc=" + caseDesc
                + ", caseDisposCd=" + caseDisposCd + ", caseId=" + caseId + ", caseLinkSk=" + caseLinkSk
                + ", caseStatCd=" + caseStatCd + ", caseSubCtgryCd=" + caseSubCtgryCd + ", caseTypeCd=" + caseTypeCd
                + ", closeDate=" + closeDate + ", createDate=" + createDate + ", createUserId=" + createUserId
                + ", deleteFlag=" + deleteFlag + ", investrUserId=" + investrUserId + ", openDate=" + openDate
                + ", priorityCd=" + priorityCd + ", reguRptRqdFlag=" + reguRptRqdFlag + ", reopenDate=" + reopenDate
                + ", srcSysCd=" + srcSysCd + ", uiDefFileName=" + uiDefFileName + ", updateUserId=" + updateUserId
                + ", validFromDate=" + validFromDate + ", validToDate=" + validToDate + ", verNo=" + verNo
                + ", caseXUserGrps=" + caseXUserGrps /*+ ", occurrenceLives=" + occurrenceLives + ", occurrenceVers="
				+ occurrenceVers + "]"*/;
    }

}