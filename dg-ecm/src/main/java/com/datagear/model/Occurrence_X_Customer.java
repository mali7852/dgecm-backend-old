package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the Occurrence_X_Customer database table.
 * 
 */
@Entity
@NamedQuery(name = "Occurrence_X_Customer.findAll", query = "SELECT o FROM Occurrence_X_Customer o")
public class Occurrence_X_Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_X_CustomerPK id;

	@Column(name = "Create_Date")
	private Timestamp createDate;

	@Column(name = "Rel_Desc")
	private String relDesc;

	// bi-directional many-to-one association to Customer_Live
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Cust_Rk", insertable = false, updatable = false)
	private Customer_Live customerLive;

	// bi-directional many-to-one association to Occurrence_Live
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Occs_Rk", insertable = false, updatable = false)
	private Occurrence_Live occurrenceLive;

	public Occurrence_X_Customer() {
	}

	public Occurrence_X_Customer(long occs_Rk, long cust_Rk, Timestamp create_Date) {
		this.id = new Occurrence_X_CustomerPK();
		this.id.setOccsRk(occs_Rk);
		this.id.setCustRk(cust_Rk);
		this.id.setRelTypeCd("P");
		this.setRelDesc("Suspected");
		this.setCreateDate(create_Date);
	}

	public Occurrence_X_CustomerPK getId() {
		return this.id;
	}

	public void setId(Occurrence_X_CustomerPK id) {
		this.id = id;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getRelDesc() {
		return this.relDesc;
	}

	public void setRelDesc(String relDesc) {
		this.relDesc = relDesc;
	}

	public Customer_Live getCustomerLive() {
		return this.customerLive;
	}

	public void setCustomerLive(Customer_Live customerLive) {
		this.customerLive = customerLive;
	}

	public Occurrence_Live getOccurrenceLive() {
		return this.occurrenceLive;
	}

	public void setOccurrenceLive(Occurrence_Live occurrenceLive) {
		this.occurrenceLive = occurrenceLive;
	}

}