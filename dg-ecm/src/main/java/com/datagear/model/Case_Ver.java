package com.datagear.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * The persistent class for the Case_Ver database table.
 */
@Entity
@NamedQuery(name = "Case_Ver.findAll", query = "SELECT c FROM Case_Ver c ")
public class Case_Ver implements Serializable {

    private static final long serialVersionUID = -7758531491932366488L;

    @EmbeddedId
    private Case_VerPK id;

    @Column(name = "Case_Ctgry_Cd")
    private String caseCtgryCd;

    @Column(name = "Case_Desc")
    private String caseDesc;

    @Column(name = "Case_Dispos_Cd")
    private String caseDisposCd;

    @Column(name = "Case_Id")
    private String caseId;

    @Column(name = "Case_Link_Sk")
    private BigDecimal caseLinkSk;

    @Column(name = "Case_Stat_Cd")
    private String caseStatCd;

    @Column(name = "Case_Sub_Ctgry_Cd")
    private String caseSubCtgryCd;

    @Column(name = "Case_Type_Cd")
    private String caseTypeCd;

    @Column(name = "Close_Date")
    private Timestamp closeDate;

    @Column(name = "Create_Date")
    private Timestamp createDate;

    @Column(name = "Create_User_Id")
    private String createUserId;

    @Column(name = "Delete_Flag")
    private String deleteFlag;

    @Column(name = "Investr_User_Id")
    private String investrUserId;

    @Column(name = "Open_Date")
    private Timestamp openDate;

    @Column(name = "Priority_Cd")
    private String priorityCd;

    @Column(name = "Regu_Rpt_Rqd_Flag")
    private String reguRptRqdFlag;

    @Column(name = "REOpen_Date")
    private Timestamp reopenDate;

    @Column(name = "Src_Sys_Cd")
    private String srcSysCd;

    @Column(name = "UI_Def_File_Name")
    private String uiDefFileName;

    @Column(name = "Update_User_Id")
    private String updateUserId;

    @Column(name = "Valid_To_Date")
    private Timestamp validToDate;

    @Column(name = "Ver_No")
    private BigDecimal verNo;

    @Column(name = "Col1")
    private String col1;

    @Column(name = "Col2")
    private String col2;

    @Column(name = "Col3")
    private String col3;

    @Column(name = "Col4")
    private String col4;

    @Column(name = "Col5")
    private String col5;


    // bi-directional many-to-one association to Case_Udf_No_Val
//	@JsonIgnore
//	@OneToMany(mappedBy="caseVer")
//	private List<Case_Udf_No_Val> caseUdfNoVals;

    // bi-directional many-to-one association to Case_Udf_No_Val
    @JsonIgnore
    @OneToMany(mappedBy = "caseVer", fetch = FetchType.LAZY)
//	@JsonBackReference
    private Set<Case_Udf_Char_Val> caseUdfCharVals;

    // bi-directional many-to-one association to Case_Udf_No_Val
    @JsonIgnore
    @OneToMany(mappedBy = "caseVer", fetch = FetchType.LAZY)
//	@JsonBackReference
    private Set<Case_Udf_Date_Val> caseUdfDateVals;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JsonIgnore
//    @JoinColumn(name = "Case_Type_Cd", referencedColumnName = "Case_Type_Cd", insertable = false, updatable = false)
//    private Case_Conf caseConf;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "Case_Rk", referencedColumnName = "Case_Rk", insertable = false, updatable = false)
    private Case_Live caseLive;

    public Case_Ver() {
    }

    public Case_Ver(long case_rk, Timestamp cur_datetime, String case_id, String case_type_cd,
                    String ui_def_file_name) {
        this.id = new Case_VerPK();
        this.id.setCaseRk(case_rk);
        this.id.setValidFromDate(cur_datetime);
        this.createDate = cur_datetime;
        this.caseId = case_id;
        this.caseTypeCd = case_type_cd;
        this.reguRptRqdFlag = "0";
        this.srcSysCd = "DGECM";
        this.createUserId = "DGECM";
        this.verNo = new BigDecimal(1.0);
        this.deleteFlag = "0";
        this.uiDefFileName = ui_def_file_name;
    }

    public Case_Udf_Char_Val addCaseUdfCharVal(Case_Udf_Char_Val caseUdfCharVal) {
        getCaseUdfCharVals().add(caseUdfCharVal);
        caseUdfCharVal.setCaseVer(this);

        return caseUdfCharVal;
    }

    public Case_Udf_Date_Val addCaseUdfDateVal(Case_Udf_Date_Val caseUdfDateVal) {
        getCaseUdfDateVals().add(caseUdfDateVal);
        caseUdfDateVal.setCaseVer(this);

        return caseUdfDateVal;
    }

    public String getCaseCtgryCd() {
        return this.caseCtgryCd;
    }

    public String getCaseDesc() {
        return this.caseDesc;
    }

    public String getCaseDisposCd() {
        return this.caseDisposCd;
    }

    @JsonUnwrapped
    public String getCaseId() {
        return this.caseId;
    }

    public BigDecimal getCaseLinkSk() {
        return this.caseLinkSk;
    }

    public String getCaseStatCd() {
        return this.caseStatCd;
    }

    public String getCaseSubCtgryCd() {
        return this.caseSubCtgryCd;
    }

    public String getCaseTypeCd() {
        return this.caseTypeCd;
    }

    public Set<Case_Udf_Char_Val> getCaseUdfCharVals() {
        return this.caseUdfCharVals;
    }

    public Set<Case_Udf_Date_Val> getCaseUdfDateVals() {
        return this.caseUdfDateVals;
    }

    public Timestamp getCloseDate() {
        return this.closeDate;
    }

    public Timestamp getCreateDate() {
        return this.createDate;
    }

    public String getCreateUserId() {
        return this.createUserId;
    }

    public String getDeleteFlag() {
        return this.deleteFlag;
    }

    @JsonUnwrapped
    public Case_VerPK getId() {
        return this.id;
    }

    public String getInvestrUserId() {
        return this.investrUserId;
    }

    public Timestamp getOpenDate() {
        return this.openDate;
    }

    public String getPriorityCd() {
        return this.priorityCd;
    }

    public String getReguRptRqdFlag() {
        return this.reguRptRqdFlag;
    }

    public Timestamp getREOpenDate() {
        return this.reopenDate;
    }

    public String getSrcSysCd() {
        return this.srcSysCd;
    }

    public String getUiDefFileName() {
        return this.uiDefFileName;
    }

    public String getUpdateUserId() {
        return this.updateUserId;
    }

    public Timestamp getValidToDate() {
        return this.validToDate;
    }

    public BigDecimal getVerNo() {
        return this.verNo;
    }

    public Case_Udf_Char_Val removeCaseUdfCharVal(Case_Udf_Char_Val caseUdfCharVal) {
        getCaseUdfCharVals().remove(caseUdfCharVal);
        caseUdfCharVal.setCaseVer(null);

        return caseUdfCharVal;
    }

    public Case_Udf_Date_Val removeCaseUdfDateVal(Case_Udf_Date_Val caseUdfDateVal) {
        getCaseUdfDateVals().remove(caseUdfDateVal);
        caseUdfDateVal.setCaseVer(null);

        return caseUdfDateVal;
    }

    public void setCaseCtgryCd(String caseCtgryCd) {
        this.caseCtgryCd = caseCtgryCd;
    }

    public void setCaseDesc(String caseDesc) {
        this.caseDesc = caseDesc;
    }

    public void setCaseDisposCd(String caseDisposCd) {
        this.caseDisposCd = caseDisposCd;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public void setCaseLinkSk(BigDecimal caseLinkSk) {
        this.caseLinkSk = caseLinkSk;
    }

    public void setCaseStatCd(String caseStatCd) {
        this.caseStatCd = caseStatCd;
    }

    public void setCaseSubCtgryCd(String caseSubCtgryCd) {
        this.caseSubCtgryCd = caseSubCtgryCd;
    }

    public void setCaseTypeCd(String caseTypeCd) {
        this.caseTypeCd = caseTypeCd;
    }

    public void setCaseUdfCharVals(Set<Case_Udf_Char_Val> caseUdfCharVals) {
        this.caseUdfCharVals = caseUdfCharVals;
    }

    public void setCaseUdfDateVals(Set<Case_Udf_Date_Val> caseUdfDateVals) {
        this.caseUdfDateVals = caseUdfDateVals;
    }

    public void setCloseDate(Timestamp closeDate) {
        this.closeDate = closeDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public void setId(Case_VerPK id) {
        this.id = id;
    }

    public void setInvestrUserId(String investrUserId) {
        this.investrUserId = investrUserId;
    }

    public void setOpenDate(Timestamp openDate) {
        this.openDate = openDate;
    }

    public void setPriorityCd(String priorityCd) {
        this.priorityCd = priorityCd;
    }

//	public List<Case_Udf_No_Val> getCaseUdfNoVals() {
//		return this.caseUdfNoVals;
//	}
//
//	public void setCaseUdfNoVals(List<Case_Udf_No_Val> caseUdfNoVals) {
//		this.caseUdfNoVals = caseUdfNoVals;
//	}

//	public Case_Udf_No_Val addCaseUdfNoVal(Case_Udf_No_Val caseUdfNoVal) {
//		getCaseUdfNoVals().add(caseUdfNoVal);
//		caseUdfNoVal.setCaseVer(this);
//
//		return caseUdfNoVal;
//	}
//
//	public Case_Udf_No_Val removeCaseUdfNoVal(Case_Udf_No_Val caseUdfNoVal) {
//		getCaseUdfNoVals().remove(caseUdfNoVal);
//		caseUdfNoVal.setCaseVer(null);
//
//		return caseUdfNoVal;
//	}

    public void setReguRptRqdFlag(String reguRptRqdFlag) {
        this.reguRptRqdFlag = reguRptRqdFlag;
    }

    public void setREOpenDate(Timestamp REOpenDate) {
        this.reopenDate = REOpenDate;
    }

    public void setSrcSysCd(String srcSysCd) {
        this.srcSysCd = srcSysCd;
    }

    public void setUiDefFileName(String UIDefFileName) {
        this.uiDefFileName = UIDefFileName;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public void setValidToDate(Timestamp validToDate) {
        this.validToDate = validToDate;
    }

    public void setVerNo(BigDecimal verNo) {
        this.verNo = verNo;
    }

    public String getCol1() {
        return col1;
    }

    public void setCol1(String col1) {
        this.col1 = col1;
    }

    public String getCol2() {
        return col2;
    }

    public void setCol2(String col2) {
        this.col2 = col2;
    }

    public String getCol3() {
        return col3;
    }

    public void setCol3(String col3) {
        this.col3 = col3;
    }

    public String getCol4() {
        return col4;
    }

    public void setCol4(String col4) {
        this.col4 = col4;
    }

    public String getCol5() {
        return col5;
    }

    public void setCol5(String col5) {
        this.col5 = col5;
    }

//    public Case_Conf getCaseConf() {
//        return caseConf;
//    }
//
//    public void setCaseConf(Case_Conf caseConf) {
//        this.caseConf = caseConf;
//    }

    @Override
    public String toString() {
        return "Case_Ver [caseCtgryCd=" + caseCtgryCd + ", caseDesc=" + caseDesc + ", caseDisposCd=" + caseDisposCd
                + ", caseId=" + caseId + ", caseLinkSk=" + caseLinkSk + ", caseStatCd=" + caseStatCd
                + ", caseSubCtgryCd=" + caseSubCtgryCd + ", caseTypeCd=" + caseTypeCd + ", closeDate=" + closeDate
                + ", createDate=" + createDate + ", createUserId=" + createUserId + ", deleteFlag=" + deleteFlag
                + ", investrUserId=" + investrUserId + ", openDate=" + openDate + ", priorityCd=" + priorityCd
                + ", reguRptRqdFlag=" + reguRptRqdFlag + ", reopenDate=" + reopenDate + ", srcSysCd=" + srcSysCd
                + ", uiDefFileName=" + uiDefFileName + ", updateUserId=" + updateUserId + ", validToDate=" + validToDate
                + ", verNo=" + verNo + ", caseUdfCharVals=" + caseUdfCharVals + ", caseUdfDateVals=" + caseUdfDateVals
                + "]";
    }

}