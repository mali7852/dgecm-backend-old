package com.datagear.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the Case_Udf_Def database table.
 * 
 */
@Entity
@NamedQuery(name = "Case_Udf_Def.findAll", query = "SELECT c FROM Case_Udf_Def c")
public class Case_Udf_Def implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Case_Udf_DefPK id;

	@Column(name = "Max_Char_Cnt")
	private BigDecimal maxCharCnt;

	@Column(name = "Ref_Table_Name")
	private String refTableName;

	@Column(name = "Udf_Desc")
	private String udfDesc;

	@Column(name = "Udf_Type_Name")
	private String udfTypeName;

	// bi-directional many-to-one association to Case_Udf_No_Val
	@JsonIgnore
	@OneToMany(mappedBy = "caseUdfDef")
	private List<Case_Udf_No_Val> caseUdfNoVals;

	// bi-directional many-to-one association to Case_Udf_Char_Val
//	@JsonIgnore
//	@JsonBackReference
	@OneToMany(mappedBy = "caseUdfDef", fetch = FetchType.LAZY)
	private List<Case_Udf_Char_Val> caseUdfCharVals;

	// bi-directional many-to-one association to Case_Udf_Date_Val
//	@JsonIgnore
//	@JsonBackReference
	@OneToMany(mappedBy = "caseUdfDef", fetch = FetchType.LAZY)
	private List<Case_Udf_Date_Val> caseUdfDateVals;

	public Case_Udf_Def() {
	}

	public Case_Udf_DefPK getId() {
		return this.id;
	}

	public void setId(Case_Udf_DefPK id) {
		this.id = id;
	}

	public BigDecimal getMaxCharCnt() {
		return this.maxCharCnt;
	}

	public void setMaxCharCnt(BigDecimal maxCharCnt) {
		this.maxCharCnt = maxCharCnt;
	}

	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

	public String getUdfDesc() {
		return this.udfDesc;
	}

	public void setUdfDesc(String udfDesc) {
		this.udfDesc = udfDesc;
	}

	public String getUdfTypeName() {
		return this.udfTypeName;
	}

	public void setUdfTypeName(String udfTypeName) {
		this.udfTypeName = udfTypeName;
	}

	public List<Case_Udf_No_Val> getCaseUdfNoVals() {
		return this.caseUdfNoVals;
	}

	public void setCaseUdfNoVals(List<Case_Udf_No_Val> caseUdfNoVals) {
		this.caseUdfNoVals = caseUdfNoVals;
	}

	public Case_Udf_No_Val addCaseUdfNoVal(Case_Udf_No_Val caseUdfNoVal) {
		getCaseUdfNoVals().add(caseUdfNoVal);
		caseUdfNoVal.setCaseUdfDef(this);

		return caseUdfNoVal;
	}

	public Case_Udf_No_Val removeCaseUdfNoVal(Case_Udf_No_Val caseUdfNoVal) {
		getCaseUdfNoVals().remove(caseUdfNoVal);
		caseUdfNoVal.setCaseUdfDef(null);

		return caseUdfNoVal;
	}

	@Override
	public String toString() {
		return "Case_Udf_Def [Udf_Name=" + id.getUdfName() + ", maxCharCnt=" + maxCharCnt + ", refTableName="
				+ refTableName + ", udfDesc=" + udfDesc + ", udfTypeName=" + udfTypeName + "]";
	}

}