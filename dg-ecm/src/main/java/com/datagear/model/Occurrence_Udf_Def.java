package com.datagear.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the Occurrence_Udf_Def database table.
 * 
 */
@Entity
@NamedQuery(name = "Occurrence_Udf_Def.findAll", query = "SELECT o FROM Occurrence_Udf_Def o")
public class Occurrence_Udf_Def implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_Udf_DefPK id;

	@Column(name = "Max_Char_Cnt")
	private BigDecimal maxCharCnt;

	@Column(name = "Ref_Table_Name")
	private String refTableName;

	@Column(name = "Udf_Desc")
	private String udfDesc;

	@Column(name = "Udf_Type_Name")
	private String udfTypeName;

	// bi-directional many-to-one association to Occurrence_Udf_Lgchr_Val
	@JsonIgnore
	@OneToMany(mappedBy = "occurrenceUdfDef")
	private List<Occurrence_Udf_Lgchr_Val> occurrenceUdfLgchrVals;

	// bi-directional many-to-one association to Occurrence_Udf_No_Val
	@JsonIgnore
	@OneToMany(mappedBy = "occurrenceUdfDef")
	private List<Occurrence_Udf_No_Val> occurrenceUdfNoVals;

	public Occurrence_Udf_Def() {
	}

	public Occurrence_Udf_DefPK getId() {
		return this.id;
	}

	public void setId(Occurrence_Udf_DefPK id) {
		this.id = id;
	}

	public BigDecimal getMaxCharCnt() {
		return this.maxCharCnt;
	}

	public void setMaxCharCnt(BigDecimal maxCharCnt) {
		this.maxCharCnt = maxCharCnt;
	}

	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

	public String getUdfDesc() {
		return this.udfDesc;
	}

	public void setUdfDesc(String udfDesc) {
		this.udfDesc = udfDesc;
	}

	public String getUdfTypeName() {
		return this.udfTypeName;
	}

	public void setUdfTypeName(String udfTypeName) {
		this.udfTypeName = udfTypeName;
	}

	public List<Occurrence_Udf_Lgchr_Val> getOccurrenceUdfLgchrVals() {
		return this.occurrenceUdfLgchrVals;
	}

	public void setOccurrenceUdfLgchrVals(List<Occurrence_Udf_Lgchr_Val> occurrenceUdfLgchrVals) {
		this.occurrenceUdfLgchrVals = occurrenceUdfLgchrVals;
	}

	public Occurrence_Udf_Lgchr_Val addOccurrenceUdfLgchrVal(Occurrence_Udf_Lgchr_Val occurrenceUdfLgchrVal) {
		getOccurrenceUdfLgchrVals().add(occurrenceUdfLgchrVal);
		occurrenceUdfLgchrVal.setOccurrenceUdfDef(this);

		return occurrenceUdfLgchrVal;
	}

	public Occurrence_Udf_Lgchr_Val removeOccurrenceUdfLgchrVal(Occurrence_Udf_Lgchr_Val occurrenceUdfLgchrVal) {
		getOccurrenceUdfLgchrVals().remove(occurrenceUdfLgchrVal);
		occurrenceUdfLgchrVal.setOccurrenceUdfDef(null);

		return occurrenceUdfLgchrVal;
	}

	public List<Occurrence_Udf_No_Val> getOccurrenceUdfNoVals() {
		return this.occurrenceUdfNoVals;
	}

	public void setOccurrenceUdfNoVals(List<Occurrence_Udf_No_Val> occurrenceUdfNoVals) {
		this.occurrenceUdfNoVals = occurrenceUdfNoVals;
	}

	public Occurrence_Udf_No_Val addOccurrenceUdfNoVal(Occurrence_Udf_No_Val occurrenceUdfNoVal) {
		getOccurrenceUdfNoVals().add(occurrenceUdfNoVal);
		occurrenceUdfNoVal.setOccurrenceUdfDef(this);

		return occurrenceUdfNoVal;
	}

	public Occurrence_Udf_No_Val removeOccurrenceUdfNoVal(Occurrence_Udf_No_Val occurrenceUdfNoVal) {
		getOccurrenceUdfNoVals().remove(occurrenceUdfNoVal);
		occurrenceUdfNoVal.setOccurrenceUdfDef(null);

		return occurrenceUdfNoVal;
	}

}