package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@NamedQuery(name = "Case_Udf_Char_Val.findAll", query = "SELECT c FROM Case_Udf_Char_Val c")
public class Case_Udf_Char_Val implements Serializable {

    private static final long serialVersionUID = -6383850807522899137L;

    @EmbeddedId
    private Case_Udf_Char_ValPK id;

    @Column(name = "Udf_Val")
    private String udfVal;

    // bi-directional many-to-one association to Case_Udf_Def
    @ManyToOne(fetch = FetchType.LAZY)
//	@JsonManagedReference
    @JsonIgnore
    @JoinColumns({
            @JoinColumn(name = "Udf_Name", referencedColumnName = "Udf_Name", insertable = false, updatable = false),
            @JoinColumn(name = "Udf_Table_Name", referencedColumnName = "Udf_Table_Name", insertable = false, updatable = false)})
    private Case_Udf_Def caseUdfDef;

    // bi-directional many-to-one association to Case_Ver
    @ManyToOne(fetch = FetchType.LAZY)
//	@JsonManagedReference
    @JsonIgnore
    @JoinColumns({
            @JoinColumn(name = "Case_Rk", referencedColumnName = "Case_Rk", insertable = false, updatable = false),
            @JoinColumn(name = "Valid_From_Date", referencedColumnName = "Valid_From_Date", insertable = false, updatable = false)})
    private Case_Ver caseVer;

    public Case_Udf_Char_Val() {
    }

    public Case_Udf_Char_Val(Long case_rk, Timestamp cur_datetime, String udfName, String value, String udfTableName, long rowNo) {
        this.id = new Case_Udf_Char_ValPK();
        this.id.setCaseRk(case_rk);
        this.id.setValidFromDate(cur_datetime);
        this.id.setUdfTableName(udfTableName);
        this.id.setUdfName(udfName);
        this.id.setRowNo(rowNo);
        this.setUdfVal(value);
    }

    public Case_Udf_Def getCaseUdfDef() {
        return caseUdfDef;
    }

    public void setCaseUdfDef(Case_Udf_Def caseUdfDef) {
        this.caseUdfDef = caseUdfDef;
    }

    @JsonUnwrapped
    public Case_Udf_Char_ValPK getId() {
        return this.id;
    }

    public void setId(Case_Udf_Char_ValPK id) {
        this.id = id;
    }

    public String getUdfVal() {
        return this.udfVal;
    }

    public void setUdfVal(String udfVal) {
        this.udfVal = udfVal;
    }

    public Case_Ver getCaseVer() {
        return this.caseVer;
    }

    public void setCaseVer(Case_Ver caseVer) {
        this.caseVer = caseVer;
    }
}
