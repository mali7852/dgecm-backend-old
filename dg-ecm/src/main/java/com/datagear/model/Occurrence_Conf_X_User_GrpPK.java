package com.datagear.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Occurrence_Conf_X_User_GrpPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "Occs_Conf_Seq_No", insertable = false, updatable = false)
	private long occsConfSeqNo;

	@Column(name = "User_Grp_Name")
	private String userGrpName;

	public Occurrence_Conf_X_User_GrpPK() {
	}

	public long getOccsConfSeqNo() {
		return this.occsConfSeqNo;
	}

	public void setOccsRk(long OccsConfSeqNo) {
		this.occsConfSeqNo = OccsConfSeqNo;
	}

	public String getUserGrpName() {
		return this.userGrpName;
	}

	public void setUserGrpName(String userGrpName) {
		this.userGrpName = userGrpName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Occurrence_Conf_X_User_GrpPK)) {
			return false;
		}
		Occurrence_Conf_X_User_GrpPK castOther = (Occurrence_Conf_X_User_GrpPK) other;
		return (this.occsConfSeqNo == castOther.occsConfSeqNo) && this.userGrpName.equals(castOther.userGrpName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.occsConfSeqNo ^ (this.occsConfSeqNo >>> 32)));
		hash = hash * prime + this.userGrpName.hashCode();

		return hash;
	}
}
