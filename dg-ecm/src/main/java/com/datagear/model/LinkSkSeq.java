package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the link_sk_seq database table.
 * 
 */
@Entity
@Table(name="link_sk_seq")
@NamedQuery(name="LinkSkSeq.findAll", query="SELECT l FROM LinkSkSeq l")
public class LinkSkSeq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private BigDecimal id;

	public LinkSkSeq() {
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

}