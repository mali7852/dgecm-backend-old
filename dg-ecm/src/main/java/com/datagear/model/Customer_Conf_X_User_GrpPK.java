package com.datagear.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Customer_Conf_X_User_GrpPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "Cust_Conf_Seq_No", insertable = false, updatable = false)
	private long custConfSeqNo;

	@Column(name = "User_Grp_Name")
	private String userGrpName;

	public Customer_Conf_X_User_GrpPK() {
	}

	public long getCustConfSeqNo() {
		return this.custConfSeqNo;
	}

	public void setOccsRk(long CustConfSeqNo) {
		this.custConfSeqNo = CustConfSeqNo;
	}

	public String getUserGrpName() {
		return this.userGrpName;
	}

	public void setUserGrpName(String userGrpName) {
		this.userGrpName = userGrpName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Customer_Conf_X_User_GrpPK)) {
			return false;
		}
		Customer_Conf_X_User_GrpPK castOther = (Customer_Conf_X_User_GrpPK) other;
		return (this.custConfSeqNo == castOther.custConfSeqNo) && this.userGrpName.equals(castOther.userGrpName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.custConfSeqNo ^ (this.custConfSeqNo >>> 32)));
		hash = hash * prime + this.userGrpName.hashCode();

		return hash;
	}
}
