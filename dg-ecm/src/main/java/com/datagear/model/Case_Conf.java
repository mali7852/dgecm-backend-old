package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;


/**
 * The persistent class for the Case_Conf database table.
 */
@Entity
@Table(name = "case_conf")
@NamedQuery(name = "Case_Conf.findAll", query = "SELECT c FROM Case_Conf c")
public class Case_Conf implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Case_Conf_Seq_No")
    private long caseConfSeqNo;

    @Column(name = "Case_Ctgry_Cd")
    private String caseCtgryCd;

    @Column(name = "Case_Sub_Ctgry_Cd")
    private String caseSubCtgryCd;

    @Column(name = "Case_Type_Cd")
    private String caseTypeCd;

    @Column(name = "Invest_WF_Def_Name")
    private String investWFDefName;

    @Column(name = "Investr_User_Id")
    private String investrUserId;

    @Column(name = "Reopen_WF_Def_Name")
    private String reopenWFDefName;

    @Column(name = "UI_Def_File_Name")
    private String uiDefFileName;

    @JsonIgnore
    @OneToMany(mappedBy = "caseConf", fetch = FetchType.LAZY)
    private Set<Case_Conf_X_User_Grp> caseConfXUserGrps;

//    @JsonIgnore
//    @OneToMany(mappedBy = "caseConf", fetch = FetchType.LAZY)
//    private Set<Case_Ver> caseVers;

    public Case_Conf() {
    }

    public long getCaseConfSeqNo() {
        return this.caseConfSeqNo;
    }

    public void setCaseConfSeqNo(long caseConfSeqNo) {
        this.caseConfSeqNo = caseConfSeqNo;
    }

    public String getCaseCtgryCd() {
        return this.caseCtgryCd;
    }

    public void setCaseCtgryCd(String caseCtgryCd) {
        this.caseCtgryCd = caseCtgryCd;
    }

    public String getCaseSubCtgryCd() {
        return this.caseSubCtgryCd;
    }

    public void setCaseSubCtgryCd(String caseSubCtgryCd) {
        this.caseSubCtgryCd = caseSubCtgryCd;
    }

    public String getCaseTypeCd() {
        return this.caseTypeCd;
    }

    public void setCaseTypeCd(String caseTypeCd) {
        this.caseTypeCd = caseTypeCd;
    }

    public String getInvestWFDefName() {
        return this.investWFDefName;
    }

    public void setInvestWFDefName(String investWFDefName) {
        this.investWFDefName = investWFDefName;
    }

    public String getInvestrUserId() {
        return this.investrUserId;
    }

    public void setInvestrUserId(String investrUserId) {
        this.investrUserId = investrUserId;
    }

    public String getReopenWFDefName() {
        return this.reopenWFDefName;
    }

    public void setReopenWFDefName(String reopenWFDefName) {
        this.reopenWFDefName = reopenWFDefName;
    }

    public String getUiDefFileName() {
        return this.uiDefFileName;
    }

    public void setUiDefFileName(String UIDefFileName) {
        this.uiDefFileName = UIDefFileName;
    }

    public Set<Case_Conf_X_User_Grp> getCaseConfXUserGrps() {
        return caseConfXUserGrps;
    }

//    public Set<Case_Ver> getCaseVers() {
//        return caseVers;
//    }
//
//    public void setCaseVers(Set<Case_Ver> caseVers) {
//        this.caseVers = caseVers;
//    }

    public void setCaseConfXUserGrps(Set<Case_Conf_X_User_Grp> caseConfXUserGrps) {
        this.caseConfXUserGrps = caseConfXUserGrps;
    }

    @Override
    public String toString() {
        return "Case_Conf [case_Conf_Seq_No=" + caseConfSeqNo + ", case_Ctgry_Cd=" + caseCtgryCd
                + ", case_Sub_Ctgry_Cd=" + caseSubCtgryCd + ", case_Type_Cd=" + caseTypeCd
                + ", invest_WF_Def_Name=" + investWFDefName + ", investr_User_Id=" + investrUserId
                + ", reopen_WF_Def_Name=" + reopenWFDefName + ", UI_Def_File_Name=" + uiDefFileName + "]";
    }

}