package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the event_rk_seq database table.
 * 
 */
@Entity
@Table(name="event_rk_seq")
@NamedQuery(name="EventRkSeq.findAll", query="SELECT e FROM EventRkSeq e")
public class EventRkSeq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private BigDecimal id;

	public EventRkSeq() {
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

}