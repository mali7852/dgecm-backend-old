package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * The persistent class for the Occurrence_Ver database table.
 * 
 */
@Entity
@NamedQuery(name = "Occurrence_Ver.findAll", query = "SELECT o FROM Occurrence_Ver o")
public class Occurrence_Ver implements Serializable /* , Cloneable */ {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_VerPK id;

	@Column(name = "Close_Date")
	private Timestamp closeDate;

	@Column(name = "Create_Date")
	private Timestamp createDate;

	@Column(name = "Create_User_Id")
	private String createUserId;

	@Column(name = "Delete_Flag")
	private String deleteFlag;

	@Column(name = "Dtction_Date")
	private Date dtctionDate;

	@Column(name = "Dtction_Time")
	private String dtctionTime;

	@Column(name = "Investr_User_Id")
	private String investrUserId;

	@Column(name = "Notif_Date")
	private Date notifDate;

	@Column(name = "Notif_Time")
	private String notifTime;

	@Column(name = "Occs_Ctgry_Cd")
	private String occsCtgryCd;

	@Column(name = "Occs_Desc")
	private String occsDesc;

	@Column(name = "Occs_Dispos_Cd")
	private String occsDisposCd;

	@Column(name = "Occs_From_Date")
	private Date occsFromDate;

	@Column(name = "Occs_From_Time")
	private String occsFromTime;

	@Column(name = "Occs_Id")
	private String occsId;

	@Column(name = "Occs_Stat_Cd")
	private String occsStatCd;

	@Column(name = "Occs_Sub_Ctgry_Cd")
	private String occsSubCtgryCd;

	@Column(name = "Occs_To_Date")
	private Date occsToDate;

	@Column(name = "Occs_To_Time")
	private String occsToTime;

	@Column(name = "Occs_Type_Cd")
	private String occsTypeCd;

	@Column(name = "Src_Sys_Cd")
	private String srcSysCd;

	@Column(name = "UI_Def_File_Name")
	private String uiDefFileName;

	@Column(name = "Update_User_Id")
	private String updateUserId;

	@Column(name = "Valid_To_Date")
	private Timestamp validToDate;

	@Column(name = "Ver_No")
	private BigDecimal verNo;

	@Column(name = "Case_Rk")
	private BigDecimal caseRk;

//	//bi-directional many-to-one association to Occurrence_Udf_Lgchr_Val
//	@JsonIgnore
//	@OneToMany(mappedBy="occurrenceVer")
//	private List<Occurrence_Udf_Lgchr_Val> occurrenceUdfLgchrVals;
//
//	//bi-directional many-to-one association to Occurrence_Udf_No_Val
//	@JsonIgnore
//	@OneToMany(mappedBy="occurrenceVer")
//	private List<Occurrence_Udf_No_Val> occurrenceUdfNoVals;
//
//	//bi-directional many-to-one association to Case_Live
//	@ManyToOne
//	@JoinColumn(name="Case_Rk", insertable=false, updatable=false)
//	private Case_Live caseLive;

	public Occurrence_Ver() {
	}

	public Occurrence_Ver(Long occs_rk, Timestamp valid_From_Date, String occs_id, String occs_type_cd,
			String ui_def_file_name) {
		this.id = new Occurrence_VerPK();
		this.id.setOccsRk(occs_rk);
		this.id.setValidFromDate(valid_From_Date);
		this.createDate = valid_From_Date;
		this.occsId = occs_id;
		this.occsTypeCd = occs_type_cd;
		this.srcSysCd = "DGECM";
		this.createUserId = "DGECM";
		this.verNo = new BigDecimal(1.0);
		this.deleteFlag = "0";
	}

	public BigDecimal getCaseRk() {
		return caseRk;
	}

	public Timestamp getCloseDate() {
		return this.closeDate;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public String getCreateUserId() {
		return this.createUserId;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public Date getDtctionDate() {
		return this.dtctionDate;
	}

	public String getDtctionTime() {
		return this.dtctionTime;
	}

	@JsonUnwrapped
	public Occurrence_VerPK getId() {
		return this.id;
	}

	public String getInvestrUserId() {
		return this.investrUserId;
	}

	public Date getNotifDate() {
		return this.notifDate;
	}

	public String getNotifTime() {
		return this.notifTime;
	}

	public String getOccsCtgryCd() {
		return this.occsCtgryCd;
	}

	public String getOccsDesc() {
		return this.occsDesc;
	}

	public String getOccsDisposCd() {
		return this.occsDisposCd;
	}

	public Date getOccsFromDate() {
		return this.occsFromDate;
	}

	public String getOccsFromTime() {
		return this.occsFromTime;
	}

	public String getOccsId() {
		return this.occsId;
	}

	public String getOccsStatCd() {
		return this.occsStatCd;
	}

	public String getOccsSubCtgryCd() {
		return this.occsSubCtgryCd;
	}

	public Date getOccsToDate() {
		return this.occsToDate;
	}

	public String getOccsToTime() {
		return this.occsToTime;
	}

	public String getOccsTypeCd() {
		return this.occsTypeCd;
	}

	public String getSrcSysCd() {
		return this.srcSysCd;
	}

	public String getUiDefFileName() {
		return this.uiDefFileName;
	}

	public String getUpdateUserId() {
		return this.updateUserId;
	}

	public Timestamp getValidToDate() {
		return this.validToDate;
	}

	public BigDecimal getVerNo() {
		return this.verNo;
	}

	public void setCaseRk(BigDecimal caseRk) {
		this.caseRk = caseRk;
	}

	public void setCloseDate(Timestamp closeDate) {
		this.closeDate = closeDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public void setDtctionDate(Date dtctionDate) {
		this.dtctionDate = dtctionDate;
	}

	public void setDtctionTime(String dtctionTime) {
		this.dtctionTime = dtctionTime;
	}

	public void setId(Occurrence_VerPK id) {
		this.id = id;
	}

	public void setInvestrUserId(String investrUserId) {
		this.investrUserId = investrUserId;
	}

	public void setNotifDate(Date notifDate) {
		this.notifDate = notifDate;
	}

	public void setNotifTime(String notifTime) {
		this.notifTime = notifTime;
	}

	public void setOccsCtgryCd(String occsCtgryCd) {
		this.occsCtgryCd = occsCtgryCd;
	}

	public void setOccsDesc(String occsDesc) {
		this.occsDesc = occsDesc;
	}

	public void setOccsDisposCd(String occsDisposCd) {
		this.occsDisposCd = occsDisposCd;
	}

	public void setOccsFromDate(Date occsFromDate) {
		this.occsFromDate = occsFromDate;
	}

	public void setOccsFromTime(String occsFromTime) {
		this.occsFromTime = occsFromTime;
	}

	public void setOccsId(String occsId) {
		this.occsId = occsId;
	}

	public void setOccsStatCd(String occsStatCd) {
		this.occsStatCd = occsStatCd;
	}

	public void setOccsSubCtgryCd(String occsSubCtgryCd) {
		this.occsSubCtgryCd = occsSubCtgryCd;
	}

	public void setOccsToDate(Date occsToDate) {
		this.occsToDate = occsToDate;
	}

	public void setOccsToTime(String occsToTime) {
		this.occsToTime = occsToTime;
	}

	public void setOccsTypeCd(String occsTypeCd) {
		this.occsTypeCd = occsTypeCd;
	}

	public void setSrcSysCd(String srcSysCd) {
		this.srcSysCd = srcSysCd;
	}

	public void setUiDefFileName(String UIDefFileName) {
		this.uiDefFileName = UIDefFileName;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public void setValidToDate(Timestamp validToDate) {
		this.validToDate = validToDate;
	}

	public void setVerNo(BigDecimal verNo) {
		this.verNo = verNo;
	}

	@Override
	public String toString() {
		return "Occurrence_Ver [id=" + id.getOccsRk() + ", validFromDate=" + id.getValidFromDate() + ", closeDate="
				+ closeDate + ", createDate=" + createDate + ", createUserId=" + createUserId + ", deleteFlag="
				+ deleteFlag + ", dtctionDate=" + dtctionDate + ", dtctionTime=" + dtctionTime + ", investrUserId="
				+ investrUserId + ", notifDate=" + notifDate + ", notifTime=" + notifTime + ", occsCtgryCd="
				+ occsCtgryCd + ", occsDesc=" + occsDesc + ", occsDisposCd=" + occsDisposCd + ", occsFromDate="
				+ occsFromDate + ", occsFromTime=" + occsFromTime + ", occsId=" + occsId + ", occsStatCd=" + occsStatCd
				+ ", occsSubCtgryCd=" + occsSubCtgryCd + ", occsToDate=" + occsToDate + ", occsToTime=" + occsToTime
				+ ", occsTypeCd=" + occsTypeCd + ", srcSysCd=" + srcSysCd + ", uiDefFileName=" + uiDefFileName
				+ ", updateUserId=" + updateUserId + ", validToDate=" + validToDate + ", verNo=" + verNo + ", caseRk="
				+ caseRk + "]";
	}

//	Need Clonable implementation
//	public Object clone() throws CloneNotSupportedException {
//		return super.clone();
//	}

//	public List<Occurrence_Udf_Lgchr_Val> getOccurrenceUdfLgchrVals() {
//		return this.occurrenceUdfLgchrVals;
//	}

//	public void setOccurrenceUdfLgchrVals(List<Occurrence_Udf_Lgchr_Val> occurrenceUdfLgchrVals) {
//		this.occurrenceUdfLgchrVals = occurrenceUdfLgchrVals;
//	}

//	public Occurrence_Udf_Lgchr_Val addOccurrenceUdfLgchrVal(Occurrence_Udf_Lgchr_Val occurrenceUdfLgchrVal) {
//		getOccurrenceUdfLgchrVals().add(occurrenceUdfLgchrVal);
//		occurrenceUdfLgchrVal.setOccurrenceVer(this);
//
//		return occurrenceUdfLgchrVal;
//	}

//	public Occurrence_Udf_Lgchr_Val removeOccurrenceUdfLgchrVal(Occurrence_Udf_Lgchr_Val occurrenceUdfLgchrVal) {
//		getOccurrenceUdfLgchrVals().remove(occurrenceUdfLgchrVal);
//		occurrenceUdfLgchrVal.setOccurrenceVer(null);
//
//		return occurrenceUdfLgchrVal;
//	}

//	public List<Occurrence_Udf_No_Val> getOccurrenceUdfNoVals() {
//		return this.occurrenceUdfNoVals;
//	}

//	public void setOccurrenceUdfNoVals(List<Occurrence_Udf_No_Val> occurrenceUdfNoVals) {
//		this.occurrenceUdfNoVals = occurrenceUdfNoVals;
//	}

//	public Occurrence_Udf_No_Val addOccurrenceUdfNoVal(Occurrence_Udf_No_Val occurrenceUdfNoVal) {
//		getOccurrenceUdfNoVals().add(occurrenceUdfNoVal);
//		occurrenceUdfNoVal.setOccurrenceVer(this);
//
//		return occurrenceUdfNoVal;
//	}

//	public Occurrence_Udf_No_Val removeOccurrenceUdfNoVal(Occurrence_Udf_No_Val occurrenceUdfNoVal) {
//		getOccurrenceUdfNoVals().remove(occurrenceUdfNoVal);
//		occurrenceUdfNoVal.setOccurrenceVer(null);
//
//		return occurrenceUdfNoVal;
//	}
//
//	public Case_Live getCaseLive() {
//		return this.caseLive;
//	}
//
//	public void setCaseLive(Case_Live caseLive) {
//		this.caseLive = caseLive;
//	}

}