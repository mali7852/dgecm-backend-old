package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Customer_Conf_X_User_Grp database table.
 * 
 */
@Entity
@NamedQuery(name="Customer_Conf_X_User_Grp.findAll", query="SELECT c FROM Customer_Conf_X_User_Grp c")
public class Customer_Conf_X_User_Grp implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Customer_Conf_X_User_GrpPK id;

	public Customer_Conf_X_User_Grp() {
	}

	public Customer_Conf_X_User_GrpPK getId() {
		return this.id;
	}

	public void setId(Customer_Conf_X_User_GrpPK id) {
		this.id = id;
	}

}