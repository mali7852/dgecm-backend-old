package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Case_X_User_Grp database table.
 * 
 */
@Embeddable
public class Case_X_User_GrpPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Case_Rk", insertable=false, updatable=false)
	private long caseRk;

	@Column(name="User_Grp_Name")
	private String userGrpName;

	public Case_X_User_GrpPK() {
	}
	public long getCaseRk() {
		return this.caseRk;
	}
	public void setCaseRk(long caseRk) {
		this.caseRk = caseRk;
	}
	public String getUserGrpName() {
		return this.userGrpName;
	}
	public void setUserGrpName(String userGrpName) {
		this.userGrpName = userGrpName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Case_X_User_GrpPK)) {
			return false;
		}
		Case_X_User_GrpPK castOther = (Case_X_User_GrpPK)other;
		return 
			(this.caseRk == castOther.caseRk)
			&& this.userGrpName.equals(castOther.userGrpName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.caseRk ^ (this.caseRk >>> 32)));
		hash = hash * prime + this.userGrpName.hashCode();
		
		return hash;
	}
}