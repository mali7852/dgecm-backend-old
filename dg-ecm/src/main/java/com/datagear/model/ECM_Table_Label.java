package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ECM_Table_Label database table.
 * 
 */
@Entity
@NamedQuery(name="ECM_Table_Label.findAll", query="SELECT e FROM ECM_Table_Label e")
public class ECM_Table_Label implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ECM_Table_LabelPK id;

	@Column(name="Label_Text")
	private String labelText;

	//bi-directional many-to-one association to ECM_Locale
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="Locale", insertable=false, updatable=false)
	private ECM_Locale ecmLocale;

	public ECM_Table_Label() {
	}

	public ECM_Table_LabelPK getId() {
		return this.id;
	}

	public void setId(ECM_Table_LabelPK id) {
		this.id = id;
	}

	public String getLabelText() {
		return this.labelText;
	}

	public void setLabelText(String labelText) {
		this.labelText = labelText;
	}

	public ECM_Locale getEcmLocale() {
		return this.ecmLocale;
	}

	public void setEcmLocale(ECM_Locale ecmLocale) {
		this.ecmLocale = ecmLocale;
	}

}