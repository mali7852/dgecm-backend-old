package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the Occurrence_X_User_Grp database table.
 * 
 */
@Entity
@NamedQuery(name = "Occurrence_X_User_Grp.findAll", query = "SELECT o FROM Occurrence_X_User_Grp o")
public class Occurrence_X_User_Grp implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_X_User_GrpPK id;

	// bi-directional many-to-one association to Occurrence_Live
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Occs_Rk", insertable = false, updatable = false)
	private Occurrence_Live occurrenceLive;

	public Occurrence_X_User_Grp() {
	}

	public Occurrence_X_User_Grp(Long occs_rk, String user_grp_name) {
		this.id = new Occurrence_X_User_GrpPK();
		this.id.setOccsRk(occs_rk);
		this.id.setUserGrpName(user_grp_name);
	}

	public Occurrence_X_User_GrpPK getId() {
		return this.id;
	}

	public void setId(Occurrence_X_User_GrpPK id) {
		this.id = id;
	}

	public Occurrence_Live getOccurrenceLive() {
		return this.occurrenceLive;
	}

	public void setOccurrenceLive(Occurrence_Live occurrenceLive) {
		this.occurrenceLive = occurrenceLive;
	}

}