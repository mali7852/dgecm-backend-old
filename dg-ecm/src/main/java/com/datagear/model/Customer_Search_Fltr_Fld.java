package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the Customer_Search_Fltr_Fld database table.
 * 
 */
@Entity
@NamedQuery(name="Customer_Search_Fltr_Fld.findAll", query="SELECT c FROM Customer_Search_Fltr_Fld c")
public class Customer_Search_Fltr_Fld implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Customer_Search_Fltr_FldPK id;

	@Column(name="Display_Ordr_No")
	private BigDecimal displayOrdrNo;

	@Column(name="Ref_Table_Name")
	private String refTableName;

	public Customer_Search_Fltr_Fld() {
	}

	public Customer_Search_Fltr_FldPK getId() {
		return this.id;
	}

	public void setId(Customer_Search_Fltr_FldPK id) {
		this.id = id;
	}

	public BigDecimal getDisplayOrdrNo() {
		return this.displayOrdrNo;
	}

	public void setDisplayOrdrNo(BigDecimal displayOrdrNo) {
		this.displayOrdrNo = displayOrdrNo;
	}

	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

}