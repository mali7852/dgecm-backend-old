package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Customer_X_User_Grp database table.
 * 
 */
@Embeddable
public class Customer_X_User_GrpPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Cust_Rk", insertable = false, updatable = false)
	private long custRk;

	@Column(name = "User_Grp_Name")
	private String userGrpName;

	public Customer_X_User_GrpPK() {
	}

	public long getCustRk() {
		return this.custRk;
	}

	public void setCustRk(long custRk) {
		this.custRk = custRk;
	}

	public String getUserGrpName() {
		return this.userGrpName;
	}

	public void setUserGrpName(String userGrpName) {
		this.userGrpName = userGrpName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Customer_X_User_GrpPK)) {
			return false;
		}
		Customer_X_User_GrpPK castOther = (Customer_X_User_GrpPK) other;
		return (this.custRk == castOther.custRk) && this.userGrpName.equals(castOther.userGrpName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.custRk ^ (this.custRk >>> 32)));
		hash = hash * prime + this.userGrpName.hashCode();

		return hash;
	}
}