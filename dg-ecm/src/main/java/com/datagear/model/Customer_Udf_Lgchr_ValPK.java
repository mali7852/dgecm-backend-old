package com.datagear.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The primary key class for the Customer_Udf_Lgchr_Val database table.
 */
@Embeddable
public class Customer_Udf_Lgchr_ValPK implements Serializable {
    // default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "Cust_Rk", insertable = false, updatable = false)
    private long custRk;

    @Column(name = "Valid_From_Date", insertable = false, updatable = false)
    private Timestamp validFromDate;

    @Column(name = "Udf_Table_Name", insertable = false, updatable = false)
    private String udfTableName;

    @Column(name = "Udf_Name", insertable = false, updatable = false)
    private String udfName;

    @Column(name = "Row_No")
    private long rowNo;

    public Customer_Udf_Lgchr_ValPK() {
    }

    public long getCustRk() {
        return this.custRk;
    }

    public void setCustRk(long custRk) {
        this.custRk = custRk;
    }

    public Timestamp getValidFromDate() {
        return this.validFromDate;
    }

    public void setValidFromDate(Timestamp validFromDate) {
        this.validFromDate = validFromDate;
    }

    public String getUdfTableName() {
        return this.udfTableName;
    }

    public void setUdfTableName(String udfTableName) {
        this.udfTableName = udfTableName;
    }

    public String getUdfName() {
        return this.udfName;
    }

    public void setUdfName(String udfName) {
        this.udfName = udfName;
    }

    public long getRowNo() {
        return this.rowNo;
    }

    public void setRowNo(long rowNo) {
        this.rowNo = rowNo;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Customer_Udf_Lgchr_ValPK)) {
            return false;
        }
        Customer_Udf_Lgchr_ValPK castOther = (Customer_Udf_Lgchr_ValPK) other;
        return (this.custRk == castOther.custRk) && this.validFromDate.equals(castOther.validFromDate)
                && this.udfTableName.equals(castOther.udfTableName) && this.udfName.equals(castOther.udfName)
                && (this.rowNo == castOther.rowNo);
    }

    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.custRk ^ (this.custRk >>> 32)));
        hash = hash * prime + this.validFromDate.hashCode();
        hash = hash * prime + this.udfTableName.hashCode();
        hash = hash * prime + this.udfName.hashCode();
        hash = hash * prime + ((int) (this.rowNo ^ (this.rowNo >>> 32)));

        return hash;
    }
}