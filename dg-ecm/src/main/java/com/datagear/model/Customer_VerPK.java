package com.datagear.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;

/**
 * The primary key class for the Customer_Ver database table.
 * 
 */
@Embeddable
public class Customer_VerPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Cust_Rk")
	private long custRk;

	@Column(name = "Valid_From_Date")
	private Timestamp validFromDate;

	public Customer_VerPK() {
	}

	public long getCustRk() {
		return this.custRk;
	}

	public void setCustRk(long custRk) {
		this.custRk = custRk;
	}

	public Timestamp getValidFromDate() {
		return this.validFromDate;
	}

	public void setValidFromDate(Timestamp validFromDate) {
		this.validFromDate = validFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Customer_VerPK)) {
			return false;
		}
		Customer_VerPK castOther = (Customer_VerPK) other;
		return (this.custRk == castOther.custRk) && this.validFromDate.equals(castOther.validFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.custRk ^ (this.custRk >>> 32)));
		hash = hash * prime + this.validFromDate.hashCode();

		return hash;
	}
}