package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Customer_Search_Fltr_Fld database table.
 * 
 */
@Embeddable
public class ECM_Column_LabelPK implements Serializable{
	private static final long serialVersionUID = 1L;

	@Column(name="Source_Name")
	private String sourceName;

	@Column(name="Obj_Name")
	private String objName;

	@Column(name="Table_Name")
	private String tableName;

	@Column(name="Column_Name")
	private String columnName;

	public ECM_Column_LabelPK(){
		
	}
	
	public String getColumn_Name() {
		return columnName;
	}

	public String getObj_Name() {
		return objName;
	}

	public String getSource_Name() {
		return sourceName;
	}

	public String getTable_Name() {
		return tableName;
	}

	public void setColumn_Name(String column_Name) {
		this.columnName = column_Name;
	}

	public void setObj_Name(String obj_Name) {
		this.objName = obj_Name;
	}

	public void setSource_Name(String source_Name) {
		this.sourceName = source_Name;
	}
	
	public void setTable_Name(String table_Name) {
		this.tableName = table_Name;
	}
	
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ECM_Column_LabelPK)) {
			return false;
		}
		ECM_Column_LabelPK castOther = (ECM_Column_LabelPK)other;
		return 
			this.sourceName.equals(castOther.sourceName)
			&& this.objName.equals(castOther.objName)
			&& this.tableName.equals(castOther.tableName)
			&& this.columnName.equals(castOther.columnName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.sourceName.hashCode();
		hash = hash * prime + this.objName.hashCode();
		hash = hash * prime + this.tableName.hashCode();
		hash = hash * prime + this.columnName.hashCode();
		
		return hash;
	}
	
}
