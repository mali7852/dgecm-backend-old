package com.datagear.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the ECM_Locale database table.
 * 
 */
@Entity
@NamedQuery(name="ECM_Locale.findAll", query="SELECT e FROM ECM_Locale e")
public class ECM_Locale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Locale")
	private String locale;

	@Column(name="Fallback_Locale")
	private String fallbackLocale;

	//bi-directional many-to-one association to ECM_Table_Label
	@JsonIgnore
	@OneToMany(mappedBy="ecmLocale")
	private List<ECM_Table_Label> ecmTableLabels;

	public ECM_Locale() {
	}

	public String getLocale() {
		return this.locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getFallbackLocale() {
		return this.fallbackLocale;
	}

	public void setFallbackLocale(String fallbackLocale) {
		this.fallbackLocale = fallbackLocale;
	}

	public List<ECM_Table_Label> getEcmTableLabels() {
		return this.ecmTableLabels;
	}

	public void setEcmTableLabels(List<ECM_Table_Label> ecmTableLabels) {
		this.ecmTableLabels = ecmTableLabels;
	}

	public ECM_Table_Label addEcmTableLabel(ECM_Table_Label ecmTableLabel) {
		getEcmTableLabels().add(ecmTableLabel);
		ecmTableLabel.setEcmLocale(this);

		return ecmTableLabel;
	}

	public ECM_Table_Label removeEcmTableLabel(ECM_Table_Label ecmTableLabel) {
		getEcmTableLabels().remove(ecmTableLabel);
		ecmTableLabel.setEcmLocale(null);

		return ecmTableLabel;
	}

}