package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the efile_rk_seq database table.
 * 
 */
@Entity
@Table(name="efile_rk_seq")
@NamedQuery(name="EfileRkSeq.findAll", query="SELECT e FROM EfileRkSeq e")
public class EfileRkSeq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private BigDecimal id;

	public EfileRkSeq() {
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

}