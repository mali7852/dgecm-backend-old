package com.datagear.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Efile_X_User_GrpPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "Efile_Rk", insertable = false, updatable = false)
	private long efileRk;

	@Column(name = "User_Grp_Name")
	private String userGrpName;

	public Efile_X_User_GrpPK() {
	}

	public long getEfileRk() {
		return this.efileRk;
	}

	public void setEfileRk(long EfileRk) {
		this.efileRk = EfileRk;
	}

	public String getUserGrpName() {
		return this.userGrpName;
	}

	public void setUserGrpName(String userGrpName) {
		this.userGrpName = userGrpName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Efile_X_User_GrpPK)) {
			return false;
		}
		Efile_X_User_GrpPK castOther = (Efile_X_User_GrpPK) other;
		return (this.efileRk == castOther.efileRk) && this.userGrpName.equals(castOther.userGrpName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.efileRk ^ (this.efileRk >>> 32)));
		hash = hash * prime + this.userGrpName.hashCode();

		return hash;
	}
}
