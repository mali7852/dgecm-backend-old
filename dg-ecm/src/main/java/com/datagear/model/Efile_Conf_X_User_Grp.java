package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Efile_Conf_X_User_Grp database table.
 * 
 */
@Entity
@NamedQuery(name="Efile_Conf_X_User_Grp.findAll", query="SELECT e FROM Efile_Conf_X_User_Grp e")
public class Efile_Conf_X_User_Grp implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Efile_Conf_X_User_GrpPK id;

	public Efile_Conf_X_User_Grp() {
	}

	public Efile_Conf_X_User_GrpPK getId() {
		return this.id;
	}

	public void setId(Efile_Conf_X_User_GrpPK id) {
		this.id = id;
	}

}