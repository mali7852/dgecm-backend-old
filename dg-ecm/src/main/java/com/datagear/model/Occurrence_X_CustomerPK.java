package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Occurrence_X_Customer database table.
 * 
 */
@Embeddable
public class Occurrence_X_CustomerPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Occs_Rk", insertable = false, updatable = false)
	private long occsRk;

	@Column(name = "Cust_Rk", insertable = false, updatable = false)
	private long custRk;

	@Column(name = "Rel_Type_Cd")
	private String relTypeCd;

	public Occurrence_X_CustomerPK() {
	}

	public long getOccsRk() {
		return this.occsRk;
	}

	public void setOccsRk(long occsRk) {
		this.occsRk = occsRk;
	}

	public long getCustRk() {
		return this.custRk;
	}

	public void setCustRk(long custRk) {
		this.custRk = custRk;
	}

	public String getRelTypeCd() {
		return this.relTypeCd;
	}

	public void setRelTypeCd(String relTypeCd) {
		this.relTypeCd = relTypeCd;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Occurrence_X_CustomerPK)) {
			return false;
		}
		Occurrence_X_CustomerPK castOther = (Occurrence_X_CustomerPK) other;
		return (this.occsRk == castOther.occsRk) && (this.custRk == castOther.custRk)
				&& this.relTypeCd.equals(castOther.relTypeCd);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.occsRk ^ (this.occsRk >>> 32)));
		hash = hash * prime + ((int) (this.custRk ^ (this.custRk >>> 32)));
		hash = hash * prime + this.relTypeCd.hashCode();

		return hash;
	}
}