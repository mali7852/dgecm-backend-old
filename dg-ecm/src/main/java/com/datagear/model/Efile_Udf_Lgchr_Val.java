package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the Efile_Udf_Lgchr_Val database table.
 * 
 */
@Entity
@NamedQuery(name = "Efile_Udf_Lgchr_Val.findAll", query = "SELECT e FROM Efile_Udf_Lgchr_Val e")
public class Efile_Udf_Lgchr_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Efile_Udf_Lgchr_ValPK id;

	@Column(name = "Udf_Val")
	private String udfVal;

	// bi-directional many-to-one association to Efile_Udf_Def
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "Udf_Name", referencedColumnName = "Udf_Name", insertable = false, updatable = false),
			@JoinColumn(name = "Udf_Table_Name", referencedColumnName = "Udf_Table_Name", insertable = false, updatable = false) })
	private Efile_Udf_Def efileUdfDef;

	// bi-directional many-to-one association to Efile_Ver
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "Efile_Rk", referencedColumnName = "Efile_Rk", insertable = false, updatable = false),
			@JoinColumn(name = "Valid_From_Date", referencedColumnName = "Valid_From_Date", insertable = false, updatable = false) })
	private Efile_Ver efileVer;

	public Efile_Udf_Lgchr_Val() {
	}

	public Efile_Udf_Lgchr_ValPK getId() {
		return this.id;
	}

	public void setId(Efile_Udf_Lgchr_ValPK id) {
		this.id = id;
	}

	public String getUdfVal() {
		return this.udfVal;
	}

	public void setUdfVal(String udfVal) {
		this.udfVal = udfVal;
	}

	public Efile_Udf_Def getEfileUdfDef() {
		return this.efileUdfDef;
	}

	public void setEfileUdfDef(Efile_Udf_Def efileUdfDef) {
		this.efileUdfDef = efileUdfDef;
	}

	public Efile_Ver getEfileVer() {
		return this.efileVer;
	}

	public void setEfileVer(Efile_Ver efileVer) {
		this.efileVer = efileVer;
	}

}