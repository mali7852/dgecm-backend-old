package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Case_X_Cust database table.
 * 
 */
@Entity
@NamedQuery(name="Case_X_Cust.findAll", query="SELECT o FROM Case_X_Cust o")
public class Case_X_Cust implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Case_X_CustPK id;

	@Column(name="Create_Date")
	private Timestamp createDate;

	@Column(name="Rel_Desc")
	private String relDesc;

	//bi-directional many-to-one association to Case_Live
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="Case_Rk", insertable=false, updatable=false)
	private Case_Live caseLive;

	//bi-directional many-to-one association to Customer_Live
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="Cust_Rk", insertable=false, updatable=false)
	private Customer_Live customerLive;

	public Case_X_Cust() {
	}

	public Case_X_Cust(long entityRk, long memberRk, Timestamp create_Date) {
		this.id = new Case_X_CustPK();
		this.id.setCaseRk(entityRk);
		this.id.setCustRk(memberRk);
		this.id.setRelTypeCd("R");
		this.setRelDesc("Relation Created Through Link Party WS");
		this.setCreateDate(create_Date);
	}

	public Case_X_CustPK getId() {
		return this.id;
	}

	public void setId(Case_X_CustPK id) {
		this.id = id;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getRelDesc() {
		return this.relDesc;
	}

	public void setRelDesc(String relDesc) {
		this.relDesc = relDesc;
	}

	public Case_Live getcaseLive() {
		return this.caseLive;
	}

	public void setcaseLive(Case_Live caseLive) {
		this.caseLive = caseLive;
	}

	public Customer_Live getcustomerLive() {
		return this.customerLive;
	}

	public void setcustomerLive(Customer_Live customerLive) {
		this.customerLive = customerLive;
	}

}