package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Customer_Search_Fltr_Fld database table.
 * 
 */
@Embeddable
public class Customer_Search_Fltr_FldPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "User_Id")
	private String userId;

	@Column(name = "Table_Name")
	private String tableName;

	@Column(name = "Fld_Name")
	private String fldName;

	public Customer_Search_Fltr_FldPK() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getFldName() {
		return this.fldName;
	}

	public void setFldName(String fldName) {
		this.fldName = fldName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Customer_Search_Fltr_FldPK)) {
			return false;
		}
		Customer_Search_Fltr_FldPK castOther = (Customer_Search_Fltr_FldPK) other;
		return this.userId.equals(castOther.userId) && this.tableName.equals(castOther.tableName)
				&& this.fldName.equals(castOther.fldName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userId.hashCode();
		hash = hash * prime + this.tableName.hashCode();
		hash = hash * prime + this.fldName.hashCode();

		return hash;
	}
}