package com.datagear.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the Efile_Live database table.
 * 
 */
@Entity
@NamedQuery(name="Efile_Live.findAll", query="SELECT e FROM Efile_Live e")
public class Efile_Live implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Efile_Rk")
	private long efileRk;

	@Column(name="Correction_Flg")
	private String correctionFlg;

	@Column(name="Coverage_End_Dt")
	private Date coverageEndDt;

	@Column(name="Coverage_Start_Dt")
	private Date coverageStartDt;

	@Column(name="Create_Date")
	private Timestamp createDate;

	@Column(name="Create_User_Id")
	private String createUserId;

	@Column(name="Delete_Flag")
	private String deleteFlag;

	@Column(name="Efile_Agency_Ref_Id")
	private String efileAgencyRefId;

	@Column(name="Efile_Agency_Status_Cd")
	private String efileAgencyStatusCd;

	@Column(name="Efile_Ctgry_Cd")
	private String efileCtgryCd;

	@Column(name="Efile_Desc")
	private String efileDesc;

	@Column(name="Efile_Id")
	private String efileId;

	@Column(name="Efile_Stat_Cd")
	private String efileStatCd;

	@Column(name="Efile_Sub_Ctgry_Cd")
	private String efileSubCtgryCd;

	@Column(name="Efile_Type_Cd")
	private String efileTypeCd;

	@Column(name="Form_Conf_Rk")
	private BigDecimal formConfRk;

	@Column(name="Output_Create_Date")
	private Timestamp outputCreateDate;

	@Column(name="Output_File_Size")
	private BigDecimal outputFileSize;

	@Column(name="Output_Line_Cnt")
	private BigDecimal outputLineCnt;

	@Column(name="Output_Name")
	private String outputName;

	@Column(name="Output_Path_Name")
	private String outputPathName;

	@Column(name="Output_Rr_Cnt")
	private BigDecimal outputRrCnt;

	@Column(name="Owner_User_Id")
	private String ownerUserId;

	@Column(name="Src_Sys_Cd")
	private String srcSysCd;

	@Column(name="Transmission_Dttm")
	private Timestamp transmissionDttm;

	@Column(name="Transmission_Id")
	private String transmissionId;

	@Column(name="UI_Def_File_Name")
	private String uiDefFileName;

	@Column(name="Update_User_Id")
	private String updateUserId;

	@Column(name="Valid_From_Date")
	private Timestamp validFromDate;

	@Column(name="Valid_To_Date")
	private Timestamp validToDate;

	@Column(name="Ver_No")
	private BigDecimal verNo;

	public Efile_Live() {
	}

	public long getEfileRk() {
		return this.efileRk;
	}

	public void setEfileRk(long efileRk) {
		this.efileRk = efileRk;
	}

	public String getCorrectionFlg() {
		return this.correctionFlg;
	}

	public void setCorrectionFlg(String correctionFlg) {
		this.correctionFlg = correctionFlg;
	}

	public Date getCoverageEndDt() {
		return this.coverageEndDt;
	}

	public void setCoverageEndDt(Date coverageEndDt) {
		this.coverageEndDt = coverageEndDt;
	}

	public Date getCoverageStartDt() {
		return this.coverageStartDt;
	}

	public void setCoverageStartDt(Date coverageStartDt) {
		this.coverageStartDt = coverageStartDt;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserId() {
		return this.createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getEfileAgencyRefId() {
		return this.efileAgencyRefId;
	}

	public void setEfileAgencyRefId(String efileAgencyRefId) {
		this.efileAgencyRefId = efileAgencyRefId;
	}

	public String getEfileAgencyStatusCd() {
		return this.efileAgencyStatusCd;
	}

	public void setEfileAgencyStatusCd(String efileAgencyStatusCd) {
		this.efileAgencyStatusCd = efileAgencyStatusCd;
	}

	public String getEfileCtgryCd() {
		return this.efileCtgryCd;
	}

	public void setEfileCtgryCd(String efileCtgryCd) {
		this.efileCtgryCd = efileCtgryCd;
	}

	public String getEfileDesc() {
		return this.efileDesc;
	}

	public void setEfileDesc(String efileDesc) {
		this.efileDesc = efileDesc;
	}

	public String getEfileId() {
		return this.efileId;
	}

	public void setEfileId(String efileId) {
		this.efileId = efileId;
	}

	public String getEfileStatCd() {
		return this.efileStatCd;
	}

	public void setEfileStatCd(String efileStatCd) {
		this.efileStatCd = efileStatCd;
	}

	public String getEfileSubCtgryCd() {
		return this.efileSubCtgryCd;
	}

	public void setEfileSubCtgryCd(String efileSubCtgryCd) {
		this.efileSubCtgryCd = efileSubCtgryCd;
	}

	public String getEfileTypeCd() {
		return this.efileTypeCd;
	}

	public void setEfileTypeCd(String efileTypeCd) {
		this.efileTypeCd = efileTypeCd;
	}

	public BigDecimal getFormConfRk() {
		return this.formConfRk;
	}

	public void setFormConfRk(BigDecimal formConfRk) {
		this.formConfRk = formConfRk;
	}

	public Timestamp getOutputCreateDate() {
		return this.outputCreateDate;
	}

	public void setOutputCreateDate(Timestamp outputCreateDate) {
		this.outputCreateDate = outputCreateDate;
	}

	public BigDecimal getOutputFileSize() {
		return this.outputFileSize;
	}

	public void setOutputFileSize(BigDecimal outputFileSize) {
		this.outputFileSize = outputFileSize;
	}

	public BigDecimal getOutputLineCnt() {
		return this.outputLineCnt;
	}

	public void setOutputLineCnt(BigDecimal outputLineCnt) {
		this.outputLineCnt = outputLineCnt;
	}

	public String getOutputName() {
		return this.outputName;
	}

	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

	public String getOutputPathName() {
		return this.outputPathName;
	}

	public void setOutputPathName(String outputPathName) {
		this.outputPathName = outputPathName;
	}

	public BigDecimal getOutputRrCnt() {
		return this.outputRrCnt;
	}

	public void setOutputRrCnt(BigDecimal outputRrCnt) {
		this.outputRrCnt = outputRrCnt;
	}

	public String getOwnerUserId() {
		return this.ownerUserId;
	}

	public void setOwnerUserId(String ownerUserId) {
		this.ownerUserId = ownerUserId;
	}

	public String getSrcSysCd() {
		return this.srcSysCd;
	}

	public void setSrcSysCd(String srcSysCd) {
		this.srcSysCd = srcSysCd;
	}

	public Timestamp getTransmissionDttm() {
		return this.transmissionDttm;
	}

	public void setTransmissionDttm(Timestamp transmissionDttm) {
		this.transmissionDttm = transmissionDttm;
	}

	public String getTransmissionId() {
		return this.transmissionId;
	}

	public void setTransmissionId(String transmissionId) {
		this.transmissionId = transmissionId;
	}

	public String getUiDefFileName() {
		return this.uiDefFileName;
	}

	public void setUiDefFileName(String UIDefFileName) {
		this.uiDefFileName = UIDefFileName;
	}

	public String getUpdateUserId() {
		return this.updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getValidFromDate() {
		return this.validFromDate;
	}

	public void setValidFromDate(Timestamp validFromDate) {
		this.validFromDate = validFromDate;
	}

	public Timestamp getValidToDate() {
		return this.validToDate;
	}

	public void setValidToDate(Timestamp validToDate) {
		this.validToDate = validToDate;
	}

	public BigDecimal getVerNo() {
		return this.verNo;
	}

	public void setVerNo(BigDecimal verNo) {
		this.verNo = verNo;
	}

}