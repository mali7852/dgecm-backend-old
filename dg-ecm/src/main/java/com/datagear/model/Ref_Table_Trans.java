package com.datagear.model;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the Ref_Table_Trans database table.
 * 
 */
@Entity
@NamedQuery(name = "Ref_Table_Trans.findAll", query = "SELECT r FROM Ref_Table_Trans r")
public class Ref_Table_Trans implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Ref_Table_TransPK id;

	@Column(name = "Val_Desc")
	private String valDesc;

	// bi-directional many-to-one association to Ref_Table_Val
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "Ref_Table_Name", referencedColumnName = "Ref_Table_Name", insertable = false, updatable = false),
			@JoinColumn(name = "Val_Cd", referencedColumnName = "Val_Cd", insertable = false, updatable = false) })
	private Ref_Table_Val refTablVal;

	public Ref_Table_Trans() {
	}

	public Ref_Table_TransPK getId() {
		return this.id;
	}

	public Ref_Table_Val getRefTablVal() {
		return refTablVal;
	}

	public String getValDesc() {
		return this.valDesc;
	}

	public void setId(Ref_Table_TransPK id) {
		this.id = id;
	}

	public void setRefTablVal(Ref_Table_Val refTablVal) {
		this.refTablVal = refTablVal;
	}

	public void setValDesc(String valDesc) {
		this.valDesc = valDesc;
	}

}