package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Efile_Udf_Char_Val database table.
 * 
 */
@Embeddable
public class Efile_Udf_Char_ValPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Efile_Rk")
	private long efileRk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Valid_From_Date")
	private java.util.Date validFromDate;

	@Column(name = "Udf_Table_Name")
	private String udfTableName;

	@Column(name = "Udf_Name")
	private String udfName;

	@Column(name = "Row_No")
	private long rowNo;

	public Efile_Udf_Char_ValPK() {
	}

	public long getEfileRk() {
		return this.efileRk;
	}

	public void setEfileRk(long EfileRk) {
		this.efileRk = EfileRk;
	}

	public java.util.Date getValidFromDate() {
		return this.validFromDate;
	}

	public void setValidFromDate(java.util.Date validFromDate) {
		this.validFromDate = validFromDate;
	}

	public String getUdfTableName() {
		return this.udfTableName;
	}

	public void setUdfTableName(String udfTableName) {
		this.udfTableName = udfTableName;
	}

	public String getUdfName() {
		return this.udfName;
	}

	public void setUdfName(String udfName) {
		this.udfName = udfName;
	}

	public long getRowNo() {
		return this.rowNo;
	}

	public void setRowNo(long rowNo) {
		this.rowNo = rowNo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Efile_Udf_Char_ValPK)) {
			return false;
		}
		Efile_Udf_Char_ValPK castOther = (Efile_Udf_Char_ValPK) other;
		return (this.efileRk == castOther.efileRk) && this.validFromDate.equals(castOther.validFromDate)
				&& this.udfTableName.equals(castOther.udfTableName) && this.udfName.equals(castOther.udfName)
				&& (this.rowNo == castOther.rowNo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.efileRk ^ (this.efileRk >>> 32)));
		hash = hash * prime + this.validFromDate.hashCode();
		hash = hash * prime + this.udfTableName.hashCode();
		hash = hash * prime + this.udfName.hashCode();
		hash = hash * prime + ((int) (this.rowNo ^ (this.rowNo >>> 32)));

		return hash;
	}
}