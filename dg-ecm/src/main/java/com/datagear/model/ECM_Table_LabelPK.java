package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the ECM_Table_Label database table.
 * 
 */
@Embeddable
public class ECM_Table_LabelPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Source_Name")
	private String sourceName;

	@Column(name = "Obj_Name")
	private String objName;

	@Column(name = "Table_Name")
	private String tableName;

	@Column(name = "Locale", insertable = false, updatable = false)
	private String locale;

	public ECM_Table_LabelPK() {
	}

	public String getSourceName() {
		return this.sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getObjName() {
		return this.objName;
	}

	public void setObjName(String objName) {
		this.objName = objName;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getLocale() {
		return this.locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ECM_Table_LabelPK)) {
			return false;
		}
		ECM_Table_LabelPK castOther = (ECM_Table_LabelPK) other;
		return this.sourceName.equals(castOther.sourceName) && this.objName.equals(castOther.objName)
				&& this.tableName.equals(castOther.tableName) && this.locale.equals(castOther.locale);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.sourceName.hashCode();
		hash = hash * prime + this.objName.hashCode();
		hash = hash * prime + this.tableName.hashCode();
		hash = hash * prime + this.locale.hashCode();

		return hash;
	}
}