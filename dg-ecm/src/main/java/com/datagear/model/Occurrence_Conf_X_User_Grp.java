package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Occurrence_Conf_X_User_Grp database table.
 * 
 */
@Entity
@NamedQuery(name="Occurrence_Conf_X_User_Grp.findAll", query="SELECT o FROM Occurrence_Conf_X_User_Grp o")
public class Occurrence_Conf_X_User_Grp implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_Conf_X_User_GrpPK id;

	public Occurrence_Conf_X_User_Grp() {
	}

	public Occurrence_Conf_X_User_GrpPK getId() {
		return this.id;
	}

	public void setId(Occurrence_Conf_X_User_GrpPK id) {
		this.id = id;
	}

}