package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the ECM_Entity_Lock database table.
 * 
 */
@Entity
@NamedQuery(name="ECM_Entity_Lock.findAll", query="SELECT e FROM ECM_Entity_Lock e")
public class ECM_Entity_Lock implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ECM_Entity_LockPK id;

	@Column(name="Lock_Date")
	private Timestamp lockDate;

	@Column(name="Lock_User_Id")
	private String lockUserId;

	public ECM_Entity_Lock() {
	}

	public ECM_Entity_Lock(long entityRk, String entityName, String lockUserId, Timestamp lockDate) {
		this.id = new ECM_Entity_LockPK(entityRk, entityName);
		this.lockUserId = lockUserId;
		this.lockDate = lockDate;
	}

	public ECM_Entity_LockPK getId() {
		return this.id;
	}

	public void setId(ECM_Entity_LockPK id) {
		this.id = id;
	}

	public Timestamp getLockDate() {
		return this.lockDate;
	}

	public void setLockDate(Timestamp lockDate) {
		this.lockDate = lockDate;
	}

	public String getLockUserId() {
		return this.lockUserId;
	}

	public void setLockUserId(String lockUserId) {
		this.lockUserId = lockUserId;
	}

}