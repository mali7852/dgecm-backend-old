package com.datagear.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Case_Conf_X_User_GrpPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="Case_Conf_Seq_No", insertable=false, updatable=false)
	private long caseConfSeqNo;

	@Column(name="User_Grp_Name")
	private String userGrpName;

	public Case_Conf_X_User_GrpPK() {
	}
	public long getCaseConfSeqNo() {
		return this.caseConfSeqNo;
	}
	public void setCaseConfSeqNo(long CaseConfSeqNo) {
		this.caseConfSeqNo = CaseConfSeqNo;
	}
	public String getUserGrpName() {
		return this.userGrpName;
	}
	public void setUserGrpName(String userGrpName) {
		this.userGrpName = userGrpName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Case_Conf_X_User_GrpPK)) {
			return false;
		}
		Case_Conf_X_User_GrpPK castOther = (Case_Conf_X_User_GrpPK)other;
		return 
			(this.caseConfSeqNo == castOther.caseConfSeqNo)
			&& this.userGrpName.equals(castOther.userGrpName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.caseConfSeqNo ^ (this.caseConfSeqNo >>> 32)));
		hash = hash * prime + this.userGrpName.hashCode();
		
		return hash;
	}
}
