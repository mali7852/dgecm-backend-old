package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the ECM_Entity_Lock database table.
 * 
 */
@Embeddable
public class ECM_Entity_LockPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Entity_Rk")
	private long entityRk;

	@Column(name = "Entity_Name")
	private String entityName;

	public ECM_Entity_LockPK() {
	}

	public ECM_Entity_LockPK(long entityRk, String entityName) {
		this.entityRk = entityRk;
		this.entityName = entityName;
	}

	public long getEntityRk() {
		return this.entityRk;
	}

	public void setEntityRk(long entityRk) {
		this.entityRk = entityRk;
	}

	public String getEntityName() {
		return this.entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ECM_Entity_LockPK)) {
			return false;
		}
		ECM_Entity_LockPK castOther = (ECM_Entity_LockPK) other;
		return (this.entityRk == castOther.entityRk) && this.entityName.equals(castOther.entityName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.entityRk ^ (this.entityRk >>> 32)));
		hash = hash * prime + this.entityName.hashCode();

		return hash;
	}
}