package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Efile_Search_Result_Field database table.
 * 
 */
@Embeddable
public class Efile_Search_Result_FieldPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "User_Id")
	private String userId;

	@Column(name = "Table_Name")
	private String tableName;

	@Column(name = "Fld_Name")
	private String fldName;

	public Efile_Search_Result_FieldPK() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getFldName() {
		return this.fldName;
	}

	public void setFldName(String fldName) {
		this.fldName = fldName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Efile_Search_Result_FieldPK)) {
			return false;
		}
		Efile_Search_Result_FieldPK castOther = (Efile_Search_Result_FieldPK) other;
		return this.userId.equals(castOther.userId) && this.tableName.equals(castOther.tableName)
				&& this.fldName.equals(castOther.fldName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userId.hashCode();
		hash = hash * prime + this.tableName.hashCode();
		hash = hash * prime + this.fldName.hashCode();

		return hash;
	}
}