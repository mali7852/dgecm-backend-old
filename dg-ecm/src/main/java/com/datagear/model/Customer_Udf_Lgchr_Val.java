package com.datagear.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the Customer_Udf_Lgchr_Val database table.
 */
@Entity
@NamedQuery(name = "Customer_Udf_Lgchr_Val.findAll", query = "SELECT c FROM Customer_Udf_Lgchr_Val c")
public class Customer_Udf_Lgchr_Val implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private Customer_Udf_Lgchr_ValPK id;

    @Column(name = "Udf_Val")
    private String udfVal;

    // bi-directional many-to-one association to Customer_Udf_Def
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "Udf_Name", referencedColumnName = "Udf_Name", insertable = false, updatable = false),
            @JoinColumn(name = "Udf_Table_Name", referencedColumnName = "Udf_Table_Name", insertable = false, updatable = false)})
    private Customer_Udf_Def customerUdfDef;

    // bi-directional many-to-one association to Customer_Ver
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "Cust_Rk", referencedColumnName = "Cust_Rk", insertable = false, updatable = false),
            @JoinColumn(name = "Valid_From_Date", referencedColumnName = "Valid_From_Date", insertable = false, updatable = false)})
    private Customer_Ver customerVer;

    public Customer_Udf_Lgchr_Val() {
    }

    public Customer_Udf_Lgchr_ValPK getId() {
        return this.id;
    }

    public void setId(Customer_Udf_Lgchr_ValPK id) {
        this.id = id;
    }

    public String getUdfVal() {
        return this.udfVal;
    }

    public void setUdfVal(String udfVal) {
        this.udfVal = udfVal;
    }

    public Customer_Udf_Def getCustomerUdfDef() {
        return this.customerUdfDef;
    }

    public void setCustomerUdfDef(Customer_Udf_Def customerUdfDef) {
        this.customerUdfDef = customerUdfDef;
    }

    public Customer_Ver getCustomerVer() {
        return this.customerVer;
    }

    public void setCustomerVer(Customer_Ver customerVer) {
        this.customerVer = customerVer;
    }

}