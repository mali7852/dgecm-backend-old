package com.datagear.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * The primary key class for the Case_Ver database table.
 * 
 */
@Embeddable
public class Case_VerPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="Case_Rk")
	private long caseRk;

//	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Valid_From_Date")
	private Timestamp validFromDate;

	public Case_VerPK() {
	}
	public Case_VerPK(long case_Rk, Timestamp valid_From_Date) {
		this.caseRk = case_Rk;
		this.validFromDate = valid_From_Date;
	}
	public long getCaseRk() {
		return this.caseRk;
	}
	public void setCaseRk(long caseRk) {
		this.caseRk = caseRk;
	}
	public Timestamp getValidFromDate() {
		return this.validFromDate;
	}
	public void setValidFromDate(Timestamp validFromDate) {
		this.validFromDate = validFromDate;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Case_VerPK)) {
			return false;
		}
		Case_VerPK castOther = (Case_VerPK)other;
		return 
			(this.caseRk == castOther.caseRk)
			&& this.validFromDate.equals(castOther.validFromDate);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.caseRk ^ (this.caseRk >>> 32)));
		hash = hash * prime + this.validFromDate.hashCode();
		
		return hash;
	}
}