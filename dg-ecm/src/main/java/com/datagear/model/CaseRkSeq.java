package com.datagear.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.Table;


/**
 * The persistent class for the case_rk_seq database table.
 * 
 */
@Entity
@Table(name="case_rk_seq")
@NamedQuery(name="CaseRkSeq.findAll", query="SELECT c FROM CaseRkSeq c")
@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery(name = "getNextCase_Rk",
                                procedureName = "case_rk_seq_next",
    resultClasses = Long.class)
})
public class CaseRkSeq implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private Long id;

	public CaseRkSeq() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}