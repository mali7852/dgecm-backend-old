package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the Occurrence_Live database table.
 */
@Entity
@NamedQuery(name = "Occurrence_Live.findAll", query = "SELECT o FROM Occurrence_Live o")
public class Occurrence_Live implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Occs_Rk")
    private long occsRk;

    @Column(name = "Close_Date")
    private Timestamp closeDate;

    @Column(name = "Create_Date")
    private Timestamp createDate;

    @Column(name = "Create_User_Id")
    private String createUserId;

    @Column(name = "Delete_Flag")
    private String deleteFlag;

    @Column(name = "Dtction_Date")
    private Date dtctionDate;

    @Column(name = "Dtction_Time")
    private String dtctionTime;

    @Column(name = "Investr_User_Id")
    private String investrUserId;

    @Column(name = "Notif_Date")
    private Date notifDate;

    @Column(name = "Notif_Time")
    private String notifTime;

    @Column(name = "Occs_Ctgry_Cd")
    private String occsCtgryCd;

    @Column(name = "Occs_Desc")
    private String occsDesc;

    @Column(name = "Occs_Dispos_Cd")
    private String occsDisposCd;

    @Column(name = "Occs_From_Date")
    private Date occsFromDate;

    @Column(name = "Occs_From_Time")
    private String occsFromTime;

    @Column(name = "Occs_Id")
    private String occsId;

    @Column(name = "Occs_Stat_Cd")
    private String occsStatCd;

    @Column(name = "Occs_Sub_Ctgry_Cd")
    private String occsSubCtgryCd;

    @Column(name = "Occs_To_Date")
    private Date occsToDate;

    @Column(name = "Occs_To_Time")
    private String occsToTime;

    @Column(name = "Occs_Type_Cd")
    private String occsTypeCd;

    @Column(name = "Src_Sys_Cd")
    private String srcSysCd;

    @Column(name = "UI_Def_File_Name")
    private String uiDefFileName;

    @Column(name = "Update_User_Id")
    private String updateUserId;

    @Column(name = "Valid_From_Date")
    private Timestamp validFromDate;

    @Column(name = "Valid_To_Date")
    private Timestamp validToDate;

    @Column(name = "Ver_No")
    private BigDecimal verNo;

    //	//bi-directional many-to-one association to Case_Live
    @ManyToOne(targetEntity = Case_Live.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "Case_Rk", updatable = false)
    private Case_Live caseLive;

    // bi-directional many-to-one association to Occurrence_X_Customer
    @JsonIgnore
    @OneToMany(mappedBy = "occurrenceLive")
    private List<Occurrence_X_Customer> occurrenceXCustomers;

    // bi-directional many-to-one association to Occurrence_X_User_Grp
    @JsonIgnore
    @OneToMany(mappedBy = "occurrenceLive")
    private List<Occurrence_X_User_Grp> occurrenceXUserGrps;

    public Occurrence_Live() {
    }

    public Occurrence_Live(Long occs_rk, Timestamp valid_From_Date, String occs_id, String occs_type_cd, String ui_def_file_name) {
        this.occsRk = occs_rk;
        this.validFromDate = valid_From_Date;
        this.createDate = valid_From_Date;
        this.occsId = occs_id;
        this.occsTypeCd = occs_type_cd;
        this.srcSysCd = "DGECM";
        this.createUserId = "DGECM";
        this.verNo = new BigDecimal(1.0);
        this.deleteFlag = "0";
    }

    public Occurrence_X_Customer addOccurrenceXCustomer(Occurrence_X_Customer occurrenceXCustomer) {
        getOccurrenceXCustomers().add(occurrenceXCustomer);
        occurrenceXCustomer.setOccurrenceLive(this);

        return occurrenceXCustomer;
    }

    public Occurrence_X_User_Grp addOccurrenceXUserGrp(Occurrence_X_User_Grp occurrenceXUserGrp) {
        getOccurrenceXUserGrps().add(occurrenceXUserGrp);
        occurrenceXUserGrp.setOccurrenceLive(this);

        return occurrenceXUserGrp;
    }

    public Timestamp getCloseDate() {
        return this.closeDate;
    }

    public void setCloseDate(Timestamp close_Date) {
        this.closeDate = close_Date;
    }

    public Timestamp getCreateDate() {
        return this.createDate;
    }

    public String getCreateUserId() {
        return this.createUserId;
    }

    public String getDeleteFlag() {
        return this.deleteFlag;
    }

    public Date getDtctionDate() {
        return this.dtctionDate;
    }

    public String getDtctionTime() {
        return this.dtctionTime;
    }

    public String getInvestrUserId() {
        return this.investrUserId;
    }

    public Date getNotifDate() {
        return this.notifDate;
    }

    public String getNotifTime() {
        return this.notifTime;
    }

    public String getOccsCtgryCd() {
        return this.occsCtgryCd;
    }

    public String getOccsDesc() {
        return this.occsDesc;
    }

    public String getOccsDisposCd() {
        return this.occsDisposCd;
    }

    public Date getOccsFromDate() {
        return this.occsFromDate;
    }

    public String getOccsFromTime() {
        return this.occsFromTime;
    }

    public String getOccsId() {
        return this.occsId;
    }

    public long getOccsRk() {
        return this.occsRk;
    }

    public String getOccsStatCd() {
        return this.occsStatCd;
    }

    public String getOccsSubCtgryCd() {
        return this.occsSubCtgryCd;
    }

    public Date getOccsToDate() {
        return this.occsToDate;
    }

    public String getOccsToTime() {
        return this.occsToTime;
    }

    public String getOccsTypeCd() {
        return this.occsTypeCd;
    }

    public List<Occurrence_X_Customer> getOccurrenceXCustomers() {
        return this.occurrenceXCustomers;
    }

    public void setOccurrenceXCustomers(List<Occurrence_X_Customer> occurrenceXCustomers) {
        this.occurrenceXCustomers = occurrenceXCustomers;
    }

    public List<Occurrence_X_User_Grp> getOccurrenceXUserGrps() {
        return this.occurrenceXUserGrps;
    }

    public void setOccurrenceXUserGrps(List<Occurrence_X_User_Grp> occurrenceXUserGrps) {
        this.occurrenceXUserGrps = occurrenceXUserGrps;
    }

    public String getSrcSysCd() {
        return this.srcSysCd;
    }

    public void setSrcSysCd(String srcSysCd) {
        this.srcSysCd = srcSysCd;
    }

    public String getUiDefFileName() {
        return this.uiDefFileName;
    }

    public void setUiDefFileName(String UIDefFileName) {
        this.uiDefFileName = UIDefFileName;
    }

    public String getUpdateUserId() {
        return this.updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Timestamp getValidFromDate() {
        return this.validFromDate;
    }

    public void setValidFromDate(Timestamp validFromDate) {
        this.validFromDate = validFromDate;
    }

    public Timestamp getValidToDate() {
        return this.validToDate;
    }

    public void setValidToDate(Timestamp validToDate) {
        this.validToDate = validToDate;
    }

    public BigDecimal getVerNo() {
        return this.verNo;
    }

    public void setVerNo(BigDecimal verNo) {
        this.verNo = verNo;
    }

    public Occurrence_X_Customer removeOccurrenceXCustomer(Occurrence_X_Customer occurrenceXCustomer) {
        getOccurrenceXCustomers().remove(occurrenceXCustomer);
        occurrenceXCustomer.setOccurrenceLive(null);

        return occurrenceXCustomer;
    }

    public Occurrence_X_User_Grp removeOccurrenceXUserGrp(Occurrence_X_User_Grp occurrenceXUserGrp) {
        getOccurrenceXUserGrps().remove(occurrenceXUserGrp);
        occurrenceXUserGrp.setOccurrenceLive(null);

        return occurrenceXUserGrp;
    }

    public void setCreate_Date(Timestamp create_Date) {
        this.createDate = create_Date;
    }

    public void setCreate_User_Id(String create_User_Id) {
        this.createUserId = create_User_Id;
    }

    public void setDelete_Flag(String delete_Flag) {
        this.deleteFlag = delete_Flag;
    }

    public void setDtction_Date(Date dtction_Date) {
        this.dtctionDate = dtction_Date;
    }

    public void setDtction_Time(String dtction_Time) {
        this.dtctionTime = dtction_Time;
    }

    public void setInvestr_User_Id(String investr_User_Id) {
        this.investrUserId = investr_User_Id;
    }

    public void setNotif_Date(Date notif_Date) {
        this.notifDate = notif_Date;
    }

    public void setNotif_Time(String notif_Time) {
        this.notifTime = notif_Time;
    }

    public void setOccs_Ctgry_Cd(String occs_Ctgry_Cd) {
        this.occsCtgryCd = occs_Ctgry_Cd;
    }

    public void setOccs_Desc(String occs_Desc) {
        this.occsDesc = occs_Desc;
    }

    public void setOccs_Dispos_Cd(String occs_Dispos_Cd) {
        this.occsDisposCd = occs_Dispos_Cd;
    }

    public void setOccs_From_Date(Date occs_From_Date) {
        this.occsFromDate = occs_From_Date;
    }

    public void setOccs_From_Time(String occs_From_Time) {
        this.occsFromTime = occs_From_Time;
    }

    public void setOccs_Id(String occs_Id) {
        this.occsId = occs_Id;
    }

    public void setOccs_Rk(long occs_Rk) {
        this.occsRk = occs_Rk;
    }

    public void setOccs_Stat_Cd(String occs_Stat_Cd) {
        this.occsStatCd = occs_Stat_Cd;
    }

    public void setOccs_Sub_Ctgry_Cd(String occs_Sub_Ctgry_Cd) {
        this.occsSubCtgryCd = occs_Sub_Ctgry_Cd;
    }

    public void setOccs_To_Date(Date occs_To_Date) {
        this.occsToDate = occs_To_Date;
    }

    public void setOccs_To_Time(String occs_To_Time) {
        this.occsToTime = occs_To_Time;
    }

    public void setOccs_Type_Cd(String occs_Type_Cd) {
        this.occsTypeCd = occs_Type_Cd;
    }

    public String getCaseId() {
		return this.caseLive == null ? null : this.caseLive.getCaseId();
    }

    public Long getCaseRk() {
        return this.caseLive == null ? null : this.caseLive.getCaseRk();
    }

    @JsonIgnore
    public Case_Live getCaseLive() {
        return this.caseLive;
    }

    public void setCaseLive(Case_Live caseLive) {
        this.caseLive = caseLive;
    }

}