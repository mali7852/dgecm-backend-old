package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Case_X_User_Grp database table.
 *
 */
@Entity
@NamedQuery(name="Case_X_User_Grp.findAll", query="SELECT c FROM Case_X_User_Grp c")
public class Case_X_User_Grp implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Case_X_User_GrpPK id;

	//bi-directional many-to-one association to Case_Live
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="Case_Rk", insertable=false, updatable=false)
	private Case_Live caseLive;

	public Case_X_User_Grp() {
	}

	public Case_X_User_Grp(long case_rk, String user_grp_name) {
		this.id = new Case_X_User_GrpPK();
		this.id.setCaseRk(case_rk);
		this.id.setUserGrpName(user_grp_name);
	}

	public Case_X_User_GrpPK getId() {
		return this.id;
	}

	public void setId(Case_X_User_GrpPK id) {
		this.id = id;
	}

	public Case_Live getCaseLive() {
		return this.caseLive;
	}

	public void setCaseLive(Case_Live caseVer) {
		this.caseLive = caseVer;
	}

}