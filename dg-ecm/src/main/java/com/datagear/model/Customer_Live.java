package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the Customer_Live database table.
 */
@Entity
@NamedQuery(name = "Customer_Live.findAll", query = "SELECT c FROM Customer_Live c")
public class Customer_Live implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Cust_Rk")
    private long custRk;

    @Column(name = "Create_Date")
    private Timestamp createDate;

    @Column(name = "Create_User_Id")
    private String createUserId;

    @Column(name = "Cust_Ctgry_Cd")
    private String custCtgryCd;

    @Column(name = "Cust_Full_Name")
    private String custFullName;

    @Column(name = "Cust_Id")
    private String custId;

    @Column(name = "Cust_Sub_Ctgry_Cd")
    private String custSubCtgryCd;

    @Column(name = "Cust_Type_Cd")
    private String custTypeCd;

    @Column(name = "Delete_Flag")
    private String deleteFlag;

    @Column(name = "Ident_Cust_Link_Sk")
    private BigDecimal identCustLinkSk;

    @Column(name = "Indv_Flag")
    private String indvFlag;

    @Column(name = "Nat_Id")
    private String natId;

    @Column(name = "Nat_Id_TYPE_CD")
    private String natIdTypeCd;

    @Column(name = "Src_Sys_Cd")
    private String srcSysCd;

    @Column(name = "UI_Def_File_Name")
    private String uiDefFileName;

    @Column(name = "Update_User_Id")
    private String updateUserId;

    @Column(name = "Valid_From_Date")
    private Timestamp validFromDate;

    @Column(name = "Valid_To_Date")
    private Timestamp validToDate;

    @Column(name = "Ver_No")
    private BigDecimal verNo;

    //bi-directional many-to-one association to Customer_X_User_Grp
    @JsonIgnore
    @OneToMany(mappedBy = "customerLive")
    private List<Customer_X_User_Grp> customerXUserGrps;

    //bi-directional many-to-one association to Occurrence_X_Customer
    @JsonIgnore
    @OneToMany(mappedBy = "customerLive")
    private List<Occurrence_X_Customer> occurrenceXCustomers;

    public Customer_Live() {
    }

    public Customer_Live(Long cust_rk, Timestamp cur_datetime, String cust_id, String cust_type_cd, String ui_def_file_name) {
        this.custRk = cust_rk;
        this.validFromDate = cur_datetime;
        this.createDate = cur_datetime;
        this.custId = cust_id;
        this.custTypeCd = cust_type_cd;
        this.srcSysCd = "DGECM";
        this.createUserId = "DGECM";
        this.verNo = new BigDecimal(1.0);
        this.deleteFlag = "0";
        this.indvFlag = "0";
    }

    public long getCustRk() {
        return this.custRk;
    }

    public void setCustRk(long custRk) {
        this.custRk = custRk;
    }

    public Timestamp getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserId() {
        return this.createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCustCtgryCd() {
        return this.custCtgryCd;
    }

    public void setCustCtgryCd(String custCtgryCd) {
        this.custCtgryCd = custCtgryCd;
    }

    public String getCustFullName() {
        return this.custFullName;
    }

    public void setCustFullName(String custFullName) {
        this.custFullName = custFullName;
    }

    public String getCustId() {
        return this.custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustSubCtgryCd() {
        return this.custSubCtgryCd;
    }

    public void setCustSubCtgryCd(String custSubCtgryCd) {
        this.custSubCtgryCd = custSubCtgryCd;
    }

    public String getCustTypeCd() {
        return this.custTypeCd;
    }

    public void setCustTypeCd(String custTypeCd) {
        this.custTypeCd = custTypeCd;
    }

    public String getDeleteFlag() {
        return this.deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public BigDecimal getIdentCustLinkSk() {
        return this.identCustLinkSk;
    }

    public void setIdentCustLinkSk(BigDecimal identCustLinkSk) {
        this.identCustLinkSk = identCustLinkSk;
    }

    public String getIndvFlag() {
        return this.indvFlag;
    }

    public void setIndvFlag(String indvFlag) {
        this.indvFlag = indvFlag;
    }

    public String getNatId() {
        return this.natId;
    }

    public void setNatId(String natId) {
        this.natId = natId;
    }

    public String getNatIdTYPECD() {
        return this.natIdTypeCd;
    }

    public void setNatIdTYPECD(String natIdTYPECD) {
        this.natIdTypeCd = natIdTYPECD;
    }

    public String getSrcSysCd() {
        return this.srcSysCd;
    }

    public void setSrcSysCd(String srcSysCd) {
        this.srcSysCd = srcSysCd;
    }

    public String getUiDefFileName() {
        return this.uiDefFileName;
    }

    public void setUiDefFileName(String UIDefFileName) {
        this.uiDefFileName = UIDefFileName;
    }

    public String getUpdateUserId() {
        return this.updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Timestamp getValidFromDate() {
        return this.validFromDate;
    }

    public void setValidFromDate(Timestamp validFromDate) {
        this.validFromDate = validFromDate;
    }

    public Timestamp getValidToDate() {
        return this.validToDate;
    }

    public void setValidToDate(Timestamp validToDate) {
        this.validToDate = validToDate;
    }

    public BigDecimal getVerNo() {
        return this.verNo;
    }

    public void setVerNo(BigDecimal verNo) {
        this.verNo = verNo;
    }

    public List<Customer_X_User_Grp> getCustomerXUserGrps() {
        return this.customerXUserGrps;
    }

    public void setCustomerXUserGrps(List<Customer_X_User_Grp> customerXUserGrps) {
        this.customerXUserGrps = customerXUserGrps;
    }

    public Customer_X_User_Grp addCustomerXUserGrp(Customer_X_User_Grp customerXUserGrp) {
        getCustomerXUserGrps().add(customerXUserGrp);
        customerXUserGrp.setCustomerLive(this);

        return customerXUserGrp;
    }

    public Customer_X_User_Grp removeCustomerXUserGrp(Customer_X_User_Grp customerXUserGrp) {
        getCustomerXUserGrps().remove(customerXUserGrp);
        customerXUserGrp.setCustomerLive(null);

        return customerXUserGrp;
    }

    public List<Occurrence_X_Customer> getOccurrenceXCustomers() {
        return this.occurrenceXCustomers;
    }

    public void setOccurrenceXCustomers(List<Occurrence_X_Customer> occurrenceXCustomers) {
        this.occurrenceXCustomers = occurrenceXCustomers;
    }

    public Occurrence_X_Customer addOccurrenceXCustomer(Occurrence_X_Customer occurrenceXCustomer) {
        getOccurrenceXCustomers().add(occurrenceXCustomer);
        occurrenceXCustomer.setCustomerLive(this);

        return occurrenceXCustomer;
    }

    public Occurrence_X_Customer removeOccurrenceXCustomer(Occurrence_X_Customer occurrenceXCustomer) {
        getOccurrenceXCustomers().remove(occurrenceXCustomer);
        occurrenceXCustomer.setCustomerLive(null);

        return occurrenceXCustomer;
    }

}