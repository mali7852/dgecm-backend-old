package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Ref_Table_Trans database table.
 * 
 */
@Embeddable
public class Ref_Table_TransPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Ref_Table_Name")
	private String refTableName;

	@Column(name = "Val_Cd")
	private String valCd;

	@Column(name = "Locale")
	private String locale;

	public Ref_Table_TransPK() {
	}

	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

	public String getValCd() {
		return this.valCd;
	}

	public void setValCd(String valCd) {
		this.valCd = valCd;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Ref_Table_TransPK)) {
			return false;
		}
		Ref_Table_TransPK castOther = (Ref_Table_TransPK) other;
		return this.refTableName.equals(castOther.refTableName) && this.valCd.equals(castOther.valCd)
				&& this.locale.equals(castOther.locale);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.refTableName.hashCode();
		hash = hash * prime + this.valCd.hashCode();
		hash = hash * prime + this.locale.hashCode();

		return hash;
	}
}