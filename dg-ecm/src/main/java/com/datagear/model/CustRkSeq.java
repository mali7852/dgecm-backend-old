package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the cust_rk_seq database table.
 * 
 */
@Entity
@Table(name="cust_rk_seq")
@NamedQuery(name="CustRkSeq.findAll", query="SELECT c FROM CustRkSeq c")
public class CustRkSeq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private BigDecimal id;

	public CustRkSeq() {
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

}