package com.datagear.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Efile_Conf_X_User_GrpPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "Efile_Conf_Seq_No", insertable = false, updatable = false)
	private long efileConfSeqNo;

	@Column(name = "User_Grp_Name")
	private String userGrpName;

	public Efile_Conf_X_User_GrpPK() {
	}

	public long getEfileConfSeqNo() {
		return this.efileConfSeqNo;
	}

	public void setOccsRk(long EfileConfSeqNo) {
		this.efileConfSeqNo = EfileConfSeqNo;
	}

	public String getUserGrpName() {
		return this.userGrpName;
	}

	public void setUserGrpName(String userGrpName) {
		this.userGrpName = userGrpName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Efile_Conf_X_User_GrpPK)) {
			return false;
		}
		Efile_Conf_X_User_GrpPK castOther = (Efile_Conf_X_User_GrpPK) other;
		return (this.efileConfSeqNo == castOther.efileConfSeqNo) && this.userGrpName.equals(castOther.userGrpName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.efileConfSeqNo ^ (this.efileConfSeqNo >>> 32)));
		hash = hash * prime + this.userGrpName.hashCode();

		return hash;
	}
}
