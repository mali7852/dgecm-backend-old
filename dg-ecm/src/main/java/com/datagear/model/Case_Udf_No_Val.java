package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Case_Udf_No_Val database table.
 * 
 */
@Entity
@NamedQuery(name="Case_Udf_No_Val.findAll", query="SELECT c FROM Case_Udf_No_Val c")
public class Case_Udf_No_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Case_Udf_No_ValPK id;

	@Column(name="Udf_Val")
	private double udfVal;

	//bi-directional many-to-one association to Case_Udf_Def
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="Udf_Name", referencedColumnName="Udf_Name", insertable=false, updatable=false),
		@JoinColumn(name="Udf_Table_Name", referencedColumnName="Udf_Table_Name", insertable=false, updatable=false)
		})
	private Case_Udf_Def caseUdfDef;

	//bi-directional many-to-one association to Case_Ver
//	@ManyToOne
//	@JoinColumns({
//		@JoinColumn(name="Case_Rk", referencedColumnName="Case_Rk", insertable=false, updatable=false),
//		@JoinColumn(name="Valid_From_Date", referencedColumnName="Valid_From_Date", insertable=false, updatable=false)
//		})
//	private Case_Ver caseVer;

	public Case_Udf_No_Val() {
	}

	public Case_Udf_No_ValPK getId() {
		return this.id;
	}

	public void setId(Case_Udf_No_ValPK id) {
		this.id = id;
	}

	public double getUdfVal() {
		return this.udfVal;
	}

	public void setUdfVal(double udfVal) {
		this.udfVal = udfVal;
	}

	public Case_Udf_Def getCaseUdfDef() {
		return this.caseUdfDef;
	}

	public void setCaseUdfDef(Case_Udf_Def caseUdfDef) {
		this.caseUdfDef = caseUdfDef;
	}

//	public Case_Ver getCaseVer() {
//		return this.caseVer;
//	}

//	public void setCaseVer(Case_Ver caseVer) {
//		this.caseVer = caseVer;
//	}

}