package com.datagear.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the Customer_X_Customer database table.
 * 
 */
@Entity
@NamedQuery(name="Customer_X_Customer.findAll", query="SELECT o FROM Customer_X_Customer o")
public class Customer_X_Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Customer_X_CustomerPK id;

	@Column(name="Mmbr_Desc")
	private String mmbrDesc;

	//bi-directional many-to-one association to Customer_Live
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="Cust_Rk", insertable=false, updatable=false)
	private Customer_Live customerLive;

	//bi-directional many-to-one association to Customer_Live
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="Mmbr_Cust_Rk", insertable=false, updatable=false)
	private Customer_Live customerLive2;

	public Customer_X_Customer() {
	}

	public Customer_X_Customer(long entityRk, long memberRk) {
		this.id = new Customer_X_CustomerPK();
		this.id.setCustRk(entityRk);
		this.id.setMmbrCustRk(memberRk);
		this.id.setMmbrTypeCd("Rel");
		this.setMmbrDesc("Desc");
	}

	public Customer_X_CustomerPK getId() {
		return this.id;
	}

	public void setId(Customer_X_CustomerPK id) {
		this.id = id;
	}

	public String getMmbrDesc() {
		return this.mmbrDesc;
	}

	public void setMmbrDesc(String MmbrDesc) {
		this.mmbrDesc = MmbrDesc;
	}

	public Customer_Live getcustomerLive() {
		return this.customerLive;
	}

	public void setcustomerLive(Customer_Live customerLive) {
		this.customerLive = customerLive;
	}

	public Customer_Live getcustomerLive2() {
		return this.customerLive2;
	}

	public void setcustomerLive2(Customer_Live customerLive2) {
		this.customerLive2 = customerLive2;
	}

}