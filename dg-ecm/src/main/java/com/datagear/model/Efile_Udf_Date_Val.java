package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the Efile_Udf_Date_Val database table.
 * 
 */
@Entity
@NamedQuery(name="Efile_Udf_Date_Val.findAll", query="SELECT c FROM Efile_Udf_Date_Val c")
public class Efile_Udf_Date_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Efile_Udf_Date_ValPK id;

	@Column(name="Udf_Val")
	private Timestamp udfVal;

	public Efile_Udf_Date_Val() {
	}

	public Efile_Udf_Date_ValPK getId() {
		return this.id;
	}

	public void setId(Efile_Udf_Date_ValPK id) {
		this.id = id;
	}

	public Timestamp getUdfVal() {
		return this.udfVal;
	}

	public void setUdfVal(Timestamp udfVal) {
		this.udfVal = udfVal;
	}

}