package com.datagear.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonGetter;

@Entity
@Table(name = "Aml_Alert")
@NamedQuery(name = "AmlAlert.findAll", query = "SELECT alt FROM AmlAlert alt")

public class AmlAlert implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "Id")
	private long id;

	@Column(name = "Alarm_id")
	private long alertId;

	@Column(name = "Entity_Num")
	private String entityNum;

	@Column(name = "Prim_Obj_level_Cd")
	private String levelCd;

	@Column(name = "Alarm_Categ_Cd")
	private String scenarioType;

	@Column(name = "Routine_Desc")
	private String scenarioDesc;

	@Column(name = "Run_Date")
	private Timestamp runDate;

	@Column(name = "MLS")
	private int mls;

	public AmlAlert() {
	}

	@JsonGetter("ID")
	public long getAlertId() {
		return alertId;
	}

	public String getEntityNum() {
		return entityNum;
	}

	public long getId() {
		return id;
	}

	@JsonGetter("ALERT_LEVEL")
	public String getLevelCd() {
		return levelCd;
	}

	@JsonGetter("MLS")
	public int getMls() {
		return mls;
	}

	@JsonGetter("RUN_DATE")
	public Timestamp getRunDate() {
		return runDate;
	}

	@JsonGetter("SCENARIO_DESC")
	public String getScenarioDesc() {
		return scenarioDesc;
	}

	@JsonGetter("SCENARIO_TYPE")
	public String getScenarioType() {
		return scenarioType;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public void setEntityNum(String entityNum) {
		this.entityNum = entityNum;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setLevelCd(String levelCd) {
		this.levelCd = levelCd;
	}

	public void setMls(int mls) {
		this.mls = mls;
	}

	public void setRunDate(Timestamp runDate) {
		this.runDate = runDate;
	}

	public void setScenarioDesc(String scenarioDesc) {
		this.scenarioDesc = scenarioDesc;
	}

	public void setScenarioType(String scenarioType) {
		this.scenarioType = scenarioType;
	}

}
