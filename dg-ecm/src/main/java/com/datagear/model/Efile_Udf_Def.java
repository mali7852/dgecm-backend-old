package com.datagear.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the Efile_Udf_Def database table.
 * 
 */
@Entity
@NamedQuery(name="Efile_Udf_Def.findAll", query="SELECT e FROM Efile_Udf_Def e")
public class Efile_Udf_Def implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Efile_Udf_DefPK id;

	@Column(name="Max_Char_Cnt")
	private BigDecimal maxCharCnt;

	@Column(name="Ref_Table_Name")
	private String refTableName;

	@Column(name="Udf_Desc")
	private String udfDesc;

	@Column(name="Udf_Type_Name")
	private String udfTypeName;

	//bi-directional many-to-one association to Efile_Udf_Lgchr_Val
	@JsonIgnore
	@OneToMany(mappedBy="efileUdfDef")
	private List<Efile_Udf_Lgchr_Val> efileUdfLgchrVals;

	//bi-directional many-to-one association to Efile_Udf_Num_Val
	@JsonIgnore
	@OneToMany(mappedBy="efileUdfDef")
	private List<Efile_Udf_Num_Val> efileUdfNumVals;

	public Efile_Udf_Def() {
	}

	public Efile_Udf_DefPK getId() {
		return this.id;
	}

	public void setId(Efile_Udf_DefPK id) {
		this.id = id;
	}

	public BigDecimal getMaxCharCnt() {
		return this.maxCharCnt;
	}

	public void setMaxCharCnt(BigDecimal maxCharCnt) {
		this.maxCharCnt = maxCharCnt;
	}

	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

	public String getUdfDesc() {
		return this.udfDesc;
	}

	public void setUdfDesc(String udfDesc) {
		this.udfDesc = udfDesc;
	}

	public String getUdfTypeName() {
		return this.udfTypeName;
	}

	public void setUdfTypeName(String udfTypeName) {
		this.udfTypeName = udfTypeName;
	}

	public List<Efile_Udf_Lgchr_Val> getEfileUdfLgchrVals() {
		return this.efileUdfLgchrVals;
	}

	public void setEfileUdfLgchrVals(List<Efile_Udf_Lgchr_Val> efileUdfLgchrVals) {
		this.efileUdfLgchrVals = efileUdfLgchrVals;
	}

	public Efile_Udf_Lgchr_Val addEfileUdfLgchrVal(Efile_Udf_Lgchr_Val efileUdfLgchrVal) {
		getEfileUdfLgchrVals().add(efileUdfLgchrVal);
		efileUdfLgchrVal.setEfileUdfDef(this);

		return efileUdfLgchrVal;
	}

	public Efile_Udf_Lgchr_Val removeEfileUdfLgchrVal(Efile_Udf_Lgchr_Val efileUdfLgchrVal) {
		getEfileUdfLgchrVals().remove(efileUdfLgchrVal);
		efileUdfLgchrVal.setEfileUdfDef(null);

		return efileUdfLgchrVal;
	}

	public List<Efile_Udf_Num_Val> getEfileUdfNumVals() {
		return this.efileUdfNumVals;
	}

	public void setEfileUdfNumVals(List<Efile_Udf_Num_Val> efileUdfNumVals) {
		this.efileUdfNumVals = efileUdfNumVals;
	}

	public Efile_Udf_Num_Val addEfileUdfNumVal(Efile_Udf_Num_Val efileUdfNumVal) {
		getEfileUdfNumVals().add(efileUdfNumVal);
		efileUdfNumVal.setEfileUdfDef(this);

		return efileUdfNumVal;
	}

	public Efile_Udf_Num_Val removeEfileUdfNumVal(Efile_Udf_Num_Val efileUdfNumVal) {
		getEfileUdfNumVals().remove(efileUdfNumVal);
		efileUdfNumVal.setEfileUdfDef(null);

		return efileUdfNumVal;
	}

}