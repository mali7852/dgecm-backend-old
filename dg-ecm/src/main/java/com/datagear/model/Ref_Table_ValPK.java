package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * The primary key class for the Ref_Table_Val database table.
 * 
 */
@Embeddable
public class Ref_Table_ValPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "Ref_Table_Name")
	private String refTableName;

	@Column(name = "Val_Cd")
	private String valCd;

	public Ref_Table_ValPK() {
	}

	@JsonGetter("refTableNm")
	public String getRefTableName() {
		return this.refTableName;
	}

	public void setRefTableName(String refTableName) {
		this.refTableName = refTableName;
	}

	@JsonGetter("valueCd")
	public String getValCd() {
		return this.valCd;
	}

	public void setValCd(String valCd) {
		this.valCd = valCd;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Ref_Table_ValPK)) {
			return false;
		}
		Ref_Table_ValPK castOther = (Ref_Table_ValPK) other;
		return this.refTableName.equals(castOther.refTableName) && this.valCd.equals(castOther.valCd);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.refTableName.hashCode();
		hash = hash * prime + this.valCd.hashCode();

		return hash;
	}
}