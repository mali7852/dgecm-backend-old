package com.datagear.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the Efile_Ver database table.
 * 
 */
@Entity
@NamedQuery(name = "Efile_Ver.findAll", query = "SELECT e FROM Efile_Ver e")
public class Efile_Ver implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Efile_VerPK id;

	@Column(name = "Correction_Flg")
	private String correctionFlg;

	@Column(name = "Coverage_End_Dt")
	private Date coverageEndDt;

	@Column(name = "Coverage_Start_Dt")
	private Date coverageStartDt;

	@Column(name = "Create_Date")
	private Timestamp createDate;

	@Column(name = "Create_User_Id")
	private String createUserId;

	@Column(name = "Delete_Flag")
	private String deleteFlag;

	@Column(name = "Efile_Agency_Ref_Id")
	private String efileAgencyRefId;

	@Column(name = "Efile_Agency_Status_Cd")
	private String efileAgencyStatusCd;

	@Column(name = "Efile_Ctgry_Cd")
	private String efileCtgryCd;

	@Column(name = "Efile_Desc")
	private String efileDesc;

	@Column(name = "Efile_Id")
	private String efileId;

	@Column(name = "Efile_Stat_Cd")
	private String efileStatCd;

	@Column(name = "Efile_Sub_Ctgry_Cd")
	private String efileSubCtgryCd;

	@Column(name = "Efile_Type_Cd")
	private String efileTypeCd;

	@Column(name = "Form_Conf_Rk")
	private BigDecimal formConfRk;

	@Column(name = "OUTPUT_Create_Date")
	private Timestamp outputCreateDate;

	@Column(name = "Output_File_Size")
	private BigDecimal outputFileSize;

	@Column(name = "Output_Line_Cnt")
	private BigDecimal outputLineCnt;

	@Column(name = "Output_Name")
	private String outputName;

	@Column(name = "Output_Path_Name")
	private String outputPathName;

	@Column(name = "Output_Rr_Cnt")
	private BigDecimal outputRrCnt;

	@Column(name = "OWNER_User_Id")
	private String ownerUserId;

	@Column(name = "Src_Sys_Cd")
	private String srcSysCd;

	@Column(name = "Transmission_Dttm")
	private Timestamp transmissionDttm;

	@Column(name = "Transmission_Id")
	private String transmissionId;

	@Column(name = "UI_Def_File_Name")
	private String uiDefFileName;

	@Column(name = "Update_User_Id")
	private String updateUserId;

	@Column(name = "Valid_To_Date")
	private Timestamp validToDate;

	@Column(name = "Ver_No")
	private BigDecimal verNo;

	// bi-directional many-to-one association to Efile_Udf_Lgchr_Val
	@JsonIgnore
	@OneToMany(mappedBy = "efileVer")
	private List<Efile_Udf_Lgchr_Val> efileUdfLgchrVals;

	// bi-directional many-to-one association to Efile_Udf_Num_Val
	@JsonIgnore
	@OneToMany(mappedBy = "efileVer")
	private List<Efile_Udf_Num_Val> efileUdfNumVals;

	public Efile_Ver() {
	}

	public Efile_VerPK getId() {
		return this.id;
	}

	public void setId(Efile_VerPK id) {
		this.id = id;
	}

	public String getCorrectionFlg() {
		return this.correctionFlg;
	}

	public void setCorrectionFlg(String correctionFlg) {
		this.correctionFlg = correctionFlg;
	}

	public Date getCoverageEndDt() {
		return this.coverageEndDt;
	}

	public void setCoverageEndDt(Date coverageEndDt) {
		this.coverageEndDt = coverageEndDt;
	}

	public Date getCoverageStartDt() {
		return this.coverageStartDt;
	}

	public void setCoverageStartDt(Date coverageStartDt) {
		this.coverageStartDt = coverageStartDt;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserId() {
		return this.createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getEfileAgencyRefId() {
		return this.efileAgencyRefId;
	}

	public void setEfileAgencyRefId(String efileAgencyRefId) {
		this.efileAgencyRefId = efileAgencyRefId;
	}

	public String getEfileAgencyStatusCd() {
		return this.efileAgencyStatusCd;
	}

	public void setEfileAgencyStatusCd(String efileAgencyStatusCd) {
		this.efileAgencyStatusCd = efileAgencyStatusCd;
	}

	public String getEfileCtgryCd() {
		return this.efileCtgryCd;
	}

	public void setEfileCtgryCd(String efileCtgryCd) {
		this.efileCtgryCd = efileCtgryCd;
	}

	public String getEfileDesc() {
		return this.efileDesc;
	}

	public void setEfileDesc(String efileDesc) {
		this.efileDesc = efileDesc;
	}

	public String getEfileId() {
		return this.efileId;
	}

	public void setEfileId(String efileId) {
		this.efileId = efileId;
	}

	public String getEfileStatCd() {
		return this.efileStatCd;
	}

	public void setEfileStatCd(String efileStatCd) {
		this.efileStatCd = efileStatCd;
	}

	public String getEfileSubCtgryCd() {
		return this.efileSubCtgryCd;
	}

	public void setEfileSubCtgryCd(String efileSubCtgryCd) {
		this.efileSubCtgryCd = efileSubCtgryCd;
	}

	public String getEfileTypeCd() {
		return this.efileTypeCd;
	}

	public void setEfileTypeCd(String efileTypeCd) {
		this.efileTypeCd = efileTypeCd;
	}

	public BigDecimal getFormConfRk() {
		return this.formConfRk;
	}

	public void setFormConfRk(BigDecimal formConfRk) {
		this.formConfRk = formConfRk;
	}

	public Timestamp getOUTPUTCreateDate() {
		return this.outputCreateDate;
	}

	public void setOUTPUTCreateDate(Timestamp OUTPUTCreateDate) {
		this.outputCreateDate = OUTPUTCreateDate;
	}

	public BigDecimal getOutputFileSize() {
		return this.outputFileSize;
	}

	public void setOutputFileSize(BigDecimal outputFileSize) {
		this.outputFileSize = outputFileSize;
	}

	public BigDecimal getOutputLineCnt() {
		return this.outputLineCnt;
	}

	public void setOutputLineCnt(BigDecimal outputLineCnt) {
		this.outputLineCnt = outputLineCnt;
	}

	public String getOutputName() {
		return this.outputName;
	}

	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}

	public String getOutputPathName() {
		return this.outputPathName;
	}

	public void setOutputPathName(String outputPathName) {
		this.outputPathName = outputPathName;
	}

	public BigDecimal getOutputRrCnt() {
		return this.outputRrCnt;
	}

	public void setOutputRrCnt(BigDecimal outputRrCnt) {
		this.outputRrCnt = outputRrCnt;
	}

	public String getOWNERUserId() {
		return this.ownerUserId;
	}

	public void setOWNERUserId(String OWNERUserId) {
		this.ownerUserId = OWNERUserId;
	}

	public String getSrcSysCd() {
		return this.srcSysCd;
	}

	public void setSrcSysCd(String srcSysCd) {
		this.srcSysCd = srcSysCd;
	}

	public Timestamp getTransmissionDttm() {
		return this.transmissionDttm;
	}

	public void setTransmissionDttm(Timestamp transmissionDttm) {
		this.transmissionDttm = transmissionDttm;
	}

	public String getTransmissionId() {
		return this.transmissionId;
	}

	public void setTransmissionId(String transmissionId) {
		this.transmissionId = transmissionId;
	}

	public String getUiDefFileName() {
		return this.uiDefFileName;
	}

	public void setUiDefFileName(String UIDefFileName) {
		this.uiDefFileName = UIDefFileName;
	}

	public String getUpdateUserId() {
		return this.updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getValidToDate() {
		return this.validToDate;
	}

	public void setValidToDate(Timestamp validToDate) {
		this.validToDate = validToDate;
	}

	public BigDecimal getVerNo() {
		return this.verNo;
	}

	public void setVerNo(BigDecimal verNo) {
		this.verNo = verNo;
	}

	public List<Efile_Udf_Lgchr_Val> getEfileUdfLgchrVals() {
		return this.efileUdfLgchrVals;
	}

	public void setEfileUdfLgchrVals(List<Efile_Udf_Lgchr_Val> efileUdfLgchrVals) {
		this.efileUdfLgchrVals = efileUdfLgchrVals;
	}

	public Efile_Udf_Lgchr_Val addEfileUdfLgchrVal(Efile_Udf_Lgchr_Val efileUdfLgchrVal) {
		getEfileUdfLgchrVals().add(efileUdfLgchrVal);
		efileUdfLgchrVal.setEfileVer(this);

		return efileUdfLgchrVal;
	}

	public Efile_Udf_Lgchr_Val removeEfileUdfLgchrVal(Efile_Udf_Lgchr_Val efileUdfLgchrVal) {
		getEfileUdfLgchrVals().remove(efileUdfLgchrVal);
		efileUdfLgchrVal.setEfileVer(null);

		return efileUdfLgchrVal;
	}

	public List<Efile_Udf_Num_Val> getEfileUdfNumVals() {
		return this.efileUdfNumVals;
	}

	public void setEfileUdfNumVals(List<Efile_Udf_Num_Val> efileUdfNumVals) {
		this.efileUdfNumVals = efileUdfNumVals;
	}

	public Efile_Udf_Num_Val addEfileUdfNumVal(Efile_Udf_Num_Val efileUdfNumVal) {
		getEfileUdfNumVals().add(efileUdfNumVal);
		efileUdfNumVal.setEfileVer(this);

		return efileUdfNumVal;
	}

	public Efile_Udf_Num_Val removeEfileUdfNumVal(Efile_Udf_Num_Val efileUdfNumVal) {
		getEfileUdfNumVals().remove(efileUdfNumVal);
		efileUdfNumVal.setEfileVer(null);

		return efileUdfNumVal;
	}

}