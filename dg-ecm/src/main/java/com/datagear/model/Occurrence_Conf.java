package com.datagear.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Occurrence_Conf database table.
 * 
 */
@Entity
@NamedQuery(name="Occurrence_Conf.findAll", query="SELECT o FROM Occurrence_Conf o")
public class Occurrence_Conf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Occs_Conf_Seq_No")
	private long occsConfSeqNo;

	@Column(name="Occs_Ctgry_Cd")
	private String occsCtgryCd;

	@Column(name="Occs_Sub_Ctgry_Cd")
	private String occsSubCtgryCd;

	@Column(name="Occs_Type_Cd")
	private String occsTypeCd;

	@Column(name="UI_Def_File_Name")
	private String uiDefFileName;

	public Occurrence_Conf() {
	}

	public long getOccsConfSeqNo() {
		return this.occsConfSeqNo;
	}

	public void setOccsConfSeqNo(long occsConfSeqNo) {
		this.occsConfSeqNo = occsConfSeqNo;
	}

	public String getOccsCtgryCd() {
		return this.occsCtgryCd;
	}

	public void setOccsCtgryCd(String occsCtgryCd) {
		this.occsCtgryCd = occsCtgryCd;
	}

	public String getOccsSubCtgryCd() {
		return this.occsSubCtgryCd;
	}

	public void setOccsSubCtgryCd(String occsSubCtgryCd) {
		this.occsSubCtgryCd = occsSubCtgryCd;
	}

	public String getOccsTypeCd() {
		return this.occsTypeCd;
	}

	public void setOccsTypeCd(String occsTypeCd) {
		this.occsTypeCd = occsTypeCd;
	}

	public String getUiDefFileName() {
		return this.uiDefFileName;
	}

	public void setUiDefFileName(String UIDefFileName) {
		this.uiDefFileName = UIDefFileName;
	}

}