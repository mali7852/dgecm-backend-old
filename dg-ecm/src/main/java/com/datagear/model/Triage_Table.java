package com.datagear.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the Triage_Table database table.
 * 
 */
@Entity
@NamedQuery(name = "Triage_Table.findAll", query = "SELECT t FROM Triage_Table t")
public class Triage_Table implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "Id")
	private long id;

	@Column(name = "Alarms_CNT")
	private int alarmsCnt;

	@Column(name = "Entity_Num")
	private String entityNum;

	@Column(name = "Alarmed_Obj_Key")
	private String alarmedObjKey;

	@Column(name = "Alarm_Name")
	private String alarmName;

	@Column(name = "Type")
	private String type;

	@Column(name = "Alarm_Risk")
	private String alarmRisk;

	@Column(name = "Owner")
	private String owner;

	@Column(name = "Aggregation")
	private BigDecimal aggregation;

	@Column(name = "Valid_From_Date")
	private Timestamp validFromDate;

	@Column(name = "MLS")
	private Integer mls;

	@Column(name = "Case_Stat_Cd")
	private String caseStatCd;

	@Column(name = "Update_User_Id")
	private String updateUserId;

	@Column(name = "Sys_Cd")
	private String sysCd;

	public Triage_Table() {
	}

	public BigDecimal getAggregation() {
		return aggregation;
	}

	public String getAlarmedObjKey() {
		return alarmedObjKey;
	}

	public String getAlarmName() {
		return alarmName;
	}

	public String getAlarmRisk() {
		return alarmRisk;
	}

	public int getAlarmsCnt() {
		return alarmsCnt;
	}

	public String getCaseStatCd() {
		return caseStatCd;
	}

	public String getEntityNum() {
		return entityNum;
	}

	public long getId() {
		return id;
	}

	public Integer getMls() {
		return mls;
	}

	public String getOwner() {
		return owner;
	}

	public String getSysCd() {
		return sysCd;
	}

	public String getType() {
		return type;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public Timestamp getValidFromDate() {
		return validFromDate;
	}

	public void setAggregation(BigDecimal aggregation) {
		this.aggregation = aggregation;
	}

	public void setAlarmedObjKey(String alarmedObjKey) {
		this.alarmedObjKey = alarmedObjKey;
	}

	public void setAlarmName(String alarmName) {
		this.alarmName = alarmName;
	}

	public void setAlarmRisk(String alarmRisk) {
		this.alarmRisk = alarmRisk;
	}

	public void setAlarmsCnt(int alarmsCnt) {
		this.alarmsCnt = alarmsCnt;
	}

	public void setCaseStatCd(String caseStatCd) {
		this.caseStatCd = caseStatCd;
	}

	public void setEntityNum(String entityNum) {
		this.entityNum = entityNum;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setMls(Integer mls) {
		this.mls = mls;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setSysCd(String sysCd) {
		this.sysCd = sysCd;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public void setValidFromDate(Timestamp validFromDate) {
		this.validFromDate = validFromDate;
	}

}