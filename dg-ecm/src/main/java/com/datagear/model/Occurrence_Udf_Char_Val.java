package com.datagear.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="Occurrence_Udf_Char_Val.findAll", query="SELECT c FROM Occurrence_Udf_Char_Val c")
public class Occurrence_Udf_Char_Val implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private Occurrence_Udf_Char_ValPK id;

	@Column(name="Udf_Val")
	private String udfVal;

	public Occurrence_Udf_Char_Val() {
	}

	public Occurrence_Udf_Char_Val(Long occs_rk, Timestamp valid_from_date, String udf_name, String udf_val) {
		this.id = new Occurrence_Udf_Char_ValPK();
		this.id.setOccsRk(occs_rk);
		this.id.setValidFromDate(valid_from_date);
		this.id.setUdfTableName("OCCS");
		this.id.setUdfName(udf_name);
		this.id.setRowNo(1);
		this.setUdfVal(udf_val);
	}

	public Occurrence_Udf_Char_ValPK getId() {
		return this.id;
	}

	public void setId(Occurrence_Udf_Char_ValPK id) {
		this.id = id;
	}

	public String getUdfVal() {
		return this.udfVal;
	}

	public void setUdfVal(String udfVal) {
		this.udfVal = udfVal;
	}
}
