package com.datagear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Case_Conf_X_User_Grp database table.
 */
@Entity
@NamedQuery(name = "Case_Conf_X_User_Grp.findAll", query = "SELECT c FROM Case_Conf_X_User_Grp c")
public class Case_Conf_X_User_Grp implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private Case_Conf_X_User_GrpPK id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "Case_Conf_Seq_No", referencedColumnName = "Case_Conf_Seq_No", insertable = false, updatable = false)
    private Case_Conf caseConf;

    public Case_Conf_X_User_Grp() {
    }

    public Case_Conf_X_User_GrpPK getId() {
        return this.id;
    }

    public void setId(Case_Conf_X_User_GrpPK id) {
        this.id = id;
    }

//    public Case_Conf getCaseConf() {
//        return caseConf;
//    }
//
//    public void setCaseConf(Case_Conf caseConf) {
//        this.caseConf = caseConf;
//    }

}