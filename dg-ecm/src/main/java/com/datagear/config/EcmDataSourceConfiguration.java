package com.datagear.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.datagear.repository",
		entityManagerFactoryRef = "ecmEntityManagerFactory",
		transactionManagerRef = "ecmTransactionManager"
)
public class EcmDataSourceConfiguration {

	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource")
	public DataSourceProperties ecmDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	@Primary
	public DataSource ecmDataSource() {
		return ecmDataSourceProperties().initializeDataSourceBuilder().build();
	}
	
	@Bean(name = "ecmEntityManagerFactory")
	@Primary
	public LocalContainerEntityManagerFactoryBean ecmEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(ecmDataSource())
				.packages("com.datagear.model")
				.persistenceUnit("ecm")
				.build();
	}

	@Bean(name = "ecmTransactionManager")
	@Primary
	public PlatformTransactionManager ecmTransactionManager(
			@Qualifier("ecmEntityManagerFactory") EntityManagerFactory ecmEntityManagerFactory) {
		return new JpaTransactionManager(ecmEntityManagerFactory);
	}
}
