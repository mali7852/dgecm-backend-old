package com.datagear.controller;

import com.datagear.customModel.DashboardStatistics;
import com.datagear.model.Case_Live;
import com.datagear.security.model.LoginUser;
import com.datagear.service.Case_LiveService;
import com.datagear.service.Occurrence_LiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/dashboard")
@CrossOrigin("*")
//@PreAuthorize("hasRole('DASHBOARD')")
public class DashboardController {
    @Autowired
    private Case_LiveService case_liveServ;

    @Autowired
    private Occurrence_LiveService occurrence_liveServ;

    @GetMapping("last-cases")
    public ResponseEntity<?> getLastCasesUnderInvestigation() {
        if (!LoginUser.getInstance().getGroups().contains("DASHBOARD"))
            return new ResponseEntity<>("User not in group DASHBOARD", HttpStatus.UNAUTHORIZED);
        List<Case_Live> lastCases = this.case_liveServ.getLastCasesUnderInvestigation();
        return new ResponseEntity<>(lastCases, HttpStatus.OK);
    }

    @GetMapping("top-users")
    public ResponseEntity<?> getTopWorkingOnUsers() {
        if (!LoginUser.getInstance().getGroups().contains("DASHBOARD"))
            return new ResponseEntity<>("User not in group DASHBOARD", HttpStatus.UNAUTHORIZED);
        List<DashboardStatistics> topUsers = this.case_liveServ.getTopWorkingOnUsers();
        return new ResponseEntity<>(topUsers, HttpStatus.OK);
    }

    @GetMapping("case-type-stats")
    public ResponseEntity<?> getCaseTypeStatistics() {
        if (!LoginUser.getInstance().getGroups().contains("DASHBOARD"))
            return new ResponseEntity<>("User not in group DASHBOARD", HttpStatus.UNAUTHORIZED);
        List<DashboardStatistics> caseTypeStatistics = this.case_liveServ.getCaseTypeStatistics();
        return new ResponseEntity<>(caseTypeStatistics, HttpStatus.OK);
    }

    @GetMapping("occs-type-stats")
    public ResponseEntity<?> getOccsTypeStatistics() {
        if (!LoginUser.getInstance().getGroups().contains("DASHBOARD"))
            return new ResponseEntity<>("User not in group DASHBOARD", HttpStatus.UNAUTHORIZED);
        List<DashboardStatistics> occsTypeStatistics = this.occurrence_liveServ.getOccsTypeStatistics();
        return new ResponseEntity<>(occsTypeStatistics, HttpStatus.OK);
    }

}
