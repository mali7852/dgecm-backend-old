package com.datagear.controller;

import com.datagear.customModel.*;
import com.datagear.model.*;
import com.datagear.service.Case_VerService;
import com.datagear.service.Customer_VerService;
import com.datagear.service.ECM_Entity_LockService;
import com.datagear.service.Occurrence_LiveService;
import com.datagear.uploader.service.FileStorageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/search")
@CrossOrigin("*")
public class SearchController {

    @Autowired
    private Case_VerService case_VerServ;

    @Autowired
    private Customer_VerService cust_VerServ;

    @Autowired
    private Occurrence_LiveService occurrence_liveServ;

    @Autowired
    private ECM_Entity_LockService entityLockServ;

    @Autowired
    private FileStorageService fileStorageServ;

    @PreAuthorize("hasRole('CASESEARCH')")
    @PostMapping(value = "case")
    public ResponseEntity<?> getSearchCase(@RequestBody SearchCaseCriteria caseCriteria) {
        Page<Case_Ver> result = case_VerServ.findBySearchCriteria(caseCriteria);
        List<Case_Ver> case_vers = result.getContent();

        List<Case_VerDTO> data = case_vers.parallelStream().map(case_ver ->
                new Case_VerDTO(case_ver)
        ).collect(Collectors.toList());

        if (case_vers.size() > 0) {
            List<Long> case_rks = data.parallelStream().map(c -> c.getCaseRk()).collect(Collectors.toList());

            List<ECM_Entity_Lock> entity_locks = this.entityLockServ.getByEntityRkList(case_rks, ECM_Entity_LockService.ENTITY_TYPE.Case.getName());

            entity_locks.forEach(l -> {
                List<Case_VerDTO> dto = data.parallelStream().filter(d -> d.getCaseRk() == l.getId().getEntityRk()).collect(Collectors.toList());
                if (dto.size() > 0)
                    dto.get(0).setLockedBy(l.getLockUserId());
            });
        }

        return new ResponseEntity<>(new SearchCaseResult(
                new PagingDetails(Integer.parseInt(caseCriteria.getPageNo() != null ? caseCriteria.getPageNo() : "1"),
                        Integer.parseInt(caseCriteria.getRowNo() != null ? caseCriteria.getRowNo() : "20"),
                        result.getTotalElements(), result.getTotalPages()),
                data), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CASESEARCH')")
    @PostMapping(value = "case-xlsx")
    public ResponseEntity<?> getSearchCaseAsXlsx(@RequestBody SearchCaseCriteria caseCriteria) {
        caseCriteria.setPageNo("0");
        caseCriteria.setRowNo(String.valueOf(Integer.MAX_VALUE));
        Page<Case_Ver> result = case_VerServ.findBySearchCriteria(caseCriteria);
        List<Case_Ver> case_vers = result.getContent();

        List<Map<String, Object>> data = case_vers.parallelStream().map(case_ver -> {
            ObjectMapper mapper = new ObjectMapper();
            try {
                return (Map<String, Object>) mapper.readValue(mapper.writeValueAsString(new Case_VerDTO(case_ver)), Map.class);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());
        Resource resource;
        try {
            resource = fileStorageServ.writeListOfMapsToXlsx(data);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CASESEARCH')")
    @PostMapping(value = "case-csv")
    public ResponseEntity<?> getSearchCaseAsCsv(@RequestBody SearchCaseCriteria caseCriteria) {
        caseCriteria.setPageNo("0");
        caseCriteria.setRowNo(String.valueOf(Integer.MAX_VALUE));
        Page<Case_Ver> result = case_VerServ.findBySearchCriteria(caseCriteria);
        List<Case_Ver> case_vers = result.getContent();

        List<Map<String, Object>> data = case_vers.parallelStream().map(case_ver -> {
            ObjectMapper mapper = new ObjectMapper();
            try {
                return (Map<String, Object>) mapper.readValue(mapper.writeValueAsString(new Case_VerDTO(case_ver)), Map.class);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());
        Resource resource;
        try {
            resource = fileStorageServ.writeListOfMapsToCsv(data);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CUSTSEARCH')")
    @PostMapping(value = "cust")
    public ResponseEntity<?> getSearchCust(@RequestBody SearchCustCriteria custCriteria) {
        Page<Customer_Ver> result = cust_VerServ.findBySearchCriteria(custCriteria);
        List<Customer_Ver> cust_vers = result.getContent();

        List<Customer_VerDTO> data = cust_vers.parallelStream().map(cust_ver ->
                new Customer_VerDTO(cust_ver)
        ).collect(Collectors.toList());

        return new ResponseEntity<>(new SearchCustResult(
                new PagingDetails(Integer.parseInt(custCriteria.getPageNo() != null ? custCriteria.getPageNo() : "1"),
                        Integer.parseInt(custCriteria.getRowNo() != null ? custCriteria.getRowNo() : "20"),
                        result.getTotalElements(), result.getTotalPages()),
                data), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('OCCSSEARCH')")
    @PostMapping(value = "occs")
    public ResponseEntity<?> getSearchOccs(@RequestBody SearchOccsCriteria occsCriteria) {
        Page<Occurrence_Live> result = occurrence_liveServ.findBySearchCriteria(occsCriteria);
        List<Occurrence_Live> occs_lives = result.getContent();

        return new ResponseEntity<>(new SearchOccsResult(
                new PagingDetails(Integer.parseInt(occsCriteria.getPageNo() != null ? occsCriteria.getPageNo() : "1"),
                        Integer.parseInt(occsCriteria.getRowNo() != null ? occsCriteria.getRowNo() : "20"),
                        result.getTotalElements(), result.getTotalPages()),
                occs_lives), HttpStatus.OK);
    }

}
