package com.datagear.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datagear.customModel.PagingDetails;
import com.datagear.customModel.SearchTriageResult;
import com.datagear.model.AmlAlert;
import com.datagear.model.Case_Ver;
import com.datagear.model.Triage_Table;
import com.datagear.security.model.LoginUser;
import com.datagear.service.AmlAlertService;
import com.datagear.service.Case_VerService;
import com.datagear.service.TriageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datagear.customModel.SearchCaseCriteria;
import com.datagear.customModel.SearchOccsResult;
import com.datagear.model.Occurrence_Live;
import com.datagear.service.Case_LiveService;
import com.datagear.service.Occurrence_LiveService;

@RestController
@RequestMapping(value = "api/triage")
@CrossOrigin("*")
public class TriageController {

	@Autowired
	private Case_VerService case_VerServ;

	@Autowired
	private Case_LiveService case_LiveServ;

	@Autowired
	private Occurrence_LiveService occs_LiveServ;

	@Autowired
	private TriageService triageServ;

	@Autowired
	private AmlAlertService sasAmlAlertServ;
	
	/**************************************Controller_Functions****************************************/

//	@PreAuthorize("hasRole('TRIGSEARCH')")
	@GetMapping(value = "")
	public ResponseEntity<?> getTriageList() {
		if (!LoginUser.getInstance().getGroups().contains("TRIAGESEARCH"))
			return new ResponseEntity<>("User not in group TRIAGESEARCH", HttpStatus.UNAUTHORIZED);
		List<Triage_Table> result = triageServ.findAll();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

//	@PreAuthorize("hasRole('TRIGSEARCH')")
	@PostMapping(value = "")
	public ResponseEntity<?> getTriageListByCriteria(@RequestBody SearchCaseCriteria caseCriteria) {
		if (!LoginUser.getInstance().getGroups().contains("TRIAGESEARCH"))
			return new ResponseEntity<>("User not in group TRIAGESEARCH", HttpStatus.UNAUTHORIZED);
		Page<Case_Ver> result = case_VerServ.findBySearchCriteria(caseCriteria);
		PagingDetails pd = new PagingDetails(
				Integer.valueOf(caseCriteria.getPageNo() == null ? "1" : caseCriteria.getPageNo()),
				Integer.valueOf(caseCriteria.getRowNo() == null ? "20" : caseCriteria.getRowNo()),
				result.getTotalElements(), result.getTotalPages());

		List<Case_Ver> case_vers = result.getContent();
		List<String> header = Arrays.asList(new String[] { "X_KYC_FULL_NAME_EN" });
		List<Map<String, String>> data = new ArrayList<>();
		case_vers.stream().forEach(case_ver -> {
			Map<String, String> dataMap = extractDataFromCaseVer(case_ver, new HashMap<>());
			if (case_ver.getCaseUdfCharVals().size() > 0) {
				case_ver.getCaseUdfCharVals().stream()
						.filter(char_val -> header.contains(char_val.getId().getUdfName())).forEach(char_val -> {
							dataMap.put("CLIENT_NAME", char_val.getUdfVal());
						});
			}
//			if (case_ver.getCaseUdfDateVals().size() > 0) {
//				case_ver.getCaseUdfDateVals().stream()
//						.filter(date_val -> header.contains(date_val.getId().getUdf_Name())).forEach(date_val -> {
//							dataMap.put(date_val.getId().getUdf_Name(), date_val.getUdf_Val().toString());
//						});
//			}
			dataMap.put("ALERT_NO", String.valueOf(
					occs_LiveServ.getByCaseRk(case_ver.getId().getCaseRk()).getContent().size()));
			data.add(dataMap);
		});

		return new ResponseEntity<SearchTriageResult>(new SearchTriageResult(pd, data), HttpStatus.OK);
	}

//	@PreAuthorize("hasRole('TRIGSEARCH')")
	@GetMapping(value = "{id}")
	public ResponseEntity<?> getTriageOccs(@PathVariable String id) {
		if (!LoginUser.getInstance().getGroups().contains("TRIAGESEARCH"))
			return new ResponseEntity<>("User not in group TRIAGESEARCH", HttpStatus.UNAUTHORIZED);
		long case_rk = 0;
		try {
			case_rk = Long.parseLong(id);
		} catch (NumberFormatException e1) {
			return new ResponseEntity<String>("CaseRk not number", HttpStatus.BAD_REQUEST);
		}

		// Pre-code
		if (!case_LiveServ.isExists(case_rk))
			return new ResponseEntity<String>("CaseRk not found", HttpStatus.BAD_REQUEST);

		Page<Occurrence_Live> pg = occs_LiveServ.getByCaseRk(case_rk);

		PagingDetails pd = new PagingDetails(0, 1000, pg.getTotalElements(), pg.getTotalPages());

		List<Occurrence_Live> occs_live_list = pg.getContent();
		return new ResponseEntity<SearchOccsResult>(new SearchOccsResult(pd, occs_live_list), HttpStatus.OK);
	}

//	@PreAuthorize("hasRole('TRIGSEARCH')")
	@GetMapping(value = "alert/{id}")
	public ResponseEntity<?> getTriageAlerts(@PathVariable String id) {
		if (!LoginUser.getInstance().getGroups().contains("TRIAGESEARCH"))
			return new ResponseEntity<>("User not in group TRIAGESEARCH", HttpStatus.UNAUTHORIZED);
		List<AmlAlert> alerts = sasAmlAlertServ.findByEntityNum(id);
		return new ResponseEntity<List<AmlAlert>>(alerts, HttpStatus.OK);
	}

	/**************************************Associated_Functions****************************************/
	
	private Map<String, String> extractDataFromCaseVer(Case_Ver cv, Map<String, String> map) {
		map.put("ACC_TYPE", cv.getCaseTypeCd());
		map.put("RISK", cv.getPriorityCd() == null ? "N/A" : cv.getPriorityCd());
		map.put("ALERT_AGE", cv.getId().getValidFromDate().toString());
		map.put("INVESTIGATOR_USER_ID", cv.getInvestrUserId());
		map.put("CASE_RK", String.valueOf(cv.getId().getCaseRk()));
		return map;
	}
}
