package com.datagear.controller;

import com.datagear.customModel.*;
import com.datagear.model.*;
import com.datagear.security.model.LoginUser;
import com.datagear.service.*;
import com.datagear.uploader.model.Comments;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/case")
@CrossOrigin("*")
public class CaseController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Case_LiveService case_LiveServ;

    @Autowired
    private Case_VerService case_VerServ;

    @Autowired
    private Case_X_User_GrpService case_X_User_GrpServ;

    @Autowired
    private Case_Udf_DefService case_Udf_DefServ;

    @Autowired
    private Case_Udf_Char_ValService case_Udf_Char_ValServ;

    @Autowired
    private Case_Udf_Date_ValService case_Udf_Date_ValServ;

    @Autowired
    private Case_Udf_Lgchr_ValService case_Udf_Lgchr_ValServ;

    @Autowired
    private Case_RkService caseRkServ;

    @Autowired
    private Case_ConfService case_ConfServ;

    @Autowired
    private Case_Conf_X_User_GrpService case_Conf_X_User_GrpService;

    @Autowired
    private ECM_EventService ecmEventServ;

    @Autowired
    private Case_X_CustService case_x_custServ;

    @Autowired
    private Customer_LiveService cust_LiveServ;

    @Autowired
    @Qualifier("caseDetailsServiceImpl")
    private EntityDetailsService caseDetailsServ;

    @Autowired
    private Occurrence_LiveService occs_LiveServ;

    @Autowired
    private ECM_Entity_LockService ecm_entity_lockServ;

    @Autowired
    private CommentsController commentsController;

    @Autowired
    private AttachmentController attachmentController;

//	@Autowired
//	private UiFileService uiFileServ;

    /*
     * =====================POST Methods=========================
     */

    @PreAuthorize("hasRole('CASECREATE') or hasRole('CASEEDIT')")
    @PostMapping(value = "save")
    public ResponseEntity<?> save(@RequestBody Map<String, Object> jsonMap) {

        if (!jsonMap.containsKey("CASE_TYPE_CD"))
            return new ResponseEntity<String>("Invalid Data", HttpStatus.BAD_REQUEST);

        String case_type_cd = (String) jsonMap.get("CASE_TYPE_CD");

        Case_Conf case_conf = case_ConfServ.getByCaseTypeCd(case_type_cd);
        if (case_conf == null) {
            logger.error("Invalid CASE_TYPE_CD " + case_type_cd);
            return new ResponseEntity<String>("Invalid CASE_TYPE_CD", HttpStatus.BAD_REQUEST);
        }

        Timestamp cur_datetime = new Timestamp(System.currentTimeMillis());

        String case_id = "";
        Case_Live case_live = null;
        boolean case_rk_exists = false;
        if (jsonMap.containsKey("CASE_ID")) {
            case_id = (String) jsonMap.get("CASE_ID");
            case_live = case_LiveServ.getByCaseId(case_id);
            case_rk_exists = case_live != null;
        }

        Long case_rk;
        if (!case_rk_exists) {
            try {
                case_rk = caseRkServ.getNextCase_Rk();
            } catch (Exception e) {
                logger.error("Cant get next case_rk_seq from stored procedure");
                return new ResponseEntity<>("Cant get next case_rk_seq from stored procedure",
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (StringUtils.isBlank(case_id))
                case_id = String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).concat("-").concat(String.valueOf(case_rk));
            case_type_cd = case_conf.getCaseTypeCd();
            String ui_def_file_name = case_conf.getUiDefFileName();
            String col1 = jsonMap.get("COL1") != null ? jsonMap.get("COL1").toString() : null;
            String col2 = jsonMap.get("COL2") != null ? jsonMap.get("COL2").toString() : null;
            String col3 = jsonMap.get("COL3") != null ? jsonMap.get("COL3").toString() : null;
            String col4 = jsonMap.get("COL4") != null ? jsonMap.get("COL4").toString() : null;
            String col5 = jsonMap.get("COL5") != null ? jsonMap.get("COL5").toString() : null;
            Case_Ver case_ver = new Case_Ver(case_rk, cur_datetime, case_id, case_type_cd, ui_def_file_name);
            case_live = new Case_Live(case_rk, cur_datetime, case_id, case_type_cd, ui_def_file_name);
            case_ver.setCol1(col1);
            case_ver.setCol2(col2);
            case_ver.setCol3(col3);
            case_ver.setCol4(col4);
            case_ver.setCol5(col5);
            case_live.setCol1(col1);
            case_live.setCol2(col2);
            case_live.setCol3(col3);
            case_live.setCol4(col4);
            case_live.setCol5(col5);
            if (jsonMap.containsKey("PRIORITY_CD")) {
                case_live.setPriorityCd((String) jsonMap.get("PRIORITY_CD"));
            }
            try {
                case_VerServ.save(case_ver);
                case_LiveServ.save(case_live);
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>("Cant Save Case Version", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            case_rk = case_live.getCaseRk();
            if (jsonMap.containsKey("PRIORITY_CD")) {
                case_live.setPriorityCd((String) jsonMap.get("PRIORITY_CD"));
            }
            if (jsonMap.get("WF_STATUS_CD") != null) {
                case_live.setCaseStatCd((String) jsonMap.get("WF_STATUS_CD"));
                case_live.setInvestrUserId(LoginUser.getInstance().getUserName());
                if (case_live.getOpenDate() == null) {
                    case_live.setOpenDate(cur_datetime);
                    this.ecmEventServ.activeCaseWorkFlow(case_rk, "Workflow " + case_conf.getInvestWFDefName() + " initialized to case", LoginUser.getInstance().getUserName());
                } else if (jsonMap.get("WF_COMPLETED") == null) {
                    this.ecmEventServ.updateCaseWF(case_rk, "", cur_datetime, LoginUser.getInstance().getUserName());
                }
            }
            if (jsonMap.get("WF_COMPLETED") != null
                    && ((String) jsonMap.get("WF_COMPLETED")).equals("1")) {
                case_live.setCloseDate(cur_datetime);
                this.ecmEventServ.terminateCaseWorkFlow(case_rk, "Workflow " + case_conf.getInvestWFDefName() + " terminated to case", LoginUser.getInstance().getUserName());
            }
            boolean chk = this.updateCaseVersion(case_live, cur_datetime, jsonMap, true);
            if (!chk)
                return new ResponseEntity<String>("Cant Save Case Version", HttpStatus.INTERNAL_SERVER_ERROR);

        }

        Set<String> set = jsonMap.keySet();
//        List<String> udfNameList = set.parallelStream().filter(f -> f.startsWith("X_")).collect(Collectors.toList());

        List<Case_Udf_Def> udf_list = this.case_Udf_DefServ.findByUdfNameList(new ArrayList<>(set));

        List<Case_Udf_Char_Val> char_vals = udf_list.parallelStream()
                .filter(udf -> udf.getUdfTypeName().equals("VARCHAR") && udf.getId().getUdfTableName().equals("CASE"))
                .map(udf -> { // update all versions and check for old values row_no = 1 changed then update val and save
                    if (!StringUtils.isBlank((String) jsonMap.get(udf.getId().getUdfName())))
                        return new Case_Udf_Char_Val(case_rk, cur_datetime, udf.getId().getUdfName(),
                                (String) jsonMap.get(udf.getId().getUdfName()), udf.getId().getUdfTableName(), 1);
                    else return null;
                }).filter(Objects::nonNull).collect(Collectors.toList());

        List<Case_Udf_Date_Val> date_vals = udf_list.parallelStream()
                .filter(udf -> udf.getUdfTypeName().equals("DATE") && udf.getId().getUdfTableName().equals("CASE"))
                .map(udf -> {
                    if (!StringUtils.isBlank((String) jsonMap.get(udf.getId().getUdfName())))
                        return new Case_Udf_Date_Val(case_rk, cur_datetime, udf.getId().getUdfName(),
                                this.strToDate((String) jsonMap.get(udf.getId().getUdfName())), udf.getId().getUdfTableName(), 1);
                    else
                        return null;
                }).filter(Objects::nonNull).collect(Collectors.toList());

        List<Case_Udf_Lgchr_Val> lgchr_vals = udf_list.parallelStream()
                .filter(udf -> udf.getUdfTypeName().equals("LNGVARCHAR") && udf.getId().getUdfTableName().equals("CASE"))
                .map(udf -> {
                    if (!StringUtils.isBlank((String) jsonMap.get(udf.getId().getUdfName())))
                        return new Case_Udf_Lgchr_Val(case_rk, cur_datetime, udf.getId().getUdfName(),
                                (String) jsonMap.get(udf.getId().getUdfName()), udf.getId().getUdfTableName(), 1);
                    else
                        return null;
                }).filter(Objects::nonNull).collect(Collectors.toList());

        try {
            case_Udf_Char_ValServ.saveList(char_vals);
        } catch (Exception e) {
            logger.error("Char values Can't be saved");
            e.printStackTrace();
        }

        try {
            case_Udf_Date_ValServ.saveList(date_vals);
        } catch (Exception e) {
            logger.error("Date values Can't be saved");
            e.printStackTrace();
        }

        try {
            case_Udf_Lgchr_ValServ.saveList(lgchr_vals);
        } catch (Exception e) {
            logger.error("Large Char values Can't be saved");
            e.printStackTrace();
        }

        List<Case_Conf_X_User_Grp> x_user_grp_list = case_Conf_X_User_GrpService.getByCaseConfSeqNo(case_conf.getCaseConfSeqNo());
        x_user_grp_list.forEach(
                x_user_grp -> {
                    String user_grp_name = x_user_grp != null ? x_user_grp.getId().getUserGrpName() : "N/A";
                    try {
                        case_X_User_GrpServ.save(new Case_X_User_Grp(case_rk, user_grp_name));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );

        if (!case_rk_exists) {
            logger.info("Case Successfully added");
            ecmEventServ.loadCase(case_rk, "Create Case WS", LoginUser.getInstance().getUserName());
            return new ResponseEntity<>(case_live, HttpStatus.OK);
        }

        logger.info("Case Successfully Updated");
        ecmEventServ.updateCase(case_rk, "Create Case WS", cur_datetime, LoginUser.getInstance().getUserName());
        return new ResponseEntity<Case_Live>(case_live, HttpStatus.OK);
    }

    /**********************************************************************************************************************/

    @PreAuthorize("hasRole('CASELNKCUST')")
    @PostMapping(value = "linkCust")
    public ResponseEntity<String> linkCustomer(@RequestBody LinkEntities link) {
        if (link == null)
            return new ResponseEntity<String>("Request Body is not correct", HttpStatus.BAD_REQUEST);

        // pre_code-Check if the link already exists
        if (case_x_custServ.isExists(link.getEntityRk(), link.getMemberRk()))
            return new ResponseEntity<String>("Link already exists", HttpStatus.BAD_REQUEST);

        if (!case_LiveServ.isExists(link.getEntityRk()))
            return new ResponseEntity<String>("Case is not exists", HttpStatus.BAD_REQUEST);

        if (!cust_LiveServ.isExists(link.getMemberRk()))
            return new ResponseEntity<String>("Customer is not exists", HttpStatus.BAD_REQUEST);

        // 1-get current datetime
        Timestamp cur_datetime = new Timestamp(System.currentTimeMillis());
//		System.out.println("cur_datetime: " + cur_datetime);

        // 2-save the case_rk and cust_rk to Case_X_Cust with the current date
        try {
            case_x_custServ.save(new Case_X_Cust(link.getEntityRk(), link.getMemberRk(), cur_datetime));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error in linking case " + link.getEntityRk() + " with customer " + link.getMemberRk());
            return new ResponseEntity<String>("Error in linking case with customer", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // 3-Create Event
        ecmEventServ.linkCaseToCustomer(link.getEntityRk(), "Link Case to Customer WS",
                LoginUser.getInstance().getUserName(), link.getMemberRk());
        return new ResponseEntity<String>("Case Successfully Linked To Customer", HttpStatus.OK);
    }

    /**********************************************************************************************************************/

    @PreAuthorize("hasRole('CASEUNLOCKANY')")
    @PostMapping(value = "unlockAny/{id}")
    public ResponseEntity<?> unlockAny(@PathVariable String id) {
        return unlockCase(id, true);
    }

    /**********************************************************************************************************************/

    @PreAuthorize("hasRole('CASESEARCH')")
    @PostMapping(value = "unlock/{id}")
    public ResponseEntity<?> unlock(@PathVariable String id) {
        return unlockCase(id, false);
    }

    /**********************************************************************************************************************/

    @PreAuthorize("hasRole('CASEEDIT')")
    @PostMapping(value = "associateToCase")
    public ResponseEntity<?> associateCase(@RequestBody JsonNode link) {
        if (!(link != null && link.has("linkRk") && link.has("selectionList") && link.get("selectionList").isArray()))
            return new ResponseEntity<String>("Request Body is not correct", HttpStatus.BAD_REQUEST);

        long linkRk = 0;
        if (link.get("linkRk") != null) {
            linkRk = link.get("linkRk").asLong();
        }

        ObjectMapper mapper = new ObjectMapper();
        ObjectReader reader = mapper.readerFor(new TypeReference<List<Case_Live>>() {
        });

        List<Case_Live> case_live_list = null;
        try {
            case_live_list = reader.readValue(link.get("selectionList"));
        } catch (Exception e) {
            System.out.println("Error on mapper converting");
            e.printStackTrace();
            return new ResponseEntity<String>("Error on mapper converting", HttpStatus.BAD_REQUEST);
        }

        Timestamp cur_datetime = new Timestamp(System.currentTimeMillis());

        int cnt = 0;
        for (Case_Live cl : case_live_list) {
            cl.setCaseLinkSk(BigDecimal.valueOf(linkRk));
            boolean chk = this.updateCaseVersion(cl, cur_datetime);
            if (chk) {
                ecmEventServ.linkCaseToCase(cl.getCaseRk(), "Associate Case WS", LoginUser.getInstance().getUserName(),
                        linkRk);
            } else
                cnt++;
        }

        if (cnt == 0)
            return new ResponseEntity<String>("Case Successfully Associated", HttpStatus.OK);
        else if (cnt == case_live_list.size())
            return new ResponseEntity<String>("Cases Association failed", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<String>("Cases not completly Associated", HttpStatus.ACCEPTED);
    }

    @PreAuthorize("hasRole('CASEEDIT')")
    @PostMapping(value = "removeLinkedCases")
    public ResponseEntity<?> removeLinkedCases(@RequestBody List<Case_Live> cases) {
        if (cases == null)
            return new ResponseEntity<String>("Cases should not be null or empty", HttpStatus.BAD_REQUEST);

        BigDecimal linkRk = cases.get(0).getCaseLinkSk();
        Timestamp cur_datetime = new Timestamp(System.currentTimeMillis());

        int cnt = 0;
        for (Case_Live cl : cases) {
            cl.setCaseLinkSk(null);
            boolean chk = this.updateCaseVersion(cl, cur_datetime);
            if (chk) {
                ecmEventServ.removeLink(cl.getCaseRk(), "CASE", "Remove linked Case",
                        LoginUser.getInstance().getUserName(), linkRk.longValue(), "CASE");
            } else
                cnt++;
        }

        try {
            case_LiveServ.saveList(cases);
        } catch (Exception e) {
            logger.error("Error on remove link between cases");
            e.printStackTrace();
            return new ResponseEntity<String>("Error on remove link between cases", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (cnt == 0)
            return new ResponseEntity<String>("Link removed successfully", HttpStatus.OK);
        else if (cnt == cases.size())
            return new ResponseEntity<String>("Cases removing failed", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<String>("Links not completly Removed", HttpStatus.ACCEPTED);
    }

    @PreAuthorize("hasRole('CASECREATE') or hasRole('CASEEDIT')")
    @PostMapping(value = "add-date-field")
    public ResponseEntity<?> addDateField(@RequestBody Map<String, Object> body) {
        if (!body.containsKey("CASE_RK") || !body.containsKey("FIELD_NAME") || !body.containsKey("FIELD_VAL"))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        String id = (String) body.get("CASE_RK");
        long case_rk;
        try {
            case_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Case_Rk must be number", HttpStatus.BAD_REQUEST);
        }

        Case_Live case_live = case_LiveServ.getByCaseRk(case_rk);
        if (case_live == null)
            return new ResponseEntity<String>("Case Not Found", HttpStatus.BAD_REQUEST);

        String fieldName = (String) body.get("FIELD_NAME");
        Case_Udf_Def case_udf_def = case_Udf_DefServ.findFirstByUdf_Name(fieldName);
        if (case_udf_def == null || !case_udf_def.getUdfTypeName().equals("DATE"))
            return new ResponseEntity<>("Field: " + fieldName + " Not Found as date", HttpStatus.BAD_REQUEST);
        Timestamp dateVal = this.strToDate((String) body.get("FIELD_VAL"));
        if (dateVal != null) {
            try {
                this.case_Udf_Date_ValServ.save(
                        new Case_Udf_Date_Val(case_rk,
                                case_live.getValidFromDate(),
                                fieldName,
                                dateVal,
                                case_udf_def.getId().getUdfTableName(),
                                1)
                );
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>("Error on saving field: " + fieldName, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CASECREATE') or hasRole('CASEEDIT')")
    @PostMapping(value = "add-char-field")
    public ResponseEntity<?> addCharField(@RequestBody Map<String, Object> body) {
        if (!body.containsKey("CASE_RK") || !body.containsKey("FIELD_NAME") || !body.containsKey("FIELD_VAL"))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        String id = (String) body.get("CASE_RK");
        long case_rk;
        try {
            case_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Case_Rk must be number", HttpStatus.BAD_REQUEST);
        }

        Case_Live case_live = case_LiveServ.getByCaseRk(case_rk);
        if (case_live == null)
            return new ResponseEntity<>("Case Not Found", HttpStatus.BAD_REQUEST);

        String fieldName = (String) body.get("FIELD_NAME");
        Case_Udf_Def case_udf_def = case_Udf_DefServ.findFirstByUdf_Name(fieldName);
        if (case_udf_def == null || !case_udf_def.getUdfTypeName().equals("VARCHAR"))
            return new ResponseEntity<>("Field: " + fieldName + " Not Found as a varchar", HttpStatus.BAD_REQUEST);
        String charVal = (String) body.get("FIELD_VAL");
        if (!StringUtils.isEmpty(charVal)) {
            try {
                this.case_Udf_Char_ValServ.save(
                        new Case_Udf_Char_Val(case_rk,
                                case_live.getValidFromDate(),
                                fieldName,
                                charVal,
                                case_udf_def.getId().getUdfTableName(),
                                1)
                );
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>("Error on saving field: " + fieldName, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*********************** Commments and Attachments ***********************/

    @PreAuthorize("hasRole('CASEADDCOMMENT')")
    @PostMapping(value = "addComment/{id}")
    public ResponseEntity<?> addComment(@PathVariable String id, @RequestBody Map<String, String> jsonMap) {
        if (!jsonMap.containsKey("subject") || !jsonMap.containsKey("desc"))
            return new ResponseEntity<>("Body is not valid", HttpStatus.BAD_REQUEST);

        long caseRk;
        try {
            caseRk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<>("Case_Rk must be number", HttpStatus.BAD_REQUEST);
        }

        Case_Live case_live = case_LiveServ.getByCaseRk(caseRk);
        if (case_live == null)
            return new ResponseEntity<>("Case Not Found", HttpStatus.BAD_REQUEST);
        String subject = jsonMap.get("subject");
        String desc = jsonMap.get("desc");
        String userId = LoginUser.getInstance().getUserName();
        return this.commentsController.addComment(subject, desc, caseRk, "CASE", userId);
    }

    @PreAuthorize("hasAnyRole('CASEUPDCOMMENT', 'CASEADDCOMMENT')")
    @PostMapping(value = "updateComment")
    public ResponseEntity<?> updateComment(@RequestPart("files") MultipartFile[] files,
                                           @RequestPart("comment") Comments comment) {
        if (comment == null)
            return new ResponseEntity<>("Comment Not found", HttpStatus.NOT_FOUND);
        String username = LoginUser.getInstance().getUserName();
        if (!comment.getUserId().equals(username) || !comment.getEntityType().equals("CASE"))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        return this.commentsController.updateComment(files, comment, username);
    }

    @PreAuthorize("hasRole('CASEDELCOMMENT')")
    @DeleteMapping("deleteComment/{id}")
    public ResponseEntity<?> deleteComment(@PathVariable(name = "id") long commentId) {
        String username = LoginUser.getInstance().getUserName();
        return this.commentsController.deleteComment(commentId, "CASE", username);
    }

    @PreAuthorize("hasAnyRole('CASEADDCOMMENT', 'CASEUPDCOMMENT')")
    @PostMapping("addNewFilesToComment")
    public ResponseEntity<?> addNewFilesToComment(@RequestParam("files") MultipartFile[] files,
                                                  @RequestParam("commentId") String commentId) {
        String userId = LoginUser.getInstance().getUserName();
        return this.attachmentController.addNewFilesToComment(files, Long.valueOf(commentId), "CASE", userId);
    }

    @PreAuthorize("hasRole('CASEDELCOMMENT')")
    @DeleteMapping("removeAttachment/{id}")
    public ResponseEntity<?> removeAttachment(@PathVariable(name = "id") long attachmentId) {
        String userId = LoginUser.getInstance().getUserName();
        return this.attachmentController.removeAttachment(attachmentId, "CASE", userId);
    }

    /*
     * =====================GET Methods=========================
     */

    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "getDetails/{id}")
    public ResponseEntity<?> getDetails(@PathVariable String id, @RequestParam("VIEWMODE") boolean isViewMode) {
        long case_rk = 0;
        try {
            case_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<>("Case_Rk must be number", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        Case_Live case_live = case_LiveServ.getByCaseRk(case_rk);
        if (case_live == null)
            return new ResponseEntity<>("Case Not Found", HttpStatus.BAD_REQUEST);

        Case_Conf case_conf = case_ConfServ.getByCaseTypeCd(case_live.getCaseTypeCd());

        List<Case_X_User_Grp> case_groups = this.case_X_User_GrpServ.getByCaseRk(case_rk);
        List<String> user_groups = LoginUser.getInstance().getGroups();

        if (case_groups.size() == 0 || user_groups.size() == 0)
            return new ResponseEntity<>("Unauthorized to view this case", HttpStatus.UNAUTHORIZED);

        List<String> groups = case_groups.parallelStream().map(g -> g.getId().getUserGrpName()).collect(Collectors.toList());
        for (int i = 0; i < groups.size(); i++) {
            if (user_groups.contains(groups.get(i)))
                break;
            if (i == (groups.size() - 1))
                return new ResponseEntity<>("Unauthorized to view this case", HttpStatus.UNAUTHORIZED);
        }

        // 1-Get Case Data
        List<Case_Udf_Char_Val> char_vals = case_Udf_Char_ValServ.getById(case_rk, case_live.getValidFromDate());
        List<Case_Udf_Date_Val> date_vals = case_Udf_Date_ValServ.getById(case_rk, case_live.getValidFromDate());
        List<Case_Udf_Lgchr_Val> lg_vals = case_Udf_Lgchr_ValServ.getById(case_rk, case_live.getValidFromDate());

        String editFlag = "0";
        caseDetailsServ.setLockUserName(null);
        if (!isViewMode) {
            ECM_Entity_Lock entityLock = this.ecm_entity_lockServ.getById(case_rk, ECM_Entity_LockService.ENTITY_TYPE.Case);
            Timestamp cur_date = new Timestamp(System.currentTimeMillis());

            // 2-Check Lock_By_User field
            if (entityLock == null) {
                entityLock = lockCase(case_rk, cur_date, "Lock Case");
                if (entityLock != null)
                    editFlag = "1";
            } else if (entityLock.getLockUserId().equals(LoginUser.getInstance().getUserName())) {
                editFlag = "1";
            } else {
                // do nothing (Keed editFlag = false)
            }
            caseDetailsServ.setLockUserName(entityLock != null ? entityLock.getLockUserId() : null);
        }

        // 3-Prepare Case Details output
        caseDetailsServ.setEntityHeader(case_live);
        caseDetailsServ.setEntityData(char_vals, date_vals, lg_vals);
        caseDetailsServ.setEditFlag(editFlag);
        caseDetailsServ.setEntityConfig(case_conf);
//		caseDetailsServ.setEntityFields(uiFile != null ? uiFile.getFields() : "No file found");

        return new ResponseEntity<>((CaseDetails) caseDetailsServ.getEntityDetails(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CASECREATE')")
    @GetMapping(value = "getEmptyDetails/{id}")
    public ResponseEntity<?> getEmptyDetails(@PathVariable(name = "id") String caseTypeCd) {
        if (caseTypeCd == null || caseTypeCd.isEmpty())
            return new ResponseEntity<>("CASE_TYPE_CD is missing", HttpStatus.BAD_REQUEST);

        Case_Conf case_conf = case_ConfServ.getByCaseTypeCd(caseTypeCd);

        if (case_conf == null) {
            return new ResponseEntity<>("Invalid CASE_TYPE_CD " + caseTypeCd, HttpStatus.NOT_FOUND);
        }

        // Prepare Case Details output
        caseDetailsServ.setEntityHeader(new Case_Live());
        caseDetailsServ.setEntityData(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        caseDetailsServ.setEditFlag("1");
        caseDetailsServ.setEntityConfig(case_conf);
        caseDetailsServ.setLockUserName(null);

        return new ResponseEntity<CaseDetails>((CaseDetails) caseDetailsServ.getEntityDetails(), HttpStatus.OK);
    }

    /**********************************************************************************************************************/

    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "getAssociatedCases/{id}")
    public ResponseEntity<List<Case_Live>> getAssociatedCases(@PathVariable String id) {
        long case_rk = 0;
        try {
            case_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<List<Case_Live>>(new ArrayList<>(), HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        if (!case_LiveServ.isExists(case_rk))
            return new ResponseEntity<List<Case_Live>>(new ArrayList<>(), HttpStatus.BAD_REQUEST);

        List<Case_Live> assocCases = case_LiveServ.getAssociatedCases(case_rk);

        return new ResponseEntity<List<Case_Live>>(assocCases, HttpStatus.OK);
    }

    /**********************************************************************************************************************/

    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "getAssociatedCust/{id}")
    public ResponseEntity<?> getAssociatedCustomer(@PathVariable String id) {
        long case_rk = 0;
        try {
            case_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("CaseRk not number", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        if (!case_LiveServ.isExists(case_rk))
            return new ResponseEntity<String>("CaseRk not found", HttpStatus.BAD_REQUEST);

        List<Case_X_Cust> case_x_cust_list = case_x_custServ.getByCaseRk(case_rk);

        if (case_x_cust_list.size() == 0)
            return new ResponseEntity<String>("Case do not have associated Customers", HttpStatus.BAD_REQUEST);

        List<Customer_Live> cust_live = new ArrayList<>();

        case_x_cust_list.stream().forEach(case_x_cust -> {
            cust_live.add(cust_LiveServ.getByCustRk(case_x_cust.getId().getCustRk()));
        });

        return new ResponseEntity<List<Customer_Live>>(cust_live, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "getAssociatedOccs/{id}")
    public ResponseEntity<?> getAssociatedOccurrence(@PathVariable String id) {
        long case_rk = 0;
        try {
            case_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("CaseRk not number", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        if (!case_LiveServ.isExists(case_rk))
            return new ResponseEntity<String>("CaseRk not found", HttpStatus.BAD_REQUEST);

        List<Occurrence_Live> assocOccs = occs_LiveServ.getByCaseRk(case_rk).getContent();

        return new ResponseEntity<List<Occurrence_Live>>(assocOccs, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "getHistory/{id}")
    public ResponseEntity<?> getHistory(@PathVariable String id) {
        long case_rk = 0;
        try {
            case_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("CaseRk not number", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        if (!case_LiveServ.isExists(case_rk))
            return new ResponseEntity<String>("CaseRk not found", HttpStatus.BAD_REQUEST);

        List<ECM_Event> events = ecmEventServ.getEntityEvents(case_rk, "CASE");
        List<HistoryDetails> history = new ArrayList<>();

        if (events.size() > 0) {
            events.parallelStream().map(h -> {
                if (h.getEventTypeCd().equals("UPDCASE")) {
                    Case_Ver curVer = this.case_VerServ.getById(h.getBusinessObjectRk(), h.getCreateDate());
                    if (curVer != null && curVer.getVerNo().longValue() > 1) {

                        Case_Ver oldVer = this.case_VerServ.getByVerNo(h.getBusinessObjectRk(),
                                BigDecimal.valueOf(curVer.getVerNo().longValue() - 1));

                        List<HistoryVerDiff> verDiff = this.case_Udf_Char_ValServ.getHistoryVerDiff(curVer.getId().getCaseRk(), curVer.getId().getValidFromDate(), oldVer.getId().getValidFromDate());

                        history.add(new HistoryDetails(h, verDiff));
                    } else
                        history.add(new HistoryDetails(h, null));
                } else {
                    history.add(new HistoryDetails(h, null));
                }
                return h;
            }).collect(Collectors.toList());
        }

        return new ResponseEntity<List<HistoryDetails>>(history, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "getGroupCases")
    public ResponseEntity<?> getGroupCases() {
        LoginUser user = LoginUser.getInstance();
        List<Long> case_rk_list = this.case_X_User_GrpServ.getCaseRkListByGroupNameList(user.getGroups());
        List<Case_Live> case_live_list = case_LiveServ.getByCaseRkList(case_rk_list);
        return new ResponseEntity<List<Case_Live>>(case_live_list, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "getComments/{id}")
    public ResponseEntity<?> getComments(@PathVariable String id) {
        long case_rk;
        try {
            case_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<>("CaseRk not number", HttpStatus.BAD_REQUEST);
        }
        List<Comments> comments = this.commentsController.getAllComments("CASE", case_rk);
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping("downloadAttachment")
    public ResponseEntity<?> downloadFile(@RequestParam("FILENAME") String fileName, HttpServletRequest request) {
        return this.attachmentController.downloadFile(fileName, request);
    }

    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "getAllCasesStatus")
    public ResponseEntity<?> getAllCasesStatus() {
        List<CaseLiveStatus> list = this.case_LiveServ.getAllCasesStatus();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    //    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "sanctionAlertList/{id}")
    public ResponseEntity<?> getSanctionAlertList(@PathVariable(name = "id") long caseRk) {
        Case_Live case_live = this.case_LiveServ.getByCaseRk(caseRk);
        if (case_live == null)
            return new ResponseEntity<>("Case Not Found", HttpStatus.BAD_REQUEST);
        List<Map<String, String>> list = this.case_Udf_Char_ValServ.getSanctionAlertList(caseRk, case_live.getValidFromDate());
        return ResponseEntity.ok(list);
    }

    //    @PreAuthorize("hasRole('CASESEARCH')")
    @GetMapping(value = "sanctionRelAlertList/{id}")
    public ResponseEntity<?> getSanctionRelAlertList(@PathVariable(name = "id") long caseRk) {
        Case_Live case_live = this.case_LiveServ.getByCaseRk(caseRk);
        if (case_live == null)
            return new ResponseEntity<>("Case Not Found", HttpStatus.BAD_REQUEST);
        List<Map<String, String>> list = this.case_Udf_Char_ValServ.getSanctionRelAlertList(caseRk, case_live.getValidFromDate());
        return ResponseEntity.ok(list);
    }

    /*
     * =====================Aِssociated Methods=========================
     */

    private ECM_Entity_Lock lockCase(long case_rk, Timestamp cur_date, String event_desc) {
        ECM_Entity_Lock entityLock = new ECM_Entity_Lock(case_rk, ECM_Entity_LockService.ENTITY_TYPE.Case.getName(),
                LoginUser.getInstance().getUserName(), cur_date);
        try {
            this.ecm_entity_lockServ.save(entityLock);
        } catch (Exception e) {
            logger.error("Error on lock case with case_rk = " + case_rk);
            e.printStackTrace();
            return null;
        }
        ecmEventServ.lockCase(case_rk, event_desc, LoginUser.getInstance().getUserName());
        return entityLock;
    }

    private ResponseEntity<?> unlockCase(String id, boolean unlockAny) {
        long case_rk = 0;
        try {
            case_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Case_Rk must be long number", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        Case_Live case_live = case_LiveServ.getByCaseRk(case_rk);
        if (case_live == null)
            return new ResponseEntity<String>("Case not found", HttpStatus.BAD_REQUEST);

        ECM_Entity_Lock entityLock = this.ecm_entity_lockServ.getById(case_rk, ECM_Entity_LockService.ENTITY_TYPE.Case);

        // Check that case already locked
        if (entityLock == null)
            return new ResponseEntity<String>("Case already unlocked", HttpStatus.BAD_REQUEST);

        // Check that logged user is the locker of this case
        if (!unlockAny && !LoginUser.getInstance().getUserName().equals(entityLock.getLockUserId()))
            return new ResponseEntity<String>("Case locked by another user: " + entityLock.getLockUserId(),
                    HttpStatus.BAD_REQUEST);

        try {
            this.ecm_entity_lockServ.unlockEntity(case_rk, ECM_Entity_LockService.ENTITY_TYPE.Case);
        } catch (Exception e) {
            logger.error("Error while unlocking the case " + case_rk);
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        ecmEventServ.unlockCase(case_rk, "Unlock Case", LoginUser.getInstance().getUserName());

        // 1-try to unlock case
        return new ResponseEntity<String>("Case Successfully Unlocked", HttpStatus.OK);
    }

    private boolean updateCaseVersion(Case_Live case_live, Timestamp cur_datetime) {
        return this.updateCaseVersion(case_live, cur_datetime, null, true);
    }

    private boolean updateCaseVersion(Case_Live case_live, Timestamp cur_datetime, Map<String, Object> jsonMap, boolean updateField) {

        long case_rk = case_live.getCaseRk();
        Timestamp valid_from_date = case_live.getValidFromDate();
        String userId = LoginUser.getInstance().getUserName();

        ObjectMapper mapper = new ObjectMapper();

        case_live.setValidFromDate(cur_datetime);
        case_live.setUpdateUserId(userId);

        Case_Ver cvo = case_VerServ.getById(case_rk, valid_from_date);
        Case_Ver case_ver = null;
        try {
            String liveStr = mapper.writeValueAsString(case_live);
            case_ver = mapper.readValue(liveStr, Case_Ver.class);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        cvo.setValidToDate(cur_datetime);
        case_ver.setValidToDate(null);
        case_ver.getId().setValidFromDate(cur_datetime);
        case_ver.setCaseLinkSk(case_live.getCaseLinkSk());
        case_ver.setVerNo(BigDecimal.valueOf(cvo.getVerNo().longValue() + 1));

        try {
            case_VerServ.save(cvo);
            case_VerServ.save(case_ver);
            case_LiveServ.save(case_live);
        } catch (Exception e) {
            logger.error("Error updating case version for case: " + case_live.getCaseRk());
            e.printStackTrace();
            return false;
        }

        if (updateField) {
            List<Case_Udf_Char_Val> char_vals = case_Udf_Char_ValServ.getById(case_rk, valid_from_date);
            List<Case_Udf_Date_Val> date_vals = case_Udf_Date_ValServ.getById(case_rk, valid_from_date);
            List<Case_Udf_Lgchr_Val> lgchr_vals = case_Udf_Lgchr_ValServ.getById(case_rk, valid_from_date);

            List<Case_Udf_Char_Val> new_chars = char_vals.parallelStream()
                    .map(c -> {
                        Case_Udf_Char_Val ch = (Case_Udf_Char_Val) SerializationUtils.clone(c);
                        ch.getId().setValidFromDate(cur_datetime);
                        if (jsonMap != null && ch.getId().getUdfTableName().equals("CASE") && ch.getId().getRowNo() == 1
                                && jsonMap.get(ch.getId().getUdfName()) != null && !((String) jsonMap.get(ch.getId().getUdfName())).equals(ch.getUdfVal()))
                            ch.setUdfVal((String) jsonMap.get(ch.getId().getUdfName()));
                        return ch;
                    }).collect(Collectors.toList());

            List<Case_Udf_Date_Val> new_dates = date_vals.parallelStream()
                    .map(d -> {
                        Case_Udf_Date_Val dt = (Case_Udf_Date_Val) SerializationUtils.clone(d);
                        dt.getId().setValidFromDate(cur_datetime);
                        if (jsonMap != null && dt.getId().getUdfTableName().equals("CASE") && dt.getId().getRowNo() == 1
                                && jsonMap.get(dt.getId().getUdfName()) != null) {
                            Timestamp dt_val = this.strToDate((String) jsonMap.get(dt.getId().getUdfName()));
                            if (dt_val != null && dt_val.compareTo(dt.getUdfVal()) != 0)
                                dt.setUdfVal(dt_val);
                        }
                        return dt;
                    }).collect(Collectors.toList());

            List<Case_Udf_Lgchr_Val> new_lgchrs = lgchr_vals.parallelStream()
                    .map(l -> {
                        Case_Udf_Lgchr_Val lg = (Case_Udf_Lgchr_Val) SerializationUtils.clone(l);
                        lg.getId().setValidFromDate(cur_datetime);
                        if (jsonMap != null && lg.getId().getUdfTableName().equals("CASE") && lg.getId().getRowNo() == 1
                                && jsonMap.get(lg.getId().getUdfName()) != null && !((String) jsonMap.get(lg.getId().getUdfName())).equals(lg.getUdfVal()))
                            lg.setUdfVal((String) jsonMap.get(lg.getId().getUdfName()));
                        return lg;
                    }).collect(Collectors.toList());
            try {
                case_Udf_Char_ValServ.saveList(new_chars);
                case_Udf_Date_ValServ.saveList(new_dates);
                case_Udf_Lgchr_ValServ.saveList(new_lgchrs);
            } catch (Exception e) {
                logger.error("Error updating case values version for case: " + case_live.getCaseRk());
                return false;
            }
        }
        return true;
    }

    private Timestamp strToDate(String strDate) {
        SimpleDateFormat dttmFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat dtFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return new Timestamp(dttmFormatter.parse(strDate).getTime());
        } catch (ParseException e1) {
            try {
                return new Timestamp(dtFormatter.parse(strDate).getTime());
            } catch (ParseException e2) {
                e2.printStackTrace();
                return null;
            }
        }
    }

    @GetMapping("test")
    public ResponseEntity<?> test() {

//		Case_Live case_live = case_LiveServ.getByCaseRk(11003);
//		this.updateCaseVersion(case_live, new Timestamp(System.currentTimeMillis()));
        return new ResponseEntity<String>("DG-ECM back-end is Working", HttpStatus.OK);
    }


}
