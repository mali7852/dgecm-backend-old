package com.datagear.controller;

import com.datagear.customModel.LinkEntities;
import com.datagear.model.*;
import com.datagear.security.model.LoginUser;
import com.datagear.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/occs")
@CrossOrigin("*")
public class OccurrenceController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Occurrence_LiveService occs_LiveServ;

    @Autowired
    private Occurrence_VerService occs_VerServ;

    @Autowired
    private Occurrence_X_User_GrpService occs_X_User_GrpServ;

    @Autowired
    private Occurrence_Udf_DefService occs_Udf_DefServ;

    @Autowired
    private Occurrence_Udf_Char_ValService occs_Udf_Char_ValServ;

    @Autowired
    private Occurrence_Udf_Date_ValService occs_Udf_Date_ValServ;

    @Autowired
    private Occurrence_RkService occsRkServ;

    @Autowired
    private Occurrence_ConfService occs_ConfServ;

    @Autowired
    private Occurrence_Conf_X_User_GrpService occs_Conf_X_User_GrpService;

    @Autowired
    private Case_X_CustService case_x_custServ;

    @Autowired
    private Case_LiveService case_liveServ;

    @Autowired
    private Occurrence_X_CustomerService occs_x_custServ;

    @Autowired
    private Customer_LiveService cust_liveServ;

    @Autowired
    private ECM_EventService ecmEventServ;

    @Autowired
    @Qualifier("occurrenceDetailsServiceImpl")
    private EntityDetailsService occsDetailsServ;

//	private UiFileService uiFileServ;

    /*
     * =====================Methods=========================
     */

    @PreAuthorize("hasRole('OCCSCREATE')")
    @PostMapping(value = "save")
    public ResponseEntity<String> save(@RequestBody Map<String, Object> jsonMap) {
        if (!jsonMap.containsKey("CASE_RK") || !jsonMap.containsKey("OCCS_TYPE_CD"))
            return new ResponseEntity<String>("Invalid Data", HttpStatus.BAD_REQUEST);

        String case_rk = (String) jsonMap.get("CASE_RK");
        Case_Live case_live = null;
        if (!StringUtils.isBlank(case_rk))
            case_live = this.case_liveServ.getByCaseRk(Long.valueOf(case_rk));
        if (case_live == null)
            return new ResponseEntity<>("Case Not found: " + case_rk, HttpStatus.BAD_REQUEST);

        String occs_id = (String) jsonMap.get("OCCS_ID");
        String occs_type_cd = (String) jsonMap.get("OCCS_TYPE_CD");
        String occs_desc = (String) jsonMap.get("OCCS_DESC");

        Occurrence_Conf occs_Conf = occs_ConfServ.getByOccsTypeCd(occs_type_cd);
        if (occs_Conf == null) {
            logger.error("Invalid ENTITY_TYPE_CD");
            return new ResponseEntity<String>("Invalid ENTITY_TYPE_CD", HttpStatus.BAD_REQUEST);
        }

        Occurrence_Live occs_live;
        if (occs_id == null || occs_id.isEmpty()) {
            occs_live = null;
        } else {
            occs_live = occs_LiveServ.findFirstByOccs_Id(occs_id);
        }
        boolean occs_rk_exists = occs_live != null;

        String ui_def_file_name = occs_Conf.getUiDefFileName();
        Timestamp cur_datetime = new Timestamp(System.currentTimeMillis());


        Long occs_rk;
        if (!occs_rk_exists) {
            try {
                occs_rk = occsRkServ.getNextOccs_Rk();
            } catch (Exception e) {
                logger.error("Cant get next occs_rk_seq from stored procedure");
                return new ResponseEntity<String>("Cant get next occs_rk_seq from stored procedure",
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (occs_id == null || occs_id.isEmpty()) {
                occs_id = String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).concat("-").concat(String.valueOf(occs_rk));
            }

            Occurrence_Ver occs_ver = new Occurrence_Ver(occs_rk, cur_datetime, occs_id, occs_type_cd, ui_def_file_name);
            occs_ver.setOccsDesc(occs_desc);
            occs_ver.setCaseRk(BigDecimal.valueOf(Long.valueOf(case_rk)));
            occs_live = new Occurrence_Live(occs_rk, cur_datetime, occs_id, occs_type_cd, ui_def_file_name);
            occs_live.setOccs_Desc(occs_desc);
            case_live.addOccurrenceLive(occs_live);
            try {
                occs_VerServ.save(occs_ver);
//                occs_LiveServ.save(occs_live);
                case_liveServ.save(case_live);
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<String>("Cant Save occs Version", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            occs_rk = occs_live.getOccsRk();
            boolean chk = this.updateOccsVersion(occs_live, cur_datetime, false);
            if (!chk)
                return new ResponseEntity<String>("Cant Save occs Version", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String user_grp_name = occs_Conf_X_User_GrpService.getUserGrpName(occs_Conf.getOccsConfSeqNo()).getId()
                .getUserGrpName();

        try {
            occs_X_User_GrpServ.save(new Occurrence_X_User_Grp(occs_rk, user_grp_name));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error occurred on saving occs_X_User_Grp",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        List<Occurrence_Udf_Char_Val> char_vals = new ArrayList<>();
        List<Occurrence_Udf_Date_Val> date_vals = new ArrayList<>();

        Set<String> set = jsonMap.keySet();
        List<String> udfNameList = new ArrayList<>(set);
        udfNameList.remove("ENTITY_ID");
        udfNameList.remove("ENTITY_TYPE_CD");

        List<Occurrence_Udf_Def> udf_list = this.occs_Udf_DefServ.findByUdfNameList(udfNameList);

        udf_list.parallelStream().filter(udf -> udf.getUdfTypeName().equals("VARCHAR"))
                .map(udf -> char_vals.add(new Occurrence_Udf_Char_Val(occs_rk, cur_datetime, udf.getId().getUdfName(),
                        (String) jsonMap.get(udf.getId().getUdfName()))))
                .collect(Collectors.toList());

        SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd HH:mm:ss.SSS");
        udf_list.parallelStream().filter(udf -> udf.getUdfTypeName().equals("DATE")).map(udf -> {
            try {
                return date_vals.add(new Occurrence_Udf_Date_Val(occs_rk, cur_datetime, udf.getId().getUdfName(),
                        new Timestamp(formatter.parse((String) jsonMap.get(udf.getId().getUdfName())).getTime())));
            } catch (ParseException e1) {
                e1.printStackTrace();
                return true;
            }
        }).collect(Collectors.toList());

        try {
            occs_Udf_Char_ValServ.saveList(char_vals);
        } catch (Exception e) {
            logger.error("Char values Cant be saved");
            e.printStackTrace();
        }

        try {
            occs_Udf_Date_ValServ.saveList(date_vals);
        } catch (Exception e) {
            logger.error("Date values Cant be saved");
            e.printStackTrace();
        }

        if (occs_rk_exists) {
            logger.info("Occurrence Successfully Updated");
            ecmEventServ.updateOccurrence(occs_rk, "Create Occurrence WS", cur_datetime, LoginUser.getInstance().getUserName());
            return new ResponseEntity<String>("Occurrence Successfully Updated", HttpStatus.OK);
        }
        logger.info("Occurrence Successfully added");
        ecmEventServ.saveOccurrence(occs_rk, "Create Occurrence WS", LoginUser.getInstance().getUserName());
        return new ResponseEntity<String>("Occurrence Successfully added", HttpStatus.OK);

    }

    /***************************************************************************************************************************************************************/

    @PreAuthorize("hasRole('OCCSLNKCASE')")
    @PostMapping(value = "linkCase")
    public ResponseEntity<String> linkCustomer(@RequestBody LinkEntities link) {
        if (link == null)
            return new ResponseEntity<String>("Request Body is not correct", HttpStatus.BAD_REQUEST);

        // pre_code-Check if the link already exists
        long occs_rk = link.getEntityRk();
        long case_rk = link.getMemberRk();
        // Check for occurrence and case existence
        Case_Live case_live = case_liveServ.getByCaseRk(case_rk);
        if (case_live == null)
            return new ResponseEntity<String>("Case is not exists", HttpStatus.BAD_REQUEST);

        Occurrence_Live occs_live = occs_LiveServ.getByOccsRk(occs_rk);
        if (occs_live == null)
            return new ResponseEntity<String>("Occurrence is not exists", HttpStatus.BAD_REQUEST);

        // Check on customer for the case in Case_X_Customer
        Case_X_Cust case_x_cust = case_x_custServ.getByCaseRk(case_rk).get(0);
        if (case_x_cust == null)
            return new ResponseEntity<String>("Case is not linked to a Customer", HttpStatus.BAD_REQUEST);
        // Check for customer existence
        Customer_Live cust_live = cust_liveServ.getByCustRk(case_x_cust.getId().getCustRk());
        if (cust_live == null)
            return new ResponseEntity<String>("Customer is not exists", HttpStatus.BAD_REQUEST);
        // Check for link existence
        if (occs_x_custServ.isExists(occs_rk, cust_live.getCustRk()))
            return new ResponseEntity<String>("Link already exists", HttpStatus.BAD_REQUEST);

        // 1-get current datetime
        Timestamp cur_datetime = new Timestamp(System.currentTimeMillis());
        // System.out.println("cur_datetime: " + cur_datetime);

        // 2-add case_live to occurrence by case_rk
        Timestamp valid_From_Date = occs_live.getValidFromDate();
        Occurrence_Ver occs_ver = occs_VerServ.getById(occs_rk, valid_From_Date);
        Occurrence_Ver new_occs_ver = (Occurrence_Ver) SerializationUtils.clone(occs_ver);

        List<Occurrence_Udf_Char_Val> chars = occs_Udf_Char_ValServ.getById(occs_rk, valid_From_Date);
        List<Occurrence_Udf_Date_Val> dates = occs_Udf_Date_ValServ.getById(occs_rk, valid_From_Date);

        List<Occurrence_Udf_Char_Val> new_chars = new ArrayList<>();
        List<Occurrence_Udf_Date_Val> new_dates = new ArrayList<>();

        if (chars.size() > 0) {
            chars.forEach(c -> {
                Occurrence_Udf_Char_Val ch = (Occurrence_Udf_Char_Val) SerializationUtils.clone(c);
                ch.getId().setValidFromDate(valid_From_Date);
                new_chars.add(ch);
            });
        }

        if (dates.size() > 0) {
            dates.forEach(d -> {
                Occurrence_Udf_Date_Val dt = (Occurrence_Udf_Date_Val) SerializationUtils.clone(d);
                dt.getId().setValidFromDate(valid_From_Date);
                new_dates.add(dt);
            });
        }

        new_occs_ver.setCaseRk(new BigDecimal(case_live.getCaseRk()));
        new_occs_ver.setId(new Occurrence_VerPK(occs_rk, cur_datetime));
        new_occs_ver.setCreateDate(cur_datetime);

        occs_live.setValidFromDate(cur_datetime);
        occs_live.setCaseLive(case_live);
        // 3-save the case_rk and cust_rk to Case_X_Cust with the current date
        try {
            occs_VerServ.save(new_occs_ver);
            occs_LiveServ.save(occs_live);
            occs_Udf_Char_ValServ.saveList(new_chars);
            occs_Udf_Date_ValServ.saveList(new_dates);
            occs_x_custServ.save(new Occurrence_X_Customer(occs_rk, cust_live.getCustRk(), cur_datetime));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error in linking occurrence " + occs_rk + " with customer " + case_rk);
            return new ResponseEntity<String>("Error in linking customer with customer",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // 4-Create Event
        ecmEventServ.linkOccurrenceToCustomer(occs_rk, "Link Occurrence to Customer WS",
                LoginUser.getInstance().getUserName(), cust_live.getCustRk());
        return new ResponseEntity<String>("Occurrence Successfully Linked To Customer", HttpStatus.OK);
    }


    /*
     * =====================GET Methods=========================
     */

    @PreAuthorize("hasRole('OCCSSEARCH')")
    @GetMapping(value = "getDetails/{id}")
    public ResponseEntity<?> getDetails(@PathVariable String id) {
        long occs_rk = 0;
        try {
            occs_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Occs_Rk not number", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        Occurrence_Live occs_live = occs_LiveServ.getByOccsRk(occs_rk);
        if (occs_live == null)
            return new ResponseEntity<String>("Occurrence not found", HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(occs_live, HttpStatus.OK);

    }

    @PreAuthorize("hasRole('OCCSSEARCH')")
    @GetMapping(value = "getHistory/{id}")
    public ResponseEntity<?> getHistory(@PathVariable String id) {
        long occs_rk = 0;
        try {
            occs_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Occs_Rk not number", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        if (!occs_LiveServ.isExists(occs_rk))
            return new ResponseEntity<String>("Occurrence not found", HttpStatus.BAD_REQUEST);

        List<ECM_Event> history = ecmEventServ.getEntityEvents(occs_rk, "OCCS");

        return new ResponseEntity<List<ECM_Event>>(history, HttpStatus.OK);
    }

    /*
     * =====================Aِssociated Methods=========================
     */

    private boolean updateOccsVersion(Occurrence_Live occs_live, Timestamp cur_datetime) {
        return this.updateOccsVersion(occs_live, cur_datetime, true);
    }

    private boolean updateOccsVersion(Occurrence_Live occs_live, Timestamp cur_datetime, boolean updateField) {
        long occs_rk = occs_live.getOccsRk();
        Timestamp valid_from_date = occs_live.getValidFromDate();

        ObjectMapper mapper = new ObjectMapper();

        Occurrence_Ver cvo = occs_VerServ.getById(occs_rk, valid_from_date);
        Occurrence_Ver occs_ver = null;
        try {
            String liveStr = mapper.writeValueAsString(occs_live);
            String verStr = mapper.writeValueAsString(cvo);
            occs_ver = mapper.readValue(liveStr, Occurrence_Ver.class);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        cvo.setValidToDate(cur_datetime);
        occs_ver.setValidToDate(null);
        occs_ver.getId().setValidFromDate(cur_datetime);
        occs_ver.setVerNo(BigDecimal.valueOf(cvo.getVerNo().longValue() + 1));

        occs_live.setValidFromDate(cur_datetime);

        try {
            occs_VerServ.save(cvo);
            occs_VerServ.save(occs_ver);
            occs_LiveServ.save(occs_live);
        } catch (Exception e) {
            logger.error("Error updating occs version for occs: " + occs_live.getOccsRk());
            e.printStackTrace();
            return false;
        }

        if (updateField) {
            List<Occurrence_Udf_Char_Val> char_vals = occs_Udf_Char_ValServ.getById(occs_rk, valid_from_date);
            List<Occurrence_Udf_Date_Val> date_vals = occs_Udf_Date_ValServ.getById(occs_rk, valid_from_date);

            List<Occurrence_Udf_Char_Val> new_chars = new ArrayList<>();
            List<Occurrence_Udf_Date_Val> new_dates = new ArrayList<>();

            char_vals.parallelStream().map(c -> {
                Occurrence_Udf_Char_Val ch = (Occurrence_Udf_Char_Val) SerializationUtils.clone(c);
                ch.getId().setValidFromDate(cur_datetime);
                new_chars.add(ch);
                return c;
            }).collect(Collectors.toList());

            date_vals.parallelStream().map(d -> {
                Occurrence_Udf_Date_Val dt = (Occurrence_Udf_Date_Val) SerializationUtils.clone(d);
                dt.getId().setValidFromDate(cur_datetime);
                new_dates.add(dt);
                return d;
            }).collect(Collectors.toList());
            try {
                occs_Udf_Char_ValServ.saveList(new_chars);
                occs_Udf_Date_ValServ.saveList(new_dates);
            } catch (Exception e) {
                logger.error("Error updating Occurrence date version for Occurrence: " + occs_live.getOccsRk());
                return false;
            }
        }
        return true;
    }

//	private void mapOccs(Occurrence_Ver old, Occurrence_Ver neo) {
//		neo.setClose_Date(old.getClose_Date());
//		neo.setCreate_Date(old.getCreate_Date());
//		neo.setCreate_User_Id(old.getCreate_User_Id());
//		neo.setDelete_Flag(old.getDelete_Flag());
//		neo.setDtction_Date(old.getDtction_Date());
//		neo.setDtction_Time(old.getDtction_Time());
//		neo.setInvestr_User_Id(old.getInvestr_User_Id());
//		neo.setNotif_Date(old.getNotif_Date());
//		neo.setNotif_Time(old.getNotif_Time());
//		neo.setOccs_Ctgry_Cd(old.getOccs_Ctgry_Cd());
//		neo.setOccs_Desc(old.getOccs_Desc());
//		neo.setOccs_Dispos_Cd(old.getOccs_Dispos_Cd());
//		neo.setOccs_From_Date(old.getOccs_From_Date());
//		neo.setOccs_From_Time(old.getOccs_From_Time());
//		neo.setOccs_Stat_Cd(old.getOccs_Stat_Cd());
//		neo.setOccs_Sub_Ctgry_Cd(old.getOccs_Sub_Ctgry_Cd());
//		neo.setOccs_To_Date(old.getOccs_To_Date());
//		neo.setOccs_To_Time(old.getOccs_To_Time());
//		neo.setOccs_Type_Cd(old.getOccs_Type_Cd());
//		neo.setSrc_Sys_Cd(old.getSrc_Sys_Cd());
//		neo.setUpdate_User_Id(old.getUpdate_User_Id());
//		neo.setValid_To_Date(old.getValid_To_Date());
//		neo.setVer_No(old.getVer_No());
//
//	}
//	@PostMapping(value = "testCase")
//	public void testCase() {
//		System.out.println("testCase");
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
//		java.util.Date d;
//		try {
//			d = dateFormat.parse("2019-02-18 16:55:06.467");
//			Timestamp dt = new Timestamp(d.getTime());
//			
//			System.out.println("d:"+d);
//			Occurrence_Ver occs_ver = occs_VerServ .getById(new Occurrence_VerPK(10001L, dt));
//			System.out.println("occs_ver:"+occs_ver);
//			
//			System.out.println("occs_ver.getId().getValid_From_Date():"+occs_ver.getId().getValid_From_Date());
//			Occurrence_VerPK id = new Occurrence_VerPK(10001L,new Timestamp(System.currentTimeMillis()));
//			Occurrence_Ver ver2 = (Occurrence_Ver) occs_ver.clone();
//			ver2.getId().setValid_From_Date(new Timestamp(System.currentTimeMillis()));
//			occs_VerServ.save(ver2);
//		} catch (ParseException e) {
//			
//		} catch (CloneNotSupportedException e) {
//			e.printStackTrace();
//			
//			e.printStackTrace();
//		}
//	}
}
