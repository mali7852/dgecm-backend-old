package com.datagear.controller;

import com.datagear.uploader.model.Attachment;
import com.datagear.uploader.model.Comments;
import com.datagear.uploader.service.AttachmentService;
import com.datagear.uploader.service.CommentsService;
import com.datagear.uploader.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Timestamp;

@Service
public class AttachmentController {

    private static final Logger logger = LoggerFactory.getLogger(AttachmentController.class);
    @Autowired
    private AttachmentService attachmentServ;
    @Autowired
    private CommentsService commentsServ;
    @Autowired
    private FileStorageService fileStorageService;

    public ResponseEntity<Resource> downloadFile(String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadAttachmentAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    public ResponseEntity<?> removeAttachment(long attachmentid, String entityType, String userId) {

        Attachment attachment = this.attachmentServ.findById(attachmentid);
        if (attachment == null)
            return ResponseEntity.ok(null);
        Comments comment = this.commentsServ.findById(attachment.getCommentId());
        if (comment == null || !comment.getUserId().equals(userId) || !comment.getEntityType().equals(entityType))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        this.fileStorageService.removeAttachment(attachment, userId);
        return ResponseEntity.ok(null);
    }

    public ResponseEntity<?> addNewFilesToComment(MultipartFile[] files, long commentId, String entityType, String userId) {

        Comments comment = this.commentsServ.findById(commentId);
        if (comment == null)
            return new ResponseEntity<>("Comment not found", HttpStatus.BAD_REQUEST);
        if (!comment.getUserId().equals(userId) || !comment.getEntityType().equals(entityType))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        Timestamp date = new Timestamp(System.currentTimeMillis());
        this.fileStorageService.addNewFilesToComment(files, commentId, userId, date);
        return ResponseEntity.ok(null);
    }

}