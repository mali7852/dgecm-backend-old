package com.datagear.controller;

import com.datagear.security.model.LoginUser;
import com.datagear.uploader.Exception.MyFileNotFoundException;
import com.datagear.uploader.model.CustomProperties;
import com.datagear.uploader.service.CustomPropertiesService;
import com.datagear.uploader.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("upload/api/custom-properties")
@PreAuthorize("hasRole('ADMINUPLOADCUSTOMPROP')")
public class CustomPropertiesController {
    private static final Logger logger = LoggerFactory.getLogger(CustomPropertiesController.class);

    @Autowired
    private CustomPropertiesService customPropertiesServ;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/")
    public ResponseEntity<?> getAll() {
        List<CustomProperties> customPropertiesList = this.customPropertiesServ.findAll();
        return new ResponseEntity<>(customPropertiesList, HttpStatus.OK);
    }

    @PostMapping("save")
    public ResponseEntity<?> uploadMultipleFiles(@RequestParam("file") MultipartFile file,
                                                 @RequestParam("desc") String desc) {
        Timestamp date = new Timestamp(System.currentTimeMillis());
        String username = LoginUser.getInstance().getUserName();
        this.fileStorageService.uploadCustomProperties(file, desc, username, date);

        return new ResponseEntity<>("Successfully Saved", HttpStatus.OK);
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> downloadFile(@RequestParam("FILENAME") String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadCustomPropertiesAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "properties; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> DeleteAttachment(@RequestParam("FILENAME") String fileName) {
        CustomProperties customProperties = this.customPropertiesServ.getByName(fileName);
        if (customProperties != null && customProperties.getDeleteFlg() != '1') {
            this.fileStorageService.deleteCustomProperties(fileName);
            customProperties.setDeleteFlg('1');
            customProperties.setDelUserId(LoginUser.getInstance().getUserName());
            this.customPropertiesServ.save(customProperties);
        } else {
            throw new MyFileNotFoundException("File is not exists: " + fileName);
        }
        return new ResponseEntity<String>("Successfully deleted", HttpStatus.OK);
    }
}
