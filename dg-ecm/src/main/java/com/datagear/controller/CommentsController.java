package com.datagear.controller;

import com.datagear.uploader.model.Attachment;
import com.datagear.uploader.model.Comments;
import com.datagear.uploader.service.AttachmentService;
import com.datagear.uploader.service.CommentsService;
import com.datagear.uploader.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;

@Service
public class CommentsController {

    @Autowired
    private CommentsService commentsServ;

    @Autowired
    private AttachmentService attachmentServ;

    @Autowired
    private FileStorageService fileStorageService;

    public List<Comments> getAllComments(String entityType, long entityRk) {

        List<Comments> comments = this.commentsServ.findByEntityTypeAndEntityRk(entityType, entityRk);
        comments.forEach(c -> {
            List<Attachment> attachments = this.attachmentServ.findByCommentId(c.getId());
            if (attachments != null && attachments.size() > 0)
                c.setAttachments(new HashSet<>(attachments));
        });

        return comments;
    }

    public ResponseEntity<?> updateComment(MultipartFile[] files, Comments comment, String username) {
        Comments newComment = this.fileStorageService.updateComment(comment, files, username);
        return ResponseEntity.ok(newComment);
    }

    public ResponseEntity<?> deleteComment(long commentId, String entityType, String username) {
        Comments comment = this.commentsServ.findById(commentId);
        if (comment == null || comment.getDeleteFlg() == '1')
            return ResponseEntity.ok().build();
        if (!comment.getUserId().equals(username) || !comment.getEntityType().equals(entityType))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        this.fileStorageService.deleteComment(comment, username);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> addComment(String subject, String desc, long entityRk, String entityType, String userId) {
        Comments c = new Comments(subject, desc, new Timestamp(System.currentTimeMillis()), entityRk, entityType,
                userId);
        Comments newc;
        try {
            newc = this.commentsServ.save(c);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Can\'t save comment", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(newc, HttpStatus.OK);
    }

}