package com.datagear.controller;

import com.datagear.customModel.CustomerDetails;
import com.datagear.customModel.HistoryDetails;
import com.datagear.customModel.HistoryVerDiff;
import com.datagear.customModel.LinkEntities;
import com.datagear.model.*;
import com.datagear.security.model.LoginUser;
import com.datagear.service.*;
import com.datagear.uploader.model.Comments;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/cust")
@CrossOrigin("*")
public class CustomerController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    Customer_Udf_Lgchr_ValService cust_Udf_Lgchr_ValServ;
    @Autowired
    private Customer_LiveService cust_LiveServ;
    @Autowired
    private Customer_VerService cust_VerServ;
    @Autowired
    private Customer_X_User_GrpService cust_X_User_GrpServ;
    @Autowired
    private Customer_Udf_DefService cust_Udf_DefServ;
    @Autowired
    private Customer_Udf_Char_ValService cust_Udf_Char_ValServ;
    @Autowired
    private Customer_Udf_Date_ValService cust_Udf_Date_ValServ;
    @Autowired
    private Cust_RkService custRkServ;

    @Autowired
    private Customer_ConfService cust_ConfServ;

    @Autowired
    private Customer_Conf_X_User_GrpService cust_Conf_X_User_GrpService;

    @Autowired
    private ECM_EventService ecmEventServ;

    @Autowired
    private Customer_X_CustomerService cust_x_custServ;

    @Autowired
    private Case_X_CustService case_X_CustServ;

    @Autowired
    @Qualifier("customerDetailsServiceImpl")
    private EntityDetailsService custDetailsServ;

//	@Autowired
//	private UiFileService uiFileServ;

    @Autowired
    private Case_LiveService case_LiveServ;

    @Autowired
    private Occurrence_X_CustomerService occs_X_CustServ;

    @Autowired
    private CommentsController commentsController;

    @Autowired
    private AttachmentController attachmentController;

    /*
     * =====================POST Methods=========================
     */

    @PreAuthorize("hasRole('CUSTCREATE')")
    @PostMapping(value = "save")
    public ResponseEntity<?> saveCustomer(@RequestBody Map<String, Object> jsonMap) {

        String cust_id = (String) jsonMap.get("CUST_ID");
        String cust_type_cd = (String) jsonMap.get("CUST_TYPE_CD");

        if (cust_id == null || cust_type_cd == null)
            return new ResponseEntity<String>("Invalid Data", HttpStatus.BAD_REQUEST);

        Customer_Conf cust_Conf = cust_ConfServ.getByCustTypeCd(cust_type_cd);
        if (cust_Conf == null) {
            logger.error("Invalid CUST_TYPE_CD");
            return new ResponseEntity<String>("Invalid CUST_TYPE_CD", HttpStatus.BAD_REQUEST);
        }

        Customer_Live cust_live = cust_LiveServ.findFirstByCust_Id(cust_id);
        boolean cust_rk_exists = cust_live != null;

        String ui_def_file_name = cust_Conf.getUiDefFileName();
        Timestamp cur_datetime = new Timestamp(System.currentTimeMillis());

        Long cust_rk;
        if (!cust_rk_exists) {
            try {
                cust_rk = custRkServ.getNextCust_Rk();
            } catch (Exception e) {
                logger.error("Cant get next cust_rk_seq from stored procedure");
                return new ResponseEntity<String>("Cant get next cust_rk_seq from stored procedure",
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }
            Customer_Ver cust_ver = new Customer_Ver(cust_rk, cur_datetime, cust_id, cust_type_cd, ui_def_file_name);
            cust_live = new Customer_Live(cust_rk, cur_datetime, cust_id, cust_type_cd, ui_def_file_name);
            try {
                cust_VerServ.save(cust_ver);
                cust_LiveServ.save(cust_live);
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<String>("Cant Save Cust Version", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            cust_rk = cust_live.getCustRk();
            boolean chk = this.updateCustVersion(cust_live, cur_datetime, false);
            if (!chk)
                return new ResponseEntity<String>("Cant Save Cust Version", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String user_grp_name = cust_Conf_X_User_GrpService.getUserGrpName(cust_Conf.getCustConfSeqNo()).getId()
                .getUserGrpName();

        try {
            cust_X_User_GrpServ.save(new Customer_X_User_Grp(cust_rk, user_grp_name));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error occurred on saving Cust_X_User_Grp",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        List<Customer_Udf_Char_Val> char_vals = new ArrayList<>();
        List<Customer_Udf_Date_Val> date_vals = new ArrayList<>();

        Set<String> set = jsonMap.keySet();
        List<String> udfNameList = new ArrayList<>(set);
        udfNameList.remove("CUST_ID");
        udfNameList.remove("CUST_TYPE_CD");

        List<Customer_Udf_Def> udf_list = this.cust_Udf_DefServ.findByUdfNameList(udfNameList);

        udf_list.parallelStream().filter(udf -> udf.getUdfTypeName().equals("VARCHAR"))
                .map(udf -> char_vals.add(new Customer_Udf_Char_Val(cust_rk, cur_datetime, udf.getId().getUdfName(),
                        (String) jsonMap.get(udf.getId().getUdfName()))))
                .collect(Collectors.toList());

        SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd HH:mm:ss.SSS");
        udf_list.parallelStream().filter(udf -> udf.getUdfTypeName().equals("DATE")).map(udf -> {
            try {
                return date_vals.add(new Customer_Udf_Date_Val(cust_rk, cur_datetime, udf.getId().getUdfName(),
                        new Timestamp(formatter.parse((String) jsonMap.get(udf.getId().getUdfName())).getTime())));
            } catch (ParseException e1) {
                e1.printStackTrace();
                return true;
            }
        }).collect(Collectors.toList());

        System.out.println("Customer Save -- Udf_Char_Size: " + char_vals.size() + " Udf_Date_Size: " + date_vals.size());

        try {
            cust_Udf_Char_ValServ.saveList(char_vals);
        } catch (Exception e) {
            logger.error("Char values Cant be saved");
            e.printStackTrace();
        }

        try {
            cust_Udf_Date_ValServ.saveList(date_vals);
        } catch (Exception e) {
            logger.error("Date values Cant be saved");
            e.printStackTrace();
        }

        if (cust_rk_exists) {
            logger.info("Customer Successfully Updated");
            ecmEventServ.updateCustomer(cust_rk, "Create Customer WS", cur_datetime, LoginUser.getInstance().getUserName());
            return new ResponseEntity<Customer_Live>(cust_live, HttpStatus.OK);
        }
        logger.info("Customer Successfully added");
        ecmEventServ.saveCustomer(cust_rk, "Create Customer WS", LoginUser.getInstance().getUserName());
        return new ResponseEntity<Customer_Live>(cust_live, HttpStatus.OK);

    }

    /***************************************************************************************************************************************************************/

    @PreAuthorize("hasRole('CUSTLNKCUST')")
    @PostMapping(value = "linkCust")
    public ResponseEntity<String> linkCustomer(@RequestBody LinkEntities link) {
        if (link == null)
            return new ResponseEntity<String>("Request Body is not correct", HttpStatus.BAD_REQUEST);

        // pre_code-Check if the link already exists
        if (cust_x_custServ.isExists(link.getEntityRk(), link.getMemberRk()))
            return new ResponseEntity<String>("Link already exists", HttpStatus.BAD_REQUEST);

        if (!cust_LiveServ.isExists(link.getEntityRk()))
            return new ResponseEntity<String>("Customer is not exists", HttpStatus.BAD_REQUEST);

        if (!cust_LiveServ.isExists(link.getMemberRk()))
            return new ResponseEntity<String>("Member Customer is not exists", HttpStatus.BAD_REQUEST);

        // 1-get current datetime
//		Timestamp cur_datetime = new Timestamp(System.currentTimeMillis());
//		System.out.println("cur_datetime: " + cur_datetime);

        // 2-save the case_rk and cust_rk to Case_X_Cust with the current date
        try {
            cust_x_custServ.save(new Customer_X_Customer(link.getEntityRk(), link.getMemberRk()));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error in linking customer " + link.getEntityRk() + " with customer " + link.getMemberRk());
            return new ResponseEntity<String>("Error in linking customer with customer",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // 3-Create Event
        ecmEventServ.linkCustomerToCustomer(link.getEntityRk(), "Link Case to Customer WS",
                LoginUser.getInstance().getUserName(), link.getMemberRk());
        return new ResponseEntity<String>("Customer Successfully Linked To Customer", HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CUSTADDCOMMENT')")
    @PostMapping(value = "addComment/{id}")
    public ResponseEntity<?> addComment(@PathVariable String id, @RequestBody Map<String, String> jsonMap) {
        long custRk;
        try {
            custRk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<>("Case_Rk must be number", HttpStatus.BAD_REQUEST);
        }
        // Pre-code
        Customer_Live cust_live = cust_LiveServ.getByCustRk(custRk);
        if (cust_live == null)
            return new ResponseEntity<>("Customer Not Found", HttpStatus.BAD_REQUEST);

        if (!jsonMap.containsKey("subject") || !jsonMap.containsKey("desc"))
            return new ResponseEntity<>("Body is not valid", HttpStatus.BAD_REQUEST);
        String subject = jsonMap.get("subject");
        String desc = jsonMap.get("desc");
        String userId = LoginUser.getInstance().getUserName();
        return this.commentsController.addComment(subject, desc, custRk, "CUST", userId);
    }

    @PreAuthorize("hasAnyRole('CUSTUPDCOMMENT', 'CUSTADDCOMMENT')")
    @PostMapping(value = "updateComment")
    public ResponseEntity<?> updateComment(@RequestPart("files") MultipartFile[] files,
                                           @RequestPart("comment") Comments comment) {
        if (comment == null)
            return new ResponseEntity<>("Comment Not found", HttpStatus.NOT_FOUND);
        String username = LoginUser.getInstance().getUserName();
        if (!comment.getUserId().equals(username) || !comment.getEntityType().equals("CUST"))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        return this.commentsController.updateComment(files, comment, username);
    }

    @PreAuthorize("hasRole('CUSTDELCOMMENT')")
    @DeleteMapping("deleteComment/{id}")
    public ResponseEntity<?> deleteComment(@PathVariable(name = "id") long commentId) {
        String username = LoginUser.getInstance().getUserName();
        return this.commentsController.deleteComment(commentId, "CUST", username);
    }

    @PreAuthorize("hasAnyRole('CUSTADDCOMMENT', 'CUSTUPDCOMMENT')")
    @PostMapping("addNewFilesToComment")
    public ResponseEntity<?> addNewFilesToComment(@RequestParam("files") MultipartFile[] files,
                                                  @RequestParam("commentId") String commentId, @RequestParam("userId") String userId) {
        return this.attachmentController.addNewFilesToComment(files, Long.parseLong(commentId), "CUST", userId);
    }

    @PreAuthorize("hasRole('CUSTDELCOMMENT')")
    @DeleteMapping("removeAttachment/{id}")
    public ResponseEntity<?> removeAttachment(@PathVariable long attachmentId) {
        String userId = LoginUser.getInstance().getUserName();
        return this.attachmentController.removeAttachment(attachmentId, "CUST", userId);
    }

    /*
     * =====================GET Methods=========================
     */

    @PreAuthorize("hasRole('CUSTSEARCH')")
    @GetMapping(value = "getDetails/{id}")
    public ResponseEntity<?> getDetails(@PathVariable String id) {
        long cust_rk = 0;
        try {
            cust_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Cust_Rk must be number.", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        Customer_Live cust_live = cust_LiveServ.getByCustRk(cust_rk);
        if (cust_live == null)
            return new ResponseEntity<String>("Customer Not Found", HttpStatus.BAD_REQUEST);

        // 1-Get Customer Data
        List<Customer_Udf_Char_Val> char_vals = cust_Udf_Char_ValServ.getById(cust_rk, cust_live.getValidFromDate());
        List<Customer_Udf_Date_Val> date_vals = cust_Udf_Date_ValServ.getById(cust_rk, cust_live.getValidFromDate());
        List<Customer_Udf_Lgchr_Val> lg_vals = cust_Udf_Lgchr_ValServ.getById(cust_rk, cust_live.getValidFromDate());

        // 2-Check Lock_By_User field
//		String editFlag = "0";
//		if (cust_live.getLockByUser() == null) {
//			lockCustomer(cust_rk, cust_live);
//			editFlag = "1";
//		} else if (cust_live.getLockByUser().equals(LoginUser.getInstance().getUserName())) {
//			editFlag = "1";
//		} else {
//			// do nothing (Keed editFlag = false)
//		}

        // 3-Get cust_conf
        Customer_Conf cust_conf = cust_ConfServ.getByCustTypeCd(cust_live.getCustTypeCd());

        // 4-Get cust_file_fields
//		UiFile uiFile = uiFileServ.getByUiName(cust_conf.getUiDefFileName());

        // 5-Get lock user display name
        // final JwtUser userDetails =
        // (JwtUser)userDetailsService.loadUserByUsername(cust_live.getLockByUser());

        // 5-Prepare Customer Details output
        custDetailsServ.setEntityHeader(cust_live);
        custDetailsServ.setEntityData(char_vals, date_vals, lg_vals);
        custDetailsServ.setEditFlag("0");
        custDetailsServ.setEntityConfig(cust_conf);
//		custDetailsServ.setEntityFields(uiFile != null ? uiFile.getFields() : "No file found");
        // custDetailsServ.setLockUserName(userDetails.getDisplayName() == null ?
        // userDetails.getUsername() : userDetails.getDisplayName()); // if then else =>
        // condition ? :

        // 4-Save cust_live and return details
        //cust_LiveServ.save(cust_live);
        return new ResponseEntity<CustomerDetails>((CustomerDetails) custDetailsServ.getEntityDetails(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CUSTSEARCH')")
    @GetMapping(value = "getAssociatedCases/{id}")
    public ResponseEntity<?> getAssociatedCases(@PathVariable String id) {
        long cust_rk = 0;
        try {
            cust_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Cust_Rk must be number.", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        Customer_Live cust_live = cust_LiveServ.getByCustRk(cust_rk);
        if (cust_live == null)
            return new ResponseEntity<String>("Customer Not Found", HttpStatus.BAD_REQUEST);

        // 1-Get links
        List<Case_X_Cust> case_x_custList = case_X_CustServ.getByCustRk(cust_rk);

        // 2-Prepare cases' list
        List<Case_Live> casesList = new ArrayList<>();

        case_x_custList.forEach(x -> {
            casesList.add(case_LiveServ.getByCaseRk(x.getId().getCaseRk()));
        });

        return new ResponseEntity<List<Case_Live>>(casesList, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CUSTSEARCH')")
    @GetMapping(value = "getAssociatedCust/{id}")
    public ResponseEntity<?> getAssociatedCustomer(@PathVariable String id) {
        long cust_rk = 0;
        try {
            cust_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Cust_Rk must be number.", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        Customer_Live cust_live = cust_LiveServ.getByCustRk(cust_rk);
        if (cust_live == null)
            return new ResponseEntity<String>("Customer Not Found", HttpStatus.BAD_REQUEST);

        // 1-Get links
        List<Customer_X_Customer> cust_x_cust = cust_x_custServ.getByCustRk(cust_rk);

        // 2-Prepare Customers' list
        List<Customer_Live> custsList = new ArrayList<>();

        cust_x_cust.forEach(x -> {
            custsList.add(cust_LiveServ.getByCustRk(x.getId().getCustRk()));
        });

        return new ResponseEntity<List<Customer_Live>>(custsList, HttpStatus.OK);
    }

    //	@PreAuthorize("hasRole('CUSTSEARCH')")
    @GetMapping(value = "getAssociatedOccs/{id}")
    public ResponseEntity<?> getAssociatedOccurrence(@PathVariable String id) {
        long cust_rk = 0;
        try {
            cust_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Cust_Rk must be number.", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        if (cust_LiveServ.isExists(cust_rk) == false)
            return new ResponseEntity<String>("Customer Not Found", HttpStatus.BAD_REQUEST);

        // 1-Get links
        List<Occurrence_X_Customer> occs_x_cust = occs_X_CustServ.getByCustRk(cust_rk);

        // 2-Prepare Occurrences' list
//		List<Long> occs_rks = new ArrayList<>();
        List<Occurrence_Live> occsList = new ArrayList<>();

        occs_x_cust.forEach(x -> {
            occsList.add(x.getOccurrenceLive());
        });

//		List<Occurrence_Live> occsList = occs_LiveServ.getByOccsRkList(occs_rks);

        return new ResponseEntity<List<Occurrence_Live>>(occsList, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CUSTSEARCH')")
    @GetMapping(value = "getHistory/{id}")
    public ResponseEntity<?> getHistory(@PathVariable String id) {
        long cust_rk = 0;
        try {
            cust_rk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<String>("Cust_Rk must be number.", HttpStatus.BAD_REQUEST);
        }

        // Pre-code
        Customer_Live cust_live = cust_LiveServ.getByCustRk(cust_rk);
        if (cust_live == null)
            return new ResponseEntity<String>("Customer Not Found", HttpStatus.BAD_REQUEST);

        List<ECM_Event> events = ecmEventServ.getEntityEvents(cust_rk, "CUST");
        List<HistoryDetails> history = new ArrayList<>();

        if (events.size() > 0) {
            events.parallelStream().map(h -> {
                if (h.getEventTypeCd().equals("UPDCUST")) {
                    Customer_Ver curVer = this.cust_VerServ.getById(h.getBusinessObjectRk(), h.getCreateDate());
                    if (curVer != null && curVer.getVerNo().longValue() > 1) {

                        Customer_Ver oldVer = this.cust_VerServ.getByVerNo(h.getBusinessObjectRk(),
                                BigDecimal.valueOf(curVer.getVerNo().longValue() - 1));

                        List<HistoryVerDiff> verDiff = this.cust_Udf_Char_ValServ.getHistoryVerDiff(curVer.getId().getCustRk(), curVer.getId().getValidFromDate(), oldVer.getId().getValidFromDate());

                        history.add(new HistoryDetails(h, verDiff));
                    } else
                        history.add(new HistoryDetails(h, null));
                } else {
                    history.add(new HistoryDetails(h, null));
                }
                return h;
            }).collect(Collectors.toList());
        }

        return new ResponseEntity<List<HistoryDetails>>(history, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CUSTSEARCH')")
    @GetMapping(value = "getComments/{id}")
    public ResponseEntity<?> getComments(@PathVariable String id) {
        long custRk;
        try {
            custRk = Long.parseLong(id);
        } catch (NumberFormatException e1) {
            return new ResponseEntity<>("CustRk not number", HttpStatus.BAD_REQUEST);
        }
        List<Comments> comments = this.commentsController.getAllComments("CUST", custRk);
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CUSTSEARCH')")
    @GetMapping("/downloadFile")
    public ResponseEntity<?> downloadFile(@RequestParam("FILENAME") String fileName, HttpServletRequest request) {
        return this.attachmentController.downloadFile(fileName, request);
    }

    /*
     * =====================Aِssociated Methods=========================
     */

    private boolean updateCustVersion(Customer_Live cust_live, Timestamp cur_datetime) {
        return this.updateCustVersion(cust_live, cur_datetime, true);
    }

    private boolean updateCustVersion(Customer_Live cust_live, Timestamp cur_datetime, boolean updateField) {
        long cust_rk = cust_live.getCustRk();
        Timestamp valid_from_date = cust_live.getValidFromDate();

        ObjectMapper mapper = new ObjectMapper();

        Customer_Ver cvo = cust_VerServ.getById(cust_rk, valid_from_date);
        Customer_Ver cust_ver = null;
        try {
            String liveStr = mapper.writeValueAsString(cust_live);
            cust_ver = mapper.readValue(liveStr, Customer_Ver.class);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        cvo.setValidToDate(cur_datetime);
        cust_ver.setValidToDate(null);
        cust_ver.getId().setValidFromDate(cur_datetime);
        cust_ver.setVerNo(BigDecimal.valueOf(cvo.getVerNo().longValue() + 1));

        cust_live.setValidFromDate(cur_datetime);

        try {
            cust_VerServ.save(cvo);
            cust_VerServ.save(cust_ver);
            cust_LiveServ.save(cust_live);
        } catch (Exception e) {
            logger.error("Error updating cust version for cust: " + cust_live.getCustRk());
            e.printStackTrace();
            return false;
        }

        if (updateField) {
            List<Customer_Udf_Char_Val> char_vals = cust_Udf_Char_ValServ.getById(cust_rk, valid_from_date);
            List<Customer_Udf_Date_Val> date_vals = cust_Udf_Date_ValServ.getById(cust_rk, valid_from_date);

            List<Customer_Udf_Char_Val> new_chars = new ArrayList<>();
            List<Customer_Udf_Date_Val> new_dates = new ArrayList<>();

            char_vals.parallelStream().map(c -> {
                Customer_Udf_Char_Val ch = (Customer_Udf_Char_Val) SerializationUtils.clone(c);
                ch.getId().setValidFromDate(cur_datetime);
                new_chars.add(ch);
                return c;
            }).collect(Collectors.toList());

            date_vals.parallelStream().map(d -> {
                Customer_Udf_Date_Val dt = (Customer_Udf_Date_Val) SerializationUtils.clone(d);
                dt.getId().setValidFromDate(cur_datetime);
                new_dates.add(dt);
                return d;
            }).collect(Collectors.toList());
            try {
                cust_Udf_Char_ValServ.saveList(new_chars);
                cust_Udf_Date_ValServ.saveList(new_dates);
            } catch (Exception e) {
                logger.error("Error updating customer date version for customer: " + cust_live.getCustRk());
                return false;
            }
        }
        return true;
    }

}
