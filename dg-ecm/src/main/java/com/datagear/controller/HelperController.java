package com.datagear.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datagear.model.Ref_Table_Val;
import com.datagear.service.Ref_Table_ValService;

@RestController
@RequestMapping(value = "api/helper")
@CrossOrigin("*")
public class HelperController {

	@Autowired
	private Ref_Table_ValService ref_Table_ValServ;

	@PreAuthorize("hasRole('CASESEARCH')")
	@PostMapping(value = "lookUps")
	public ResponseEntity<?> getLookUps(@RequestBody List<String> lookups) {
		if (lookups == null || lookups.size() == 0)
			return new ResponseEntity<String>("Null or Empty array", HttpStatus.BAD_REQUEST);

		List<Ref_Table_Val> lookupVals = ref_Table_ValServ.getLookUpList(lookups); 
		
		return new ResponseEntity<List<Ref_Table_Val>>(lookupVals, HttpStatus.OK);
	}

}
