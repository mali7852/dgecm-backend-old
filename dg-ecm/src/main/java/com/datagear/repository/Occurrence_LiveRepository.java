package com.datagear.repository;

import com.datagear.customModel.DashboardStatistics;
import com.datagear.model.Occurrence_Live;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Occurrence_LiveRepository extends JpaRepository<Occurrence_Live, Long>, JpaSpecificationExecutor<Occurrence_Live> {

    Occurrence_Live findFirstByOccsId(String occs_id);

    Page<Occurrence_Live> findByCaseLiveCaseRk(long case_rk, Pageable pageable);

    List<Occurrence_Live> findByOccsRkIn(List<Long> occs_rks);

    @Query(value = "" +
            "select distinct Occs_Type_Cd                                                                as name, " +
            "                count(Occs_Rk)                                                              as count, " +
            "                LOG((count(Occs_Rk)), 2) as y " +
            "from DGCmgmt.Occurrence_Live " +
            "where Occs_Type_Cd is not null " +
            "group by Occs_Type_Cd " +
            "order by count desc",
            nativeQuery = true)
    List<DashboardStatistics> getOccsTypeStatistics();
}
