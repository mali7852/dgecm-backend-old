package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_Conf_X_User_Grp;
import com.datagear.model.Customer_Conf_X_User_GrpPK;

@Repository
public interface Customer_Conf_X_User_GrpRepository extends JpaRepository<Customer_Conf_X_User_Grp, Customer_Conf_X_User_GrpPK> {

	Customer_Conf_X_User_Grp findFirstByIdCustConfSeqNo(long cust_Conf_Seq_No);

}
