package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.ECM_Locale;

@Repository
public interface ECM_LocaleRepository extends JpaRepository<ECM_Locale, String> {

}
