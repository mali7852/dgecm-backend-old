package com.datagear.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_Udf_Date_Val;
import com.datagear.model.Customer_Udf_Date_ValPK;

@Repository
public interface Customer_Udf_Date_ValRepository extends JpaRepository<Customer_Udf_Date_Val, Customer_Udf_Date_ValPK> {

	List<Customer_Udf_Date_Val> findByIdCustRkAndIdValidFromDate(long cust_rk, Timestamp valid_From_Date);

}
