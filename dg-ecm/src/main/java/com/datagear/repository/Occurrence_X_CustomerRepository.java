package com.datagear.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_X_Customer;
import com.datagear.model.Occurrence_X_CustomerPK;

@Repository
public interface Occurrence_X_CustomerRepository extends JpaRepository<Occurrence_X_Customer, Occurrence_X_CustomerPK> {

	Occurrence_X_Customer findFirstByIdOccsRkAndIdCustRk(long occs_rk, long cust_rk);

	List<Occurrence_X_Customer> findByIdCustRk(long cust_rk);

}
