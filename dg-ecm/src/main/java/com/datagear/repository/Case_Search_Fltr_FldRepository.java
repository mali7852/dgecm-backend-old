package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Search_Fltr_Fld;
import com.datagear.model.Case_Search_Fltr_FldPK;

@Repository
public interface Case_Search_Fltr_FldRepository extends JpaRepository<Case_Search_Fltr_Fld, Case_Search_Fltr_FldPK> {

}
