package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Efile_Search_Criteria_Field;
import com.datagear.model.Efile_Search_Criteria_FieldPK;

@Repository
public interface Efile_Search_Criteria_FieldRepository extends JpaRepository<Efile_Search_Criteria_Field, Efile_Search_Criteria_FieldPK> {

}
