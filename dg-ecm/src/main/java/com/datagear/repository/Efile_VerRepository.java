package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Efile_Ver;
import com.datagear.model.Efile_VerPK;

@Repository
public interface Efile_VerRepository extends JpaRepository<Efile_Ver, Efile_VerPK> {

}
