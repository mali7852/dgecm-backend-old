package com.datagear.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.ECM_Event;

@Repository
public interface ECM_EventRepository extends JpaRepository<ECM_Event, Long> {

	List<ECM_Event> findByBusinessObjectRkAndBusinessObjectName(long object_rk, String string);

}
