package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.OccsRkSeq;

@Repository
public interface OccsRkSeqRepository extends JpaRepository<OccsRkSeq, Long> {

}
