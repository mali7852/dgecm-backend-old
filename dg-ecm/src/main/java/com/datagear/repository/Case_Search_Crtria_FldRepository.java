package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Search_Crtria_Fld;
import com.datagear.model.Case_Search_Crtria_FldPK;

@Repository
public interface Case_Search_Crtria_FldRepository extends JpaRepository<Case_Search_Crtria_Fld, Case_Search_Crtria_FldPK> {

}
