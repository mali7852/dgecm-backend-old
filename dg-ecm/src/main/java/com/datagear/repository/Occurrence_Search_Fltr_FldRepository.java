package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_Search_Fltr_Fld;
import com.datagear.model.Occurrence_Search_Fltr_FldPK;

@Repository
public interface Occurrence_Search_Fltr_FldRepository extends JpaRepository<Occurrence_Search_Fltr_Fld, Occurrence_Search_Fltr_FldPK> {

}
