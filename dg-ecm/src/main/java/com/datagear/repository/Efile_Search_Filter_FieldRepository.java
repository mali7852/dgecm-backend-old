package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Efile_Search_Filter_Field;
import com.datagear.model.Efile_Search_Filter_FieldPK;

@Repository
public interface Efile_Search_Filter_FieldRepository extends JpaRepository<Efile_Search_Filter_Field, Efile_Search_Filter_FieldPK> {

}
