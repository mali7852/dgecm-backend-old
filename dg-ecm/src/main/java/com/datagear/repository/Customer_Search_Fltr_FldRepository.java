package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_Search_Fltr_Fld;
import com.datagear.model.Customer_Search_Fltr_FldPK;

@Repository
public interface Customer_Search_Fltr_FldRepository extends JpaRepository<Customer_Search_Fltr_Fld, Customer_Search_Fltr_FldPK> {

}
