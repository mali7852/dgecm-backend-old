package com.datagear.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_X_Cust;
import com.datagear.model.Case_X_CustPK;

@Repository
public interface Case_X_CustRepository extends JpaRepository<Case_X_Cust, Case_X_CustPK> {

	Case_X_Cust findFirstByIdCaseRkAndIdCustRk(long case_rk, long cust_rk);

	List<Case_X_Cust> findByIdCaseRk(long case_rk);

	List<Case_X_Cust> findByIdCustRk(long cust_rk);

}
