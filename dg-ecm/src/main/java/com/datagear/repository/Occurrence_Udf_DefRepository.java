package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_Udf_Def;
import com.datagear.model.Occurrence_Udf_DefPK;

import java.util.List;

@Repository
public interface Occurrence_Udf_DefRepository extends JpaRepository<Occurrence_Udf_Def, Occurrence_Udf_DefPK> {

	Occurrence_Udf_Def findFirstByIdUdfName(String udf_name);
    List<Occurrence_Udf_Def> findByIdUdfNameIn(List<String> udfNameList);

}
