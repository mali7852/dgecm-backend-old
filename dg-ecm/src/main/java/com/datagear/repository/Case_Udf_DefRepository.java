package com.datagear.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Udf_Def;
import com.datagear.model.Case_Udf_DefPK;

@Repository
public interface Case_Udf_DefRepository extends JpaRepository<Case_Udf_Def, Case_Udf_DefPK> {

	Case_Udf_Def findFirstByIdUdfName(String udf_name);

	List<Case_Udf_Def> findByIdUdfNameIn(List<String> names);

}
