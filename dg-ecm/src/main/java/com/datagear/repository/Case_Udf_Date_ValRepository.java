package com.datagear.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Udf_Date_Val;
import com.datagear.model.Case_Udf_Date_ValPK;

@Repository
public interface Case_Udf_Date_ValRepository extends JpaRepository<Case_Udf_Date_Val, Case_Udf_Date_ValPK> {

	List<Case_Udf_Date_Val> findByIdCaseRkAndIdValidFromDate(long case_rk, Timestamp valid_from_date);

}
