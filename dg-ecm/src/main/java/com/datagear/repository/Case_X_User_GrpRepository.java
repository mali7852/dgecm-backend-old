package com.datagear.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_X_User_Grp;
import com.datagear.model.Case_X_User_GrpPK;

@Repository
public interface Case_X_User_GrpRepository extends JpaRepository<Case_X_User_Grp, Case_X_User_GrpPK> {

    @Query("SELECT c.id.caseRk FROM Case_X_User_Grp c WHERE c.id.userGrpName=:group")
    List<Long> findIdCaseRkByIdUserGrpName(@Param("group") String group);

    @Query("SELECT c.id.caseRk FROM Case_X_User_Grp c WHERE c.id.userGrpName IN :groups")
    List<Long> findIdCaseRkByIdUserGrpNameIn(@Param("groups") List<String> groups);

    List<Case_X_User_Grp> findByIdCaseRk(long case_rk);

}
