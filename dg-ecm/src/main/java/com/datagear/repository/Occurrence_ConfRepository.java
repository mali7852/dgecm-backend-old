package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_Conf;

@Repository
public interface Occurrence_ConfRepository extends JpaRepository<Occurrence_Conf, Long> {

	Occurrence_Conf findFirstByOccsTypeCd(String occs_type_cd);

	Occurrence_Conf findFirstByUiDefFileName(String ui_Def_File_Name);

}
