package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_Conf;

@Repository
public interface Customer_ConfRepository extends JpaRepository<Customer_Conf, Long> {

	Customer_Conf findFirstByCustTypeCd(String cust_type_cd);

	Customer_Conf findFirstByUiDefFileName(String ui_Def_File_Name);

}
