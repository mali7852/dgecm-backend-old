package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Triage_Table;

@Repository
public interface Triage_TableRepository extends JpaRepository<Triage_Table, Long> {

}
