package com.datagear.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.AmlAlert;

@Repository
public interface AmlAlertRepository extends JpaRepository<AmlAlert, Long> {
	
	List<AmlAlert> findByEntityNum(String entity_num);

}
