package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Efile_X_User_Grp;
import com.datagear.model.Efile_X_User_GrpPK;

@Repository
public interface Efile_X_User_GrpRepository extends JpaRepository<Efile_X_User_Grp, Efile_X_User_GrpPK> {

}
