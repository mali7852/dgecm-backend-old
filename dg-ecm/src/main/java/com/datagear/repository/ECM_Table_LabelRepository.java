package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.ECM_Table_Label;
import com.datagear.model.ECM_Table_LabelPK;

@Repository
public interface ECM_Table_LabelRepository extends JpaRepository<ECM_Table_Label, ECM_Table_LabelPK> {

}
