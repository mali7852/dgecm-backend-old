package com.datagear.repository;

import com.datagear.model.Customer_Udf_Lgchr_Val;
import com.datagear.model.Customer_Udf_Lgchr_ValPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface Customer_Udf_Lgchr_ValRepository extends JpaRepository<Customer_Udf_Lgchr_Val, Customer_Udf_Lgchr_ValPK> {

    List<Customer_Udf_Lgchr_Val> findByIdCustRkAndIdValidFromDate(long cust_rk, Timestamp validFromDate);
}
