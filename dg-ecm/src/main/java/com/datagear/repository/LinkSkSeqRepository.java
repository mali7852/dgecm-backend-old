package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.LinkSkSeq;

@Repository
public interface LinkSkSeqRepository extends JpaRepository<LinkSkSeq, Long> {

}
