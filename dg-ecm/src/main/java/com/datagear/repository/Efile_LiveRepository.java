package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Efile_Live;

@Repository
public interface Efile_LiveRepository extends JpaRepository<Efile_Live, Long> {

}
