package com.datagear.repository;

import com.datagear.model.Customer_Ver;
import com.datagear.model.Customer_VerPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Repository
public interface Customer_VerRepository extends JpaRepository<Customer_Ver, Customer_VerPK>, JpaSpecificationExecutor<Customer_Ver> {

    Customer_Ver findFirstByIdCustRkAndIdValidFromDate(long cust_rk, Timestamp valid_from_date);

    Customer_Ver findFirstByIdCustRkAndVerNo(long custRk, BigDecimal verNo);
}
