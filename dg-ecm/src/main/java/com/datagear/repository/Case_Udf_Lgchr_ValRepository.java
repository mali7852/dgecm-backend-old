package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Udf_Lgchr_Val;
import com.datagear.model.Case_Udf_Lgchr_ValPK;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface Case_Udf_Lgchr_ValRepository extends JpaRepository<Case_Udf_Lgchr_Val, Case_Udf_Lgchr_ValPK> {

    List<Case_Udf_Lgchr_Val> findByIdCaseRkAndIdValidFromDate(long case_rk, Timestamp valid_from_date);
}
