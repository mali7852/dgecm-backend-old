package com.datagear.repository;

import com.datagear.customModel.CaseLiveStatus;
import com.datagear.customModel.DashboardStatistics;
import com.datagear.model.Case_Live;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface Case_LiveRepository extends JpaRepository<Case_Live, Long> {

    public Case_Live findFirstByCaseId(String case_id);

    public List<Case_Live> findByCaseLinkSk(BigDecimal bigDecimal);

    public List<Case_Live> findByCaseRkIn(List<Long> list);

    @Query(value = "" +
            "select top 10 * " +
            "from DGCmgmt.Case_Live " +
            "where Close_Date is null " +
            "  and Open_Date is not null " +
            "order by Valid_From_Date desc", nativeQuery = true)
    List<Case_Live> getLastCasesUnderInvestigation();

    @Query(value = "" +
            "select distinct top 10 " +
            "                    Update_User_Id as name, " +
            "                    count(case_Rk) as count, " +
            "                    ((count(case_Rk)) * 100.0 / (select count(1) from DGCmgmt.Case_Live where Update_User_Id is not null)) as y " +
            "from DGCmgmt.Case_Live " +
            "where Update_User_Id is not null " +
            "group by Update_User_Id " +
            "order by count desc",
            nativeQuery = true)
    List<DashboardStatistics> getTopWorkingOnUsers();

    @Query(value = "" +
            "select distinct Case_Stat_Cd                                                          as name, " +
            "                count(case_Rk)                                                        as count, " +
            "                LOG((count(case_Rk)), 2)                                              as y " +
            "from DGCmgmt.Case_Live " +
            "where Case_Stat_Cd is not null " +
            "group by Case_Stat_Cd " +
            "order by count desc",
            nativeQuery = true)
    List<DashboardStatistics> getCaseTypeStatistics();

    
    @Query(value = "" +
            "select Case_Id as caseId, Case_Stat_Cd as caseStatCd, Case_Type_Cd as caseTypeCd " +
            "from DGCmgmt.Case_Live ",
            nativeQuery = true)
    List<CaseLiveStatus> getAllCasesStatus();
}
