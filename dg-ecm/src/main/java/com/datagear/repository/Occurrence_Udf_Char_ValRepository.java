package com.datagear.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_Udf_Char_Val;
import com.datagear.model.Occurrence_Udf_Char_ValPK;

@Repository
public interface Occurrence_Udf_Char_ValRepository extends JpaRepository<Occurrence_Udf_Char_Val, Occurrence_Udf_Char_ValPK> {

	List<Occurrence_Udf_Char_Val> findByIdOccsRkAndIdValidFromDate(long occs_rk, Timestamp valid_From_Date);

}
