package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import com.datagear.model.ECM_Entity_Lock;
import com.datagear.model.ECM_Entity_LockPK;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ECM_Entity_LockRepository extends JpaRepository<ECM_Entity_Lock, ECM_Entity_LockPK> {

    ECM_Entity_Lock findFirstByIdEntityRkAndIdEntityName(long entityRk, String entityName);

    @Modifying
    @Transactional
    void deleteByIdEntityRkAndIdEntityName(long entityRk, String entityName);

    List<ECM_Entity_Lock> findByIdEntityRkInAndIdEntityName(List<Long> case_rks, String name);
}
