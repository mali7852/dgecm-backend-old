package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Udf_No_Val;
import com.datagear.model.Case_Udf_No_ValPK;

@Repository
public interface Case_Udf_No_ValRepository extends JpaRepository<Case_Udf_No_Val, Case_Udf_No_ValPK> {

}
