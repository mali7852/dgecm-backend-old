package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_Udf_No_Val;
import com.datagear.model.Occurrence_Udf_No_ValPK;

@Repository
public interface Occurrence_Udf_No_ValRepository extends JpaRepository<Occurrence_Udf_No_Val, Occurrence_Udf_No_ValPK> {

}
