package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Ref_Table_Trans;
import com.datagear.model.Ref_Table_TransPK;

@Repository
public interface Ref_Table_TransRepository extends JpaRepository<Ref_Table_Trans, Ref_Table_TransPK> {

}
