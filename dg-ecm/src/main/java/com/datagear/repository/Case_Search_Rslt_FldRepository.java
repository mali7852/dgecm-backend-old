package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Search_Rslt_Fld;
import com.datagear.model.Case_Search_Rslt_FldPK;

@Repository
public interface Case_Search_Rslt_FldRepository extends JpaRepository<Case_Search_Rslt_Fld, Case_Search_Rslt_FldPK> {

}
