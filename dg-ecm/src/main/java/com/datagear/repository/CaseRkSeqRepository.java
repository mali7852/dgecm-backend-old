package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.CaseRkSeq;

@Repository
public interface CaseRkSeqRepository extends JpaRepository<CaseRkSeq, Long> {

}
