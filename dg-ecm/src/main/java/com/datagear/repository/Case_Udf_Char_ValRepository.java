package com.datagear.repository;

import com.datagear.customModel.HistoryVerDiff;
import com.datagear.model.Case_Udf_Char_Val;
import com.datagear.model.Case_Udf_Char_ValPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Repository
public interface Case_Udf_Char_ValRepository extends JpaRepository<Case_Udf_Char_Val, Case_Udf_Char_ValPK> {

    List<Case_Udf_Char_Val> findByIdCaseRkAndIdValidFromDate(long case_rk, Timestamp valid_from_date);

    @Query(value = ""
            + "SELECT concat(a.Udf_Table_Name, '.', a.Udf_Name, '.', a.Row_No) as field \n" +
            "		, b.Udf_Val as oldVal, a.Udf_Val as curVal \n" +
            "  FROM Case_Udf_Char_Val a \n" +
            "  join Case_Udf_Char_Val b \n" +
            "  on a.case_rk = b.case_rk \n" +
            "  and a.Udf_Table_Name = b.Udf_Table_Name \n" +
            "  and a.Udf_Name = b.Udf_Name \n" +
            "  and a.Row_No = b.Row_No \n" +
            "  where a.case_rk = :caseRk \n" +
            "  and a.Valid_From_Date = :curDate \n" +
            "  and b.Valid_From_Date = :oldDate \n" +
            "  and a.Udf_Val != b.Udf_Val",
            nativeQuery = true)
    List<HistoryVerDiff> getHistoryVerDiff(@Param("caseRk") long case_rk, @Param("curDate") Timestamp curDate, @Param("oldDate") Timestamp oldDate);

    @Query(value = "" +
			"select X_INCIDENT_ID, X_SOURCE_NAME, X_PARTYNAME_MAXRANK, X_INCIDENT_DESC, X_INCIDENT_ROWNO " +
            "from " +
            "( " +
            "  select Udf_Name, Udf_Val, Row_No from DGECM.DGCmgmt.Case_Udf_Char_Val " +
            "where Case_Rk = :caseRk " +
            "and Valid_From_Date = :vfd " +
            "and Udf_Table_Name = 'X_ENTITY_LIST1' " +
            "group by Udf_Name, Udf_Val, row_no " +
            ") d " +
            "pivot " +
            "( " +
            "  max(Udf_Val) " +
            "  for Udf_Name in (X_INCIDENT_ID, X_SOURCE_NAME, X_PARTYNAME_MAXRANK, X_INCIDENT_DESC, X_INCIDENT_ROWNO) " +
            ") piv",
            nativeQuery = true)
    List<Map<String, String>> getSanctionAlertList(@Param("caseRk")long caseRk, @Param("vfd")Timestamp validFromDate);

    @Query(value = "" +
			"select ENTITY_NAME, X_RANK, WATCH_LIST_NAME, X_YEAR_OF_BIRTH, CITIZENSHIP_COUNTRY_NAME, ROWNO, " +
            "  X_FURTHERINFO, X_ALIASES, X_CONREL, X_SOURCES, X_KEYWORDS, X_KEYDELTA, X_REMARKS " +
            "from " +
            "( " +
            "select * from ( " +
            "select Udf_Name, Udf_Val, Row_No from DGECM.DGCmgmt.Case_Udf_Char_Val " +
            "where case_rk = :caseRk " +
            "and Valid_From_Date = :vfd " +
            "and Udf_Table_Name = 'X_ENTITY_LIST' " +
            "union ( " +
            "select Udf_Name, Udf_Val, Row_No from DGECM.DGCmgmt.Case_Udf_Lgchr_Val " +
            "where case_rk = :caseRk " +
            "and Valid_From_Date = :vfd " +
            "and Udf_Table_Name = 'X_ENTITY_LIST' " +
            ") " +
            ") r " +
            "group by Udf_Name, Udf_Val, row_no " +
            ") d " +
            "pivot " +
            "( " +
            "  max(Udf_Val) " +
            "  for Udf_Name in (ENTITY_NAME, X_RANK, WATCH_LIST_NAME, X_YEAR_OF_BIRTH, CITIZENSHIP_COUNTRY_NAME, ROWNO,  " +
            "  X_FURTHERINFO, X_ALIASES, X_CONREL, X_SOURCES, X_KEYWORDS, X_KEYDELTA, X_REMARKS) " +
            ") piv",
	nativeQuery = true)
    List<Map<String, String>> getSanctionRelAlertList(@Param("caseRk")long caseRk, @Param("vfd")Timestamp validFromDate);
}
