package com.datagear.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_X_Customer;
import com.datagear.model.Customer_X_CustomerPK;

@Repository
public interface Customer_X_CustomerRepository extends JpaRepository<Customer_X_Customer, Customer_X_CustomerPK> {

	Customer_X_Customer findFirstByIdCustRkAndIdMmbrCustRk(long cust_rk, long mmbr_cust_rk);

	List<Customer_X_Customer> findByIdCustRk(long cust_rk);

}
