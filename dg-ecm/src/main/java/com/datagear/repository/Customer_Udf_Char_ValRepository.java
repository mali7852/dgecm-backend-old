package com.datagear.repository;

import java.sql.Timestamp;
import java.util.List;

import com.datagear.customModel.HistoryVerDiff;
import com.datagear.model.Customer_Udf_Char_Val;
import com.datagear.model.Customer_Udf_Char_ValPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface Customer_Udf_Char_ValRepository extends JpaRepository<Customer_Udf_Char_Val, Customer_Udf_Char_ValPK> {

	List<Customer_Udf_Char_Val> findByIdCustRkAndIdValidFromDate(long cust_rk, Timestamp valid_From_Date);

    @Query(value = ""
            + "SELECT concat(a.Udf_Table_Name, '.', a.Udf_Name, '.', a.Row_No) as field \n" +
            "		, b.Udf_Val as oldVal, a.Udf_Val as curVal \n" +
            "  FROM Customer_Udf_Char_Val a \n" +
            "  join Customer_Udf_Char_Val b \n" +
            "  on a.Cust_rk = b.Cust_rk \n" +
            "  and a.Udf_Table_Name = b.Udf_Table_Name \n" +
            "  and a.Udf_Name = b.Udf_Name \n" +
            "  and a.Row_No = b.Row_No \n" +
            "  where a.Cust_Rk = :custRk \n" +
            "  and a.Valid_From_Date = :curDate \n" +
            "  and b.Valid_From_Date = :oldDate \n" +
            "  and a.Udf_Val != b.Udf_Val",
            nativeQuery = true)
    List<HistoryVerDiff> getHistoryVerDiff(@Param("custRk") long cust_rk, @Param("curDate") Timestamp curDate, @Param("oldDate") Timestamp oldDate);
}
