package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_Ver;
import com.datagear.model.Occurrence_VerPK;

import java.sql.Timestamp;

@Repository
public interface Occurrence_VerRepository extends JpaRepository<Occurrence_Ver, Occurrence_VerPK> {

    Occurrence_Ver findFirstByIdOccsRkAndIdValidFromDate(long occs_rk, Timestamp valid_from_date);
}
