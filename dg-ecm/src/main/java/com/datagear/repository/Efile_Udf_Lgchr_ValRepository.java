package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Efile_Udf_Lgchr_Val;
import com.datagear.model.Efile_Udf_Lgchr_ValPK;

@Repository
public interface Efile_Udf_Lgchr_ValRepository extends JpaRepository<Efile_Udf_Lgchr_Val, Efile_Udf_Lgchr_ValPK> {

}
