package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_X_User_Grp;
import com.datagear.model.Occurrence_X_User_GrpPK;

@Repository
public interface Occurrence_X_User_GrpRepository extends JpaRepository<Occurrence_X_User_Grp, Occurrence_X_User_GrpPK> {

}
