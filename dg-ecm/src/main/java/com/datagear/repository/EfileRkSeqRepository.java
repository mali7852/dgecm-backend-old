package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.EfileRkSeq;

@Repository
public interface EfileRkSeqRepository extends JpaRepository<EfileRkSeq, Long> {

}
