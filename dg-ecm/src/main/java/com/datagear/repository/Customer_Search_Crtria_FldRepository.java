package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_Search_Crtria_Fld;
import com.datagear.model.Customer_Search_Crtria_FldPK;

@Repository
public interface Customer_Search_Crtria_FldRepository extends JpaRepository<Customer_Search_Crtria_Fld, Customer_Search_Crtria_FldPK> {

}
