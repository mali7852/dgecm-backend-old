package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Efile_Udf_Def;
import com.datagear.model.Efile_Udf_DefPK;

@Repository
public interface Efile_Udf_DefRepository extends JpaRepository<Efile_Udf_Def, Efile_Udf_DefPK> {

}
