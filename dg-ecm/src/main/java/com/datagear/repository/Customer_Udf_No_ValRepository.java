package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_Udf_No_Val;
import com.datagear.model.Customer_Udf_No_ValPK;

@Repository
public interface Customer_Udf_No_ValRepository extends JpaRepository<Customer_Udf_No_Val, Customer_Udf_No_ValPK> {

}
