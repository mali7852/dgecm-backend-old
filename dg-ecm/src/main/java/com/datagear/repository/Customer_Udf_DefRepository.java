package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_Udf_Def;
import com.datagear.model.Customer_Udf_DefPK;

import java.util.List;

@Repository
public interface Customer_Udf_DefRepository extends JpaRepository<Customer_Udf_Def, Customer_Udf_DefPK> {

	Customer_Udf_Def findFirstByIdUdfName(String udf_name);
    List<Customer_Udf_Def> findByIdUdfNameIn(List<String> udfNameList);
}
