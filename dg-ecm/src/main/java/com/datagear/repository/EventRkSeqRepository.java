package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.EventRkSeq;

@Repository
public interface EventRkSeqRepository extends JpaRepository<EventRkSeq, Long> {

}
