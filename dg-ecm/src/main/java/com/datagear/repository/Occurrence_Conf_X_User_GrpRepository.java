package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_Conf_X_User_Grp;
import com.datagear.model.Occurrence_Conf_X_User_GrpPK;

@Repository
public interface Occurrence_Conf_X_User_GrpRepository extends JpaRepository<Occurrence_Conf_X_User_Grp, Occurrence_Conf_X_User_GrpPK> {

	Occurrence_Conf_X_User_Grp findFirstByIdOccsConfSeqNo(long occs_Conf_Seq_No);

}
