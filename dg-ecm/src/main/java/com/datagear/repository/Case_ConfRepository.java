package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Conf;

@Repository
public interface Case_ConfRepository extends JpaRepository<Case_Conf, Long> {

	Case_Conf findFirstByCaseTypeCd(String case_Type_Cd);

	Case_Conf findFirstByUiDefFileName(String ui_Def_File_Name);

}
