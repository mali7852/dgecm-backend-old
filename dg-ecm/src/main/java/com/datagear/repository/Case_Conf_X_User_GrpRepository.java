package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Conf_X_User_Grp;
import com.datagear.model.Case_Conf_X_User_GrpPK;

import java.util.List;

@Repository
public interface Case_Conf_X_User_GrpRepository extends JpaRepository<Case_Conf_X_User_Grp, Case_Conf_X_User_GrpPK> {

	List<Case_Conf_X_User_Grp> findByIdCaseConfSeqNo(long case_conf_seq_no);

}
