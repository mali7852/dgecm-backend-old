package com.datagear.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Ref_Table_Val;
import com.datagear.model.Ref_Table_ValPK;

@Repository
public interface Ref_Table_ValRepository extends JpaRepository<Ref_Table_Val, Ref_Table_ValPK> {

	List<Ref_Table_Val> findByIdRefTableNameIn(List<String> lookups);

}
