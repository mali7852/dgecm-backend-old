package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_Live;

@Repository
public interface Customer_LiveRepository extends JpaRepository<Customer_Live, Long> {

	Customer_Live findFirstByCustId(String id);

}
