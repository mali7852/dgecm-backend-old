package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Customer_X_User_Grp;
import com.datagear.model.Customer_X_User_GrpPK;

@Repository
public interface Customer_X_User_GrpRepository extends JpaRepository<Customer_X_User_Grp, Customer_X_User_GrpPK> {

}
