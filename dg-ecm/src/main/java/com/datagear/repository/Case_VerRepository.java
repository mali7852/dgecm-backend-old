package com.datagear.repository;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.datagear.model.Case_Ver;
import com.datagear.model.Case_VerPK;

@Repository
public interface Case_VerRepository extends JpaRepository<Case_Ver, Case_VerPK>, JpaSpecificationExecutor<Case_Ver> {

	Case_Ver findFirstByIdCaseRkAndIdValidFromDate(long case_rk, Timestamp valid_From_Date);

	Case_Ver findFirstByIdCaseRkAndVerNo(long caseRk, BigDecimal verNo);

}
