package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.ECM_Column_Label;
import com.datagear.model.ECM_Column_LabelPK;

@Repository
public interface ECM_Column_LabelRepository extends JpaRepository<ECM_Column_Label, ECM_Column_LabelPK> {

}
