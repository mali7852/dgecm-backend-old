package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Efile_Conf;

@Repository
public interface Efile_ConfRepository extends JpaRepository<Efile_Conf, Long> {

}
