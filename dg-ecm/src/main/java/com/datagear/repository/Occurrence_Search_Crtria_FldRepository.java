package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.Occurrence_Search_Crtria_Fld;
import com.datagear.model.Occurrence_Search_Crtria_FldPK;

@Repository
public interface Occurrence_Search_Crtria_FldRepository extends JpaRepository<Occurrence_Search_Crtria_Fld, Occurrence_Search_Crtria_FldPK> {

}
