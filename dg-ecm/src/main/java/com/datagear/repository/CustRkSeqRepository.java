package com.datagear.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datagear.model.CustRkSeq;

@Repository
public interface CustRkSeqRepository extends JpaRepository<CustRkSeq, Long> {

}
