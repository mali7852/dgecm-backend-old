package com.datagear.security.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.datagear.security.jwt.JwtUser;
import com.datagear.security.model.User;
import com.datagear.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class UserRestController {

	@Value("${jwt.header}")
	private String tokenHeader;

//	@Autowired
//	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	@Qualifier("jwtUserDetailsService")
	private UserDetailsService userDetailsService;

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public JwtUser getAuthenticatedUser(HttpServletRequest request) {
		// System.out.println("getAuthenticatedUser");
//        String token = request.getHeader(tokenHeader).substring(7);
//        String username = jwtTokenUtil.getUsernameFromToken(token);
		JwtUser user = (JwtUser) userDetailsService.loadUserByUsername("admin");
		return user;
	}

	@Autowired
    UserRepository userRepository;

	@RequestMapping(value = "/allUser", method = RequestMethod.GET)
	public List<User> geAllUser(HttpServletRequest request) {
		// System.out.println("getAllUser");
		// System.out.println("secret: " + jwtTokenUtil.getSecret());
		//userRepository.getAll_users();
		return userRepository.getUserNames();
	}
}
