package com.datagear.security.controller;

import com.datagear.security.jwt.JwtAuthenticationRequest;
import com.datagear.security.jwt.JwtTokenUtil;
import com.datagear.security.jwt.JwtUser;
import com.datagear.security.model.LoginUser;
import com.datagear.security.service.JwtAuthenticationResponse;
import com.datagear.security.service.SMCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Objects;

@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = "Cache-Control, Content-Language, Content-Type, Expires, Last-Modified")
@RestController
public class AuthenticationRestController {
    @Autowired
    private SMCService SMCService;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    @RequestMapping(value = "${jwt.route.authentication.path}", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        // Reload password post-security so we can generate the token
        final JwtUser userDetails = (JwtUser) userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        LoginUser loginUser = prepareUser(userDetails, token);

        // Return the token
        return ResponseEntity.ok(loginUser);
    }

    @RequestMapping(value = "${jwt.route.authentication.sas.path}", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest, HttpServletRequest request) throws AuthenticationException {
        authenticateWithSAS(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        int saspwIndex = (authenticationRequest.getUsername() + "").indexOf("@saspw");
        int atIndex = (authenticationRequest.getUsername() + "").indexOf("@");

        // Reload password post-security so we can generate the token
        String userName = authenticationRequest.getUsername();

        if (saspwIndex > 0)
            userName = userName.substring(0, saspwIndex);
        else if (atIndex > 0)
            userName = userName.substring(0, atIndex);

        JwtUser userDetails = SMCService.getUserDetailById(userName);

        final String token = jwtTokenUtil.generateToken(userDetails);

        LoginUser loginUser = prepareUser(userDetails, token);

        return ResponseEntity.ok(loginUser);
    }

    @RequestMapping(value = "${jwt.route.authentication.path}/withToken", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationByToken(@RequestBody String token) {
        // Reload password post-security so we can generate the token
        final JwtUser userDetails = (JwtUser) userDetailsService.loadUserByUsername(jwtTokenUtil.getUsernameFromToken(token));

        if (!jwtTokenUtil.validateToken(token, userDetails))
            return new ResponseEntity<String>("Token is not valid!", HttpStatus.UNAUTHORIZED);

        LoginUser loginUser = prepareUser(userDetails, token);

        // Return the token
        return ResponseEntity.ok(loginUser);
    }

    @RequestMapping(value = "${jwt.route.authentication.refresh}", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        // System.out.println("refreshAndGetAuthenticationToken");
        String authToken = request.getHeader(tokenHeader);
        final String token = authToken.substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        // System.out.println("handleAuthenticationException");
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    /**
     * Authenticates the user. If something is wrong, an {@link AuthenticationException} will be thrown
     */
    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled!", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Bad credentials!", e);
        }
    }

    private void authenticateWithSAS(@NotNull String username, @NotNull String password) {
        try {
            SMCService.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled!", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Bad credentials!", e);
        }
    }

    private LoginUser prepareUser(JwtUser userDetails, String token) {
        LoginUser loginUser = LoginUser.getInstance();
        loginUser.setUserName(userDetails.getUsername());
        loginUser.setDisplayName(userDetails.getDisplayName());
        loginUser.setToken(token);
        loginUser.setUserId(userDetails.getId());
        ArrayList<String> roles = new ArrayList<>();
        userDetails.getAuthorities().forEach(a -> {
            roles.add(a.getAuthority());
        });
        loginUser.setRoles(roles);
        loginUser.setTokenExpiration(jwtTokenUtil.getExpirationDateFromToken(token));

        return loginUser;
    }
}
