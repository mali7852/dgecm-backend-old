package com.datagear.security.jwt;

import java.util.ArrayList;
import java.util.List;

import com.datagear.security.model.Group;
import com.datagear.security.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public final class JwtUserFactory {

	private JwtUserFactory() {
	}

	public static JwtUser create(User user) {
		return new JwtUser(user.getId().toString(), user.getUsername(), user.getDisplayName(), user.getFirstname(),
				user.getLastname(), user.getEmail(), user.getPassword(), mapToGrantedAuthorities(user.getGroups()),
				user.getEnabled(), user.getLastPasswordResetDate(), getGroups(user.getGroups()));
	}

	private static List<GrantedAuthority> mapToGrantedAuthorities(List<Group> groups) {
//        return groups.stream()
//                .map(group -> new SimpleGrantedAuthority(group.getName()))
//                .collect(Collectors.toList());
		// get roles instead of groups
		List<GrantedAuthority> authorities = new ArrayList<>();
		groups.forEach(a -> {
			a.getCapabilities().forEach(b -> {
				authorities.add(new SimpleGrantedAuthority(b.getName()));
			});
		});
		return authorities;

	}

	private static List<String> getGroups(List<Group> group_list) {
		List<String> groups = new ArrayList<>();
		group_list.forEach(a -> {
			if (a.getIsActive() == '1')
				groups.add(a.getName());
		});
		return groups;
	}
}
