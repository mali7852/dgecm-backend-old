package com.datagear.security.service;

import com.datagear.security.jwt.JwtTokenUtil;
import com.datagear.security.jwt.JwtUser;
import com.datagear.security.jwt.JwtUserFactory;
import com.datagear.security.model.User;
import com.datagear.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return JwtUserFactory.create(user);
        }
    }

    public JwtUser loadUserByToken(String token) {
        String username = this.jwtTokenUtil.getUsernameFromToken(token);
        String displayName = this.jwtTokenUtil.getDisplayNameFromToken(token);
        List<String> groups = this.jwtTokenUtil.getGroupsFromToken(token);
        List<String> roles = this.jwtTokenUtil.getRolesFromToken(token);
        Timestamp passwordLastUpdateDate = this.jwtTokenUtil.getpasswordLastUpdateDateFromToken(token);
        List<GrantedAuthority> authorities = roles.parallelStream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
        JwtUser user = new JwtUser(null, username, displayName, null, null, null, null,
                authorities, true, passwordLastUpdateDate, groups);

        return user;
    }

    public List<String> getUserCapabilities(long id) {
        Optional<User> user = this.userRepository.findById(id);
        if (!user.isPresent())
            return null;
        User userObject = user.get();
        List<String> capabilities = new ArrayList<>();
        userObject.getGroups().forEach(a -> {
            a.getCapabilities().forEach(b -> {
                capabilities.add(b.getName());
            });
        });
        // get roles instead of groups
        return capabilities;
    }
}
