package com.datagear.security.service;

import com.datagear.security.jwt.JwtUser;
import com.sas.iom.SASIOMDefs.GenericError;
import com.sas.iom.SASIOMDefs.VariableArray2dOfStringHolder;
import com.sas.meta.SASOMI.ISecurityPackage.InvalidCredHandle;
import com.sas.meta.SASOMI.ISecurity_1_1;
import com.sas.meta.SASOMI.ISecurity_1_1Package.InvalidInfoType;
import com.sas.meta.SASOMI.ISecurity_1_1Package.InvalidOptionName;
import com.sas.meta.SASOMI.ISecurity_1_1Package.InvalidOptionValue;
import com.sas.metadata.remote.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.*;

@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Service
public class SMCService {
    private static final Logger log = LoggerFactory.getLogger(SMCService.class);
    @Value("${smc.server.serverport}")
    String serverPort;
    private String ServerUser;
    private String ServerUserPassword;
    @Value("${smc.server.servername}")
    private String serverName;
    @Value("${smc.server.password}")
    private String serverPass;
    @Value("${smc.server.username}")
    private String serverUser;
    @Value("${smc.ldap.domain.name}")
    private String domainName;
    //    @Value("${com.datagear.goaml.security.sas.user-group-name}")
//    private String groupName;
    private Map<String, String> authMapping = new HashMap<>();
    /**
     * The following statements instantiate the object factory.
     */
    private MdFactory _factory = null;
    private MdOMRConnection connection = null;
    private ISecurity_1_1 iSecurity = null;
    private List<String> groups;
    private Set<String> capabilities;

    /**
     * Default constructor
     */
    public SMCService() {
        this.authMapping.put("BE00007T:A600009E", "CASESUBSCRIBE"); // Subscribe to Any Case
        this.authMapping.put("BE00008A:A600009R", "CUSTDELCOMMENT"); // Delete My Party Comment
//        this.authMapping.put("BE00008M:A60000AF", "Terminate Report Workflow");
        this.authMapping.put("BE00007H:A6000099", "CASECREATE"); // Create Case
        this.authMapping.put("BE00008O:A60000AU", "EFILECREATE"); // Create EFile
//        this.authMapping.put("BE000082:A600009K", "Delete My Incident Comment");
        this.authMapping.put("BE00008Q:A60000A6", "EFILEADDCOMMENT"); // Add Comment To EFile
//        this.authMapping.put("BE00007Z:A600009I", "Delete Any Incident Attachment");
//        this.authMapping.put("ace A5X8F02F.A500002T", "Search Administration");
        this.authMapping.put("BE00008P:A60000AV", "EFILEEDIT"); // Edit EFile
        this.authMapping.put("BE00007W:A600009F", "OCCSSEARCH"); // Search Incidents
        this.authMapping.put("BE00007V:A60000AE", "CASETERMINATEWF"); // Terminate Case Workflow
        this.authMapping.put("BE000079:A60000AK", "ADMINUPLOADCUSTOMPROP"); // Upload Custom Properties
        this.authMapping.put("BE000086:A600009O", "CUSTEDIT"); // Edit Party
        this.authMapping.put("BE00008N:A60000A5", "EFILESEARCH"); // Search EFiles
//        this.authMapping.put("BE000080:A600009J", "Add Comment To Any Incident");
        this.authMapping.put("BE00007N:A60000A9", "CASEADDCOMMENT"); // Add Comment To Any Case Anytime
        this.authMapping.put("BE000088:A600009Q", "CUSTADDCOMMENT"); // Add Comment To Any Party
//        this.authMapping.put("BE00008R:A60000AH", "Delete Any EFile Comment");
        this.authMapping.put("BE00008W:A600009T", "OCCSLNKCASE"); // Add Incident To Case
//        this.authMapping.put("BE00007O:A60000AA", "Delete Any Case Comment");
//        this.authMapping.put("BE00008T:A60000AI", "Delete Any EFile Attachment");
        this.authMapping.put("BE00007P:A600009C", "CASEDELCOMMENT"); // Delete My Case Comment
//        this.authMapping.put("BE00008U:A60000A8", "Subscribe to Any EFile");
        this.authMapping.put("BE000083:A600009L", "OCCSSUBSCRIBE"); // Subscribe to Any Incident
//        this.authMapping.put("BE00007C:A60000AM", "Upload Report Configuration");
        this.authMapping.put("BE00007J:A60000AS", "CASEEDIT"); // Edit My Case Anytime
        this.authMapping.put("BE00007F:A60000AP", "ADMINDOCUMENTATION"); // View Documentation
        this.authMapping.put("BE000078:A60000AJ", "ADMINUPLOADUI"); // Upload UI Definitions
        this.authMapping.put("BE00008B:A600009S", "CUSTSUBSCRIBE"); // Subscribe to Any Party
        this.authMapping.put("BE00007M:A600009B", "CASEDELCOMMENT"); // Delete Any Case Attachment
        this.authMapping.put("BE000087:A600009P", "CUSTDELCOMMENT"); // Delete Any Party Attachment
        this.authMapping.put("BE00007U:A600009V", "CASEACTIVATEWF"); // Activate Case Workflow
//        this.authMapping.put("BE00008I:A60000A2", "Delete Any Report Attachment");
//        this.authMapping.put("BE00007E:A60000AO", "Refresh Labels");
//        this.authMapping.put("BE00008D:A600009Y", "Create Report");
        this.authMapping.put("BE00007I:A60000AR", "CASEEDIT"); // Edit Any Case Anytime
        this.authMapping.put("BE00007D:A60000AN", "ADMINCLEARCACHE"); // Clear Cache
        this.authMapping.put("BE000089:A60000AD", "CUSTDELCOMMENT"); // Delete Any Party Comment
        this.authMapping.put("BE00007G:A6000098", "CASESEARCH"); // Search Cases
        this.authMapping.put("BE000085:A600009N", "CUSTCREATE"); // Create Party
//        this.authMapping.put("BE000077:A6000097", "View ECM Hub Link");
        this.authMapping.put("BE00007X:A600009G", "OCCSCREATE"); // Create Incident
//        this.authMapping.put("BE00007L:A600009A", "Edit My Closed Case");
//        this.authMapping.put("BE00008S:A60000A7", "Delete My EFile Comment");
//        this.authMapping.put("BE00007K:A60000AT", "Edit Any Closed Case");
//        this.authMapping.put("BE00008E:A600009Z", "Edit Report");
//        this.authMapping.put("BE00008J:A60000A4", "Subscribe to Any Report");
//        this.authMapping.put("BE00008C:A600009X", "Search Reports");
        this.authMapping.put("BE000084:A600009M", "CUSTSEARCH"); // Search Parties
//        this.authMapping.put("BE000081:A60000AC", "Delete Any Incident Comment");
        this.authMapping.put("BE00007S:A600009U", "CASEPRINT"); // Print Case
//        this.authMapping.put("BE00008L:A600009W", "Activate Report Workflow");
//        this.authMapping.put("BE00007B:A60000AL", "Upload Search Configuration");
        this.authMapping.put("BE00007R:A60000AB", "CASEUNLOCKANY"); // Reassign Any Case
//        this.authMapping.put("BE00008G:A60000AG", "Delete Any Report Comment");
        this.authMapping.put("BE00007Y:A600009H", "OCCSEDIT"); // Edit Incident
        this.authMapping.put("BE00007Q:A600009D", "CASESETOWNER"); // Reassign My Case
//        this.authMapping.put("BE00008X:A60000AQ", "Bulk Load Index");
//        this.authMapping.put("BE00008V:A60000AW", "Add Attachment To EFile");
        this.initializeFactory();
    }

    public void initializeFactory() {
        try {

            _factory = new MdFactoryImpl(false);

            boolean debug = false;
            if (debug) {
                _factory.setDebug(false);
                _factory.setLoggingEnabled(false);

                _factory.getUtil().setOutputStream(System.out);
                _factory.getUtil().setLogStream(System.out);
            }

        } catch (Exception e) {
            log.error("Exception:" + e);
            log.error("Exception Message:" + e.getMessage());
        }
    }

    /**
     * The following statements define variables for SAS Metadata Server connection
     * properties, instantiate a connection factory, issue the makeOMRConnection
     * method, and check exceptions for error conditions.
     */
    public boolean connectToServer() {
        String serverUser = this.ServerUser;
        String serverPass = this.ServerUserPassword;

        try {
            // _factory.closeConnection();

            connection = _factory.getConnection();

            connection.makeOMRConnection(serverName, serverPort, ServerUser, ServerUserPassword);

        } catch (MdException e) {
            log.info("In Catch");
            Throwable t = e.getCause();
            if (t != null) {

                String ErrorType = e.getSASMessageSeverity();
                String ErrorMsg = e.getSASMessage();
                if (ErrorType == null) {

                } else {

                    log.info(ErrorType + ": " + ErrorMsg);
                }
                if (t instanceof org.omg.CORBA.COMM_FAILURE) {

                    log.info(e.getLocalizedMessage());
                } else if (t instanceof org.omg.CORBA.NO_PERMISSION) {

                    log.info(e.getLocalizedMessage());
                }
            } else {

                log.info(e.getLocalizedMessage());
            }

            log.error("Exception:" + e);
            log.error("Exception Message:" + e.getMessage());
            return false;
        } catch (RemoteException e) {
            log.error("Exception:" + e);
            log.error("Exception Message:" + e.getMessage());
            return false;
        }

        return true;
    }

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        this.setServerUserPassword(authentication.getCredentials().toString());
        this.setServerUser(authentication.getPrincipal().toString());
        this.initializeFactory();
        boolean isConnected = this.connectToServer();
        if (isConnected) {
            return null;
        } else {
            throw new BadCredentialsException("1000");
        }

    }

    /**
     * This example displays the status of the SAS Metadata Server.
     */
//    public void displayServerInformation() {
//        try {
//            MdOMRConnection connection = _factory.getConnection();
//
//            log.info("\nGetting server status...");
//            int status = connection.getServerStatus();
//            switch (status) {
//                case MdOMRConnection.SERVER_STATUS_OK:
//                    log.info("Server is running");
//                    break;
//                case MdOMRConnection.SERVER_STATUS_PAUSED:
//                    log.info("Server is paused");
//                    break;
//                case MdOMRConnection.SERVER_STATUS_ERROR:
//                    log.info("Server is not running");
//                    break;
//            }
//
//            int version = connection.getPlatformVersion();
//            log.info("Server platform version: " + version);
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//    }
//
//    public Boolean CheckUser(String user) throws RemoteException, MdException {
//        this.ServerUser = this.serverUser;
//        this.ServerUserPassword = this.serverPass;
//        connectToServer();
//        if (this.getRepositories() != null) {
//
//            CMetadata repository = this.getRepositories().get(0);
//            if (repository != null) {
//
//                MdObjectStore store = _factory.createObjectStore();
//
//                String xmlSelect = "<XMLSELECT Search=\"Person[@Name='" + user + "' OR @Id='" + user + "']\"/>";
//                String template = "<Templates>" + "<Person />" + "</Templates>";
//
//                String sOptions = xmlSelect + template;
//
//                int flags = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_TEMPLATE | MdOMIUtil.OMI_GET_METADATA;
//                List tableList = _factory.getOMIUtil().getMetadataObjectsSubset(store, repository.getFQID(),
//                        MetadataObjects.PERSON, flags, sOptions);
//
//                // log.info(tableList.size() + " person number with name:" + userID);
//                if (tableList.size() > 0)
//                    return true;
//                else
//                    return false;
//            } else
//                return false;
//        } else
//            return false;
//
//    }

    /**
     * This example retrieves a list of the repositories that are registered on the
     * SAS Metadata Server.
     *
     * @return the list of available repositories (list of CMetadata objects)
     */
    public List<CMetadata> getRepositories() {
        List<CMetadata> reposList = null;
        try {

            MdOMIUtil omiUtil = _factory.getOMIUtil();
            reposList = omiUtil.getRepositories();

            return reposList;
        } catch (MdException e) {
            log.error("Exception:" + e);
            log.error("Exception Message:" + e.getMessage());
        } catch (RemoteException e) {
            log.error("Exception:" + e);
            log.error("Exception Message:" + e.getMessage());
        }
        return reposList;
    }

//    public void createUser(UserDTO user) {
//        CMetadata repository = getRepositories().get(0);
//        if (repository != null) {
//            try {
//                log.info("\nCreating objects on the server...");
//
//                String reposFQID = repository.getFQID();
//
//                String shortReposID = reposFQID.substring(reposFQID.indexOf(".") + 1, reposFQID.length());
//
//                MdObjectStore store = _factory.createObjectStore();
//
//                Person table = (Person) _factory.createComplexMetadataObject(store, null, user.getName(),
//                        MetadataObjects.PERSON, shortReposID);
//
//                Map<String, String> map = table.getAttrs();
//
//                // new
//                table.setPublicType("User");
//                table.setUsageVersion(1000000.0);
//                // new
//
//                table.setDisplayName(user.getDisplayName());
//                table.setDesc(user.getDesc());
//                table.setTitle(user.getJobTitle());
//
//                table.updateMetadataAll();
//
////				setUserEmails(user, table, shortReposID, store);
////				setUserPhones(user, table, shortReposID, store);
////				setUserAddresses(user, table, shortReposID, store);
//
//                createLogin(user.getName(), user.getLoginDTOs());
//                // addUserToGroup(user.getName(), user.getGroupDTOs()); // assign to group
//                // assignRoleToUser(user.getName(), user.getRoleDTOs());
//                InternalLoginDTO internalLoginDTO = user.getInternalLogin();
//                log.info("before internal login" + user.getInternalLogin());
//                log.info("after internal login");
//                if (internalLoginDTO != null) {
//                    log.info("Now Adding Internal");
//                    addUserInternalPasswordToMeta(user.getName(), internalLoginDTO.getPassword());
////					this.disableuser(user.getName());
//                }
//                store.dispose();
//
//            } catch (MdException e) {
//                log.error("Exception:" + e);
//                log.error("Exception Message:" + e.getMessage());
//            } catch (RemoteException e) {
//                log.error("Exception:" + e);
//                log.error("Exception Message:" + e.getMessage());
//            }
//        }
//
//    }


//    public String CreateGroup(GroupDTO group) {
//
//        try {
//            CMetadata repository = this.getRepositories().get(0);
//            if (repository != null) {
//                String reposFQID = repository.getFQID();
//
//                String shortReposID = reposFQID.substring(reposFQID.indexOf(".") + 1,
//
//                        reposFQID.length());
//                MdObjectStore store = this._factory.createObjectStore();
//                if (IsGroupFound(group.getName()) != "found") {
//
//                    IdentityGroup identityGroup = (IdentityGroup) _factory.createComplexMetadataObject(store, null,
//                            group.getName(), MetadataObjects.IDENTITYGROUP, shortReposID);
//                    identityGroup.setDisplayName(group.getDisplayName());
//                    identityGroup.setDesc(group.getDesc());
//                    identityGroup.setPublicType("UserGroup");
//                    identityGroup.setUsageVersion(1000000.0);
//                    for (int i = 0; i < group.getRoles().size(); i++) {
//                        RoleDTO roleDTO = group.getRoles().get(i);
//                        String RoleName = roleDTO.getName();
//                        if (RoleName != null) {
//                            IdentityGroup role = this.getRoleByName(RoleName);
//                            if (role != null)
//                                this.addIdentityGroupToGroupHelper(identityGroup, role);
//                        }
//
//                    }
//
//                    identityGroup.updateMetadataAll();
//                    return "Group Created Successfully";
//                } else {
//                    return "Group Name Is Found Choose other Name";
//                }
//
//            }
//
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//            return "Error Occure, Check the Connetion." + e.getMessage();
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//            return e.getMessage();
//        }
//        return "Error";
//    }

//    public IdentityGroup getGroup(String name) {
//        try {
//            CMetadata repository = getRepositories().get(0);
//            String reposID = repository.getFQID();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID, MetadataObjects.IDENTITYGROUP,
//                    flags, "");
//
//            Iterator iter = tables.iterator();
//            while (iter.hasNext()) {
//                IdentityGroup ptable = (IdentityGroup) iter.next();
//                if (ptable.getName().equals(name))
//                    return ptable;
//
//            }
//
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//
//    }
//
//    public List<String> getusergroup(String user) throws RemoteException, MdException {
//        List<String> s = new ArrayList<String>();
//        this.ServerUser = this.serverUser;
//        this.ServerUserPassword = this.serverPass;
//        connectToServer();
//        if (this.getRepositories() != null) {
//
//            CMetadata repository = this.getRepositories().get(0);
//            if (repository != null) {
//
//                MdObjectStore store = _factory.createObjectStore();
//
//                String xmlSelect = "<XMLSELECT Search=\"Person[@Name='" + user + "' OR @Id='" + user + "']\"/>";
//                String template = "<Templates>" + "<Person />" + "</Templates>";
//
//                String sOptions = xmlSelect + template;
//
//                int flags = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_TEMPLATE | MdOMIUtil.OMI_GET_METADATA;
//                String reposID = repository.getFQID();
//                List tableList = _factory.getOMIUtil().getMetadataObjectsSubset(store, repository.getFQID(),
//                        MetadataObjects.PERSON, flags, sOptions);
//
//                Iterator iter = tableList.iterator();
//                while (iter.hasNext()) {
//                    Person ptable = (Person) iter.next();
//                    AssociationList columns = ptable.getIdentityGroups();
//                    for (int i = 0; i < columns.size(); i++) {
//
//                        IdentityGroup column = (IdentityGroup) columns.get(i);
//                        if (column.getPublicType().equalsIgnoreCase("usergroup"))
//                            s.add(column.getName());
//
//                    }
//                }
//                return s;
//            } else
//                return null;
//        } else
//            return null;
//    }
//
//    public void addUserInternalPasswordToMeta(String userName, String password) {
//        try {
//
//            ISecurity_1_1 is;
//            is = connection.MakeISecurityConnection();
//            is.SetInternalPassword(userName, password);
//
//        } catch (RemoteException e) {
//
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (GenericError e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//
//    }
//
//    public boolean disableuser(String userName) {
//
//        try {
//            CMetadata repository = this.getRepositories().get(0);
//            MdObjectStore objectStore = _factory.createObjectStore();
//
//            String reposID = repository.getFQID();
//
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID, MetadataObjects.PERSON, flags,
//                    "");
//
//            Iterator iter = tables.iterator();
//            log.info("Person size " + tables.size());
//            ISecurity_1_1 is;
//            is = connection.MakeISecurityConnection();
//            while (iter.hasNext()) {
//                Person ptable = (Person) iter.next();
//
//                if (ptable.getName().equals(userName)) {
//
//                    ptable.getInternalLoginInfo().setIsDisabled(1);
//
//                    ptable.updateMetadataAll();
//                    return true;
//                }
//
//            }
//            return false;
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return false;
//
//    }
//
//    public boolean enableUser(String userName) {
//
//        try {
//            CMetadata repository = this.getRepositories().get(0);
//
//            MdObjectStore objectStore = _factory.createObjectStore();
//
//            String reposID = repository.getFQID();
//
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID, MetadataObjects.PERSON, flags,
//                    "");
//
//            Iterator iter = tables.iterator();
//            log.info("Person size " + tables.size());
//            ISecurity_1_1 is;
//            is = connection.MakeISecurityConnection();
//            while (iter.hasNext()) {
//                Person ptable = (Person) iter.next();
//
//                if (ptable.getName().equals(userName)) {
//
//                    ptable.getInternalLoginInfo().setIsDisabled(0);
//
//                    ptable.updateMetadataAll();
//                    return true;
//                }
//            }
//            return false;
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return false;
//
//    }
//
//    public Person getPerson(String name) {
//        try {
//            CMetadata repository = this.getRepositories().get(0);
//
//            System.out
//                    .println("\nRetrieving all Persons objects contained in " + " repository " + repository.getName());
//            String reposID = repository.getFQID();
//
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID, MetadataObjects.PERSON, flags,
//                    "");
//
//            Iterator iter = tables.iterator();
//            while (iter.hasNext()) {
//                Person ptable = (Person) iter.next();
//                if (ptable.getName().equals(name)) {
//                    log.info("AMRAMR: " + ptable.getLogins().size());
//                    return ptable;
//                }
//            }
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//    }

//    public Login createLogin(String username, List<LoginDTO> loginDTOs) {
//
//        log.info("In createLogin, Logins size that will be added:=> " + loginDTOs.size());
//        CMetadata repository = getRepositories().get(0);
//        try {
//            String reposID = repository.getFQID();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            String reposFQID = repository.getFQID();
//
//            String shortReposID = reposFQID.substring(reposFQID.indexOf(".") + 1, reposFQID.length());
//
//            Person p = getPerson(username);
//
//            List auths = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID,
//                    MetadataObjects.AUTHENTICATIONDOMAIN, flags, "");
//
//            List<Login> old_logins = new ArrayList<Login>();
//            for (int i = 0; i < p.getLogins().size(); i++) {
//                old_logins.add((Login) p.getLogins().get(i));
//                log.info("==: " + ((Login) p.getLogins().get(i)).getUserID());
//            }
//
//            p.getLogins().clear();
//
//            log.info("OLDE LOGINS: " + old_logins.size());
//            for (int i = 0; i < old_logins.size(); i++)
//                log.info(old_logins.get(i).getUserID());
//
//            log.info("*-*-*-*-*-*-*-*-*-*-*-*");
//            for (int i = 0; i < loginDTOs.size(); i++) {
//                if (loginDTOs.get(i).getEnabled() == 'N' || loginDTOs.get(i).getApprovedeletion() == 'Y') {
//                    continue;
//                }
//                Login myObject2 = (Login) this._factory.createComplexMetadataObject(store, null, "Login_Object" + i,
//                        MetadataObjects.LOGIN, shortReposID);
//
//                String UserId2 = "";
//                if (loginDTOs.get(i).getUserId().indexOf("@") > -1) {
//                    UserId2 = loginDTOs.get(i).getUserId().substring(0, loginDTOs.get(i).getUserId().indexOf("@"));
//                } else if (username.indexOf("\\") > -1) {
//                    UserId2 = loginDTOs.get(i).getUserId().substring(loginDTOs.get(i).getUserId().indexOf("\\") + 1);
//                } else
//                    UserId2 = loginDTOs.get(i).getUserId();
//
//                String userName_ = this.domainName + "\\" + UserId2;
//
//                boolean flag = false;
//
//                log.info("Start Logins Olds .... ");
//                for (int ii = 0; ii < old_logins.size(); ii++) {
//
//                    if (old_logins.get(ii).getUserID().equals(loginDTOs.get(i).getUserId())) {
//                        userName_ = loginDTOs.get(i).getUserId();
//                        myObject2.setUserID(userName_);
//                        myObject2.setPassword(loginDTOs.get(i).getPassword());
//
//                        flag = true;
//                        break;
//                    }
//
//                }
//                if (!flag) {
//                    log.info("... Object not in Old Logins ... ");
//                    myObject2.setUserID(userName_);
//                }
//                log.info("*****Logins:*****" + myObject2.getUserID());
//                myObject2.setPassword(loginDTOs.get(i).getPassword());
//
//                Iterator iter = auths.iterator();
//
//                myObject2.updateMetadataAll();
//                p.getLogins().add(myObject2);
//
//            }
//            log.info("SIZE: " + p.getLogins().size() + ", " + p.getIdentityGroups());
//            p.updateMetadataAll();
//
//            log.info("****");
//
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//
//    }

    public JwtUser getUserDetailById(String userID) {
        if (this.getRepositories() != null) {

            CMetadata repository = this.getRepositories().get(0);
            if (repository != null) {
                try {
                    final String[][] options = {{"ReturnUnrestrictedSource", ""}};
                    // Defines a stringholder for the info output parameter.
                    VariableArray2dOfStringHolder info = new VariableArray2dOfStringHolder();
                    // Issues the GetInfo method for the current iSecurity connection user.
                    iSecurity = connection.MakeISecurityConnection();
                    iSecurity.GetInfo("GetIdentityInfo", "", options, info);
                    String[][] returnArray = info.value;
                    // Specifies a title for the output.
                    String userID2 = returnArray[4][1];
                    MdObjectStore store = _factory.createObjectStore();

                    String xmlSelect = "<XMLSELECT Search=\"Person[@Name='" + userID + "' OR @Id='" + userID2
                            + "']\"/>";
                    String template = "<Templates>" + "<Person />" + "</Templates>";

                    String sOptions = xmlSelect + template;

                    int flags = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_TEMPLATE | MdOMIUtil.OMI_GET_METADATA;
                    List tableList = _factory.getOMIUtil().getMetadataObjectsSubset(store, repository.getFQID(),
                            MetadataObjects.PERSON, flags, sOptions);

                    Iterator iter = tableList.iterator();

                    while (iter.hasNext()) {

                        Person person = (Person) iter.next();
                        this.groups = new ArrayList<>();
                        this.capabilities = new HashSet<>();

                        /********** end user logins ************/
                        String id = person.getId();
                        String username = person.getName();
                        String displayName = person.getDisplayName();
                        String email = person.getEmailAddresses().size() > 0 ? ((Email) person.getEmailAddresses().get(0)).getName() : null;
                        String password = null, firstName = null, lastName = null;
                        boolean enabled = true;
                        // any time before now Bcuz sas don't give this info
                        Timestamp lastUpdateDate = new Timestamp(System.currentTimeMillis() - 1000000);

                        // get user groups and roles
                        AssociationList identityGroups = person.getIdentityGroups();
                        for (int i = 0; i < identityGroups.size(); i++) {

                            IdentityGroup identityGroup = (IdentityGroup) identityGroups.get(i);
                            if (identityGroup.getPublicType().equalsIgnoreCase("role")) {
                                this.getNestedCapabilities(identityGroup);

                            } else if (identityGroup.getPublicType().equalsIgnoreCase("usergroup")) {
                                this.getNestedGroups(identityGroup);
                            }

                        }
                        /*********** End user Groups and Emails ***********/
                        List<GrantedAuthority> authorities = new ArrayList<>(this.capabilities.size());
                        for (String capability : this.capabilities) {
                            authorities.add(new SimpleGrantedAuthority(capability));
                        }

                        /***************************/
                        JwtUser userDTO = new JwtUser(id, username, displayName, firstName, lastName, email,
                                password, authorities, enabled, lastUpdateDate, this.groups);
                        return userDTO;
                    }
                } catch (MdException e) {
                    log.error("Exception:" + e);
                    log.error("Exception Message:" + e.getMessage());
                } catch (RemoteException e) {
                    log.error("Exception:" + e);
                    log.error("Exception Message:" + e.getMessage());
                } catch (InvalidCredHandle e) {
                    log.error("Exception:" + e);
                    log.error("Exception Message:" + e.getMessage());
                } catch (InvalidInfoType e) {
                    log.error("Exception:" + e);
                    log.error("Exception Message:" + e.getMessage());
                } catch (InvalidOptionName e) {
                    log.error("Exception:" + e);
                    log.error("Exception Message:" + e.getMessage());
                } catch (InvalidOptionValue e) {
                    log.error("Exception:" + e);
                    log.error("Exception Message:" + e.getMessage());
                } catch (GenericError e) {
                    log.error("Exception:" + e);
                    log.error("Exception Message:" + e.getMessage());
                }
            }
        } else {
            System.err.println("--getUserDetail-- rep null");
        }
        return null;

    }

    private void getNestedGroups(IdentityGroup identityGroup) throws MdException, RemoteException {
        if (this.groups.contains(identityGroup.getName()))
            return;
        this.groups.add(identityGroup.getName());
        AssociationList list = identityGroup.getIdentityGroups();
        for (int i = 0; i < list.size(); i++) {
            IdentityGroup ig = (IdentityGroup) list.get(i);
            if (ig.getPublicType().equalsIgnoreCase("usergroup"))
                this.getNestedGroups(ig);
            else if (ig.getPublicType().equalsIgnoreCase("role"))
                this.getNestedCapabilities(ig);
        }
    }

    private void getNestedCapabilities(IdentityGroup identityGroup) throws MdException, RemoteException {
        AssociationList list = identityGroup.getAccessControlEntries();
        for (int i = 0; i < list.size(); i++) {
            AccessControlEntry ace = (AccessControlEntry) list.get(i);
            if (this.authMapping.get(ace.getName()) != null)
                this.capabilities.add(this.authMapping.get(ace.getName()));
        }
    }


    //    public List<String> getGroupMembers() {
//
//        List<String> lU = new ArrayList<String>();
//        this.ServerUser = this.serverUser;
//        this.ServerUserPassword = this.serverPass;
//        if (connectToServer()) {
//            CMetadata repository = this.getRepositories().get(0);
//            if (repository != null) {
//                try {
//                    log.info("\nReading User Detail from the server...");
//                    log.info("Reading User Detail from the server...");
//                    UserDTO userDTO = new UserDTO();
//                    MdObjectStore store = _factory.createObjectStore();
//
//                    String xmlSelect = "<XMLSELECT Search=\"IdentityGroup[@Name='" + groupName + "']\"/>";
//                    String template = "<Templates>" + "<IdentityGroup />" + "</Templates>";
//
//                    String sOptions = xmlSelect + template;
//
//                    int flags = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_TEMPLATE | MdOMIUtil.OMI_GET_METADATA;
//                    List tableList = _factory.getOMIUtil().getMetadataObjectsSubset(store, repository.getFQID(),
//                            MetadataObjects.IDENTITYGROUP, flags, sOptions);
//
//                    Iterator iter = tableList.iterator();
//                    while (iter.hasNext()) {
//                        IdentityGroup identityGroup = (IdentityGroup) iter.next();
//                        log.info("" + identityGroup.getName() + " " + identityGroup.getMemberIdentities().size());
//                        AssociationList identities = identityGroup.getMemberIdentities();
//                        for (int i = 0; i < identities.size(); i++) {
//                            Identity identity = (Identity) identities.get(i);
//                            if (identity.getPublicType().equalsIgnoreCase("user"))
//                                lU.add(identity.getName());
//                            String e = identity.getName();
//                            lU.add(e);
//                            System.out.println("lU" + lU);
////						if(identity.getPublicType().equalsIgnoreCase("user"));
////						log.info(identity.getName() + ":"+identity.getPublicType());
//                        }
//                        log.info(identityGroup.getGroups().size() + "jjjjjjjjjj");
//                        AssociationList identityGroups = identityGroup.getIdentityGroups();
//                        for (int j = 0; j < identityGroups.size(); j++) {
//                            IdentityGroup ii = (IdentityGroup) identityGroups.get(j);
//                            log.info(ii.getName());
//                            String xmlSelect2 = "<XMLSELECT Search=\"IdentityGroup[@Name='" + ii.getName() + "']\"/>";
//                            String template2 = "<Templates>" + "<IdentityGroup />" + "</Templates>";
//
//                            String sOptions2 = xmlSelect2 + template2;
//
//                            int flags2 = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_TEMPLATE | MdOMIUtil.OMI_GET_METADATA;
//                            List tableList2 = _factory.getOMIUtil().getMetadataObjectsSubset(store, repository.getFQID(),
//                                    MetadataObjects.IDENTITYGROUP, flags2, sOptions2);
//
//                            Iterator iter2 = tableList2.iterator();
//                            while (iter2.hasNext()) {
//                                IdentityGroup identityGroup2 = (IdentityGroup) iter2.next();
//                                log.info("" + identityGroup2.getName() + " " + identityGroup2.getMemberIdentities().size());
//                                AssociationList identities2 = identityGroup2.getMemberIdentities();
//                                for (int i = 0; i < identities2.size(); i++) {
//                                    Identity identity = (Identity) identities2.get(i);
//                                    if (identity.getPublicType().equalsIgnoreCase("user"))
//                                        lU.add(identity.getName());
//                                    String e = identity.getName();
//                                    lU.add(e);
////								if(identity.getPublicType().equalsIgnoreCase("user"))
////								log.info(identity.getName()+ ":"+identity.getPublicType());
//                                }
//                            }
//
//
//                            // log.info(identity.getName());
//                        }
//
//                    }
//
//                } catch (MdException e) {
//                    log.error("Exception:" + e);
//                    log.error("Exception Message:" + e.getMessage());
//                } catch (RemoteException e) {
//                    log.error("Exception:" + e);
//                    log.error("Exception Message:" + e.getMessage());
//                }
//            }
//        }
//        //	lU.remove(groupName);
//        System.out.println("lU=========>>>" + lU);
//        return lU.stream()
//                .distinct()
//                .collect(Collectors.toList());
//    }
//
//    public boolean connectionStatus(String userName, String password) {
//        // String serverName = "192.168.1.96";
//        // String serverPort = "8561";
//
//        log.info("---connectionStatus---");
//        String serverUser = userName;
//        String serverPass = password;
//
//        try {
//            _factory.closeConnection();
//
//            connection = _factory.getConnection();
//
//            log.info("serverName:" + serverName);
//            log.info("serverPort:" + serverPort);
//            connection.makeOMRConnection(serverName, serverPort, serverUser, serverPass);
//
//        } catch (MdException e) {
//            Throwable t = e.getCause();
//            if (t != null) {
//                String ErrorType = e.getSASMessageSeverity();
//                String ErrorMsg = e.getSASMessage();
//                if (ErrorType == null) {
//
//                } else {
//
//                    log.info(ErrorType + ": " + ErrorMsg);
//                }
//                if (t instanceof org.omg.CORBA.COMM_FAILURE) {
//
//                    log.info(e.getLocalizedMessage());
//                } else if (t instanceof org.omg.CORBA.NO_PERMISSION) {
//
//                    log.info(e.getLocalizedMessage());
//                }
//            } else {
//
//                log.info(e.getLocalizedMessage());
//            }
//
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//            return false;
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//            return false;
//        }
//
//        return true;
//
//    }
//
//    private void setPersonInternalLogin(Person p, InternalLoginDTO internalLoginDTO) {
//
//        log.info("setPersonInternalLogin");
//
//        try {
//
//            if (internalLoginDTO == null) {
//                log.info("internal login dto is null");
//
//                p.getInternalLoginInfo().delete();
//            } else if (internalLoginDTO.getPassword() == null || internalLoginDTO.getPassword().length() == 0) {
//                log.info("No update at internal login");
//            } else {
//
//                this.addUserInternalPasswordToMeta(p.getName(), internalLoginDTO.getPassword());
//
//            }
//        } catch (Exception e) {
//        }
//
//    }
//
//    public List<AuthenticationDomainDTO> getAllAuthenticationDomains() {
//
//        try {
//            CMetadata repository = getRepositories().get(0);
//            String reposID = repository.getFQID();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List auths = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID,
//                    MetadataObjects.AUTHENTICATIONDOMAIN, flags, "");
//
//            Iterator iter = auths.iterator();
//            List<AuthenticationDomainDTO> results = new ArrayList<>();
//            while (iter.hasNext()) {
//                AuthenticationDomain auth_ = (AuthenticationDomain) iter.next();
//                AuthenticationDomainDTO authdto = new AuthenticationDomainDTO();
//
//                authdto.setDescription(auth_.getDesc());
//                authdto.setName(auth_.getName());
//                authdto.setTrustedOnly(auth_.getTrustedOnly());
//                authdto.setOutboundOnly(auth_.getOutboundOnly());
//
//                results.add(authdto);
//
//            }
//
//            return results;
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//    }
//
//    public void parseHeader(HttpServletRequest request) {
//        log.info("---parseHeader---");
//        String header = request.getHeader("Authorization");
//        boolean flag = false;
//        log.info("header:" + header);
//        if (header != null) {
//            String[] credntial = header.substring(7).trim().split("--&");
//            if (credntial.length == 2) {
//                this.setServerUser(credntial[0]);
//                this.setServerUserPassword(credntial[1]);
//                log.info(this.ServerUser + "&&&" + this.ServerUserPassword);
//                flag = true;
//            }
//        }
//
//    }
//
    public String getServerUser() {
        return ServerUser;
    }

    //
    public void setServerUser(String serverUser) {
        ServerUser = serverUser;
    }

    public String getServerUserPassword() {
        return ServerUserPassword;
    }

    //
    public void setServerUserPassword(String serverUserPassword) {
        ServerUserPassword = serverUserPassword;
    }
//
//    public List<LoginDTO> getAllLogins() {
//
//        try {
//            CMetadata repository = this.getRepositories().get(0);
//
//            log.info("\nRetrieving all Logins objects contained in " + " repository " + repository.getName());
//            List<LoginDTO> loginDTOs = new ArrayList<>();
//            String reposID = repository.getFQID();
//
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List logins = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID, MetadataObjects.LOGIN, flags,
//                    "");
//            log.info("Number of logins " + logins.size());
//            Iterator iter = logins.iterator();
//            while (iter.hasNext()) {
//                Login ptable = (Login) iter.next();
//                log.info("Login User Id " + ptable.getUserID());
//                AssociationList x = ptable.getAssociatedIdentitys();
//                if (x.size() > 0) {
//                    log.info("before identities");
//                }
//                for (int i = 0; i < x.size(); i++) {
//
//                    Identity gg = (Identity) x.get(i);
//                    log.info("Identity name " + gg.getName());
//                    log.info("Identity is hidden " + gg.getIsHidden());
//                    log.info("Is locked by " + gg.getLockedBy());
//                    log.info("public type" + gg.getPublicType());
//
//                }
//                log.info("identity");
//                Identity identity = ptable.getAssociatedIdentity();
//                if (identity != null) {
//                    log.info("inside identity");
//                    log.info("name of identity " + identity.getName());
//                }
//                LoginDTO loginDTO = new LoginDTO();
//                loginDTO.setPassword(ptable.getPassword());
//
//                ;
//                log.info("&&&&&&&&&&&&&&&&&&7777");
//                loginDTO.setUserId(ptable.getUserID());
//
//                loginDTOs.add(loginDTO);
//
//                // AuthenticationDomainDTO authenticationDomainDTO=new
//                // AuthenticationDomainDTO();
//                // AuthenticationDomain authenticationDomain=ptable.getDomain();
//                // log.info("^^^^^^^^^^^^^");
//                // if(authenticationDomain!=null)
//                // authenticationDomainDTO.setName(authenticationDomain.getName());
//                // loginDTO.setAuthenticationDomainDTO(authenticationDomainDTO);
//            }
//
//            return loginDTOs;
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//
//    }
//
//    public UserDTO getPersonByLoginUserId(String UserId) {
//        log.info("-------------");
//        log.info("in getPersonByLoginUserId Method: ....");
//        log.info("UserId:" + UserId);
//        // log.info("index of //" + UserId.indexOf("\\"));
//        String UserId2 = "";
//        if (UserId.indexOf("@") > -1) {
//            UserId2 = UserId.substring(0, UserId.indexOf("@"));
//        } else if (UserId.indexOf("\\") > -1) {
//            UserId2 = UserId.substring(UserId.indexOf("\\") + 1);
//        } else
//            UserId2 = UserId;
//
//        log.info("This UserId2 After remove Domain name if exists ...");
//        log.info("UserId2 " + UserId2);
//        try {
//            CMetadata repository = this.getRepositories().get(0);
//
//            // log.info("\nRetrieving all Logins objects contained in " + "
//            // repository " + repository.getName());
//            String reposID = repository.getFQID();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL;
//            List logins = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID, MetadataObjects.LOGIN, flags,
//                    "");
//
//            log.info(logins.size() + " Number of logins");
//
//            Iterator iter1 = logins.iterator();
//            while (iter1.hasNext()) {
//                log.info("iter1.hasNext");
//                log.info("------------");
//                log.info("Loop Throug All Logins :");
//
//                Login ptable = (Login) iter1.next();
//                log.info("getAttrs:" + ptable.getAttrs());
//                Identity list = ptable.getAssociatedIdentity();
//                AssociationList identities = ptable.getAssociatedIdentitys();
//                for (int i = 0; i < identities.size(); i++) {
//                    Identity identity = (Identity) identities.get(0);
//                    log.info("Identity name *&*" + identity.getName());
//                }
//                log.info("All Identites: ");
//                log.info("list:" + list);
//                if (list != null)
//                    log.info("getPublicType:" + list.getPublicType());
//                log.info("getUserID:" + ptable.getUserID());
//                log.info("UserId2 The login Form ID => :" + UserId2);
//
//                // log.info("domainName =" + domainName);
//                log.info("Name of Login Object: " + ptable.getName());
//                log.info("ID of login object iteslf: " + ptable.getId());
//
//                String UserWithDomain = UserId2 + "@" + domainName;
//                String UserWithDomain2 = domainName + "\\" + UserId2;
//
//                log.info("UserWithDomain " + UserWithDomain);
//                log.info("UserWithDomain2 " + UserWithDomain2);
//
//                log.info("-----------");
//                log.info("Apply Conditions: ");
//                if (list != null && (ptable.getUserID().equalsIgnoreCase(UserId2)
//                        || ptable.getUserID().equalsIgnoreCase(UserWithDomain)
//                        || ptable.getUserID().equalsIgnoreCase(UserWithDomain2))) {
//
//                    log.info("Conditions Applied... ");
//                    log.info("UserId =" + ptable.getUserID() + "  ");
//                    log.info("Name of Login " + ptable.getName());
//                    log.info("ID " + ptable.getId());
//                    // log.info("");
//                    // log.info(list.getName());
//                    log.info("Now Get User Details by this Id: " + list.getId());
//                    return this.getUserDetailById(list.getId());
//
//                }
//            }
//
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//
//    }
//
//    public String iUniqueLogin(String userId) {
//        CMetadata repository = this.getRepositories().get(0);
//        if (repository != null) {
//            try {
//                // log.info("\nReading User Detail from the server...");
//                UserDTO userDTO = new UserDTO();
//                MdObjectStore store = _factory.createObjectStore();
//
//                // String xmlSelect = "<XMLSELECT Search=\"*[@UserID='" + userId + "']\"/>";
//                String template = "";
//
//                // String sOptions = xmlSelect + template;
//                String reposID = repository.getFQID();
//                int flags = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//
//                List logins = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID, MetadataObjects.LOGIN,
//                        flags, "");
//                String userName = "";
//
//                Iterator iter = logins.iterator();
//                log.info(logins.size() + " Number of Logins");
//                while (iter.hasNext()) {
//                    log.info("**************8");
//                    Login login = (Login) iter.next();
//
//                    //
//                    AssociationList x = login.getAssociatedIdentitys();
//                    if (x.size() > 0) {
//                        log.info("before identities");
//                    }
//                    for (int i = 0; i < x.size(); i++) {
//
//                        Identity gg = (Identity) x.get(i);
//                        log.info("Identity name " + gg.getName());
//                        userName = gg.getName();
//
//                    }
//                    // log.info("identity");
//                    // Identity identity=login.getAssociatedIdentity();
//                    // if(identity!=null) {
//                    // log.info("inside identity");
//                    // log.info("name of identity "+identity.getName());
//                    // }
//
//                    //
//                    String UserId2 = "";
//                    log.info(" Number of Logins" + login.getAccessControls().getParent().getAttrs());
//                    ;
//                    if (login.getUserID().indexOf("@") > -1) {
//                        UserId2 = login.getUserID().substring(0, login.getUserID().indexOf("@"));
//                    } else if (login.getUserID().indexOf("\\") > -1) {
//                        UserId2 = login.getUserID().substring(login.getUserID().indexOf("\\") + 1);
//                    } else
//                        UserId2 = login.getUserID();
//                    log.info(UserId2);
//                    if (UserId2.equalsIgnoreCase(userId) && x.size() > 0)
//                        return userName;
//                }
//
//            } catch (MdException e) {
//                log.error("Exception:" + e);
//                log.error("Exception Message:" + e.getMessage());
//            } catch (RemoteException e) {
//                log.error("Exception:" + e);
//                log.error("Exception Message:" + e.getMessage());
//            }
//
//        }
//        return "NotFound";
//    }
//
//    public void logout() {
//        log.info("From logout service");
//        // this._factory.closeOMRConnection();
//        try {
//            if (this.connection != null) {
//                this.connection.closeOMRConnection();
//            }
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        // this.connection.disconnectOMRConnection(arg0);n();
//    }
//
//    public int IsUserFound(String personName) {
//        log.info(personName);
//        if (this.getRepositories() != null) {
//
//            CMetadata repository = this.getRepositories().get(0);
//            log.info(personName);
//            log.info("repository:" + repository);
//            if (repository != null) {
//                try {
//                    log.info("\nReading User Detail from the server...");
//                    UserDTO userDTO = new UserDTO();
//                    MdObjectStore store = _factory.createObjectStore();
//
//                    String xmlSelect = "<XMLSELECT Search=\"Person[@Name='" + personName + "']\"/>";
//                    String template = "";
//
//                    String sOptions = xmlSelect + template;
//
//                    int flags = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_TEMPLATE | MdOMIUtil.OMI_GET_METADATA;
//                    List tableList = _factory.getOMIUtil().getMetadataObjectsSubset(store, repository.getFQID(),
//                            MetadataObjects.PERSON, flags, sOptions);
//                    log.info(tableList.size() + " table List");
//                    Iterator iter = tableList.iterator();
//                    while (iter.hasNext()) {
//
//                        Person table = (Person) iter.next();
//                        log.info("Found");
//                        return 0;
//                    }
//                    log.info(" notFound");
//                    return 1;
//                } catch (MdException e) {
//                    log.error("Exception:" + e);
//                    log.error("Exception Message:" + e.getMessage());
//                } catch (RemoteException e) {
//                    log.error("Exception:" + e);
//                    log.error("Exception Message:" + e.getMessage());
//                }
//
//            }
//        }
//        log.info("Error in is found user");
//        return 2;
//    }
//
//    public String IsGroupFound(String groupName) {
//        if (this.getRepositories() != null) {
//
//            CMetadata repository = this.getRepositories().get(0);
//            log.info("repository:" + repository);
//            if (repository != null) {
//                try {
//                    log.info("\nReading groupName  from the server...");
//
//                    MdObjectStore store = _factory.createObjectStore();
//
//                    String xmlSelect = "<XMLSELECT Search=\"IdentityGroup[@Name='" + groupName + "']\"/>";
//                    String template = "";
//
//                    String sOptions = xmlSelect + template;
//
//                    int flags = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_TEMPLATE | MdOMIUtil.OMI_GET_METADATA;
//                    List tableList = _factory.getOMIUtil().getMetadataObjectsSubset(store, repository.getFQID(),
//                            MetadataObjects.IDENTITYGROUP, flags, sOptions);
//                    if (tableList.size() > 0)
//                        return "found";
//
//                    return "notfound";
//                } catch (MdException e) {
//                    log.error("Exception:" + e);
//                    log.error("Exception Message:" + e.getMessage());
//                    return e.getMessage();
//                } catch (RemoteException e) {
//                    log.error("Exception:" + e);
//                    log.error("Exception Message:" + e.getMessage());
//                    return e.getMessage();
//                } catch (Exception e) {
//                    log.error("Exception:" + e);
//                    log.error("Exception Message:" + e.getMessage());
//                    return e.getMessage();
//                }
//
//            }
//        }
//        return "NetworkError";
//
//    }
//
//    public void AssignUserToGroup(IdentityGroup group, Person user) {
//
//        try {
//            group.getMemberIdentities().add(user);
//            group.updateMetadataAll();
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//
//    }

    public void addIdentityGroupToGroupHelper(IdentityGroup parentGroup, IdentityGroup childGroup) {
        try {
            if (childGroup != null) {
                log.info("add with name " + childGroup.getName() + " to " + parentGroup.getName());
                log.info(parentGroup.getIdentityGroups() + " parentGroup.getIdentityGroups()");
                log.info(parentGroup.getIdentityGroups().size() + " parentGroup.getIdentityGroups() size");

                parentGroup.getIdentityGroups().add(childGroup);
                parentGroup.updateMetadataAll();

            }

        } catch (MdException e) {
            log.error("Exception:" + e);
            log.error("Exception Message:" + e.getMessage());
        } catch (RemoteException e) {
            log.error("Exception:" + e);
            log.error("Exception Message:" + e.getMessage());
        }

    }

//    public void removeIdentityFromGroup(IdentityGroup group, Identity user) {
//        try {
//            AssociationList memberIdentities = group.getMemberIdentities();
//            List deleteList = new ArrayList();
//            for (int i = 0; i < memberIdentities.size(); i++) {
//                Identity identity = (Identity) memberIdentities.get(i);
//                if (identity.getName().equals(user.getName())) {
//                    deleteList.add(identity);
//                }
//
//            }
//            if (deleteList.size() > 0) {
//                log.info("Deleting " + deleteList.size() + " objects");
//                _factory.deleteMetadataObjects(deleteList);
//            }
//
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//    }
//
//    // edit in metaData
//    public String editGroup(GroupDTO groupDTO) throws MdException {
//        try {
//            log.info("editGroup()");
//            log.info("information of group ------");
//            log.info(groupDTO.toString());
//            IdentityGroup group = this.getGroup(groupDTO.getName());
//            if (group == null) {
//                return "This group is not Found";
//            }
//            // group.setGroupType("UserGroup");
//            if (group.getGroupType().equals("USERGROUP"))
//                group.setGroupType("UserGroup");
//            if (group.getUsageVersion() == 0)
//                group.setUsageVersion(1000000.0);
//            group.setDisplayName(groupDTO.getDisplayName());
//            group.setDesc(groupDTO.getDesc());
//            group.getIdentityGroups().clear();
//            log.info("Role size inside group=" + groupDTO.getRoles().size());
//            for (int i = 0; i < groupDTO.getRoles().size(); i++) {
//                RoleDTO roleDTO = groupDTO.getRoles().get(i);
//                String roleName = roleDTO.getName();
//                if (roleName != null) {
//                    IdentityGroup role = getRoleByName(roleName);
//                    if (role != null)
//                        this.addIdentityGroupToGroupHelper(group, role);
//
//                }
//
//            }
//            group.updateMetadataAll();
//            return "Update successfully";
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//            return e.getMessage();
//        }
//    }
//
//    // delete from metadata
//    public boolean deleteGroup(String groupName) {
//        CMetadata repository = this.getRepositories().get(0);
//        if (repository != null) {
//            try {
//                log.info("\nDeleting the objects from the server...");
//                MdObjectStore store = _factory.createObjectStore();
//
//                List deleteList = new ArrayList();
//
//                String reposID = repository.getFQID();
//                String xmlSelect = "<XMLSELECT Search=\"IdentityGroup[@Name='" + groupName + "']\"/>";
//                String template = "<Templates>" + "<IdentityGroup />" + "</Templates>";
//
//                String sOptions = xmlSelect + template;
//
//                int flags = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_TEMPLATE | MdOMIUtil.OMI_GET_METADATA;
//                List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID,
//                        MetadataObjects.IDENTITYGROUP, flags, sOptions);
//
//                Iterator iter = tables.iterator();
//                while (iter.hasNext()) {
//                    IdentityGroup ptable = (IdentityGroup) iter.next();
//                    if (ptable.getName().equalsIgnoreCase(groupName)) {
//                        deleteList.add(ptable);
//                        log.info(ptable.getName() + " name");
//                        log.info(ptable.getPublicType() + " public");
//                    }
//
//                }
//                //
//
//                if (deleteList.size() > 0) {
//                    log.info("Deleting " + deleteList.size() + " objects");
//                    _factory.deleteMetadataObjects(deleteList);
//
//                    return true;
//                }
//
//                // store.dispose();
//            } catch (MdException e) {
//                log.error("Exception:" + e);
//                log.error("Exception Message:" + e.getMessage());
//            } catch (RemoteException e) {
//                log.error("Exception:" + e);
//                log.error("Exception Message:" + e.getMessage());
//            }
//        }
//        return false;
//    }

//    IdentityGroup getRoleByName(String RoleName) {
//
//        try {
//            CMetadata repository = getRepositories().get(0);
//            String reposID = repository.getFQID();
//            List<GroupDTO> groupDTOs = new ArrayList<>();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID, MetadataObjects.IDENTITYGROUP,
//                    flags, "");
//
//            Iterator iter = tables.iterator();
//            while (iter.hasNext()) {
//                IdentityGroup ptable = (IdentityGroup) iter.next();
//                if (ptable.getName().equalsIgnoreCase(RoleName) && (ptable.getPublicType().equalsIgnoreCase("role")
//                        || ptable.getGroupType().equalsIgnoreCase("role")))
//                    return ptable;
//            }
//
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//    }

//    IdentityGroup getRoleById(String RoleId) {
//        log.info("RoleId in getRoleById: " + RoleId);
//        try {
//            CMetadata repository = getRepositories().get(0);
//            String reposID = repository.getFQID();
//            List<GroupDTO> groupDTOs = new ArrayList<>();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID, MetadataObjects.IDENTITYGROUP,
//                    flags, "");
//
//            Iterator iter = tables.iterator();
//            while (iter.hasNext()) {
//                IdentityGroup ptable = (IdentityGroup) iter.next();
//                if (ptable.getName().equals(RoleId) && (ptable.getPublicType().equalsIgnoreCase("role")
//                        || ptable.getGroupType().equalsIgnoreCase("role"))) {
//                    log.info("... Role Found ... ");
//                    return ptable;
//                }
//            }
//
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//    }
//
//    // ----------- ----------- ----------- ----------- -----------
//    public List<CabapilityDTO> getCapabilities() {
//        try {
//            CMetadata repository = getRepositories().get(0);
//            String reposID = repository.getFQID();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID,
//                    MetadataObjects.APPLICATIONACTION, flags, "");
//
//            log.info("Try Getting Cabapilities: " + tables.size());
//            Iterator iter = tables.iterator();
//
//            int ii = 0;
//            List<CabapilityDTO> cabs_ = new ArrayList<>();
//            while (iter.hasNext()) {
//                ApplicationAction ptable = (ApplicationAction) iter.next();
//                CabapilityDTO cabDTO = new CabapilityDTO();
//
////				log.info("name: " + ptable.getName());
////				log.info("Description: " + ptable.getDesc());
////				log.info("PublicType: " + ptable.getPublicType());
////				log.info("ActionIdentifier: " + ptable.getActionIdentifier());
////				log.info("ActionType: " + ptable.getActionType());
////				log.info("AccessControls: " + ptable.getAccessControls().size());
//
//                // log.info("AccessControls: " + ptable.getAccessControls().size());
//                // for(int j=0; j<ptable.getAccessControls().size(); j++) {
//                // AccessControl obj = (AccessControl) ptable.getAccessControls().get(j);
//                // log.info(obj.getName() +", " + obj.getDesc() + ", " +
//                // obj.getPublicType());
//                // }
//                //// log.info("SoftwareTrees: "+ ptable.getSoftwareTrees().size());
//                //// for(int e=0; e<ptable.getSoftwareTrees().size(); e++) {
//                //// AssociationList a = (AssociationList) ptable.getSoftwareTrees().get(e);
//                //// log.info(a.getName() +", " + s.getDesc() + ", " +
//                // s.getPublicType());
//                // }
//                // log.info("------ ------ ------ ------ ------");
//
//                cabDTO.setName(ptable.getName());
//                cabDTO.setDesc(ptable.getDesc());
//
//                cabDTO.setAccessControlEntryId(ptable.getId());
//
//                cabs_.add(cabDTO);
//                // if(ii==10)
//                // break;
//                //
//                // ii++;
//                // log.info(ptable.getAssociationNames());
//            }
//            log.info("Try Getting Cabapilities <APPLICATIONACTION>: " + tables.size());
//            return cabs_;
//
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//
//    }
//
//    public List<CabapilityDTO> getCapabilities2() {
//        try {
//            CMetadata repository = getRepositories().get(0);
//            String reposID = repository.getFQID();
//            List<GroupDTO> groupDTOs = new ArrayList<>();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID,
//                    MetadataObjects.SOFTWARECOMPONENT, flags, "");
//
//            log.info("Try Getting Cabapilities: " + tables.size());
//            Iterator iter = tables.iterator();
//            // int z = 0;
//            List<CabapilityDTO> cabs_ = new ArrayList<>();
//            while (iter.hasNext()) {
//                SoftwareComponent ptable = (SoftwareComponent) iter.next();
//                CabapilityDTO cabDTO = new CabapilityDTO();
//
//                if (ptable.getPublicType().equalsIgnoreCase("Application")) {
//                    // z++;
//                    log.info("name: " + ptable.getName());
//                    log.info("ProductName: " + ptable.getProductName());
//                    log.info("Description: " + ptable.getDesc());
//                    log.info("PublicType: " + ptable.getPublicType());
//                    log.info("NameState: " + ptable.getNameState());
//
//                    cabDTO.setName(ptable.getName());
//                    cabDTO.setDesc(ptable.getDesc());
//                    cabDTO.setId(ptable.getId());
//                    // cabDTO.setProductName(ptable.getProductName());
//
//                    cabs_.add(cabDTO);
//                }
//
//                // for(int j=0; j<ptable.getAccessControls().size(); j++) {
//                // AccessControl obj = (AccessControl) ptable.getAccessControls().get(j);
//                // if(obj.getPublicType().equalsIgnoreCase("Application"))
//                // obj.getPublicType());
//                // }
//                // for(int e=0; e<ptable.getSoftwareTrees().size(); e++) {
//                // ApplicationAction ac= (ApplicationAction) ptable.getSoftwareTrees().get(e);
//                // ac.getPublicType());
//                // }
//                log.info("------ ------ ------ ------ ------");
//
//            }
//            log.info("Try Getting Cabapilities <SOFTWARECOMPONENT>: " + tables.size());
//            return cabs_;
//
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//    }

    /**
     * Get direct roles of group
     *
     * @param identityGroup
     * @return
     */
//    ArrayList<String> getRolesFromGroup(IdentityGroup identityGroup) {
//        ArrayList<String> roleDTOs = new ArrayList<>();
//        try {
//            AssociationList identities = identityGroup.getIdentityGroups();
//            for (int i = 0; i < identities.size(); i++) {
//                IdentityGroup identity = (IdentityGroup) identities.get(i);
//                // get role
//                if (identity.getPublicType().equalsIgnoreCase("role")
//                        || identity.getGroupType().equalsIgnoreCase("role")) {
//                    RoleDTO role = new RoleDTO();
//                    role.setName(identity.getName());
//                    role.setType_(identity.getPublicType());
//                    roleDTOs.add(role.toString());
//                }
//            }
//        } catch (Exception e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//
//        return roleDTOs;
//    }

//    public AccessControlEntry getCabapilityById(String cabId) {
//
//        log.info("In Function, getCabapilityById: " + cabId);
//        try {
//            CMetadata repository = getRepositories().get(0);
//            log.info("\nRetrieving all Groups objects contained in " + " repository " + repository.getName());
//
//            String reposID = repository.getFQID();
//            List<GroupDTO> groupDTOs = new ArrayList<>();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            List tables = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID,
//                    MetadataObjects.ACCESSCONTROLENTRY, flags, "");
//
//            Iterator iter = tables.iterator();
//            while (iter.hasNext()) {
//                AccessControlEntry ptable = (AccessControlEntry) iter.next();
//                if (ptable.getId().equals(cabId)) {
//                    log.info("-- access entry found --");
//                    return ptable;
//                }
//
//            }
//
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//        return null;
//    }
//
//    public void addCabapilityToRoleHelper(IdentityGroup role, AccessControlEntry cabapility) {
//        log.info("In Function addCabapilityToRoleHelper ... ");
//        try {
//            role.getAccessControlEntries().add(cabapility);
//            role.updateMetadataAll();
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//
//    }
//
//    private void disableUser_byRemovingAccount(UserDTO userdto) {
//        CMetadata repository = getRepositories().get(0);
//        try {
//            String reposID = repository.getFQID();
//            MdObjectStore store = _factory.createObjectStore();
//
//            int flags = MdOMIUtil.OMI_GET_METADATA | MdOMIUtil.OMI_ALL_SIMPLE;
//            String reposFQID = repository.getFQID();
//
//            String shortReposID = reposFQID.substring(reposFQID.indexOf(".") + 1, reposFQID.length());
//
//            Person p = getPerson(userdto.getName());
//
//            List auths = _factory.getOMIUtil().getMetadataObjectsSubset(store, reposID,
//                    MetadataObjects.AUTHENTICATIONDOMAIN, flags, "");
//
//            List<Login> old_logins = new ArrayList<Login>();
//            for (int i = 0; i < p.getLogins().size(); i++) {
//                old_logins.add((Login) p.getLogins().get(i));
//                log.info("==: " + ((Login) p.getLogins().get(i)).getUserID());
//            }
//
//            p.getLogins().clear();
//            p.updateMetadataAll();
//
//        } catch (RemoteException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        } catch (MdException e) {
//            log.error("Exception:" + e);
//            log.error("Exception Message:" + e.getMessage());
//        }
//    }
//
//    public boolean UserHasInternalLoginInMeta(String username) {
//        if (this.getRepositories() != null) {
//
//            CMetadata repository = this.getRepositories().get(0);
//
//            try {
//                UserDTO userDTO = new UserDTO();
//                MdObjectStore store = _factory.createObjectStore();
//
//                int flags = MdOMIUtil.OMI_XMLSELECT | MdOMIUtil.OMI_TEMPLATE | MdOMIUtil.OMI_GET_METADATA;
//                List tableList = _factory.getOMIUtil().getMetadataObjectsSubset(store, repository.getFQID(),
//                        MetadataObjects.INTERNALLOGIN, flags, null);
//                log.info("tableList.size:" + tableList.size());
//                Iterator iter = tableList.iterator();
//                while (iter.hasNext()) {
//
//                    InternalLogin table = (InternalLogin) iter.next();
//                    if (table.getForIdentity() != null && table.getForIdentity().getName().equals(username)) {
//                        return true;
//                    }
//                }
//                return false;
//
//            } catch (MdException e) {
//                log.error("Exception:" + e);
//                log.error("Exception Message:" + e.getMessage());
//            } catch (RemoteException e) {
//                log.error("Exception:" + e);
//                log.error("Exception Message:" + e.getMessage());
//            }
//        }
//
//        return false;
//    }
//
//    public void updatePasswordStatus(String username, int status) {
//        // this.passwordChangeRepository.updatePasswordStatus(status, username);
//    }

}