package com.datagear.security.repository;

import java.util.List;

import com.datagear.security.model.Capability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CapabilityRepository extends JpaRepository<Capability, Integer>{
@Query("Select D from Capability D where D.id not in(select C.id.cId from Group_capability C where C.id.gId=?1)")
List<Capability>getRemainingCapabilities(int groupId);
}
