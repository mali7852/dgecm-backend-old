package com.datagear.security.repository;

import com.datagear.security.model.Group_CapabilityPK;
import com.datagear.security.model.Group_capability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Group_CapabilityRepository extends JpaRepository<Group_capability, Group_CapabilityPK> {

}
