package com.datagear.security.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ECMCapability database table.
 * 
 */
@Entity
@Table(name = "ECMCapability")

@NamedQuery(name = "Capability.findAll", query = "SELECT c FROM Capability c")
public class Capability implements Serializable {
	private static final long serialVersionUID = 1L;
//	@ManyToMany(mappedBy="capabilities")
//   private List<Group>groups;
//   public void setGroups(List<Group> groups) {
//	this.groups = groups;
//}
//   public List<Group> getGroups() {
//	return groups;
//}
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "Description")
	private String description;

	@Column(name = "Is_Active")
	public char isActive;

	@Column(name = "Name")
	private String name;

	public Capability() {
	}

	public String getDescription() {
		return this.description;
	}

	public int getId() {
		return this.id;
	}

	public char getIsActive() {
		return isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setIsActive(char isActive) {
		this.isActive = isActive;
	}

	public void setName(String name) {
		this.name = name;
	}

}