package com.datagear.security.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

// Singleton Class 
public class LoginUser {

	private static LoginUser instance;

	public static LoginUser getInstance() {
		if (instance == null) {
			instance = new LoginUser();
		}
		return instance;
	}

	private String userName;
	private String displayName;
	private List<String> roles = new ArrayList<>();
	private String userId;
	private String token;
	private Date tokenExpiration;
	private List<String> groups;
	
	private LoginUser() {

	}
	public String getDisplayName() {
		return displayName;
	}

	@JsonProperty("ROLES")
	public List<String> getRoles() {
		return roles;
	}

	@JsonProperty("TOKEN")
	public String getToken() {
		return token;
	}

	@JsonProperty("EXPIRATION")
	public Date getTokenExpiration() {
		return tokenExpiration;
	}

	@JsonProperty("USER_ID")
	public String getUserId() {
		return userId;
	}

	@JsonProperty("USER_NAME")
	public String getUserName() {
		return userName;
	}
	
	@JsonIgnore
	public List<String> getGroups(){
		return this.groups;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setTokenExpiration(Date tokenExpiration) {
		this.tokenExpiration = tokenExpiration;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setGroups(List<String> groups) {
		this.groups= groups;
	}
}
