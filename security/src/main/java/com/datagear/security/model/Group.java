package com.datagear.security.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The persistent class for the Group database table.
 * 
 */
@Entity
// @NamedQuery(name="Group.findAll", query="SELECT g FROM Group g")
@Table(name = "ECMGroup")
public class Group implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty("id")
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@JsonProperty("name")
	@Column(name = "Name")
//	@Enumerated(EnumType.STRING)
	private String name;

	@Column(name = "Description")
	private String description;

	@Column(name = "Is_Active")
	public char isActive;

//	@ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY)
//	private List<User> users;
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "Group_X_Capability", joinColumns = @JoinColumn(name = "G_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "C_ID", referencedColumnName = "ID"))
	private Set<Capability> capabilities;

	public Group() {
	}

	public Set<Capability> getCapabilities() {
		return capabilities;
	}

	public String getDescription() {
		return description;
	}

	public int getId() {
		return this.id;
	}

	public char getIsActive() {
		return isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setCapabilities(Set<Capability> capabilities) {
		this.capabilities = capabilities;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setIsActive(char isActive) {
		this.isActive = isActive;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public List<User> getUsers() {
//		return users;
//	}
//
//	public void setUsers(List<User> users) {
//		this.users = users;
//	}

}