package com.datagear.security.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "ECMUser")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    // @SequenceGenerator(name = "user_seq", sequenceName = "user_seq",
    // allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "UserName", length = 50, unique = true)
    @Size(min = 4, max = 50)
    private String username;

    @Column(name = "Password", length = 100)
    @Size(min = 4, max = 100)
    private String password;

    @Column(name = "FirstName", length = 50)
    @Size(min = 3, max = 50)
    private String firstname;

    @Column(name = "LastName", length = 50)
    @Size(min = 3, max = 50)
    private String lastname;

    @Column(name = "Email", length = 50)
    @Size(min = 4, max = 50)
    private String email;

    @Column(name = "Enabled")
    private Boolean enabled;

    @Column(name = "LastPasswordResetDate")
    private Timestamp lastPasswordResetDate;

    @Column(name = "DisplayName")
    private String DisplayName;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "USER_X_GROUP", joinColumns = {
            @JoinColumn(name = "U_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
            @JoinColumn(name = "G_ID", referencedColumnName = "ID")})
    private List<Group> groups;

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Timestamp getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Timestamp lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}