package com.datagear.security.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class User_GroupPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "G_ID")
	private int gId;

	@Column(name = "U_ID")
	private int uId;

	public User_GroupPK() {
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User_GroupPK other = (User_GroupPK) obj;
		if (gId != other.gId)
			return false;
		if (uId != other.uId)
			return false;
		return true;
	}

	public int getGId() {
		return this.gId;
	}

	public int getUId() {
		return this.uId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + gId;
		result = prime * result + uId;
		return result;
	}

	public void setGId(int gId) {
		this.gId = gId;
	}

	public void setUId(int uId) {
		this.uId = uId;
	}

}
