package com.datagear.security.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonProperty;

@Embeddable
public class Group_CapabilityPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("cId")
	@Column(name = "C_ID")
	private int cId;

	@JsonProperty("gId")
	@Column(name = "G_ID")
	private int gId;

	public Group_CapabilityPK() {
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group_CapabilityPK other = (Group_CapabilityPK) obj;
		if (cId != other.cId)
			return false;
		if (gId != other.gId)
			return false;
		return true;
	}

	public int getCId() {
		return this.cId;
	}

	public int getGId() {
		return this.gId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cId;
		result = prime * result + gId;
		return result;
	}

	public void setCId(int cId) {
		this.cId = cId;
	}

	public void setGId(int gId) {
		this.gId = gId;
	}

}
